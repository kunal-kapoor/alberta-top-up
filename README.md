# Alberta Traveller Screening

## Summary
The premiere of Alberta has made an announcement that Alberta will strengthen safety measures for travellers. This
application allows to keep track of land border crossings with the US and Alberta. Travellers complete their self-isolation
plan at Calgary and Edmonton international airports to determine whether they require additional support in order to
self isolate.

## Table of Contents

1. [Features](#features)
2. [Prerequisites](#prerequisites)
3. [Technology stack](#technology-stack)
4. [Installation](#installation)
5. [V1](#V1)
6. [V1 Frontend routes](#V1-frontend-routes)
7. [V2](#V2)
8. [V2 Frontend Routes](#V2-frontend-routes)
9. [API routes](#api-routes)
10. [Database](#database)
11. [Testing](#testing)
12. [Pipeline Integration](#pipeline-integration)

## Features

1. Travel screening form
2. Admin portal with login
3. Secure lookup of submissions
4. Secure search of submitted forms by confirmation number
5. Generation of confirmation number based on government requirements

## Prerequisites

- [npm](https://www.npmjs.com/)
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)
- A Unix based OS, DB2 will not work properly with windows

## Technology stack

- [NestJS](https://nestjs.com/) - Backend framework
- [TypeScript](https://www.typescriptlang.org/) - Backend programming language
- [React](https://reactjs.org/) - Frontend library
- [JavaScript](https://www.w3schools.com/Js/js_es6.asp) - Frontend programming language
- [IBM Cloud AppID](https://www.ibm.com/ca-en/cloud/app-id) - Authentication service
- [DB2](https://www.ibm.com/analytics/db2) - Relational database

## Installation
Follow these steps to get started.

1. Add a `.env` file to `/server`
- `cp server/.env.example server/.env`
- Update `server/.env` with variables provided by existing dev.

2. Run project
- `make run-local`

3. Run DB Migrations
- `make local-db-migrate`

### Running the project with docker


To run the database, server, and client:
```
make run-local
```

To tear down your environment:
```
make close-local
```

To seed the database:
```
make local-db-seed records={NUMBER_OF_RECORDS}
```

To migrate the database:
```
make local-db-migrate
```

Workspaces:
- Database - `make local-db-workspace`
- Server - `make local-server-workspace`
- Client - `make local-client-workspace`

### Running the project with npm

Server:
```
cd server && npm install && npm run watch
```

Client:
```
cd client && npm install && npm run start
```

Database seed:
```
cd server && npm run db:seed
```

Database migrate:
```
cd server && npm run db:migrate
```

## V1

### Client Routes

#### Traveller

- `/` - Traveller form page
- `/confirmation` - Successful traveller form submission page

#### Admin

- `/` - Back office / Service Alberta login page
- `/form` - Traveller form page
- `/back-office-login` - Back office login page
- `/back-office-lookup` - Back Office lookup page
- `/back-office-lookup-last-name/:query?` - Back Office last name search page
- `/back-office-lookup-confirmation-number/:confirmationNumber` - Back Office submission details page
- `/admin-lookup` - Access Control for Report Dashboard

## V2
V2 is the currently in production version of the Alberta Health traveller pilot program. This version has a number of changes from the V1 application. The entirety of the V1 application remains unchanged in the source code, and V2 is a collection of modified pages and components that are swapped with the originals based on the ENVs and build commands used to spin up the application. This allows us to build multiple different versions or sections of the application from the same source. The same goes for the admin application vs. the public facing application within V2 itself. When deployed, only the appropriate section of the application is made available depending on the build command used, for example when deploying to the environment hosting the administrator application, only the admin routes will be available (with some exceptions), and the root route of the application will now be the service alberta login page as opposed to the eligibility form when the public application is built. 

### Client Routes
 
#### Local Development
These are the routes available when building the application locally. This will include both the V1 Traveller and Admin routes, but the relevant routes for V2 development are as follows:

- `/` - Traveller Eligibility form
- `/enroll` - Traveller enrollment form
- `/enroll/confirmation` - Enrollment confirmation page
- `/daily/:token` - Daily survey page for an enrolled household
- `/service-alberta-login` - Admin portal login page
- `/monitoring` - Monitoring portal page with all enrolled travellers
- `/household/:id` - Household details page
- `/rtop-admin-upload-report` - Nightly testing report CSV upload page
- `/rtop-admin-report-dashboard` - Dashboard page for report lookup
- `/alert-system` - Alert system page

#### Traveller
These are the routes available when built in 'Traveller' mode

- `/` - Traveller Eligibility form
- `/enroll` - Traveller enrollment form
- `/enroll/confirmation` - Enrollment confirmation page
- `/daily/:token` - Daily survey page for an enrolled household

#### Admin
These are the routes available when built in 'Admin' mode 

- `/` - Service Alberta admin portal login page
- `/monitoring` - Monitoring portal page with all enrolled travellers
- `/household/:id` - Household details page
- `/rtop-admin-upload-report` - Nightly testing report CSV upload page
- `/rtop-admin-report-dashboard` - Dashboard page for report lookup
- `/alert-system` - Alert system page
- `/daily/:token` - Daily survey page for an enrolled household - Fill the daily survey on behalf of a household


## Server Routes

#### Traveller

- `/form` - [POST] Submits a new form

#### Admin

- `/auth/validate` - [GET] Validates a user access token
- `/admin/back-office/lastname/:lname` - [GET] Searches and retrieves a list of forms via traveller last name
- `/admin/back-office/form/:confirmationNumber` - [GET] Searches and retrieves a submitted form by confirmation number
- `/admin/back-office/form/:id` - [PATCH] Updates the address / phone number of a submitted form
- `/admin/back-office/form/:id/determination` - [PATCH] Updates a submitted form by adding a determination
- `/admin/service-alberta/form` - [GET] Searches and retrieves a list of forms by via query/filter/order/orderBy/page
- `/admin/service-alberta/form/:confirmationNumber` - [GET] Searches and retrieves a submitted form by confirmation number
- `/admin/service-alberta/form/:id/assign` - [POST] Assigns the agent of the form
- `/admin/service-alberta/form/:id/assign/undo` - [POST] Un-assigns the agent of the form
- `/admin/service-alberta/form/:id/activity` - [POST] Updates a submitted form by adding a determination
- `/admin/report/accesscontrols` - [POST] Creates access controls for the provided agent
- `/admin/report/accesscontrols` - [PUT] Updates access controls for the provided agent
- `/admin/report/accesscontrols` - [GET] Retrieves the entire list of active report access controls for agents

#### Other

- `/health` - [GET] Runs a health check on the server

## Database
This application uses a relational IBM DB2 database. The data is stored as JSON blob inside one of the fields.

More information about Db2 Database:
- https://www.ibm.com/ca-en/products/db2-database
- https://www.ibm.com/analytics/db2

## Testing

### Integration Testing
V2 of the application uses Cypress for integration testing of the various components and API interactions. Requests made are explicitly not mocked in order to simulate actual usage of the application, and by extension various test suites are dependant on the successful execution of previous test suites

Tests cover both expected fail and success cases, and ideally cover an entire UI flow or 'user story' rather than targeting a single component or interaction. This also allows us to leverage the testing framework to rapidly create households without needing to seed the database, in the event that manual testing is needed to be done, or multiple new households need to be created quickly. Running the `/enrollment-form/happy-path.spec` test either in Cypress' GUI mode, or as part of the full test collection in headless mode will create a new household.

#### Cypress
- Setup
  A Cypress-specific ENV file is needed for some of the tests to run correctly. Please contact another dev for the contents of this file. This file should be named cypress.env.json and should be placed within the `/client` folder.
- GUI mode
  To run Cypress in GUI mode, run the make command `make cypress-integration`. This will open the Cypress browser and allow selecting which (or all) tests to run. Cypress is incredibly memory-intensive in GUI mode, so this is only recommended for either diagnosing a failing test, or for writing new test cases. This will also allow running the tests under the `/client/cypress/integration/manual` folder, which are disabled when run in headless or pipeline modes.
- Headless mode
  To run Cypress in headless mode, run the make command `make cypress-headless`. This will run the entire contents of the `/client/cypress/integration/automatic` folder with command line output. This mode still creates the DOM and makes actions against it, just without opening or rendering a browser window. Manual tests are disabled for this mode due to the lack of ability to request user input. This mode is best used for doing regression testing locally, prior to a PR, or to find which tests need to be updated after a change without needing to wait for a pipeline to fail.
- Pipeline mode
  To run the pipeline mode, run the make command `make cypress-pipeline`. This mode is nearly identical to the headless mode, with the exception that various persistent logging options are disabled, and the maximum wait-before-fail time is extended. This mode is primarily used by the pipeline test runner.

### API Testing

#### Unit Testing
The API uses Jest to perform standard unit testing on all routes and DB interactions

## Pipeline Integration
This application leverages Bitbucket Pipelines for performing pre-merge regression testing.
On PR creation or update, BB Pipelines will run the entire API test collection, as well as the Cypress integration tests. During this process the pipeline test runner environment spins up a version of the application with modified ENVs via the `docker-compose.test.yml`. This modifies the database target and connection variables to point to a remote test-specific database in the IBM cloud. This is done due to the quirks of trying to run DB2 in any environment that cannot or will not give the database process elevated permissions, such as the BB pipeline environment. The pipeline test runner contributes to the mandatory pre-merge checks on every PR, as the repo is set up prevent merging with a failed pipeline.