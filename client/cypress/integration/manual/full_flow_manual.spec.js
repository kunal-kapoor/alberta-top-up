import { 
  passContactStep, 
  fillPrimaryContact, 
  fillArrivalInformation, 
  fillIsolationQuestionnaire, 
  fillAdditionalTravellers,
} from '../utils/utils';

describe("Enrollment flow manual test - tester input", () => {

  it("Uses tester input to speed fill the enrollment form", () => {
    cy.visit("http://localhost:3000/enroll");

    //Pass contact verification
    passContactStep();

    // Prompts for number of travellers
    const additionalTravellers = prompt("How many additional travellers?", 0)

    //Fill primary contact portion
    fillPrimaryContact();

    // Marks form primary as non-exempt
    cy.get('[value="Non-Exempt"]').click();

    //Fill arrival information portion
    fillArrivalInformation();

    //uses prompt value for additional travellers and fills the form for each, if number is 0 or less, skip additional travellers
    if (additionalTravellers > 0) {
      cy.get('[name="hasAdditionalTravellers"]').check("Yes");
      cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
      cy.get(`[data-value="${additionalTravellers}"]`).click();
    } else {
      cy.get('[name="hasAdditionalTravellers"]').check("No");
    }

    fillAdditionalTravellers({numberOfFieldSets: additionalTravellers, omitOne: false});

    //Fill isolation questionnaire portion, pass omit flag to not fill one field
    fillIsolationQuestionnaire({omitOne: false});

    cy.contains("Continue").click();

    cy.contains("Your application will be verified upon entry into Canada. Please make note of your confirmation number(s).")
  })
})