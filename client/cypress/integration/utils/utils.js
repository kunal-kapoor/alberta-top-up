
import axios from "axios";
import moment from 'moment';

/** Fills correct selection of eligibility form */
export const correctSelection = () => {
  cy.get('[name="residency"]').check('Yes');
  cy.get('[name="symptoms"]').check('No');
  cy.get('[name="covidContact"]').check('No');
  cy.get('[name="quarantinePlan"]').check('Yes');
  cy.get('[name="remainingInAlberta"]').check('Yes');
  cy.get('[name="useABTraceTogetherApp"]').check('Yes');
}

/** Fills Primary Contact form portion EXCEPT exemption status */
export const fillPrimaryContact = () => {
  cy.get('[name="firstName"]').type("Automated");
  cy.get('[name="lastName"]').type("Traveller");
  cy.get('[name="dateOfBirth"]').type("1990/01/01");
  cy.get('[name="phoneNumber"]').type("2501112345");
  cy.get('[name="email"]').type("dev-null@freshworks.io");
  cy.get('[name="confirmEmail"]').type("dev-null@freshworks.io");

  cy.get('[id="mui-component-select-gender"]').click();
  cy.get('[data-value="Male"]').click();

  cy.get('[id="mui-component-select-citizenshipStatus"]').click();
  cy.get('[data-value="Citizen"]').click();
  cy.get('[name="preDepartureVaccination"]').check('Yes');
  cy.get('[id="mui-component-select-timeSinceVaccination"]').click();
  cy.get('[data-value="1-2 weeks"]').click();
  cy.get('[id="mui-component-select-dosesOfVaccine"]').click();
  cy.get('[data-value="2"]').click();
}

/** Fills Arrival Information form  portion */
export const fillArrivalInformation = () => {
  cy.get('[id="mui-component-select-nameOfAirportOrBorderCrossing"]').click();
  cy.get('[data-value="Airport - Calgary International (YYC)"]').click();

  cy.get('[name="arrivalDate"]').type(moment().format("YYYY/MM/DD"));
  cy.get('[name="arrivalCityOrTown"]').type("Some Place");

  cy.get('[id="mui-component-select-arrivalCountry"]').click();
  cy.get('[data-value="United States of America (the)"]').click();

  cy.get('[name="dateLeftCanada"]').type(moment().subtract('10', 'days').format("YYYY/MM/DD"));
  cy.get('[name="lengthOfAbsence"]').should("have.value", "10"); // Ensure date calculation is correct

  cy.get('[id="mui-component-select-countryOfResidence"]').click();
  cy.get('[data-value="Canada"]').click();

  cy.get('[id="mui-component-select-reasonForTravel"]').click();
  cy.get('[data-value="Returning Home"]').click();
  
  cy.get('[id="mui-component-select-durationOfStay"]').click();
  cy.get('[data-value="14 days or more"]').click();

  cy.get('[id="mui-component-select-numberOfAdditionalCitiesAndCountries"]').click();
  cy.get('[data-value="2"]').click();

  cy.get('[name="additionalCitiesAndCountries[0].cityOrTown"]').type("Some City");

  cy.get('[id="mui-component-select-additionalCitiesAndCountries[0].country"]').click();
  cy.get('[data-value="Aruba"]').click();

  cy.get('[name="additionalCitiesAndCountries[1].cityOrTown"]').type("Some Town");

  cy.get('[id="mui-component-select-additionalCitiesAndCountries[1].country"]').click();
  cy.get('[data-value="Argentina"]').click();

}

/** Fills Isolation Questionnaire form portion */

export const fillIsolationQuestionnaire = ({omitOne}) => {
  // If omit is true, don't fill the address field
  if (!omitOne) {
    cy.get('[name="address"]').type("1234 Automated Test Street");
  }

  cy.get('[role="combobox"]').click();
  cy.get('[role="option"]').contains("Airdrie").click();

  cy.get('[id="mui-component-select-provinceTerritory"]').click();
  cy.get('[data-value="Alberta"]').click();

  cy.get('[name="postalCode"]').type("T0M0E0");
  cy.get('[name="quarantineLocationPhoneNumber"]').type("1234567890");

  cy.get('[id="mui-component-select-typeOfPlace"]').click();
  cy.get('[data-value="Private residence"]').click();

  cy.get('[id="mui-component-select-howToGetToPlace"]').click();
  cy.get('[data-value="Private vehicle"]').click();
}

/** Fills X additional travellers dynamic form portion */
export const fillAdditionalTravellers = ({numberOfFieldSets, omitOne}) => {
  
  for (let i = 0; i < numberOfFieldSets; i++) {

    cy.get(`[name="additionalTravellers[${i}].firstName"]`).type(`Additional Traveller ${i}`);
    cy.get(`[name="additionalTravellers[${i}].lastName"]`).type("Surname");
    cy.get(`[name="additionalTravellers[${i}].dateOfBirth"]`).type("1990/01/01}");

    // If omit is true, don't fill the gender field
    if (!omitOne) {
      cy.get(`[id="mui-component-select-additionalTravellers[${i}].gender"]`).click();
      cy.get('[data-value="Male"]').click();
    }
    
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].exemptionType"]`).click();
    cy.get('[data-value="Non-Exempt"]').click();

    cy.get(`[id="mui-component-select-additionalTravellers[${i}].citizenshipStatus"]`).click();
    cy.get('[data-value="Citizen"]').click();

    cy.get(`[id="mui-component-select-additionalTravellers[${i}].reasonForTravel"]`).click();
    cy.get('[data-value="Returning Home"]').click();

    cy.get(`[id="mui-component-select-additionalTravellers[${i}].durationOfStay"]`).click();
    cy.get('[data-value="14 days or more"]').click();
    cy.get(`[name="additionalTravellers[${i}].preDepartureVaccination"]`).check('Yes');
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].timeSinceVaccination"]`).click();
    cy.get('[data-value="1-2 weeks"]').click();
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].dosesOfVaccine"]`).click();
    cy.get('[data-value="2"]').click();
  }
}

/** Fills four day survey */
export const fillFourDaySurvey = ({omitOne}) => {
  if (!omitOne) {
    cy.get('[name="difficultyCompletingRegistration"]').check('No');
  }
  cy.get('[name="difficultyGettingConfirmationId"]').check('No');
  cy.get('[name="difficultyFindingStaff"]').check('No');
  cy.get('[name="acceptableTimeLength"]').check('No');
  cy.get('[name="easeOfUnderstandingProgram"]').check('No');
  cy.get('[name="difficultyLocatingBooth"]').check('No');
  cy.get('[name="receivedTestResults"]').check('No');
  cy.get('[name="acceptableWaitTime"]').check('No');
  cy.get('[name="difficultyCompletingCheckin"]').check('No');
  cy.get('[name="teamHelpful"]').check('No');
  cy.get('[name="adultsInHousehold"]').check('1');
  cy.get('[name="childrenInHousehold"]').check('0');
  cy.get('[name="employmentStatus"]').check('Unemployed');
}

/** Fills thirteen day survey */
export const fillThirteenDaySurvey = ({omitOne}) => {
  if (!omitOne) {
    cy.get('[name="difficultyCompletingCheckin"]').check('No');
  }
  cy.get('[name="difficultyDlingABTrace"]').check('No');
  cy.get('[name="difficultyBookingSecondTest"]').check('No');
  cy.get('[name="difficultyGettingSecondTest"]').check('No');
  cy.get('[name="acceptableSecondTimeLength"]').check('No');
  cy.get('[name="teamHelpful"]').check('No');
  cy.get('[name="potentialTravel"]').check('No');
  // cy.get('[name="potentialLostIncome"]').check('No');
  // cy.get('[name="potentialLostIncomeDays"]').check('No');
  cy.get('[name="programMeetExpectations"]').check('No');
  cy.get('[name="wouldUseAgain"]').check('No');
  cy.get('[name="wouldCoverCost"]').check('No');
  // cy.get('[name="costPerPerson"]').check('0');
  cy.get('[name="wouldRecommend"]').check('No');
}

/** Fills thirteen day travel portion */
export const fillThirteenDayTravel = () => {
  cy.get('[name="potentialLostIncomeDays.vacation"]').check();
  cy.get('[id="mui-component-select-potentialLostIncomeDays.vacation.count"').click();
  cy.get('[data-value="5"]').click();

  cy.get('[name="potentialLostIncomeDays.sick"]').check();
  cy.get('[id="mui-component-select-potentialLostIncomeDays.sick.count"').click();
  cy.get('[data-value="5"]').click();

  cy.get('[name="potentialLostIncomeDays.withoutPay"]').check();
  cy.get('[id="mui-component-select-potentialLostIncomeDays.withoutPay.count"').click();
  cy.get('[data-value="5"]').click();

  cy.get('[name="potentialLostIncomeDays.other"]').check();
  cy.get('[id="mui-component-select-potentialLostIncomeDays.other.count"').click();
  cy.get('[data-value="5"]').click();
}

/** Service Alberta Login 2FA */
export const loginAdminPortal2FA = async() => {

  const params = {
    grant_type: 'password',
    username: Cypress.env('IBM_USER'),
    password: Cypress.env('IBM_PASS')
  };

  const data = Object.entries(params)
    .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
    .join('&');

  const options = {
    method: 'POST',
    headers: { 'content-type': 'application/x-www-form-urlencoded', 'Authorization': `${Cypress.env('IBM_TOKEN')}`},
    data,
    url: 'https://us-south.appid.cloud.ibm.com/oauth/v4/05b4e556-e108-4500-823a-bbef8da28d64/token',
  };

  try {
  const response = await axios(options);  // wrap in async function
  const base64string = response.data.id_token.split('.')[1]
  const userToken = JSON.parse(window.atob(base64string))

  const auth = JSON.stringify({
    userType: "Service Alberta",
    isAuthenticated: true,
    user: {
      accessToken: `${response.data.access_token} ${response.data.id_token}`,
      authissuer: "GOA",
      idTokenPayload: userToken
    } 
  })
  localStorage.setItem('auth', auth);
  } catch(e) {
  }
}

/** Finds a given household by first name in the admin panel */
export const findHousehold = ({datestring, filter}) => {
  cy.get('[type="button"]').contains(filter).click();
  cy.get('[name="query"]').type(`Automated Monitoring-${datestring}`);
  cy.get('[data-testid="lookup-search-button"]').click();
}


/** Completes the daily survey for N travellers */
export const completeDailyForHousehold = ({numberOfSurveys, fillBirthdate, day1AddtQuestions = false}) => {
  for (let i = 0; i < numberOfSurveys; i++) {
    cy.get(`[data-testid="${i}"]`).click();
    if(fillBirthdate) {
      cy.get('[name="birthday"]').type("1990/01/01");
      cy.contains("Verify Date of Birth").click();
    }
    cy.get('[name="coughSymptom"]').check("No");
    cy.get('[name="shortnessOfBreathSymptom"]').check("No");
    cy.get('[name="temperatureSymptom"]').check("No");
    cy.get('[name="feverishSymptom"]').check("No");
    cy.get('[name="muscleAchesSymptom"]').check("No");
    cy.get('[name="lossOfSmellSymptom"]').check("No");
    cy.get('[name="headacheSymptom"]').check("No");
    cy.get('[name="gastrointestinalSymptom"]').check("No");
    cy.get('[name="usingABTraceTogether"]').check("Yes")

    if(day1AddtQuestions) {
      cy.get('[name="preDepartureTestDate"]').type(moment().subtract(1, 'day').format('YYYY/MM/DD'))
      cy.get('[id="mui-component-select-preDepartureTestCountry"]').click();
      cy.get('[data-value="United States of America (the)"]').click();
      cy.get('[id="mui-component-select-airline"]').click();
      cy.get('[data-value="Air Canada - AC"]').click();
      cy.get('[name="flightNumber"]').type('1234');
      cy.get('[name="seatNumber"]').type('A12');
    }

    cy.contains('Submit Daily Check-In').click();
  }
}

export const passEligibilityStep = () => {
  correctSelection();

  cy.contains('Continue').click();

  cy.contains('How would you prefer to receive');
  
}

export const passContactStep = () => {
  passEligibilityStep();

  cy.contains('I don’t have access to either of these').click()

  cy.contains("First name* (primary contact)")

}