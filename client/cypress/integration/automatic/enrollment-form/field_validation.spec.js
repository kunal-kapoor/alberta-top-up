import { passContactStep } from '../../utils/utils';
import moment from 'moment';

describe("Fails date field validation", () => {
  beforeEach(() => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passContactStep();
  })

  it("Fails birth date field validation - future date", () => {
    const date = moment().add('1', 'day').format("YYYY/MM/DD")

    cy.get("[name='dateOfBirth']").type(date).blur()

    cy.contains('Date of birth cannot be greater than today');
  })
  
  it("Fails Arrival date field validation - future date greater than 5 days", () => {
    const date = moment().add('6', 'days').format("YYYY/MM/DD")

    cy.get("[name='arrivalDate']").type(date).blur()

    cy.contains('Arrival Date must not be after 5 days from today');
  })

  it("Fails Arrival date field validation - too long ago", () => {

    cy.get("[name='arrivalDate']").type("2020/01/01").blur()

    cy.contains('Arrival Date must not be further than 1 day ago');

  })

  it("Fails Arrival date field validation - invalid date", () => {
    cy.get("[name='arrivalDate']").type("01/01/2020").blur()

    cy.contains('Not a valid date');
  })

  // Temporarily disabled until validation decision is made
  // it("Fails date left canada field validation - future date", () => {
  //   const date = moment().add('1', 'day').format("YYYY/MM/DD")

  //   cy.get("[name='dateLeftCanada']").type(date).blur()

  //   cy.contains('Date Left Canada cannot be in the future');
  // })

  it("Fails postal code field validation - incorrect length", () => {
    cy.get('[name="postalCode"]').type("T0M0E").blur();


    cy.contains('Please provide a valid postal code');
  })
  
  it("Fails postal code field validation - invalid code ", () => {
    cy.get('[name="postalCode"]').type("Z1A1AZ").blur();


    cy.contains('Please provide a valid postal code');
  })

  it("Fails pre-departure questions - Alberta air border and exempt primary", () => {
    cy.get('[value="Exempt"]').click();
    cy.get('[id="mui-component-select-exemptOccupation"]').click();
    cy.get('[data-value="Crew member (air)"]').click();
    cy.get('[id="mui-component-select-exemptOccupationDetails"]').click();
    cy.get('[data-value="Air transportation"]').click();

    cy.get('[id="mui-component-select-nameOfAirportOrBorderCrossing"]').click();
    cy.get('[data-value="Airport - Calgary International (YYC)"]').click();

    cy.contains("Continue").click();

    cy.contains('Must specify your pre-departure test date');
    cy.contains('Must specify pre-departure test country');
  }) 


  it("Fails pre-departure questions - Alberta air border and exempt additional traveller", () => {
    cy.get('[id="mui-component-select-nameOfAirportOrBorderCrossing"]').click();
    cy.get('[data-value="Airport - Calgary International (YYC)"]').click();

    cy.get('[name="hasAdditionalTravellers"]').check("Yes");
    cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
    cy.get('[data-value="1"]').click();

    cy.get(`[id="mui-component-select-additionalTravellers[0].exemptionType"]`).click();
    cy.get('[data-value="Exempt"]').click();
    cy.get(`[id="mui-component-select-additionalTravellers[0].exemptOccupation"]`).click();
    cy.get('[data-value="Crew member (air)"]').click();
    cy.get(`[id="mui-component-select-additionalTravellers[0].exemptOccupationDetails"]`).click();
    cy.get('[data-value="Air transportation"]').click();

    cy.contains("Continue").click();

    cy.contains('Enter a valid date within the past 5 days');
    cy.contains('Must specify pre-departure test country');
  }) 

})