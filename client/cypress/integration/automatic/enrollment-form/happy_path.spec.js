import { 
  passContactStep, 
  fillPrimaryContact, 
  fillArrivalInformation, 
  fillIsolationQuestionnaire, 
  fillAdditionalTravellers 
} from '../../utils/utils';
import moment from 'moment';

describe("Passes enrollment validation", () => {

  it("Passes enrollment validation - all dynamic fields", () => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passContactStep();

    //Fill primary contact portion
    fillPrimaryContact();

    // Marks form primary as exempt
    cy.get('[value="Exempt"]').click();
    cy.get('[id="mui-component-select-exemptOccupation"]').click();
    cy.get('[data-value="Crew member (air)"]').click();
    cy.get('[id="mui-component-select-exemptOccupationDetails"]').click();
    cy.get('[data-value="Air transportation"]').click();

    //Fill arrival information portion
    fillArrivalInformation();

    // Answers pre-departure questions
    cy.get('[name="preDepartureTestDate"]').type(moment().subtract(1, 'day').format('YYYY/MM/DD'))
    cy.get('[id="mui-component-select-preDepartureTestCountry"]').click();
    cy.get('[data-value="United States of America (the)"]').click();
    
    //Marks additional members and travellers outside household as No
    cy.get('[name="hasAdditionalTravellers"]').check("Yes");
    cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
    cy.get('[data-value="2"]').click();

    fillAdditionalTravellers({numberOfFieldSets: 2, omitOne: false});

    // Mark additional travellers as exempt and answer pre-departure questions
    for (let i = 0; i < 2; i++) {

    cy.get(`[id="mui-component-select-additionalTravellers[${i}].exemptionType"]`).click();
    cy.get('[data-value="Exempt"]').click();
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].exemptOccupation"]`).click();
    cy.get('[data-value="Crew member (air)"]').click();
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].exemptOccupationDetails"]`).click();
    cy.get('[data-value="Air transportation"]').click();

    cy.get(`[name="additionalTravellers[${i}].preDepartureTestDate"]`).type(moment().subtract(1, 'day').format('YYYY/MM/DD'))
    cy.get(`[id="mui-component-select-additionalTravellers[${i}].preDepartureTestCountry"]`).click();
    cy.get('[data-value="United States of America (the)"]').click();
    }

    //Fill isolation questionnaire portion, pass omit flag to not fill one field
    fillIsolationQuestionnaire({omitOne: false});

    cy.contains("Continue").click();

    cy.contains("Your application will be verified upon entry into Canada. Please make note of your confirmation number(s).")
  })
})