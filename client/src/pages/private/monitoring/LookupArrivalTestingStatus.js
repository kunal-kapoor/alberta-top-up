import React, { Fragment, useEffect, useState } from 'react';
import moment from 'moment';
import { Formik, Form as FormikForm, FastField } from 'formik';
import { useParams, useHistory, useLocation } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Tooltip from '@material-ui/core/Tooltip';
import Container from '@material-ui/core/Container';
import HelpIcon from '@material-ui/icons/Help';

import MuiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import { EnrollmentForm } from '../../../components/form/enrollment-form/EnrollmentForm';
import {
   ServiceAlbertaDeterminationSchema, 
   ServiceAlbertaStatuses, 
   notesRequiredForStatus, 
   EnrollmentStatusColumns, 
   SubmissionColumns, 
   HouseholdStatus, 
   PrimaryContact,
   EnrollmentStatus,
   ContactAdminOptions,
   ContactMethod,
  } from '../../../constants';
import { 
  Page, 
  Button, 
  ScrollContainer, 
  Menu, 
  Card, 
  InputFieldLabel, 
  StyledTableCell, 
  StyledTableRow, 
  StyledTableSortLabel, 
  Table,
  StatusChips
} from '../../../components/generic';
import { 
  useMonitoringPortalShowSubmission, 
  useDailyMonitoringLookupConfirmationNumber, 
  useMonitoringPortalActivity, 
  useConfirmationOnExit, 
  useToast
} from '../../../hooks';
import { 
  StyledDialogTitle, 
  StyledDialogContent, 
  StyledDialogActions 
} from '../../../components/generic/Modal';
import { 
  RenderRadioGroup, 
  RenderSelectField, 
  RenderTextField,
  RenderDateField, 
} from '../../../components/fields';
import { AxiosPrivate, dateToString, stringToDate } from '../../../utils';


export default () => {
  const params = useParams();
  const history = useHistory();
  const location = useLocation();
  const { openToast } = useToast();
  const [isMobileDrawerOpen, setMobileDrawerOpen] = useState(false);
  const [copyMessage, setCopyMessage] = useState("Copy link");
  const [isEditDialogOpen, setEditDialogOpen] = useState(false);

  const {
    lookup,
    assign,
    unassign,
    error,
    initialValues,
    formValues,
    isFetching: isLookupFetching,
    tableData,
    statusTableData,
    enrollmentActions,
    setStatusData,
    statusReason,
    changeStatus,
    generateLink,
    showStatusHistory,
    sendReminderLink
  } = useDailyMonitoringLookupConfirmationNumber();

  const {
    submit,
    isFetching: isDeterminationFetching,
    success
  } = useMonitoringPortalActivity();

  const {
    showSubmission
  } = useMonitoringPortalShowSubmission();

  useConfirmationOnExit();

  const ContentStyles = StyledDialogContent(true);

  /**
   * On page load, grab the ID from the url and perform a search query to find the matching form data.
   */
  useEffect(() => {
    const { formId } = location.state || {};
    (async () => {
      document.title = 'Alberta COVID-19 Mandatory Testing Program - Traveller Submissions'

      await lookup(params.confirmationNumber);
    })();
  }, []);

  useEffect(() => {
    (async () => {
      if (success) {
        await lookup(params.confirmationNumber);
      }
    })();
  },[success])

  const handleSubmitEditContact = async(values, confirmationNumber) => {
    try {
      values.arrivalDate = moment(values.arrivalDate).format('YYYY-MM-DD-HH.mm.ss.SSSSSS');
      await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${formValues.id}/contact-info`, values);
      await lookup(confirmationNumber);
    } catch(e) {
      openToast({ status: 'error', message: e.message || "Failed to get household data" });
    }
    setEditDialogOpen(false)
  }
  useEffect(() => {
    if(statusTableData.rows.length > 0 )
    {
      showStatusHistory(renderTravellerSubmissions());
    }
  },[statusTableData])

  const renderSidebar = () => (
    <Box
      p={4}
      height="100%"
      bgcolor="common.white"
      boxShadow="-15px 0px 20px -15px rgba(0, 0, 0, 0.2)"
      borderColor="divider"
      borderTop={1}
      overflow="auto"
    >
      <Formik
        onSubmit={values => submit(formValues.id, values)}
        validationSchema={ServiceAlbertaDeterminationSchema}
        initialValues={initialValues}
      >
        {({values}) => (
        <FormikForm>
          <Grid container spacing={2}>

            {/** Title + (Actions) */}
            <Grid item xs={12}>
              <Box mb={1} component={Grid} container alignItems="center" justify="space-between">
                <Grid item>
                  <Typography variant="subtitle1" color="textSecondary">
                    {formValues.assignedToMe ? 'Update Submission' : 'Submission Details'}
                  </Typography>
                </Grid>
                {(formValues.agent || enrollmentActions.length) && (
                  <Grid item>
                    <Menu
                      label="Actions"
                      size="small"
                      color="primary"
                      variant="contained"
                      options={[...(formValues.agent? [{
                        label: 'Unassign Record',
                        onClick: () => unassign(formValues.id, params.confirmationNumber, formValues.agent),
                      }]: []), ...enrollmentActions]}
                    />
                  </Grid>
                )}
              </Box>
              <Divider />
            </Grid>

            {/** Assigned Agent */}
            <Grid item xs={12}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>Assigned Agent</Typography>
              <Box display="flex" alignItems="center">
                <AccountCircleOutlinedIcon />&nbsp;{formValues.agent || 'Unassigned'}
              </Box>
            </Grid>

            {/** Status + Notes */}
            {!formValues.assignedToMe ? (
              <Fragment>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="textSecondary" gutterBottom>Current Status</Typography>
                  {formValues.activities[0]?.status || 'None'}
                </Grid>
              </Fragment>
            ) : (
              <Fragment>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="textSecondary" gutterBottom>Status</Typography>
                  <FastField
                    name="status"
                    placeholder="Select New Status..."
                    component={RenderSelectField}
                    options={ServiceAlbertaStatuses}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography
                      variant="subtitle2"
                      color="textSecondary"
                      gutterBottom>
                    Notes {notesRequiredForStatus(values.status) && '*'}
                  </Typography>
                  <FastField
                    name="note"
                    component={RenderTextField}
                    placeholder="Add notes to document your interaction..."
                    variant="outlined"
                    multiline
                    rows={7}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    type="submit"
                    text="Submit"
                    loading={isDeterminationFetching}
                  />
                </Grid>
              </Fragment>
            )}

            {/** (Assign To Me) */}
            {!formValues.agent && (
              <Grid item xs={12}>
                <Button
                  text="Assign To Me"
                  onClick={() => assign(formValues.id, params.confirmationNumber)}
                />
              </Grid>
            )}

            {/** Previous Notes */}
            <Box mt={1.5} component={Grid} item xs={12}>
              <Typography variant="subtitle2" gutterBottom>Previous Notes</Typography>
              <Divider />
              {formValues.activities.length === 0 ? (
                <Box py={1.5} component={Typography} variant="body2" color="common.darkGrey">
                  There are no notes on this record
                </Box>
              ) : formValues.activities.map(({ date, agent, status, type, note }, index, array) => {
                const isLastChild = index === array.length - 1;
                const formattedDate = dateToString(date, true, 'DD MMM YYYY, hh:mm A');
                return (
                  <Fragment key={index}>
                    <Box pt={2} pb={3}>
                      <Grid container alignItems="center" justify="space-between" spacing={1}>
                        <Grid item>
                          <Box
                            fontSize="14px"
                            lineHeight="20px"
                            letterSpacing="-0.25px"
                          >
                            {formattedDate}
                          </Box>
                        </Grid>
                        <Grid item>
                          <Box
                            fontSize="14px"
                            lineHeight="20px"
                            letterSpacing="-0.25px"
                            display="flex"
                            alignItems="center"
                          >
                            <AccountCircleOutlinedIcon fontSize="small" />&nbsp;{agent}
                          </Box>
                        </Grid>
                      </Grid>
                      <Box
                        pt={2.5}
                        fontSize="16px"
                        fontWeight="bold"
                        lineHeight="19px"
                        letterSpacing="0"
                        color="secondary.main"
                      >
                        {type}: {status}
                      </Box>
                      <Box pt={1.25}>
                        <Typography variant="body2">{note}</Typography>
                      </Box>
                    </Box>
                    {!isLastChild && <Divider />}
                  </Fragment>
                );
              })}
            </Box>
          </Grid>
        </FormikForm>)}
      </Formik>
    </Box>
  );

  const renderTravellerSubmissions = () => (
    <Container maxWidth="md">
          <Card style={{ overflowX: 'auto' }} noPadding>
            <MuiTable style={{ tableLayout: 'fixed' }} size="medium">
              <TableHead>
                <TableRow>
                  {statusTableData.columns.map(({ value, label }) => {
                    if (value === 'submission') {
                      return (<StyledTableCell style={{ width: '75%' }} key={value}>
                        {label}
                      </StyledTableCell>)
                    }
                    else {
                      return (<StyledTableCell style={{ width: '10%' }} key={value}>
                        {label}
                      </StyledTableCell>)
                    }
                  }
                  )}
                </TableRow>
              </TableHead>
              <TableBody>
                {statusTableData.rows.length === 0 ? (
                  <TableRow style={{ height: 100 }}>
                    <StyledTableCell colSpan={statusTableData.columns.length} align="center">
                      No results
                    </StyledTableCell>
                  </TableRow>
                ) : (statusTableData.rows.map((row, index) => (
                  <StyledTableRow key={index}>
                    {Object.keys(row).map((key, col_index) => {
                      if (statusTableData.columns[col_index].value === 'submission') {
                        return (
                          <StyledTableCell style={{ width: '75%' }} key={key}>
                            <Table
                              columns={SubmissionColumns}
                              rows={row[key]}
                            />
                          </StyledTableCell>
                        )
                      }
                      else if (statusTableData.columns[col_index].value === 'status') {
                        return(<StyledTableCell style={{ width: '10%' }} key={key}>
                          <StatusChips status={row[key]} />
                        </StyledTableCell>)
                      }
                      return (
                        <StyledTableCell style={{ width: '10%' }} key={key}>
                          {row[key]}
                        </StyledTableCell>
                      )
                    })}
                  </StyledTableRow>
                )))}
              </TableBody>
            </MuiTable>
          </Card>
        </Container>
  );

  if (isLookupFetching) return (
    <Page hideFooter centerContent>
      <CircularProgress />
    </Page>
  );

  if (error) return (
    <Page hideFooter centerContent>
      <Container maxWidth="sm" align="center">
        <Typography variant="body1" paragraph>{error}</Typography>
        <Button
          text="Go Back"
          onClick={() => history.goBack()}
          fullWidth={false}
        />
      </Container>
    </Page>
  );

  return (
    <Page hideFooter>
      <Box component={Grid} height="calc(100vh - 44px)" container>

        {/** Form */}
        <Box component={Grid} item xs={12} sm={7} height="100%">
          <ScrollContainer
            headerContent={(
              <Box
                p={3}
                bgcolor="secondary.main"
                color="common.white"
                display="flex"
                alignItems="center"
                justifyContent="space-between"
              >
                <Container maxWidth="sm" disableGutters>
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="h4">Mandatory Testing Submission</Typography>
                    <Grid item>
                      <Button
                        style={{ width: 125 }}
                        text="Back"
                        variant="outlined"
                        size="small"
                        color="inherit"
                        fullWidth={false}
                        onClick={() => history.goBack()}
                      />
                    </Grid>
                  </Box>
                </Container>
              </Box>
            )}
            bodyContent={(
              <Box p={3}>
                <Container maxWidth="md" disableGutters>
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} align="center">
                      <Card>
                        <Typography variant="overline" color="textPrimary">
                          Arrival Date:
                        </Typography>
                        <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                          {dateToString(formValues.arrivalDate, true, 'DD MMM YYYY')}
                        </Box>
                      </Card>
                    </Grid>
                     <Grid item xs={12} sm={6} align="center">
                      <Card>
                        <Typography variant="overline" color="textPrimary">
                          Confirmation Number:
                        </Typography>
                        <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                          {formValues.confirmationNumber}
                        </Box>
                      </Card>
                    </Grid> 
                    <Grid item xs={12}>
                      <Card 
                        title={
                          <div
                            style={{display: 'flex', justifyContent: 'space-between'}}
                          >
                            Primary Contact
                            <Button 
                              style={{ width: 125 }}
                              text="Edit"
                              variant="outlined"
                              size="small"
                              color="inherit"
                              fullWidth={false}
                              onClick={() => setEditDialogOpen(true)}
                            />
                          </div>
                        } 
                        isTitleSmall
                      >
                        <Grid style={{ overflowWrap: 'anywhere' }} container spacing={2}>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Full Name" />
                            <Typography variant="body1" color="textPrimary">{`${formValues.firstName} ${formValues.lastName}`}</Typography>
                          </Grid>
                          {formValues.enrollmentStatus === EnrollmentStatus.ENROLLED && <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Daily Reminder" />
                            <Grid item>
                              {formValues.dailyReminderLink && (
                                <Grid item xs={12}>
                                  <Button
                                    style={{ width: 260 }}
                                    text="Submit for household"
                                    variant="outlined"
                                    size="small"
                                    color="inherit"
                                    fullWidth={false}
                                    onClick={() => {
                                      const link = `${window.location.origin}/${formValues.dailyReminderLink}`;
                                      window.open(link, '_blank');
                                  }}
                                  />
                                </Grid>
                              )}

                              {formValues.dailyReminderTravellerLink && formValues.contactMethod !== ContactMethod.CALLIN && (
                                <Grid item xs={12}>
                                  <Tooltip title={copyMessage}>
                                    <Button
                                      style={{ width: 260, marginTop: '7px' }}
                                      text="Copy link"
                                      variant="outlined"
                                      size="small"
                                      color="inherit"
                                      fullWidth={false}
                                      onClick={() => { 
                                        navigator.clipboard.writeText(formValues.dailyReminderTravellerLink).then(function() {
                                          setCopyMessage("Copied!");
                                        }); }
                                      }
                                    />
                                  </Tooltip>
                                </Grid>
                              )}

                              {!formValues.dailyReminderLink && formValues.contactMethod === ContactMethod.CALLIN && (
                                <Grid item xs={12}>
                                  <Button
                                    style={{ width: 260, marginTop: '7px' }}
                                    text="Generate daily reminder link"
                                    variant="outlined"
                                    size="small"
                                    color="inherit"
                                    fullWidth={false}
                                    onClick={() => generateLink(formValues.id, formValues.confirmationNumber)}
                                  />
                                </Grid>
                              )}

                              {formValues.contactMethod !== ContactMethod.CALLIN && (
                                <Grid item xs={12}>
                                  <Button
                                    style={{ width: 260, marginTop: '7px' }}
                                    text="Send daily check-in notification"
                                    variant="outlined"
                                    size="small"
                                    color="inherit"
                                    fullWidth={false}
                                    disabled={isLookupFetching}
                                    onClick={() => sendReminderLink(formValues.id, formValues.confirmationNumber)}
                                  />
                                </Grid>
                              )}
                            </Grid>
                          </Grid> }
                          {formValues.contactMethod && (
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Contact Method" />
                            <Typography variant="body1" color="textPrimary">{ContactAdminOptions.find(item => item.value === formValues.contactMethod)?.label}</Typography>
                          </Grid>
                          )}
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Phone Number" />
                            <Typography variant="body1" color="textPrimary">{formValues.phoneNumber}</Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Email" />
                            <Typography variant="body1" color="textPrimary">{formValues.email || 'Not Provided'}</Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Enrollment Status" />
                            <Typography variant="body1" color="textPrimary">{formValues.enrollmentStatus}</Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Overall Household Status" />
                            <Box display="flex" flexWrap="wrap">
                              {formValues.householdCardStatus && (
                                <StatusChips status={formValues.householdCardStatus} />
                              )}
                              { statusReason && formValues.householdCardStatus && formValues.householdCardStatus !== HouseholdStatus.GREEN && (
                                <Tooltip title={statusReason}>
                                  <HelpIcon />
                                </Tooltip>
                              ) }
                            </Box>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Submission" />
                            <Typography variant="body1" color="textPrimary">
                              {!isLookupFetching && (<Button
                                text="View Submission"
                                style={{ width: 260}}
                                variant="outlined"
                                size="small"
                                color="inherit"
                                fullWidth={false}
                                onClick={() => showSubmission(
                                  <EnrollmentForm
                                    isFetching={false}
                                    initialValues={formValues.household}
                                    isEditing={false}
                                    setIsEditing={false}
                                    isDisabled
                                    canEdit={false}
                                  />
                                )}
                              />)}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Card>
                    </Grid>
                    <Grid item xs={12}>
                      
                        <Table
                          columns={tableData.columns}
                          rows={tableData.rows}
                          isLoading={isLookupFetching}
                        />
                      
                    </Grid>
                  </Grid>
                </Container>
              </Box>
            )}
            footerContent={(
              <Hidden smUp>
                <Button
                  style={{ height: 95, borderRadius: 0 }}
                  text="Submit Your Determination"
                  onClick={() => setMobileDrawerOpen(true)}
                />
              </Hidden>
            )}
          />
        </Box>

        {/** Sidebar - Desktop */}
        <Hidden xsDown>
          <Box component={Grid} item xs={12} sm={5} height="100%">
            {renderSidebar()}
          </Box>
        </Hidden>

        {/** Sidebar - Mobile */}
        <Hidden smUp>
          <Drawer
            anchor="right"
            open={isMobileDrawerOpen}
            onClose={() => setMobileDrawerOpen(false)}
          >
            {renderSidebar()}
          </Drawer>
        </Hidden>
      </Box>
      <Dialog 
        open={isEditDialogOpen}
        onClose={() => setEditDialogOpen(false)}
        maxWidth="sm" 
        fullWidth
      >
        <Formik 
          initialValues={{email: formValues.email, phoneNumber: formValues.phoneNumber, arrivalDate: dateToString(formValues.arrivalDate), contactMethod: formValues.contactMethod}}
          validationSchema={PrimaryContact}
          onSubmit={(values) => handleSubmitEditContact(values, params.confirmationNumber)}
        >
          <FormikForm>
            <StyledDialogTitle component="div">
              <Typography component="div" variant="h4">Edit Primary Contact Information</Typography>
            </StyledDialogTitle>
            <ContentStyles>

              <Container maxWidth="sm">
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12}>
                    <FastField
                      name="email"
                      label="Contact Email*"
                      component={RenderTextField}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <FastField
                      name="phoneNumber"
                      label="Contact Phone*"
                      component={RenderTextField}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <FastField
                      name="arrivalDate"
                      label="Arrival Date* (YYYY/MM/DD)"
                      component={RenderDateField}
                      disableFuture={false}
                    />
                  </Grid>
                  <Grid item  xs={12} sm={3}>
                    <FastField
                      name="contactMethod"
                      label="Contact Method*"
                      component={RenderRadioGroup}
                      options={ContactAdminOptions}
                    />
                  </Grid>
                </Grid>
              </Container>

            </ContentStyles>
            <StyledDialogActions>
              <Button
                style={{ minWidth: 150 }}
                text="Submit"
                type="submit"
                color="primary"
                variant="outlined"
                size="small"
                fullWidth={false}
              />
              <Button
                style={{ minWidth: 150 }}
                text='Close'
                onClick={() => setEditDialogOpen(false)}
                color="primary"
                variant="outlined"
                size="small"
                fullWidth={false}
              />
            </StyledDialogActions>
          </FormikForm>
        </Formik>
      </Dialog>
    </Page>
  );
};
