import React, { useEffect, useRef } from 'react';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { DailyMonitoringLookupFilters, EnrollmentStatusColumns } from '../../../constants';
import { useDailyMonitoringLookup } from '../../../hooks';
import { withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import { Page, Filter, Table, Button, InputFieldLabel, PageHeader } from '../../../components/generic';
import { getOptionalURLParams } from '../../../utils';
import { FastField, Formik, Form as FormikForm } from 'formik';
import { RenderDateField , RenderTextField, RenderSelectField, RenderRadioGroup} from '../../../components/fields';
import { ReportFilterValidationSchema } from '../../../constants/report-validation';
import { useListActiveAgents } from '../../../hooks/admin';
import ClearIcon from "@material-ui/icons/Clear";
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';

export default () => {
  const history = useHistory();
  const {
    onFilter,
    onChangePage,
    onSort,
    onSubmit,
    clearFilter,
    initialFilters,
    zeroStates,
    selectedSearchFilters,
    tableData,
    isFetching,
  } = useDailyMonitoringLookup();

  const {
    initialAgent,
    hasExecuted: hasFetchedAgents,
    options
  } = useListActiveAgents();

  const { cardStatus: filterValue = DailyMonitoringLookupFilters[1].value } = getOptionalURLParams(history);
  
  const initialValues = Object.assign({}, initialFilters, {agent: (initialAgent && initialAgent.name) || 'all', program: 'border_pilot'});  

  // Reference to formik form instance
  const lookupForm = useRef();

  const resetFilter = (field) => {
    if(field === 'date') {
      resetFilter('fromDate');
      resetFilter('toDate');
      return;
    }

    if(lookupForm.current) {
      lookupForm.current.setFieldValue(field, zeroStates[field] || '');
    }

    clearFilter(field);
  };

  const StyledDateSelector = withStyles((theme) => ({
    root: {
      [theme.breakpoints.up('md')]: {
        maxWidth: '220px'
      }
    },
  }))(FastField);
  
  useEffect(() => {
    document.title = 'Alberta COVID-19 Border Testing Pilot Program Admin Login - Traveller Submissions'
  }, [])
  
  return (
    <Page hideFooter>

      <PageHeader
        header="All Submissions">
      </PageHeader>

      {/** White Banner */}
      {/* <Grid container xs={12}> */}
        <Box pt={[3, 3]} pb={[3, 3]}>
        {initialFilters && hasFetchedAgents && <Formik
            validationSchema={ReportFilterValidationSchema}
            initialValues={initialValues}
            innerRef={lookupForm}
            onSubmit={values => onSubmit(values)}>
          <FormikForm>
            <Container maxWidth="md">
              <Grid container justify="space-between" alignItems="center" spacing={1}>
                <Grid item style={{position: 'relative'}}>
                  <StyledDateSelector
                      name="fromDate"
                      label="From Date (YYYY/MM/DD)"
                      component={RenderDateField}
                      placeholder="YYYY/MM/DD"
                      disableFuture={false}
                  />
                </Grid>
                <Grid item>
                  <StyledDateSelector
                      name="toDate"
                      label="To Date (YYYY/MM/DD)"
                      component={RenderDateField}
                      placeholder="YYYY/MM/DD"
                      disableFuture={false}
                  />
                </Grid>
                <Grid item style={{minWidth: '140px', maxWidth: '140px'}}>
                  <FastField
                      name="agent"
                      label="Agent"
                      options={options}
                      component={RenderSelectField}
                    />
                </Grid>
                <Grid item>
                  <FastField
                      name="enrollmentStatus"
                      label="Enrollment Status"
                      component={RenderSelectField}
                      options={EnrollmentStatusColumns}
                    />
                </Grid>
                <Grid item>
                  <InputFieldLabel label={<span>Search <Tooltip title="Name/Confirmation #/Email/Phone #"><InfoIcon /></Tooltip></span>}></InputFieldLabel>
                  <FastField
                    name="query"
                    placeholder="Search"
                    label=""
                    component={RenderTextField}
                  />
                </Grid>
                <Grid item>
                  <FastField
                      name="program"
                      label="Program"
                      component={RenderRadioGroup}
                      options={[
                        { value: 'arrival_testing', label: 'Mandatory Testing' },
                        { value: 'border_pilot', label: 'Border Pilot' }
                      ]}
                    />
                </Grid>
                <Grid item style={{textAlign: 'center', marginTop: '30px'}}>
                  <label>&nbsp;</label>
                  <Button
                      style={{minWidth: '100px', padding: '10px 10px'}}
                      data-testid="lookup-search-button"
                      text="Search"
                      type="submit"
                      color="primary"
                      size="small"
                      endIcon={<SearchIcon></SearchIcon>}
                      fullWidth={false}
                  />
                </Grid>
              </Grid>
            </Container>
          </FormikForm>
        </Formik>
      }
      </Box>

      {/** Table */}
      <Box py={3}>

        <Container maxWidth="md" style={{paddingBottom: '12px'}}>
          <Grid container justify="space-between" alignItems="center" spacing={2}>
            <Grid item>
              <Grid container alignItems="center" spacing={2}>
                <Grid item>
                  <Typography variant="subtitle2" color="textSecondary">Filter:</Typography>
                </Grid>
                <Grid item>
                  <Filter
                    onClick={onFilter}
                    options={DailyMonitoringLookupFilters}
                    value={filterValue}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container alignItems="center" spacing={2}>
                <Grid item>
                </Grid>
                <Grid item>
                  <Filter
                    endIcon={<ClearIcon/>}
                    onClick={field => resetFilter(field)}
                    options={selectedSearchFilters}
                    alwaysSelected={true}>
                  </Filter>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <Container maxWidth="md">
          <Table
            columns={tableData.columns}
            rows={tableData.rows}
            totalRows={tableData.totalRows}
            currentPage={tableData.currentPage}
            rowsPerPage={20}
            isLoading={isFetching}
            onChangePage={onChangePage}
            onSort={onSort}
          />
        </Container>
      </Box>
    </Page>
  );
};
