import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Prompt, useParams, useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { useArrivalTestingEnrollmentForm, useConfirmationOnExit, useToast } from '../../../hooks';
import { Page, AlertError, PageHeader } from '../../../components/generic';
import { Route, ArrivalTestingEnrollmentFormInitialValues } from '../../../constants';
import { ArrivalTestingEnrollmentForm } from '../../../components/form/arrival-testing/ArrivalTestingEnrollmentForm';
import { AxiosPublic } from '../../../utils';
import { CollectionNotice } from '../../../components/generic/CollectionNotice';

export default () => {
  const { t } = useTranslation();
  const { submit, isFetching } = useArrivalTestingEnrollmentForm();
  const { verificationToken } = useParams();
  const { openToast } = useToast();
  const [submittedContactInformation, setSubmittedContactInformation] = useState();
  const [hasVerifiedToken, setHasVerifiedToken] = useState(false);
  const history = useHistory();

  useEffect(() => {
    (async () => {
      document.title = t("Alberta Mandatory COVID-19 Testing Program");
      document.description = t("One member of each household must complete the mandatory testing registration");
      setTimeout(() => window.scrollTo(0, 0), 100);
      try {
        setSubmittedContactInformation(await AxiosPublic.get(`/api/v2/rtop/traveller/verification/${verificationToken}`));
        setHasVerifiedToken(true);
      } catch(e) {
        let message = '';

        switch(e?.response?.data?.error?.code || '') {
          case 'invalid_token':
            message = 'Link is not valid. Please verify your contact method again';
            break;
          case 'expired_token':
            message = 'Link has expired. Please verify your contact method again';
            break;
          default:
            message = e.message || 'Something wrong happened. Please verify your contact method again';
            break;
        }

        openToast({ status:"error", message});

        history.push(Route.EligibilityForm);
      }
    })();
  }, []);

  useConfirmationOnExit();

  return (
    <Page>
      {/** Prompts the user before refreshing the page - specifically - the logo is clicked */}
      <Prompt message={(location) => {
        const isHeaderLogoClicked = location.pathname === Route.Root;
        return isHeaderLogoClicked ? t("Are you sure you want to leave?") : true
      }} />

      <PageHeader
        header="Alberta Mandatory COVID-19 Testing Program"
        subheader="One member of each household must complete the mandatory testing registration."
        alertProp="REACT_APP_TESTING_FORM_ALERT">
      </PageHeader>

      {/** Form */}
      <Container maxWidth="sm">
        <Box mt={2} mb={4}>
          {
            hasVerifiedToken 
              && 
            <ArrivalTestingEnrollmentForm
              onSubmit={submit}
              isFetching={isFetching}
              canEdit={true}
              initialValues={{
                ...ArrivalTestingEnrollmentFormInitialValues,
                //values that are overriden
                phoneNumber: submittedContactInformation?.contactPhoneNumber || '',
                email: submittedContactInformation?.contactEmail || '',
                // Hidden Field
                contactMethod: submittedContactInformation?.contactMethod,
              }}
            />
          }
        </Box>
      </Container>
    </Page>
  );
};
