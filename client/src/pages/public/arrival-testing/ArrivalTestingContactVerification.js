import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { useToast, useVerification } from '../../../hooks';
import { useConfirmationOnExit } from '../../../hooks';

import { Page, AlertError, PageHeader } from '../../../components/generic';
import { ContactOptions } from '../../../constants';
import { ContactVerificationQuestionnaire } from '../ContactVerification';
import { VerificationSent } from '../../../components/form/enrollment-form/VerificationSent';
import { AxiosPublic } from '../../../utils';
import { VerifySMS } from '../../../components/form/enrollment-form/VerifySMS';
import { CollectionNotice } from '../../../components/generic/CollectionNotice';

export default () => {
  const { t } = useTranslation();
  const { openToast } = useToast();
  const history = useHistory();
  const { submit, isFetching, response, hasSent, reset } = useVerification();
  const [step, setStep] = useState('symptomVerification');
  const [formData, setFormData] = useState({});
  const [contactVerified, setContactVerified] = useState(false);


  const submitContactVerification = async ({contactEmail, contactPhoneNumber, contactMethod}) => {
    const req = Object.assign({}, formData, {
      email: contactEmail,
      phoneNumber: contactPhoneNumber,
      contactMethod,
      program: 'arrival_testing'
    });
    setFormData(req);

    submit(req);
  }

  const resetVerification = () => {
      reset();
      setContactVerified(false);
      setStep('contactVerification');
  };

  const submitSmsCode = async(value) => {
    try {
      const res = await AxiosPublic.post(`/api/v2/rtop/traveller/verification/verify/code`, {code: value.verificationCode, verificationId: response.token, program: 'arrival_testing'})
      history.replace(`/arrival-testing/enroll/${res.token}`)
    } catch (e) {
      const message = e?.response?.data?.error?.code === 'verification_not_found'?
        'Code is invalid. Please try again.': 'Failed to verify SMS code'
      openToast({ status: 'error', message});
    }
  }

  useEffect(() => {
    (async () => {
      document.title = t("Alberta Mandatory COVID-19 Testing Program");
      document.description = t("One member of each household must complete the mandatory testing registration");
      setTimeout(() => window.scrollTo(0, 0), 100);
    })();
  }, []);

  useEffect(() => {
    if (hasSent && formData.contactMethod !== ContactOptions[1].value) {
      setContactVerified(true)
    }
  }, [hasSent])

  useConfirmationOnExit();

  return (
    <Page>
      <PageHeader
        header="Alberta Mandatory COVID-19 Testing Program"
        subheader="One member of each household must complete the mandatory testing registration."
        alertProp="REACT_APP_TESTING_FORM_ALERT">
      </PageHeader>

      {/** Contact Verification Questionnaire */}
      {
        <Container maxWidth="sm">
          <Box mt={2} mb={4}>
            {!hasSent && !contactVerified && (
              <ContactVerificationQuestionnaire
                onSubmit={submitContactVerification}
                methodLabel="What is your preferred communication method?"
                isFetching={isFetching}
                initialValues={{
                  contactMethod: '',
                  contactEmail: '',
                  contactPhoneNumber: '',
                }}
              />)}
            {
              hasSent && !contactVerified 
                && 
                <VerifySMS onSubmit={submitSmsCode} onReset={resetVerification}/>
            }
            {
              contactVerified 
                && 
                <VerificationSent onReset={resetVerification}/>
            }
          </Box>
        </Container>
      }
    </Page>
  );
};
