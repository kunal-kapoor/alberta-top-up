import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Prompt, useParams, useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { useEnrollmentForm, useConfirmationOnExit, useToast } from '../../hooks';
import { Page, AlertError } from '../../components/generic';
import { Route, EnrollmentFormInitialValues } from '../../constants';
import { EnrollmentForm } from '../../components/form/enrollment-form/EnrollmentForm';
import { AxiosPublic } from '../../utils';
import { CollectionNotice } from '../../components/generic/CollectionNotice';

export default () => {
  const { t } = useTranslation();
  const { submit, isFetching } = useEnrollmentForm();
  const { verificationToken } = useParams();
  const { openToast } = useToast();
  const [submittedContactInformation, setSubmittedContactInformation] = useState();
  const [hasVerifiedToken, setHasVerifiedToken] = useState(false);
  const history = useHistory();

  useEffect(() => {
    (async () => {
      document.title = t("Alberta COVID-19 Border Testing Pilot Program");
      document.description = t("One member of each household must complete the pilot application");
      setTimeout(() => window.scrollTo(0, 0), 100);
      try {
        setSubmittedContactInformation(await AxiosPublic.get(`/api/v2/rtop/traveller/verification/${verificationToken}`));
        setHasVerifiedToken(true);
      } catch(e) {
        let message = '';

        switch(e?.response?.data?.error?.code || '') {
          case 'invalid_token':
            message = 'Link is not valid. Please verify your contact method again';
            break;
          case 'expired_token':
            message = 'Link has expired. Please verify your contact method again';
            break;
          default:
            message = e.message || 'Something wrong happened. Please verify your contact method again';
            break;
        }

        openToast({ status:"error", message});

        history.push(Route.EligibilityForm);
      }
    })();
  }, []);

  useConfirmationOnExit();

  return (
    <Page>

      {/** Prompts the user before refreshing the page - specifically - the logo is clicked */}
      <Prompt message={(location) => {
        const isHeaderLogoClicked = location.pathname === Route.Root;
        return isHeaderLogoClicked ? t("Are you sure you want to leave?") : true
      }} />

      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Box mb={2.5} component={Typography} variant="h1">
            {"Alberta COVID-19 Border Testing Pilot Program"}
          </Box>
          <Typography variant="subtitle1">
            {"One member of each household must complete the pilot application."}
          </Typography>
        </Container>
      </Box>

      {/** Collection Notice */}
      <CollectionNotice/>
      <AlertError
        message={
          <div>
            <div style={{fontWeight: 600}}>Due to the increased threat of COVID-19 variants, protocols have changed effective immediately.</div>
            <ul>
              <li>
                Participants are now required to remain in quarantine until receipt of the second negative test result taken on day 7 or 8 counting arrival day as day 1. 
              </li>
              <li>
                Travelers currently in the program who have not yet received the results of their second test are required to return to quarantine immediately until they receive such.
              </li>
              <li>
                Participants are not allowed to return to school, out of school care, daycare or post secondary institutions until 14 days after arrival.
              </li>
              <li>
                Participants are not allowed to return to work outside of their residence until 14 days after arrival. 
              </li>
            </ul>
          </div>
        }
      />
      {window._env_.REACT_APP_FORM_ALERT && <AlertError
        message={t(window._env_.REACT_APP_FORM_ALERT)}
      />}

      {/** Form */}
      <Container maxWidth="sm">
        <Box mt={2} mb={4}>
          {hasVerifiedToken && (<EnrollmentForm
            onSubmit={submit}
            isFetching={isFetching}
            canEdit={true}
            initialValues={{
              ...EnrollmentFormInitialValues,
              //values that are overriden
              phoneNumber: submittedContactInformation?.contactPhoneNumber || '',
              email: submittedContactInformation?.contactEmail || '',
              // Hidden Field
              contactMethod: submittedContactInformation?.contactMethod,
            }}
          />
        )}
        </Box>
      </Container>
    </Page>
  );
};
