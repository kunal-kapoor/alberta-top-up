import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { useToast, useVerification } from '../../hooks';
import { useConfirmationOnExit } from '../../hooks';

import { Page, AlertError } from '../../components/generic';
import { ContactOptions } from '../../constants';
import { EligibilityCheckQuestionnaire } from './EligibilityCheckQuestionnaire';
import { ContactVerificationQuestionnaire } from './ContactVerification';
import { EnrollmentNotEligible } from '../../components/form/enrollment-form/EnrollmentNotEligible';
import { VerificationSent } from '../../components/form/enrollment-form/VerificationSent';
import { AxiosPublic } from '../../utils';
import { VerifySMS } from '../../components/form/enrollment-form/VerifySMS';
import { EligibilityQuestions, answersAreEligible } from '../../constants/eligibilityQuestions';
import { CollectionNotice } from '../../components/generic/CollectionNotice';

export default () => {
  const { t } = useTranslation();
  const { openToast } = useToast();
  const history = useHistory();
  const { submit, isFetching, response, hasSent, reset } = useVerification();
  const [step, setStep] = useState('symptomVerification');
  const [formData, setFormData] = useState({});
  const [contactVerified, setContactVerified] = useState(false);

  const submitSymptomVerificationQuestionnaire = values => {
    if(answersAreEligible(values)) {
        setStep('contactVerification');
    } else {
      setStep('notEligible');
    }
  }

  const submitContactVerification = async ({contactEmail, contactPhoneNumber, contactMethod}) => {
    const req = Object.assign({}, formData, {
      email: contactEmail,
      phoneNumber: contactPhoneNumber,
      contactMethod,
      program: 'border_pilot'
    });
    setFormData(req);

    submit(req);
  }

  const resetVerification = () => {
      reset();
      setContactVerified(false);
      setStep('contactVerification');
  };

  const submitSmsCode = async(value) => {
    try {
      const res = await AxiosPublic.post(`/api/v2/rtop/traveller/verification/verify/code`, {code: value.verificationCode, verificationId: response.token})
      history.replace(`/enroll/${res.token}`)
    } catch (e) {
      const message = e?.response?.data?.error?.code === 'verification_not_found'?
        'Code is invalid. Please try again.': 'Failed to verify SMS code'
      openToast({ status: 'error', message});
    }
  }

  useEffect(() => {
    (async () => {
      document.title = t("Alberta COVID-19 Border Testing Pilot Program");
      document.description = t("One member of each household must complete the pilot application");
      setTimeout(() => window.scrollTo(0, 0), 100);
    })();
  }, []);

  useEffect(() => {
    if (hasSent && formData.contactMethod !== ContactOptions[1].value) {
      setContactVerified(true)
    }
  }, [hasSent])

  useConfirmationOnExit();

  return (
    <Page>
      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Box mb={2.5} component={Typography} variant="h1">
            {"Alberta COVID-19 Border Testing Pilot Program"}
          </Box>
          <Typography variant="subtitle1">
            {"One member of each household must complete the pilot application."}
          </Typography>
        </Container>
      </Box>

      {/** Collection Notice */}
      <CollectionNotice/>
      <AlertError
        message={
          <div>
            <div style={{fontWeight: 600}}>Due to the increased threat of COVID-19 variants, protocols have changed effective immediately.</div>
            <ul>
              <li>
                Participants are now required to remain in quarantine until receipt of the second negative test result taken on day 7 or 8 counting arrival day as day 1. 
              </li>
              <li>
                Travelers currently in the program who have not yet received the results of their second test are required to return to quarantine immediately until they receive such.
              </li>
              <li>
                Participants are not allowed to return to school, out of school care, daycare or post secondary institutions until 14 days after arrival.
              </li>
              <li>
                Participants are not allowed to return to work outside of their residence until 14 days after arrival. 
              </li>
            </ul>
          </div>
        }
      />
      {window._env_.REACT_APP_FORM_ALERT && <AlertError
        message={t(window._env_.REACT_APP_FORM_ALERT)}
      />}

      {/** Verification Questionnaire */}
      {step === 'symptomVerification' && (
        <Container maxWidth="sm">
          <Box mt={2} mb={4}>
            <EligibilityCheckQuestionnaire
              onSubmit={submitSymptomVerificationQuestionnaire}
              isFetching={isFetching}
              initialValues={EligibilityQuestions.reduce((values, {name}) => {
                values[name] = '';

                return values;
              }, {})}/>
          </Box>
        </Container>
      )}

      {/** Not eligible to participate */}
      {step === 'notEligible' && (
        <Container maxWidth="sm">
            <Box mt={2} mb={4}>
                <EnrollmentNotEligible />
            </Box>
        </Container>
      )}

      {/** Contact Verification Questionnaire */}
      {step === 'contactVerification' && (
        <Container maxWidth="sm">
          <Box mt={2} mb={4}>
            {!hasSent && !contactVerified && (<ContactVerificationQuestionnaire
              onSubmit={submitContactVerification}
              isFetching={isFetching}
              initialValues={{
                contactMethod: '',
                contactEmail: '',
                contactPhoneNumber: '',
              }}/>)}
            {
              hasSent && !contactVerified 
                && 
              <VerifySMS onSubmit={submitSmsCode} onReset={resetVerification}/>
            }
            {contactVerified && (
                <VerificationSent onReset={resetVerification}/>
            )}
          </Box>
        </Container>
      )}

    </Page>
  );
};
