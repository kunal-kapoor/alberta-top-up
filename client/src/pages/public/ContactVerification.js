import React from 'react';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { ContactOptions, ContactValidationSchema } from '../../constants';
import { FastField } from 'formik';

import { Card, Button } from '../../components/generic';
import { FocusError } from '../../components/generic';
import { RenderRadioGroup, RenderTextField } from '../../components/fields';
import { Box } from '@material-ui/core';


export const ContactVerificationQuestionnaire = ({ onSubmit, initialValues, isDisabled, isFetching, methodLabel }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={ContactValidationSchema}
      onSubmit={onSubmit}
    >{({values}) => (
        <FormikForm>
            <FocusError />
            <Card title={t('Contact Information')}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <FastField
                            name="contactMethod"
                            label={methodLabel || "How would you prefer to receive your daily check-in notice?"}
                            component={RenderRadioGroup}
                            disabled={isDisabled}
                            options={ContactOptions}
                        />
                    </Grid>
                    {values.contactMethod === 'email' && (
                    <Grid item xs={12}>
                        <FastField
                            name="contactEmail"
                            label="Email*"
                            placeholder="Required"
                            component={RenderTextField}
                            disabled={isDisabled}
                        />

                    </Grid>)}
                    {values.contactMethod === 'sms' && (
                    <Grid item xs={12}>
                        <FastField
                            name="contactPhoneNumber"
                            label="Phone Number*"
                            placeholder="Required"
                            component={RenderTextField}
                            disabled={isDisabled}
                        />

                    </Grid>)}
                    <Grid item xs={12}>
                        <Link
                            component="button"
                            variant="body2"
                            type="submit"
                            style={{ cursor: 'pointer', color: '#0A70C4' }}
                            onClick={() => values.contactMethod = "callIn" }
                            >
                            I don’t have access to either of these
                        </Link>
                    </Grid>
                    {(!isDisabled) && (
                    <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                        <Box ml={[1, 0]} mr={[1, 0]}>
                            <Button
                                text="Send Verification"
                                type="submit"
                                loading={isFetching}
                            />
                        </Box>
                    </Grid>
                    )}
                </Grid>            
            </Card>
        </FormikForm>)}
    </Formik>
  );
};