import React from 'react'
import { Box, Container } from '@material-ui/core'
import { Page } from 'components/generic'
import { Form } from 'components/form/TopUpForm'
import { useTopupForm } from 'hooks'

export default function TopUp() {
  const { submit, isFetching } = useTopupForm()
  return (
    <Page>
      <Container maxWidth='sm'>
        <Box mt={2} mb={4}>
          <Form
            onSubmit={submit}
            isFetching={isFetching}
            initialValues={{
              firstName: '',
              lastName: '',
              dateOfBirth: '',
              phoneNumber: '',
              email: '',
              sin: ''
            }}
          />
        </Box>
      </Container>
    </Page>
  )
}