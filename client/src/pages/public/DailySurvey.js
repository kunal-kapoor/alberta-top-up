import React, { useState, useEffect } from 'react';
import { useHistory, Prompt } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import Collapse from '@material-ui/core/Collapse';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import Hidden from '@material-ui/core/Hidden';
import moment from 'moment';

import { useToast, useModal } from '../../hooks';
import { useConfirmationOnExit, useAuth } from '../../hooks';
import { Page, AlertError } from '../../components/generic';
import { DailyForm } from '../../components/form/DailyForm';
import { Route, DailySymptoms, DailyCheckOthers, CardStatusReason, AirlinesAndAbbreviationValues, Questions, DailySurveySymptomsInitialValues, DailySurveyOthersInitialValues } from '../../constants';
import { AxiosPublic, AxiosPrivate } from '../../utils';
import { HouseholdMemberForm } from '../../components/form/HouseholdMemberForm';
import { TravellerSelector } from '../../components/form/TravellerSelector';
import { stringToDate } from '../../utils';
import IsolationPlanPassIcon from '../../assets/images/isolation-pass.svg';
import IsolationPlanFailIcon from '../../assets/images/isolation-fail-admin.svg'
import { CollectionNotice } from '../../components/generic/CollectionNotice';
import { WeeklySurveyDayFour } from '../../components/form/weeklySurveyDayFour';
import { WeeklySurveyDayThirteen } from '../../components/form/weeklySurveyDayThirteen';
import { useWeeklySurvey } from '../../hooks/weekly-survey';

export default () => {
  const history = useHistory();
  const { t, i18n } = useTranslation();
  const { token } = useParams();
  const { openToast } = useToast();
  const { openModal, closeModal } = useModal();
  const { submit, isFetching: weeklySurveyFetching, response } = useWeeklySurvey();


  /** Data States */
  const [householdData, setHouseholdData] = useState();
  const [currentSelectedTraveller, setCurrentTraveller] = useState();

  /** Flag States */
  const [isTravellerSelected, setTravellerSelected] = useState(false);
  const [isBirthdateValidated, setBirthdateValidated] = useState(false);
  const [isFlowComplete, setFlowComplete] = useState(false);
  const [isHouseholdRejected, setHouseholdRejected] = useState(false);
  const [rejectedNames, setRejectedNames] = useState(false);
  const [isFetching, setFetching] = useState(false);
  const [isGreyBannerExpanded, setGreyBannerExpanded] = useState(window.innerWidth > 750);
  const [isWeeklySurveyDay, setWeeklySurveyDay] = useState();
  const { state: { isAuthenticated } } = useAuth();
  const [validatedBirthDate, setValidatedBirthdate] = useState(null);
  const [arrivalDate, setArrivalDate] = useState(null);
  const isDateWithinNDays = (s, n) => stringToDate(s).isSame(moment().subtract(n,'d'), "day" );
  const isDateBetweenNDays = (arrivalDate, days) => moment().isBetween(stringToDate(arrivalDate), stringToDate(arrivalDate).add(days, 'days'), undefined, '[]');
  const [contactAgent, setContactAgent] = useState(false);
  const [disableDOBVerification, setDisableDOBVerification] = useState(false);
  const [isAnyTravellerBlockedAccessForDOB, setTravellerBlockedAccessForDOB] = useState(false);

  const isWithinNMinutes = (s, n) => s && moment.utc().isBefore(moment(s).utc(true).add(n,'minutes'));
  const isDOBVerificationAttemptMaxed = (dobVerificationAttempt, dobAttemptTime) => {
    const isMaxed = !isAuthenticated ? dobVerificationAttempt > 4 && isWithinNMinutes(dobAttemptTime, '5'): false;
    setTravellerBlockedAccessForDOB(prevState => prevState ? prevState : isMaxed);
    return isMaxed;
  }
  const [hasAnySymptoms, setHasAnySymptoms] = useState(false);
  const [isOtherQuestionsRequired, setOtherQuestionsRequired] = useState(false);
  const [isFlightInformationRequired, setFlightInformationRequired] = useState(false);
  const [preFilledFlightInformation, setPreFilledFlightInformation] = useState({});

  useEffect(() => {
    const lng = history.location.pathname.substring(1) || 'en';
    (async () => {
      await i18n.changeLanguage(lng);
      document.title = t("Alberta Border Pilot Daily Check-In");
      document.description = t("One member of each household must complete the pilot application");
      setTimeout(() => window.scrollTo(0, 0), 100);
    })();
  }, []);

  /** Initial data request for page load */
  useEffect(() => {
    (async() => {
      try {
        if (token) {
          const { travellers = [], travellerSurvey = null, abttRequired = false, flightInformationRequired = false, filledFlightInformation = {}} = await AxiosPublic.get(`/api/v2/rtop/traveller/reminder/${token}`).catch(err => 
          {
            if (err.response?.data.error) throw err.response?.data.error;
            throw err.response?.data;
           });
          setBirthdateValidated(isAuthenticated);
          setWeeklySurveyDay(travellerSurvey);
          travellers.length > 0 && setArrivalDate(travellers[0].arrivalDate);
          setOtherQuestionsRequired(abttRequired);
          setFlightInformationRequired(flightInformationRequired);
          setPreFilledFlightInformation(filledFlightInformation);
          setHouseholdData(travellers);
        }
      } catch(e) {
        openToast({ status: 'error', message: e?.message || t("Failed to get household data") });
        setContactAgent(true);
      }
    })()
  }, [token])
  
  /** Triggers scrolling to top of page after 3/13 test completion */
  useEffect(() => {
    if (response) {
      window.scrollTo(0, 0);
    }
  }, [response])


  /** Triggering the flow complete and traveler selector components */
  useEffect(() => {

    /** All Complete is true by default unless one or more travellers have the not_submitted status */
    let allComplete = true

    /** Sets default page state based on travellers statuses and number of travellers */
    if (householdData && householdData.length) {
      const redStatusNames = householdData.filter(d => d.status === 'red')
          .map(({firstName, lastName}) => `${firstName} ${lastName}`);

      if(redStatusNames.length > 1) {
        const last = redStatusNames.pop();
        setRejectedNames(`${redStatusNames.join(', ')} and ${last}`);
      } else if(redStatusNames.length) {
        setRejectedNames(redStatusNames[0]);
      }

      householdData.map((element) => {
        if (element.submissionStatus === 'not_submitted') {
          allComplete = false;
        }

        /** Triggers the rejected status for the household
         *  rejected trumps all, so one or more rejected travellers rejects the whole household 
         */
        if (element.status === 'red') {
          if (!element.cardStatusReason || element.cardStatusReason !== CardStatusReason.ABTRACETOGETHER_FLAG ) {
            setHasAnySymptoms(true);
          }
          setHouseholdRejected(true);
        }
        
      })

      /** Auto sets the selected traveller if there is only one in the household */
      if (householdData.length === 1) {
        const travellerStatus = householdData[0].submissionStatus

        /** Sets the flow to complete if the traveller has submitted the form
         * used for revisiting the page after submission
         * else auto sets the traveller as 'selected'
         */
        if (travellerStatus === 'submitted' || travellerStatus === 'rejected'){
          allComplete=true;
        } else {
          if( isDOBVerificationAttemptMaxed(householdData[0].dobVerificationAttempt, householdData[0].dobAttemptTime) )
          {
            setDisableDOBVerification(true);
          }
          setTravellerSelected(true);
          setCurrentTraveller(householdData[0]);
        }
      }
      /** Sets flow complete to false if there are no travellers in the household somehow or the request fails */
    } else {
      allComplete = false;
    }
    setFlowComplete(allComplete);
    setTravellerBlockedAccessForDOB(false);
  }, [householdData])

  useEffect(() => {
    /** if admin logouts or change in session */
    setBirthdateValidated(isAuthenticated);
  },[isAuthenticated]);

  /** Handler for selecting the currently actioned traveller */
  const handleTravellerSelect = (value) => {
    setCurrentTraveller(value);
    setTravellerSelected(true);
  }

  /** Handler for submitting the birthday validation form */
  const submitBirthdayValidation = async (values) => {
    try {
      const birthdateFormatted = moment(values.birthday, 'YYYY/MM/DD').format('YYYY-MM-DD');
      const birthdateValidation = await AxiosPublic.get(`/api/v2/rtop/traveller/${currentSelectedTraveller.confirmationNumber}/verify/${token}/${birthdateFormatted}`);
      
      if(birthdateValidation.verified) {
        setFetching(false);
        setBirthdateValidated(true);
        setValidatedBirthdate(birthdateFormatted);
      } else {
        openToast({ status: 'error', message: t("Incorrect Date of Birth") });
      }
    } catch(e) {
      openToast({ status: 'error', message: e.message || t("Failed to submit form") });
      setDisableDOBVerification(e?.response?.data?.error?.code === 'max_verification_attempts');
      setFetching(false);
    }
  }

  /** handler for submitting the daily symptom survey */
  const submitDailySurvey = async (values) => {
    const valuesCopy = { ...values };

    const symptoms = DailySymptoms
      .filter((symptom) => valuesCopy[symptom.value] === "Yes")
      .map((symptom) => symptom.label);
    const others = DailyCheckOthers
      .filter((other) => valuesCopy[other.value] === "No")
      .map((other) => other.text);

    if (valuesCopy.airline === AirlinesAndAbbreviationValues.Other) {
      valuesCopy.airline = valuesCopy.airlineDetails;
      delete valuesCopy.airlineDetails;
    }

    if(valuesCopy.noPreDepartureTest !== undefined) {
      valuesCopy.preDepartureTest = valuesCopy.noPreDepartureTest? 'No': 'Yes';
      delete valuesCopy.noPreDepartureTest;
    }

    const symptomsData = (
      <Box style={{ textAlign: "left" }}>
        {symptoms.length > 0 && (
          <>
            <Typography variant="body1">You are reporting the following symptoms:</Typography>
            <p>
              <ul>
                {symptoms.map((symptom) =>
                  <li>{symptom}</li>
                )}
              </ul>
            </p>
          </>
        )}
        {others.length > 0 && (
          <>
            <Typography variant="body1">You are reporting that:</Typography>
            <p>
              <ul>
                {others.map((other) =>
                  <li>{other}</li>
                )}
              </ul>
            </p>
          </>
        )}
        <Typography variant="body1">Is this correct?</Typography>
      </Box>
    )

    if(symptoms.length || others.length) {
      openModal({
        title: 'Daily Check-In Submission',
        description: symptomsData,
        negativeActionText: 'No',
        positiveActionText: 'Yes',
        disableOnClick: true,
        disabled: false,
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => {
          closeModal();
          handleModal(valuesCopy);
        },
      });
    } else {
      handleModal(valuesCopy);
    }
  }

  const handleModal = async(values) => {
    try {
      setFetching(true);

        const request = {
          answers: Object.entries(values).map(([question, answer]) => ({question, answer})),
        };
        const { travellers = [], flightInformationRequired = false, filledFlightInformation = {} } = isAuthenticated ? await AxiosPrivate.post(`/api/v2/rtop/traveller/${currentSelectedTraveller.confirmationNumber}/reminder/${token}`, request) : 
                            await AxiosPublic.post(`/api/v2/rtop/traveller/${currentSelectedTraveller.confirmationNumber}/reminder/${token}/${validatedBirthDate}`, request) ;
        setHouseholdData(travellers);
        setFlightInformationRequired(flightInformationRequired);
        setPreFilledFlightInformation(filledFlightInformation);
        resetFlow();

      setFetching(false);
    } catch(e) {
      openToast({ status: 'error', message: e.message || t("Failed to submit form") });
      setContactAgent(true);
      setFetching(false);
    }
  }

  /** Handler for resetting the view back to the traveler selection on submission completion */
  const resetFlow = () => { 
    setTravellerSelected(false);
    setCurrentTraveller(null);
    setBirthdateValidated(isAuthenticated);
  }

  /** Handler for submitting the weekly survey forms */
  const handleSurveySubmit = (values) => {
    const questions = Object.keys(values);
    const answers = Object.values(values);
    const potentialLostIncomeDaysIndex = questions.findIndex((e) => e == 'potentialLostIncomeDays');
    if (potentialLostIncomeDaysIndex && potentialLostIncomeDaysIndex >= 0) {
      const potentialLostIncomeDaysObject = {};
      Object.entries(answers[potentialLostIncomeDaysIndex]).forEach(([key, value]) => {
        if (value.checked) {
          potentialLostIncomeDaysObject[key] = value.count
        }
      });
      answers[potentialLostIncomeDaysIndex] = Object.keys(potentialLostIncomeDaysObject).length === 0 ? "" : JSON.stringify(potentialLostIncomeDaysObject);
    }
    let surveyDTO = questions
      .map((element, index) => ({question: element, answer: answers[index]}))
      .filter(element => element.answer);
    submit(token, isWeeklySurveyDay, surveyDTO)
  }

  useConfirmationOnExit();

  return (
    <Page>

      {/** Prompts the user before refreshing the page - specifically - the logo is clicked */}
      <Prompt message={(location) => {
        const isHeaderLogoClicked = location.pathname === Route.Root;
        return isHeaderLogoClicked ? t("Are you sure you want to leave?") : true
      }} />

      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          {
            (isWeeklySurveyDay === "DAY_4" || isWeeklySurveyDay === "DAY_13") && !response
            ? 
              <Box mb={2.5} component={Typography} variant="h1">
                {t("Alberta Border Pilot Daily Check-In")}
              </Box>
            :
              <>
                <Box mb={2.5} component={Typography} variant="h1">
                  {t("Alberta Border Pilot Daily Check-In")}
                </Box>
                <Typography variant="subtitle1">
                  {t("Fill out the information for each traveller every day.")}
                </Typography>
              </>
          }
          <Hidden smUp>
            <Box
              mt={0.25}
              component={Typography}
              variant="subtitle1"
              color="common.white"
              style={{ cursor: 'pointer', textDecoration: 'underline' }}
              onClick={() => setGreyBannerExpanded(prevState => !prevState)}
            >
              {!isGreyBannerExpanded ? t("Show More") : t("Show Less")}
            </Box>
          </Hidden>
        </Container>
      </Box>

      {/** Collection Notice */}
      <CollectionNotice/>

      {/** Confirmation Component */}
      {/** Shows either the weekly survey, or the regular landing page depending on flag */}
      {
        isWeeklySurveyDay === "DAY_4" && !response
        ? 
          <WeeklySurveyDayFour submit={handleSurveySubmit} isFetching={weeklySurveyFetching}/>
        :
        isWeeklySurveyDay === "DAY_13" && !response
        ?
          <WeeklySurveyDayThirteen submit={handleSurveySubmit} isFetching={weeklySurveyFetching}/>
        :
          <>
            <AlertError
              message={
                <div>
                  <div style={{fontWeight: 600}}>Due to the increased threat of COVID-19 variants, protocols have changed effective immediately.</div>
                  <ul>
                    <li>
                      Participants are now required to remain in quarantine until receipt of the second negative test result taken on day 7 or 8 counting arrival day as day 1. 
                    </li>
                    <li>
                      Travelers currently in the program who have not yet received the results of their second test are required to return to quarantine immediately until they receive such.
                    </li>
                    <li>
                      Participants are not allowed to return to school, out of school care, daycare or post secondary institutions until 14 days after arrival.
                    </li>
                    <li>
                      Participants are not allowed to return to work outside of their residence until 14 days after arrival. 
                    </li>
                  </ul>
                </div>
              }
            />
            {window._env_.REACT_APP_DAILY_CHECKIN_ALERT && <AlertError
              message={
                <>
                  {t(window._env_.REACT_APP_DAILY_CHECKIN_ALERT)}
                  <br></br><br></br>
                  {t('Please continue to complete your daily symptom checkins.')}
                </>
              }
            />}

            {/** Reminder to book appointment to test on Day-7 */}
            {isDateWithinNDays(arrivalDate, 6) && (
              <Container maxWidth="sm" style={{marginTop: '8px'}}>
              <Alert variant="filled" severity="warning">
                {t("Remember to book your testing appointment")}
              </Alert>
              </Container>
            )}

            {/** Guidelines to call monitoring team */}
            {contactAgent && (
              <Container maxWidth="sm" style={{marginTop: '8px'}}>
              <Alert variant="filled" severity="warning">
                {t("Please contact the monitoring team")}
              </Alert>
              </Container>
            )}

            {/** Daily Check In notice for Day 3-7 */}
            {isDateBetweenNDays(moment(arrivalDate).add('2','days'), 7) && (
              <Container maxWidth="sm" style={{marginTop: '8px'}}>
                <Alert severity="info">
                  Participants are required to remain in quarantine until in receipt of their negative test results from the day 7 or 8 test.  Participants can leave quarantine for the sole purpose of attending their scheduled test at a participating Shoppers Drug Mart on day 7 or 8.  You should schedule this second test as soon as you receive a negative result from the test taken upon entry.  If you develop any symptoms, report them immediately and you will receive instructions on how to proceed to testing at an AHS facility.
                  <br/><br/>
                  For further details, please visit the program website: <Link style={{ cursor: 'pointer', color: '#0A70C4' }} href="https://www.alberta.ca/international-border-pilot-project.aspx#toc-3" target="_blank">https://www.alberta.ca/international-border-pilot-project.aspx#toc-3</Link>
                  <br/><br/>
                  For participating pharmacy locations for the second test, please visit the Shoppers Drug Mart website: <Link style={{ cursor: 'pointer', color: '#0A70C4' }} href="https://www1.shoppersdrugmart.ca/en/health-and-pharmacy/covid-19" target="_blank">https://www1.shoppersdrugmart.ca/en/health-and-pharmacy/covid-19</Link>
                  {/* <Link style={{ cursor: 'pointer', color: '#0A70C4' }} href="https://www1.shoppersdrugmart.ca/en/health-and-pharmacy/covid-19" target="_blank">Don’t forget to book your follow up test, when you receive your negative result from test one.</Link> */}
                </Alert>
              </Container>
            )}

            {!isTravellerSelected && isAnyTravellerBlockedAccessForDOB && (
              <Container maxWidth="sm" style={{marginTop: '8px'}}>
                <Alert variant="outlined" severity="warning" style={{ color: "black" }}>
                  {t("You have exceeded the number verification attempts at entering the correct date of birth. Please try again after 5 minutes.")}
                </Alert>
              </Container>
            )}


            {/** Confirmation Component */}
            {
              isFlowComplete && (
                  <Container maxWidth="sm">
                    <Box mt={4} mb={4}>
                      <Box p={2} border={2} borderRadius={10} borderColor={isHouseholdRejected ? "red" :"#16c92e"}>
                        <Grid container spacing={4} align="center">
                          <Grid item xs={12} sm={3}>
                            <Box p={[0, 0]} border={1} borderRadius={4} borderColor="white">
                              <img
                                src={isHouseholdRejected ? IsolationPlanFailIcon : IsolationPlanPassIcon}
                                height={96}
                                alt={t("Status")}
                              />
                              <Box mt={2} component={Typography} variant="caption" color="text.primary" display="block">
                                {t("Status")}
                              </Box>
                            </Box>
                          </Grid>
                          <Grid item xs={12} sm={9} alignItems="center" container>
                            <div> {/** TODO: Update copy for success / red statuses. add conditional for isRejected */}
                              <Box mb={2} component={Typography} variant="subtitle2" color="text.secondary">
                                {!isHouseholdRejected && "Thank you for submitting your symptoms."}

                                {isHouseholdRejected && (
                                  <>
                                  {hasAnySymptoms? (
                                    <span>
                                      {rejectedNames} must immediately isolate. Isolation is required for 10 days or until symptoms resolve whichever is longer or until you receive a negative COVID-19 test result. <a target="_blank" href="https://www.albertahealthservices.ca/topics/Page17058.aspx">Book a COVID-19 test.</a>
                                    </span>
                                  ): isOtherQuestionsRequired? (
                                    <span>
                                      Downloading, enrolling in, and ensuring ABTraceTogether is on your phone during the pilot is a requirement of the border pilot program. As you have responded no to the question about ABTT, you are now in contravention of the pilot requirements and you need to quarantine immediately. Our support team will be following up with you.
                                    </span>
                                  ): ''}
                                  </>
                                )}
                              </Box>
                            </div>
                          </Grid>
                        </Grid>
                      </Box>
                    </Box>
                  </Container>)
      }

            {/** Traveller Selector */}
            {
              !isTravellerSelected && householdData
                ?
                  <TravellerSelector options={householdData} handleSelect={handleTravellerSelect} handleDOBAttempts={isDOBVerificationAttemptMaxed} />
                :
                  null
            }

            {/** Birthdate confirmation */}
            {
              !isBirthdateValidated && isTravellerSelected
                ?
                  <HouseholdMemberForm
                    onSubmit={submitBirthdayValidation}
                    isFetching={isFetching}
                    disabled={disableDOBVerification}
                    initialValues={{'birthday': ''}}
                  />
                :
                  null
            }

            {/** Daily survey */}
            {
              isTravellerSelected && isBirthdateValidated && !isFlowComplete
                ?
                  <Container maxWidth="sm">
                    <Box mt={2} mb={4}>
                      <DailyForm
                        onSubmit={submitDailySurvey}
                        isFetching={isFetching}
                        isAuthenticated={isAuthenticated}
                        confirmationNumber={currentSelectedTraveller.confirmationNumber}
                        isOtherQuestionsRequired={isOtherQuestionsRequired}
                        flightInformationRequired={isFlightInformationRequired}
                        preFilledFlightInformation={preFilledFlightInformation}
                      />
                    </Box>
                  </Container>
                :
                  null 
            }
          </>
      }
    </Page>
  );
};
