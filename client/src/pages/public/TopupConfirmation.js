import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { Prompt, Redirect, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import IsolationPlanPassIcon from '../../assets/images/isolation-pass.svg';
import IsolationPlanFailIcon from '../../assets/images/isolation-fail-traveller.svg';

import { Route } from '../../constants';
import { useConfirmationOnExit } from '../../hooks';

import { Page } from '../../components/generic';

export default () => {
  const { state } = useLocation();
  const { id } = state || {};
  const { t } = useTranslation();

  if (!state) return <Redirect to={Route.Root} />;
  useConfirmationOnExit();

  return (
    <Page>

      {/** Prompts the user before navigating backwards or forwards */}
      <Prompt message={t("Are you sure you want to leave?")} />

      {/** Orange Banner */}
      <Box pt={[1.5, 3]} pb={[1.5, 3]} bgcolor="warning.main">
        <Container maxWidth="md">
          <Box display="flex" alignItems="center">
            <InfoOutlinedIcon />
            <Box ml={1.5}>
              <Typography variant="subtitle2" color="textPrimary">
                {t("Do not close this page.") + " " + t("You may be asked to present your confirmation below.")}
              </Typography>
            </Box>
          </Box>
        </Container>
      </Box>

      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Box mb={2.5} component={Typography} variant="h1">
            {t("Thank You.")}
          </Box>
          <Typography variant="subtitle1">
            {t("Your form has been submitted.")}
          </Typography>
        </Container>
      </Box>

      {/** Message */}
      <Container maxWidth="sm">
        <Box mt={4} mb={4}>
          <Box p={2} border={1} borderRadius={4} borderColor="divider">
            <Grid container spacing={4} align="center">
              <Grid item xs={12} sm={6} alignItems="center" container>
                <div>
                  <Box mb={2} component={Typography} variant="subtitle2" color="text.secondary">
                    {t("Proceed to the provincial check point, if available at your location, where you may be asked to confirm how you will comply with the provincial order to self isolate.")}
                  </Box>
                  <Typography variant="body2" color="textPrimary">
                    {t("You may be asked to present your confirmation below.")}
                  </Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Box p={[2.5, 4]} border={1} borderRadius={4} borderColor="divider">
                  <Typography variant="overline" color="textPrimary" paragraph>
                    {t("Confirmation Number:")}
                  </Typography>
                  <Typography id="confirmationNumber" variant="h2" color="textSecondary">
                    {id}
                  </Typography>
                  <img
                    src={IsolationPlanPassIcon}
                    height={96}
                    alt={t("Isolation Plan Status")}
                  />
                  <Box mt={2} component={Typography} variant="caption" color="text.primary" display="block">
                    {t("Isolation Plan Status")}
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </Page>
  );
};
