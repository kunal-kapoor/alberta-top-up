import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FastField, useFormikContext } from 'formik';
import { useTranslation } from "react-i18next";

import { Card } from '../../generic';
import { RenderTextField, RenderDateField } from '../../fields';
import { ContactOptions } from '../../../constants';

export const ArrivalTestingPrimaryContact = ({ isDisabled, contactMethod, setFieldValue, canEdit }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  return (
    <Card title={t('Primary Contact for your travelling party')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="firstName"
            label="First name* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="lastName"
            label="Last name* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="dateOfBirth"
            label="Date of birth* (YYYY/MM/DD)"
            placeholder="Required"
            component={RenderDateField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="phoneNumber"
            label="Phone Number* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled || (values.contactMethod === ContactOptions[1].value)}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="email"
            label="Email* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled || (values.contactMethod === ContactOptions[0].value)}
          />
        </Grid>
        {
          (values.contactMethod !== ContactOptions[0].value && canEdit)
            &&
            (<Grid item xs={12}>
              <FastField
                name="confirmEmail"
                label="Confirm Email* (primary contact)"
                placeholder="Required"
                autoComplete="off"
                onPaste={(event) => event.preventDefault()}
                component={RenderTextField}
                disabled={isDisabled || (contactMethod === ContactOptions[0].value)}
              />
            </Grid>)
        }
      </Grid>
    </Card>
  );
};