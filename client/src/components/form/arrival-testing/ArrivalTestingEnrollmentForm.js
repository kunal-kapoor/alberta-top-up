import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { ArrivalTestingEnrollmentFormSchema } from '../../../constants';

import { Button, FocusError } from '../../generic';
import { ArrivalTestingPrimaryContact } from './ArrivalTestingPrimaryContact';
import { ArrivalTestingTravelInformation } from './ArrivalTestingTravelInformation';
import { ArrivalTestingArrivalInformation } from './ArrivalTestingArrivalInformation';
import { ArrivalTestingIsolationQuestionnaire } from './ArrivalTestingIsolationQuestionnaire';

export const ArrivalTestingEnrollmentForm = ({ onSubmit, initialValues, isFetching, isDisabled, canEdit, adminMode, cancelHandler }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={ArrivalTestingEnrollmentFormSchema}
      enableReinitialize
      onSubmit={onSubmit}
    >
      {({setFieldValue}) => (
        <FormikForm>
            <FocusError />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <ArrivalTestingPrimaryContact setFieldValue={setFieldValue} isDisabled={isDisabled} canEdit={canEdit} />
              </Grid>
              <Grid item xs={12}>
                <ArrivalTestingArrivalInformation isDisabled={isDisabled} setFieldValue={setFieldValue} />
              </Grid>
              <Grid item xs={12}>
                <ArrivalTestingTravelInformation setFieldValue={setFieldValue} isDisabled={isDisabled} />
              </Grid>
              <Grid item xs={12}>
                <ArrivalTestingIsolationQuestionnaire isDisabled={isDisabled} adminMode={adminMode}/>
              </Grid>
              {(!isDisabled) && (
                adminMode
                  ?
                  <Grid container xs={12} sm={12} justify="space-between" style={{ margin: '16px 8px 16px 8px'}}>
                    <Grid item xs={12} sm={5}>
                      <Button
                        text="Cancel"
                        variant="outlined"
                        onClick={cancelHandler}
                        loading={isFetching}
                      />
                    </Grid>
                    <Grid item xs={12} sm={5}>
                      <Button
                        text="Submit"
                        type="submit"
                        variant="contained"
                        loading={isFetching}
                      />
                    </Grid>
                  </Grid>
                  :
                  <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                    <Box ml={[1, 0]} mr={[1, 0]}>
                      <Button
                        text="Continue"
                        type="submit"
                        loading={isFetching}
                      />
                    </Box>
                  </Grid>  
              )}
            </Grid>
          </FormikForm>
        )}
      </Formik>
    );
};