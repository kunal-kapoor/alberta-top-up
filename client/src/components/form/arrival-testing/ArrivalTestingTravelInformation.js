import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FastField, useFormikContext, FieldArray } from 'formik';
import { useTranslation } from "react-i18next";

import { 
  Questions, 
  Question, 
} from '../../../constants';

import { Card } from '../../generic';
import { RenderTextField, RenderDateField, RenderRadioGroup, RenderDynamicField} from '../../fields';

export const ArrivalTestingTravelInformation = ({ isDisabled, setFieldValue }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  return (
    <Card title={t('Travel Information')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="hasAdditionalTravellers"
            label="Are you travelling with additional members from your household?*"
            component={RenderRadioGroup}
            disabled={isDisabled}
            options={Questions}
          />
        </Grid>
        {
          values.hasAdditionalTravellers === Question.Yes && 
            <Grid item xs={12}>
              <FieldArray name="additionalTravellers">
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <FastField
                      name="additionalTravellers"
                      selectName="numberOfAdditionalTravellers"
                      component={RenderDynamicField}
                      label="Number of additional travellers?*"
                      disabled={isDisabled}
                      itemMin={1}
                      itemMax={10}
                      itemFields={[
                        { name: 'firstName', label: "First name*", placeholder: "Required", component: RenderTextField },
                        { name: 'lastName', label: "Last name*", placeholder: "Required", component: RenderTextField },
                        { name: 'dateOfBirth', label: "Date of birth* (YYYY/MM/DD)", placeholder: "Required", component: RenderDateField },
                      ]}
                      itemsWrapper={(fields) => (
                        <Card title={t("Additional Traveller Information")} isTitleSmall={true}>
                          {!isDisabled && (
                            <Typography variant="body2" paragraph>
                              {t('For each traveller, please list their first name, last name and date of birth')}
                            </Typography>
                          )}
                          {fields}
                        </Card>
                      )}
                    />
                  </Grid>
                </Grid>
              </FieldArray>
            </Grid>
        }

      </Grid>
    </Card>
  );
};