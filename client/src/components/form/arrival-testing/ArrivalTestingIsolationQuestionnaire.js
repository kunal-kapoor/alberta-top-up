import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Field, useFormikContext } from 'formik';
import { useTranslation } from "react-i18next";

import { Vehicles, Places, Provinces, Place, Vehicle, Province, AlbertaCities } from '../../../constants';

import { Card } from '../../generic';
import { RenderTextField, RenderSelectField, RenderAutocompleteField } from '../../fields';
import { IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { withStyles } from '@material-ui/core/styles';

export const ArrivalTestingIsolationQuestionnaire = ({ isDisabled, isEditing, canEdit, setIsEditing, adminMode }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  const StyledEditButton = withStyles((theme) => ({
    root: {
      position: 'absolute',
      left: '0',
      top: '-8px',
      color: theme.palette.common.white,
      '&:hover': {
        color: theme.palette.common.lightGray,
      },  
    }
  }))(IconButton);
  
  const EditButton = (<StyledEditButton
      centerRipple={false}
      onClick={() => {
        setIsEditing(true);
      }}
    >
      <EditIcon />
    </StyledEditButton>);
  
  const Title = canEdit? (<span style={{display: 'flex', justifyContent: 'space-between'}}>
    <span>{t("Isolation Questionnaire")}</span><span style={{height: '24px', width: '24px', position: 'relative'}}>{EditButton}</span>
  </span>): t("Isolation Questionnaire");

  return (
    <Card title={Title}>
      <Grid container spacing={2}>

        {!isDisabled && (
          !adminMode
            &&
          <Grid item xs={12}>
            <Box mb={1}>
              <Typography variant="body2">
                {t("Quarantine and isolation mean staying home, not attending work, school, social events or any other public gatherings. You are also prohibited from going outside in your neighborhood and from taking public transportation like buses")}
              </Typography>
            </Box>
            <Typography component="div" variant="body2">
              <ul>
                <li>
                  {t("When you or members of your household do not have symptoms, you are in quarantine and must watch for symptoms (cough, fever, shortness of breath, runny nose or sore throat).")}
                </li>
                <li>
                  {t("If you or members of your household develop COVID or COVID-like symptoms, you must stay in isolation and avoid close contact with people in your household who are ill and those who are well, especially seniors and people with chronic conditions or compromised immune systems.")}
                </li>
              </ul>
            </Typography>
          </Grid>
        )}

            <Grid item xs={12}>
              <Typography varient="body1">
                {t('What is the address where you will be quarantining or isolating?')}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12}>
              <Field
                name="address"
                label="Address*"
                component={RenderTextField}
                placeholder="Required"
                disabled={isDisabled && !isEditing}
                multiline
                rows={3}
              />
            </Grid>
            <Grid container spacing={2} item>
              <Grid item xs={12} sm={6}>
                <Field
                  name="cityOrTown"
                  label="City or Town*"
                  component={RenderAutocompleteField}
                  placeholder="Required"
                  options={AlbertaCities}
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field
                name="provinceTerritory"
                label="Province/Territory*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Provinces}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field
                name="postalCode"
                label="Postal Code"
                component={RenderTextField}
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            {values.provinceTerritory === Province.Other && (
            <Grid item xs={12}>
              <Field
                name="provinceTerritoryDetails"
                label="If Other, Please specify the Province/Territory*"
                component={RenderTextField}
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            )}
            <Grid item xs={12}>
              <Field
                name="quarantineLocationPhoneNumber"
                label="Phone Number at Isolation Location*"
                component={RenderTextField}
                placeholder="Required"
                disabled={isDisabled && !isEditing}
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="typeOfPlace"
                label="What type of a place is it?*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Places}
              />
            </Grid>
            {values.typeOfPlace === Place.Other && (
              <Grid item xs={12}>
                <Field
                  name="typeOfPlaceDetails"
                  label="Please provide further details*"
                  placeholder="Required"
                  component={RenderTextField}
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            )}
            <Grid item xs={12}>
              <Field
                name="howToGetToPlace"
                label="How will you get to this location?*"
                component={RenderSelectField}
                disabled={isDisabled && !isEditing}
                options={Vehicles}
              />
            </Grid>
            {values.howToGetToPlace === Vehicle.Other && (
              <Grid item xs={12}>
                <Field
                  name="howToGetToPlaceDetails"
                  label="Please provide further details*"
                  placeholder="Required"
                  component={RenderTextField}
                  disabled={isDisabled && !isEditing}
                />
              </Grid>
            )}

      </Grid>
    </Card>
  );
};
