import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { FastField } from 'formik';
import { useTranslation } from "react-i18next";
import moment from 'moment';

import { AirportOrBorderCrossings, Countries } from '../../constants';

import { Card } from '../generic';
import { RenderDateField, RenderDynamicField, RenderSelectField, RenderTextField } from '../fields';

export const ArrivalInformation = ({ isDisabled }) => {
  const { t } = useTranslation();
  
  return (
    <Card title={t("Arrival Information")}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="nameOfAirportOrBorderCrossing"
            label="Name of Airport or Border Crossing*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={AirportOrBorderCrossings}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="arrivalDate"
            label="Arrival Date* (YYYY/MM/DD)"
            component={RenderDateField}
            placeholder="Required"
            disabled={isDisabled}
            disableFuture={false}
            minDate={moment().subtract(5,'day')}
          />
        </Grid>

        <Grid item xs={12}>
          <Box mt={1} mb={-0.75} component={Typography} fontSize="20px" variant="subtitle2" color="text.secondary">
            {t("Arriving from")}
          </Box>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="arrivalCityOrTown"
            label="City or Town*"
            component={RenderTextField}
            placeholder="Required"
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="arrivalCountry"
            label="Country*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={Countries}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="additionalCitiesAndCountries"
            selectName="numberOfAdditionalCitiesAndCountries"
            component={RenderDynamicField}
            label="How many cities and countries did you and members of your household visit in the last 14 days?"
            disabled={isDisabled}
            itemMin={1}
            itemMax={10}
            itemFields={[
              { name: 'cityOrTown', label: "City or Town*", placeholder: "Required", component: RenderTextField },
              { name: 'country', label: "Country*", options: Countries, component: RenderSelectField },
            ]}
            itemsWrapper={(fields) => (
              <Card title={t("Locations Visited In Past 14 Days")} isTitleSmall={true}>
                {!isDisabled && (
                  <Typography variant="body2" paragraph>
                    {t('Name of every city and country that you and members of your household have visited for longer than 12 hours in the last 14 days')}
                  </Typography>
                )}
                {fields}
              </Card>
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Box mt={1}>
            <Typography component="p" style={{ opacity: 0.5 }} variant="caption">
              {t("Countries listed are taken from the ISO 3166 Standard")}
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};