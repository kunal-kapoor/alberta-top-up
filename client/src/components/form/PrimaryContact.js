import React from 'react'
import Grid from '@material-ui/core/Grid'
import { FastField } from 'formik'
import { useTranslation } from 'react-i18next'

import { Card } from 'components/generic'
import { RenderTextField, RenderDateField } from 'components/fields'

export const PrimaryContact = ({ isDisabled }) => {
  const { t } = useTranslation();
  
  return (
    <Card title={t('Primary Contact')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="firstName"
            label="First name*"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="lastName"
            label="Last name*"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="dateOfBirth"
            label="Date of birth* (YYYY/MM/DD)"
            placeholder="Required"
            component={RenderDateField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="phoneNumber"
            label="Phone Number*"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="email"
            label="Email"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
      </Grid>
    </Card>
  );
};