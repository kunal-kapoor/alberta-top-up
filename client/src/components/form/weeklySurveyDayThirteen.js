import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Formik, Form, FastField } from 'formik';
import { useTranslation } from "react-i18next";


import { FocusError, Card, Button } from '../generic';
import { RenderRadioGroup, RenderSelectField, RenderDynamicCheckboxGroup } from '../fields';
import { 
  Questions, 
  TeamHelpful, 
  PotentialLostIncomeDays, 
  DayThirteenSurveyValidationSchema,
  CostPaidPerPerson, 
  DifficultyOptions
} from '../../constants';
import Alert from '@material-ui/lab/Alert';

export const WeeklySurveyDayThirteen = ({ submit, isFetching }) => {
  const { t } = useTranslation();

  return (
    <Container maxWidth="sm">
      <Alert severity="info">
      Prior to completing today’s daily check-in report, we would like to ask you a few questions about your overall experience with the program. The survey should take you 3 - 5 minutes and this information will be used to improve the program. Once you have completed the survey below, you will be taken to a new page to complete your daily check-in report. We value your time and thank you for sharing your feedback.
      </Alert>
      <Box mt={2} mb={4}>
        <Formik
          validationSchema={DayThirteenSurveyValidationSchema}
          initialValues={{
            difficultyCompletingCheckin: '',
            difficultyDlingABTrace: '',
            difficultyBookingSecondTest: '',
            difficultyGettingSecondTest: '',
            acceptableSecondTimeLength: '',
            potentialTravel: '',
            potentialLostIncome: '',
            potentialLostIncomeDays: {}, // Optional,
            teamHelpful: '',
            wouldUseAgain: '',
            wouldCoverCost: '',
            costPerPerson: '',
          }}
          onSubmit={submit}
        >
          {({values, setFieldValue}) => (
            <Form>
              <FocusError/>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Card title={t('Feedback questionnaire')}>
                    <Grid container spacing={2}>
                      <Grid item xs={12}>
                        <Box my={1}>
                          <Box m={1}>
                            <Box my={2}>
                              <FastField
                                name="difficultyCompletingCheckin"
                                label="Have you had difficulties submitting your daily check-in report about symptoms?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyDlingABTrace"
                                label="Did you have difficulties downloading the AB TraceTogether app?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyBookingSecondTest"
                                label="Did you have difficulties booking your second COVID-19 test (swab)?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyGettingSecondTest"
                                label="Did you have difficulties completing your second COVID-19 test (swab)?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="acceptableSecondTimeLength"
                                label="Did you feel the length of time it took to get your second COVID-19 test result was acceptable?"
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>

                            <Box my={2}>
                              <FastField
                                name="teamHelpful"
                                label="If you contacted the eHealth team for support, did you find this helpful?"
                                component={RenderRadioGroup}
                                options={TeamHelpful}
                              />
                            </Box>

                            <Box my={2}>
                              <FastField
                                name="potentialTravel"
                                label="If this program were not available and you were required to quarantine for the full 14 days, would you have traveled?"
                                onChange={(event) => {
                                  setFieldValue('potentialTravel', event.target.value)
                                  setFieldValue('potentialLostIncome', '')
                                  setFieldValue('potentialLostIncomeDays', {})
                                }}
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>
                            {
                              values.potentialTravel === Questions[0].value && (
                                <Box my={2}>
                                  <FastField
                                    name="potentialLostIncome"
                                    label="Would you have missed work?"
                                    onChange={(event) => {
                                      setFieldValue('potentialLostIncome', event.target.value)
                                      setFieldValue('potentialLostIncomeDays', {})
                                    }}
                                    component={RenderRadioGroup}
                                    options={Questions}
                                  />
                                </Box>
                              )
                            }
                            {
                              values.potentialLostIncome === Questions[0].value
                              ?
                                <Box my={2}>
                                  <FastField
                                    name="potentialLostIncomeDays"
                                    label="Would you have had to use: (check all that apply)"
                                    component={RenderDynamicCheckboxGroup}
                                    options={[
                                      { name: 'vacation', label: "Vacation days", component: RenderSelectField, options: PotentialLostIncomeDays, fieldValue: 'count' },
                                      { name: 'sick', label: "Sick days", component: RenderSelectField, options: PotentialLostIncomeDays, fieldValue: 'count' },
                                      { name: 'withoutPay', label: "Leave without pay", component: RenderSelectField, options: PotentialLostIncomeDays, fieldValue: 'count' },
                                      { name: 'other', label: "Other", component: RenderSelectField, options: PotentialLostIncomeDays, fieldValue: 'count' }
                                    ]}
                                  />
                                </Box>
                              : 
                                null
                            }
                            
                            <Box my={2}>
                              <FastField
                                name="wouldUseAgain"
                                label="For future travel, would you use this program again?"
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="wouldCoverCost"
                                label="Would you be willing to use this program if there were fees to cover the costs for testing or program administration?"
                                component={RenderRadioGroup}
                                onChange={(event) => {
                                  setFieldValue('wouldCoverCost', event.target.value)
                                  setFieldValue('costPerPerson', '')
                                }}
                                options={Questions}
                              />
                            </Box>
                            {values.wouldCoverCost === Questions[0].value && (
                              <Box my={2}>
                                <FastField
                                  name="costPerPerson"
                                  label="How much would you pay per person?"
                                  component={RenderSelectField}
                                  options={CostPaidPerPerson}
                                />
                              </Box>
                            )}
                          </Box>
                        </Box>
                      </Grid>

                      <Grid item xs={12} sm={12}>
                        <Button
                          text="Submit Survey"
                          disabled={isFetching}
                          type="submit"
                        />
                      </Grid>
                    </Grid>
                  </Card>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Box>
    </Container>
  )
}