import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FastField, Field, useFormikContext } from 'formik';
import { useTranslation } from "react-i18next";

import { Card } from '../../generic';
import { RenderTextField, RenderDateField, RenderSelectField, RenderRadioGroup } from '../../fields';
import { Gender, CitizenshipStatus, ExemptionType, ExemptOccupation, ExemptOccupationDetails, ContactOptions, Questions, VaccinationTimeframe, VaccinationDoseCount } from '../../../constants';

export const PrimaryContact = ({ isDisabled, contactMethod, setFieldValue, canEdit }) => {
  const { values } = useFormikContext();
  const { t } = useTranslation();

  const onExemptionTypeChange = event => {
    setFieldValue('exemptionType', event.target.value);
    setFieldValue('exemptOccupationDetails', '');
    setFieldValue('exemptOccupation', '');
    setFieldValue('noPreDepartureTest', false);
    setFieldValue('preDepartureTestDate', '');
    setFieldValue('preDepartureTestCountry', '');
    setFieldValue('airline', '');
    setFieldValue('airlineDetails', '');
    setFieldValue('flightNumber', '');
    setFieldValue('seatNumber', '');
  };

  const onOccupationChange = event => {
    setFieldValue('exemptOccupation', event.target.value);
    setFieldValue('exemptOccupationDetails', '');
  };
  
  const onVaccinationChange = event => {
    setFieldValue('preDepartureVaccination', event.target.value);
    setFieldValue('timeSinceVaccination', '');
    setFieldValue('dosesOfVaccine', '');
  }

  return (
    <Card title={t('Primary Contact for your travelling party')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="firstName"
            label="First name* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="lastName"
            label="Last name* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="dateOfBirth"
            label="Date of birth* (YYYY/MM/DD)"
            placeholder="Required"
            component={RenderDateField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="phoneNumber"
            label="Phone Number* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled || (values.contactMethod === ContactOptions[1].value)}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="email"
            label="Email* (primary contact)"
            placeholder="Required"
            component={RenderTextField}
            disabled={isDisabled || (values.contactMethod === ContactOptions[0].value)}
          />
        </Grid>
        {
            (values.contactMethod !== ContactOptions[0].value && canEdit)
              &&
              (<Grid item xs={12}>
                <FastField
                  name="confirmEmail"
                  label="Confirm Email* (primary contact)"
                  placeholder="Required"
                  autoComplete="off"
                  onPaste={(event) => event.preventDefault()}
                  component={RenderTextField}
                  disabled={isDisabled || (contactMethod === ContactOptions[0].value)}
                />
              </Grid>)
          }
        <Grid item xs={12}>
          <FastField
            name="gender"
            label="Which of the following best describes your gender identity?*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={Gender}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
              name="citizenshipStatus"
              label="What is your Canadian citizenship status?*"
              component={RenderSelectField}
              disabled={isDisabled}
              options={CitizenshipStatus}
            />
        </Grid>
        <Grid item xs={12}>
          <FastField
              name="exemptionType"
              onChange={onExemptionTypeChange}
              label="Do you belong to to any category of person who is exempt from the Federal Quarantine requirements?*"
              component={RenderRadioGroup}
              disabled={isDisabled}
              options={ExemptionType}
          />
        </Grid>
        {values.exemptionType === ExemptionType[0].value && (
          <Grid item xs={12}>
            <FastField
              name="exemptOccupation"
              onChange={onOccupationChange}
              label="Reason for exempt*"
              component={RenderSelectField}
              disabled={isDisabled}
              options={ExemptOccupation}
          />
          </Grid>
        )}
        {values.exemptionType === ExemptionType[0].value && values.exemptOccupation in ExemptOccupationDetails && !!ExemptOccupationDetails[values.exemptOccupation].length && (
          <Grid item xs={12}>
            <Field
              name="exemptOccupationDetails"
              label="Please provide further details about your occupation*"
              component={RenderSelectField}
              disabled={isDisabled}
              options={ExemptOccupationDetails[values.exemptOccupation]}
          />
          </Grid>
        )}

        <Grid item xs={12}>
          <FastField
              name="preDepartureVaccination"
              label="Did you receive a vaccine for COVID-19 before you entered Canada?*"
              onChange={onVaccinationChange}
              component={RenderRadioGroup}
              disabled={isDisabled}
              options={Questions}
          />
        </Grid>
        {values.preDepartureVaccination === Questions[0].value && (
          <>
            <Grid item xs={12}>
              <FastField
                name="timeSinceVaccination"
                label="Approximately how many weeks ago was your first injection of vaccine?*"
                component={RenderSelectField}
                disabled={isDisabled}
                options={VaccinationTimeframe}
            />
            </Grid>
            <Grid item xs={12}>
              <FastField
                name="dosesOfVaccine"
                label="How many doses of vaccine did you receive before you entered Canada?*"
                component={RenderSelectField}
                disabled={isDisabled}
                options={VaccinationDoseCount}
              />
            </Grid>
          </>
        )}
      </Grid>
    </Card>
  );
};