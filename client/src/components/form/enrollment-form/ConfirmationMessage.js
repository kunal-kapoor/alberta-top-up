import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

import IsolationPlanPassIcon from '../../../assets/images/isolation-pass.svg';
import IsolationPlanFailIcon from '../../../assets/images/isolation-fail-traveller.svg';
import { useTranslation } from 'react-i18next';

export const ConfirmationMessage = ({travellers, viableIsolationPlan}) => {
  const { t } = useTranslation();

  return (
    <Container maxWidth="sm">
      <Box mt={4} mb={4}>
        <Box p={2} border={1} borderRadius={4} borderColor="divider">
          <Grid container spacing={4} align="center">
            <Grid item xs={12} sm={6} alignItems="center" container>
              <div>
                <Box mb={2} component={Typography} variant="subtitle2" color="text.secondary">
                  {t("Your application will be verified upon entry into Canada. Please make note of your confirmation number(s).")}
                </Box>
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Box p={[2.5, 4]} border={1} borderRadius={4} borderColor="divider">
                <img
                  src={viableIsolationPlan ? IsolationPlanPassIcon : IsolationPlanFailIcon}
                  height={96}
                  alt={t("Isolation Plan Status")}
                />
                <Box mt={2} component={Typography} variant="caption" color="text.primary" display="block">
                  {t("Isolation Plan Status")}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Grid container style={{marginTop: '32px'}}>
          {(travellers || []).map(t => (
            <Grid key={t.confirmationNumber} item xs={12} sm={6}>
              <Box p={[2.5, 4]} border={1} borderRadius={4} borderColor="divider">
                <Typography variant="h6" color="textSecondary">
                    {t.firstName} {t.lastName}
                </Typography>

                <Typography variant="h2" color="textSecondary">
                    {t.confirmationNumber}
                </Typography>
              </Box>

            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  )
}