import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FastField, useFormikContext, Field, FieldArray } from 'formik';
import { useTranslation } from "react-i18next";
import moment from 'moment';

import { 
  Questions, 
  Question, 
  Gender, 
  CitizenshipStatus, 
  ExemptionType, 
  ExemptOccupation, 
  ExemptOccupationDetails, 
  Countries, 
  TravelReason, 
  StayDuration,
  VaccinationDoseCount,
  VaccinationTimeframe,
  CountriesWithoutCanada
} from '../../../constants';

import { Card } from '../../generic';
import { isAirport } from '../../../utils/rtop-form';
import { RenderTextField, RenderDateField, RenderRadioGroup, RenderDynamicField, RenderSelectField, RenderCheckbox } from '../../fields';

export const TravelInformation = ({ isDisabled, setFieldValue }) => {
  const { values, setFieldTouched } = useFormikContext();
  const { t } = useTranslation();

  const onExemptionTypeChange = event => {
    const currentTarget = event.target.name.split('.')[0]
    setFieldValue(`${currentTarget}.exemptionType`, event.target.value);
    setFieldValue(`${currentTarget}.exemptOccupationDetails`, '');
    setFieldValue(`${currentTarget}.exemptOccupation`, '');
    setFieldValue(`${currentTarget}.seatNumber`, '');
    setFieldValue(`${currentTarget}.noPreDepartureTest`, false);
    setFieldValue(`${currentTarget}.preDepartureTestDate`, '');
    setFieldValue(`${currentTarget}.preDepartureTestCountry`, '');
  };

  const onOccupationChange = event => {
    const currentTarget = event.target.name.split('.')[0]
    setFieldValue(`${currentTarget}.exemptOccupation`, event.target.value);
    setFieldValue(`${currentTarget}.exemptOccupationDetails`, '');
  };

  const onVaccinationChange = event => {
    const currentTarget = event.target.name.split('.')[0]
    setFieldValue(`${currentTarget}.preDepartureVaccination`, event.target.value);
    setFieldValue(`${currentTarget}.timeSinceVaccination`, '');
    setFieldValue(`${currentTarget}.dosesOfVaccine`, '');
  }

  const onPreDepartureTestChange = event => {
    const currentTarget = event.target.name.split('.')[0]
    setFieldValue(`${currentTarget}.noPreDepartureTest`, event.target.checked);
    setFieldValue(`${currentTarget}.preDepartureTestDate`, '');
    setFieldValue(`${currentTarget}.preDepartureTestCountry`, '');
  }

  const onPrimaryPreDepartureTestChange = event => {
    setFieldValue(`noPreDepartureTest`, event.target.checked);
    setFieldValue(`preDepartureTestDate`, '');
    setFieldValue(`preDepartureTestCountry`, '');
  };

  const onAdditionalTravellersChange = event => {
    setFieldValue('hasAdditionalTravellers', event.target.value);
    setFieldValue('numberOfAdditionalTravellers', '');
    setFieldValue('additionalTravellers', [])
    setFieldTouched('additionalTravellers', false, false)
  }

  const hasSelectedAirport = isAirport(values.nameOfAirportOrBorderCrossing);

  return (
    <Card title={t('Travel Information')}>
      <Grid container spacing={2}>

      { // Show predeparture test questions for exempt travellers arriving by air
        // Non-exempt travellers will fill these out on day-1 questionnaire
        values.exemptionType === ExemptionType[0].value && hasSelectedAirport && (
          <>
              {
                values.noPreDepartureTest !== true
                &&
                <Grid item xs={12}>
                  <Field
                    label="When was your pre-departure covid-19 test? This refers to the test a few days prior to your flight to Canada.*"
                    name="preDepartureTestDate"
                    component={RenderDateField}
                    placeholder="Required"
                    disabled={isDisabled}
                    minDate={moment().subtract(5, 'day')}
                    maxDate={moment().subtract(1, 'day')}
                  />
                </Grid>
              }
              <Grid item xs={12}>
                <Field
                  name="noPreDepartureTest"
                  label="I did not receive a pre-departure COVID-19 test"
                  component={RenderCheckbox}
                  disabled={isDisabled}
                  onChange={onPrimaryPreDepartureTestChange}
                />
              </Grid>
              {
                values.noPreDepartureTest !== true
                &&
                <Grid item xs={12}>
                    <Field
                    label="In what country was the pre-departure test done?*"
                    name="preDepartureTestCountry"
                    component={RenderSelectField}
                    disabled={isDisabled}
                    options={CountriesWithoutCanada}
                  />
                </Grid>
            }
          </>
        )}

        {values.preDepartureTest === Questions[0].value && (
          <>
            <Grid item xs={12}>
              <Field
                label="When was your pre-departure COVID-19 test?*"
                name="preDepartureTestDate"
                component={RenderDateField}
                placeholder="Required"
                minDate={moment().subtract(5, 'day')}
                maxDate={moment().subtract(1, 'day')}
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                label="In what country was the pre-departure test done?*"
                name="preDepartureTestCountry"
                component={RenderSelectField}
                disabled={isDisabled}
                options={CountriesWithoutCanada}
              />
            </Grid>
          </>
        )}

        <Grid item xs={12}>
          <FastField
            name="hasAdditionalTravellers"
            label="Are you travelling with additional members from your household?*"
            component={RenderRadioGroup}
            disabled={isDisabled}
            options={Questions}
            onChange={onAdditionalTravellersChange}
          />
        </Grid>
        {values.hasAdditionalTravellers === Question.Yes && (
          <Grid item xs={12}>
            <FieldArray name="additionalTravellers">
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <FastField
                    name="additionalTravellers"
                    selectName="numberOfAdditionalTravellers"
                    component={RenderDynamicField}
                    label="Number of additional travellers?*"
                    disabled={isDisabled}
                    itemMin={1}
                    itemMax={10}
                    itemFields={[
                      { name: 'firstName', label: "First name*", placeholder: "Required", component: RenderTextField },
                      { name: 'lastName', label: "Last name*", placeholder: "Required", component: RenderTextField },
                      { name: 'dateOfBirth', label: "Date of birth* (YYYY/MM/DD)", placeholder: "Required", component: RenderDateField },
                      { name: 'gender', label: "Gender Identity*", placeholder: "Required", component: RenderSelectField, options:Gender },
                      { name: 'exemptionType', label: "Do you belong to to any category of person who is exempt from the Federal Quarantine requirements?*", placeholder: "Required", component: RenderSelectField, options:ExemptionType, md: 12, onChange: onExemptionTypeChange },
                      {
                        showIf: idx => values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value,
                        name: 'exemptOccupation', label: "Occupation type/category", placeholder: "", component: RenderSelectField, options: ExemptOccupation, md: 12, onChange: onOccupationChange
                      },
                      {
                        showIf: idx => values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value && values.additionalTravellers[idx]['exemptOccupation'] in ExemptOccupationDetails && !!ExemptOccupationDetails[values.additionalTravellers[idx]['exemptOccupation']].length,
                        name: 'exemptOccupationDetails', label: "Occupation Details", placeholder: "", component: RenderSelectField, md: 12, dynamicOptions: idx => ExemptOccupationDetails[values.additionalTravellers[idx]['exemptOccupation']]
                      },
                      { name: 'citizenshipStatus', label: "What is your Canadian citizenship status?*", placeholder: "Required", component: RenderSelectField, options:CitizenshipStatus, md: 12},
                      { 
                        showIf: idx => isAirport(values.nameOfAirportOrBorderCrossing) && values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value,
                        name: 'seatNumber', label: "Seat number(s) for flight", placeholder: "", component: RenderTextField 
                      },

                      { name: 'reasonForTravel', label: "Reason for travel", placeholder: "", component: RenderSelectField, options:TravelReason },
                      { name: 'durationOfStay', label: "Expected duration of stay", placeholder: "", component: RenderSelectField, options: StayDuration },
        
                      { 
                        showIf: idx => isAirport(values.nameOfAirportOrBorderCrossing) && values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value &&  values.additionalTravellers[idx]['noPreDepartureTest'] !== true,
                        name: 'preDepartureTestDate', label: "When was your pre-departure covid-19 test? This refers to the test a few days prior to your flight to Canada.*", component: RenderDateField, placeholder: "Required", minDate: moment().subtract(5, 'day'), maxDate: moment().subtract(1, 'day'), md: 12
                      },
                      { 
                        showIf: idx => isAirport(values.nameOfAirportOrBorderCrossing) && values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value,
                        name: 'noPreDepartureTest', label: "I did not receive a pre-departure COVID-19 test", component: RenderCheckbox, md: 12, onChange: onPreDepartureTestChange
                      },
                      { 
                        showIf: idx => isAirport(values.nameOfAirportOrBorderCrossing) && values.additionalTravellers[idx] && values.additionalTravellers[idx]['exemptionType'] === ExemptionType[0].value && values.additionalTravellers[idx]['noPreDepartureTest'] !== true,
                        name: 'preDepartureTestCountry', label: "In what country was the pre-departure test done?*", component: RenderSelectField, md: 12, options: CountriesWithoutCanada,
                      },



                      { name: 'preDepartureVaccination', label: "Did you receive a vaccine for COVID-19 before you entered Canada?*", component: RenderRadioGroup, md: 12, options: Questions, onChange: onVaccinationChange },
                      {
                        showIf: idx => values.additionalTravellers[idx] && values.additionalTravellers[idx]['preDepartureVaccination'] === Questions[0].value,
                        name: 'timeSinceVaccination', label: "Approximately how many weeks ago was your first injection of vaccine?*", placeholder: "", component: RenderSelectField, md: 12, options: VaccinationTimeframe
                      },
                      {
                        showIf: idx => values.additionalTravellers[idx] && values.additionalTravellers[idx]['preDepartureVaccination'] === Questions[0].value,
                        name: 'dosesOfVaccine', label: "How many doses of vaccine did you receive before you entered Canada?*", placeholder: "", component: RenderSelectField, md: 12, options: VaccinationDoseCount,
                      },
                    ]}
                    itemsWrapper={(fields) => (
                      <Card title={t("Additional Traveller Information")} isTitleSmall={true}>
                        {!isDisabled && (
                          <Typography variant="body2" paragraph>
                            {t('For each traveller, please list their first name, last name and date of birth')}
                          </Typography>
                        )}
                        {fields}
                      </Card>
                    )}
                  />
                </Grid>
              </Grid>
            </FieldArray>

          </Grid>
        )}
        <Grid item xs={12}>
          <FastField
            name="additionalCitiesAndCountries"
            selectName="numberOfAdditionalCitiesAndCountries"
            component={RenderDynamicField}
            label="How many countries have you, or members of your household that you traveled with, visited in the 14 days prior to your flight to Canada (for any duration, including transit stops). If you are completing
                  this form prior to your flight back to Canada, please include any remaining future travel plans in your
                  itinerary."
            disabled={isDisabled}
            itemMin={1}
            itemMax={10}
            itemFields={[
              { name: 'cityOrTown', label: "City or Town*", placeholder: "Required", component: RenderTextField },
              { name: 'country', label: "Country*", options: Countries, component: RenderSelectField },
            ]}
            itemsWrapper={(fields) => (
              <Card title={t("Locations Visited In Past 14 Days")} isTitleSmall={true}>
                {!isDisabled && parseInt(values.numberOfAdditionalCitiesAndCountries) > 1 &&(
                  <Typography variant="body2" paragraph>
                    {t('Please enter the countries in order of most recently to first visited.')}
                  </Typography>
                )}
                {fields}
              </Card>
            )}
          />
        </Grid>
      </Grid>
    </Card>
  );
};