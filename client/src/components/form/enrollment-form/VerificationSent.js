import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

import IsolationPlanPassIcon from '../../../assets/images/isolation-pass.svg';
import { Link } from 'react-router-dom';

export const VerificationSent = ({onReset}) => (
    <Container maxWidth="sm">
        <Box mt={4} mb={4}>
            <Box p={2} border={1} borderRadius={4} borderColor="divider">
                <Grid container spacing={4} align="center">
                <Grid item xs={12} sm={6} alignItems="center" container>
                    <div>
                    <Box mb={2} component={Typography} variant="subtitle2" color="text.secondary" data-cy="confirmationPanel">
                        A link to your application form has been sent to your email
                    </Box>
                    <Typography variant="body2" color="textPrimary">
                        {"Please follow the link to complete your application"}
                    </Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box p={[2.5, 4]} border={1} borderRadius={4} borderColor="divider">
                    <img
                        src={IsolationPlanPassIcon}
                        height={96}
                    />
                    </Box>
                </Grid>
                </Grid>
            </Box>

            <Grid item xs={12} alignItems="center" container justify="center">
                <Link to="#" onClick={onReset}>I didn't receive a link</Link>
            </Grid>
        </Box>
    </Container>
);