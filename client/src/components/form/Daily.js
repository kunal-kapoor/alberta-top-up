import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Field, useFormikContext } from 'formik';
import { useTranslation } from "react-i18next";

import { Card, Button, InputFieldLabel } from '../generic';
import { RenderRadioGroup, RenderTextField, RenderSelectField, RenderDateField, RenderCheckbox } from '../fields';
import { Questions, DailySymptoms, DailyCheckOthers, AirlinesAndAbbreviations, AirlinesAndAbbreviationValues, CountriesWithoutCanada } from '../../constants';
import moment from 'moment';


export const Daily = ({ isDisabled, isFetching, isOtherQuestionsRequired, flightInformationRequired }) => {
  const { t } = useTranslation();
  const { values, setFieldValue } = useFormikContext();

  const onPreDepartureTestChange = event => {
    setFieldValue(`noPreDepartureTest`, event.target.checked);
    setFieldValue(`preDepartureTestDate`, '');
    setFieldValue(`preDepartureTestCountry`, '');
  }

  return (
    <Card title={t('Daily Check-In')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Box my={1}>
            <Typography variant="h4">Are you experiencing any of the following:</Typography>
            <Typography variant="body1">Report any symptoms that are not related to a pre-existing illness.</Typography>
            <Box m={1}>
              <ul>
                {DailySymptoms.map(item => (
                  <Box>
                    <li>{item.label}</li>
                    <Box my={2}>
                      <Field
                        name={item.value}
                        component={RenderRadioGroup}
                        disabled={isDisabled}
                        options={Questions}
                      />
                    </Box>
                  </Box>
                ))}
              </ul>
            </Box>
            {isOtherQuestionsRequired && DailyCheckOthers.map(item => (
              <Field
                label={item.label}
                name={item.value}
                component={RenderRadioGroup}
                disabled={isDisabled}
                options={Questions}
              />
            ))}
            {flightInformationRequired && (
              <Grid container spacing={2}>
                
                {
                  values.noPreDepartureTest !== true 
                    && 
                  <Grid item xs={12}>
                    <Field
                      label="When was your pre-departure covid-19 test? This does not refer to the test you received at the airport but the test you received a few days prior to arriving in Canada.*"
                      name="preDepartureTestDate"
                      component={RenderDateField}
                      placeholder="Required"
                      minDate={moment().subtract(5, 'day')}
                      maxDate={moment().subtract(1, 'day')}
                    />
                  </Grid>
                }
                <Grid item xs={12}>
                  <InputFieldLabel label={<span>I did not receive a pre-departure COVID-19 test</span>}></InputFieldLabel>
                  <Field
                    name="noPreDepartureTest"
                    component={RenderCheckbox}
                    disabled={isDisabled}
                    onChange={onPreDepartureTestChange}
                  />
                </Grid>
                {
                  values.noPreDepartureTest !== true 
                    && 
                  <Grid item xs={12}>
                    <Field
                      label="In what country was the pre-departure test done?*"
                      name="preDepartureTestCountry"
                      component={RenderSelectField}
                      disabled={isDisabled}
                      options={CountriesWithoutCanada}
                    />
                  </Grid>
                }
                <Grid item xs={12}>
                  <Field
                    name="airline"
                    label="Airlines flown for flight arriving to Canada*"
                    component={RenderSelectField}
                    disabled={isDisabled}
                    options={AirlinesAndAbbreviations}
                  />
                </Grid>
                {values.airline === AirlinesAndAbbreviationValues.Other && (
                  <Grid item xs={12}>
                    <Field
                      name="airlineDetails"
                      label="Please specify the Airline*"
                      component={RenderTextField}
                      disabled={isDisabled}
                    />
                  </Grid>
                )}
                <Grid item xs={12} sm={6}>
                  <Field
                    name="flightNumber"
                    label="Flight number(s) for flight arriving to Canada*"
                    component={RenderTextField}
                    disabled={isDisabled}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Field
                    name="seatNumber"
                    label="Seat number(s) for flight arriving to Canada*"
                    component={RenderTextField}
                    disabled={isDisabled}
                  />
                </Grid>
              </Grid>
            )}
          </Box>
        </Grid>

        <Grid item xs={12} sm={12}>
          <Button
            text="Submit Daily Check-In"
            sFetching={isFetching}
            type="submit"
          />
        </Grid>
      </Grid>
    </Card>
  );
};
