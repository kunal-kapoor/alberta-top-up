import React from 'react'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import { Formik, Form as FormikForm } from 'formik'
import { useTranslation } from 'react-i18next'

import { TopUpFormSchema } from 'constants/validation'

import { Button, FocusError } from 'components/generic'
import { PersonalInfo } from 'components/form/PersonalInfo'

export const Form = ({
  onSubmit,
  initialValues,
  isFetching,
  isDisabled,
  canEdit,
  isEditing,
  setIsEditing,
}) => {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={TopUpFormSchema}
      onSubmit={onSubmit}
    >
      <FormikForm>
        <FocusError />
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <PersonalInfo isDisabled={isDisabled} />
          </Grid>
          {(!isDisabled || isEditing) && (
            <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
              <Box ml={[1, 0]} mr={[1, 0]}>
                <Button
                  text={!isEditing? t('Submit Form'): 'Update Form'}
                  type='submit'
                  loading={isFetching}
                />
              </Box>
            </Grid>
          )}
        </Grid>
      </FormikForm>
    </Formik>
  );
};