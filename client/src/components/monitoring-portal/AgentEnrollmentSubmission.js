import React, { useEffect, useState } from 'react';

import Grid from '@material-ui/core/Grid';
import { useAgentEnrollmentForm, useToast } from '../../hooks';
import { Box, Dialog, DialogContent } from '@material-ui/core';
import { Button } from '../generic';
import { answersAreEligible, EligibilityQuestions } from '../../constants/eligibilityQuestions';
import { EnrollmentForm } from '../form/enrollment-form/EnrollmentForm';
import { EligibilityCheckQuestionnaire } from '../../pages/public/EligibilityCheckQuestionnaire';
import { ConfirmationMessage } from '../form/enrollment-form/ConfirmationMessage';
import { EnrollmentFormInitialValues } from '../../constants';

/**
 * Agent enrollment form component for submission on behalf of a traveller
 * This component is meant to provide a streamlined version of the enrollment flow.
 * In essence, it's the EligibilityForm and EnrollmentForm smushed together in a single dialog that can be put anywhere.
 */
export const AgentEnrollmentSubmission = () => {
  const { submit, isFetching, response } = useAgentEnrollmentForm();
  const { openToast } = useToast();
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isEligible, setEligibility] = useState(false);
  const [isEnrolled, setIsEnrolled] = useState(false);

  /** Validates eligibility questionaire values */
  const submitSymptomVerificationQuestionnaire = values => {
    if(answersAreEligible(values)) {
        setEligibility(true);
    } else {
      openToast({status:"error",message:"Traveller is ineligible for pilot program"});
    }
  }

  /** Resets validation flags on dialog close */
  const resetValidation = () => {
    setEligibility(false);
    setIsEnrolled(false)
  }

  /** Closes the dialog */
  const handleCancelForm = () => {
    setDialogOpen(false)
  }

  useEffect(() => {
    if (response) {
      setIsEnrolled(true)
    }
  }, [response])

  return (
    <>
      <Box
        component={Button}
        mr={3}
        style={{ minWidth: 175 }}
        text="Submit Enrollment Form"
        onClick={() => setDialogOpen(true)}
        variant="outlined"
        fullWidth={false}
        size="small"
      />
      <Dialog
        open={isDialogOpen}
        maxWidth="md"
        onExited={resetValidation}
        disableBackdropClick={true}
      >
        <DialogContent>

          {/* Verification Form */}
          {
            !isEligible
              &&
            <Grid container spacing={2}>
              <EligibilityCheckQuestionnaire
                onSubmit={submitSymptomVerificationQuestionnaire}
                isFetching={isFetching}
                adminMode={true}
                cancelHandler={handleCancelForm}
                initialValues={EligibilityQuestions.reduce((values, {name}) => {
                  values[name] = '';

                  return values;
                }, {})}
              />
            </Grid>
          }
          
          {/* Enrollment Form */}
          {
            (isEligible && !isEnrolled)
              &&
            <EnrollmentForm
              onSubmit={submit}
              isFetching={false}
              canEdit={true}
              adminMode={true}
              cancelHandler={handleCancelForm}
              initialValues={EnrollmentFormInitialValues}
            />
          }

          {/** Enrollment Confirmation */}
          {
            isEnrolled
              &&
              <>
                <ConfirmationMessage travellers={response.travellers} viableIsolationPlan={response.viableIsolationPlan} />
                <Grid container xs={12} sm={12} justify="center" style={{ margin: '16px 8px 16px 8px'}}>
                  <Grid item xs={12} sm={10}>
                    <Button
                      text="Close"
                      variant="outlined"
                      onClick={() => setDialogOpen(false)}
                      loading={isFetching}
                    />
                  </Grid>
                </Grid>
              </>
          }
        </DialogContent>
      </Dialog>
    </>
    
  )
}