import React, { Fragment, useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import LogoSmall from '../../assets/images/logo-small.svg';
import LogoFrench from '../../assets/images/logo-french.png';

import { Route, UserType } from '../../constants';
import { useAuth } from '../../hooks';
import { useReportsAccessControl } from '../../hooks/admin';

import { Button, Menu } from '.';
import { AgentEnrollmentSubmission } from '../monitoring-portal/AgentEnrollmentSubmission';
import { isBorderPilotApp } from '../../appversion';

export const Header = () => {
  const history = useHistory();
  const location = useLocation();
  const [menuOptions, setMenuOptions] = useState([]);
  const { t, i18n } = useTranslation();
  const { clearAuthState, state: { userType, isAuthenticated, user } } = useAuth();

  const changeLanguage = async () => {
    const lng = i18n.language === 'en' ? 'fr' : 'en';
    await i18n.changeLanguage(lng);
    history.replace(Route.Root + lng);
    document.title = t("Alberta Isolation Questionnaire");
  }
  const { reportAccess } = useReportsAccessControl();

  useEffect(() => {
    const dynamicMenuOpts = [{ label: 'Logout', onClick: clearAuthState }];

    if(userType === UserType.BackOffice) {
      dynamicMenuOpts.push({ label: 'Submission Lookup', onClick: () => history.push(Route.BackOfficeLookup)})
    }
    else if(userType === UserType.ServiceAlberta) {
      dynamicMenuOpts.push({ label: 'Upload Testing Records', onClick: () => history.push(Route.RtopAdminUploadNightlyReport)})
      
      if(history.location.pathname !== Route.RtopAdminReportDashboard) {
        dynamicMenuOpts.push({ label: 'Report Dashboard', onClick: () => history.push(Route.RtopAdminReportDashboard)})
      }

      if(history.location.pathname !== (isBorderPilotApp()? Route.RtopAdminLookupDailyReports.dynamicRoute(): Route.ServiceAlbertaLookup.dynamicRoute())) {
        dynamicMenuOpts.push({ label: 'Submission Lookup', onClick: () => history.push(isBorderPilotApp()? Route.RtopAdminLookupDailyReports.dynamicRoute(): Route.ServiceAlbertaLookup.dynamicRoute())})
      }
      
    }
    
    if(reportAccess.hasAccess && history.location.pathname !== Route.ReportDashboard) {
      dynamicMenuOpts.push({ label: 'Reports', onClick: () => history.push(Route.ReportDashboard)})
    }

    setMenuOptions(dynamicMenuOpts);
  }, [location]);

  return (
    <Fragment>
      <Box py={0.75} px={[1.25, 3]} display="flex" alignItems="center" justifyContent="space-between">
        <img
          style={{ cursor: 'pointer' }}
          height={32}
          src={i18n.language === 'en' ? LogoSmall : LogoFrench}
          alt="Logo"
          onClick={() => history.push(!isAuthenticated ? Route.Root : {
            [UserType.ServiceAlberta]: Route.ServiceAlbertaLookup.dynamicRoute(),
            [UserType.BackOffice]: Route.AdminForm,
          }[userType] || Route.Root)}
        />

        <Box display="flex">
          <Hidden smUp>
            {!isAuthenticated && (location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <Menu
              options={[
                { label: t('Language'), onClick: changeLanguage }
              ]}
              label={( <MoreVertIcon /> )}
            />
            )}

            {isAuthenticated && (location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <Menu
              options={[
                { label: t('Language'), onClick: changeLanguage },
                { label: 'Logout', onClick: clearAuthState }
              ]}
              label={(
                <Fragment>
                  <AccountCircleOutlinedIcon/>&nbsp;{user.idTokenPayload?.name || user.idTokenPayload?.email || 'Unknown'}
                </Fragment>
              )}
            />
            )}
          </Hidden>

          <Hidden xsDown>
          {(location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <div>
                <Box
                  component={Button}
                  mr={3}
                  style={{ minWidth: 175 }}
                  text={t('Language')}
                  variant="outlined"
                  fullWidth={false}
                  size="small"
                  onClick={ changeLanguage }
                />
              </div>
            )}
          </Hidden>
            {
              isAuthenticated && (
                <>
                  <AgentEnrollmentSubmission/>
                  <Menu
                    options={menuOptions}
                    label={(
                      <Fragment>
                        <AccountCircleOutlinedIcon/>&nbsp;{user.idTokenPayload?.name || user.idTokenPayload?.email || 'Unknown'}
                      </Fragment>
                    )}
                  />
                </>
              )
            }
        </Box>
      </Box>
    </Fragment>
  );
};