import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Route, DailyMonitoringLookupColumns, DailyMonitoringLookupFilters, HouseholdMembersColumns, TravellerMonitoringColumns, EnrollmentStatus, DailySymptoms, DailyCheckOthers, DailySurveyFlightInformation } from '../constants';
import {
  AxiosPrivate,
  normalizeEnrollmentFormValues,
  getOptionalURLParams,
  setOptionalURLParams,
  removeOptionalURLParam,
  dateToString,
  utcToLocalString
} from '../utils';
import { useModal, useToast } from '.';

import { Button, Menu, StatusChips } from '../components/generic';
import { WithdrawnForm } from '../components/form/WithdrawnForm';
import { HouseholdStatus } from '../constants';
import moment from 'moment';
import Switch from '@material-ui/core/Switch';


export const useDailyMonitoringLookup = () => {
    const { openToast } = useToast();
    const { openModal, closeModal } = useModal();
    const history = useHistory();
    const [isFetching, setFetching] = useState(true);
    const [tableData, setTableData] = useState({
      columns: DailyMonitoringLookupColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
  
    /**
     * Format text that should be used for filter pills.
     * 
     * display: Given the current filter values, should return a string that should be displayed
     * isZeroState: Given the current filter values, should return true if the current values can
     *    be considered to be the values zero state
     */
    const formatFilterPills = {
      date: {
        display: ({fromDate, toDate}) => {
          if(fromDate && toDate) {
            return `${dateToString(fromDate)} - ${dateToString(toDate)}`;
          } else if (fromDate) {
            return `From ${dateToString(fromDate)}`;
          } else if (toDate) {
            return `To ${dateToString(toDate)}`;
          } else {
            return '';
          }
        },
        isZeroState: ({fromDate, toDate}) => !fromDate && !toDate
      },
      query: {
        display: ({query}) => {
          if(query && query.length > 18) {
            query = `${query.substring(0,18)}...`;
          }
          return `${query}`
        },
        isZeroState: ({query}) => !query
      }
    }
  
    const [initialFilters, setInitialFilters] = useState(null);
    const [selectedSearchFilters, setSelectedSearchFilters] = useState([]);
    const [zeroStates] = useState({
      enrollmentStatus: 'all',
      fromDate: '',
      toDate: '',
      query: '',
      program: 'border_pilot'
    });
  
    const validDateOrEmpty = dstr => {
      if(!dstr) { return ''; }
      const asDate = moment(dstr);
      return asDate.isValid()? asDate.format('YYYY-MM-DD'): '';
    };
  
    // Parse url filters / sorting / query string from the current url
    // Formats dates + decodes url params if necessary
    const parseUrlFilters = () => {
      let { page = 0, fromDate='', toDate='', agent='', enrollmentStatus='all', query = '', order = 'asc', orderBy = DailyMonitoringLookupColumns[0].value, cardStatus = DailyMonitoringLookupFilters[1].value, program="border_pilot" } = getOptionalURLParams(history);
      if(fromDate) {
        fromDate = moment(fromDate).startOf('day');
      } 
  
      if(toDate) {
        toDate = moment(toDate).endOf('day');
      }
  
      if(cardStatus) {
        try {
          cardStatus = decodeURIComponent(cardStatus);
        } catch {}
      }
  
      if(query) {
        try {
          query = decodeURIComponent(query);
        } catch {}
      }
  
      return {page, agent, cardStatus, enrollmentStatus, query, order, orderBy, fromDate, toDate, program};
    }
  
    // Set initial filters on page load
    useEffect(() => {
      const {cardStatus, fromDate, toDate, query, agent, enrollmentStatus, program} = parseUrlFilters();
      
      setInitialFilters({
        enrollmentStatus: enrollmentStatus || 'all',
        agent: agent || 'all',
        fromDate: validDateOrEmpty(fromDate),
        toDate: validDateOrEmpty(toDate),
        query,
        program
      });
    }, []);
  
    useEffect(() => {
      (async () => {
        try {
          setFetching(true);
  
          const filterValues = parseUrlFilters();
          const { page, agent, query, order, orderBy, cardStatus, fromDate, toDate, enrollmentStatus, program } = filterValues;
  
          // Find appliccable filters that have been changed (not in their zero-state)
          const appliedFilters = ['query','date'].filter(f => !formatFilterPills[f].isZeroState(filterValues));
          
          // Update state with the filters that have been applied
          // this populates the filter "pills" at the top of the work list
          setSelectedSearchFilters(appliedFilters.map(f => ({
            value: f,
            label: formatFilterPills[f].display(filterValues)
          })));
          
          // Construct query
          const queryFilter = query ? `query=${query}&` : '';
  
          let qs = `${queryFilter}cardStatus=${cardStatus}&orderBy=${orderBy}&order=${order}&page=${page}&agent=${agent}&enrollmentStatus=${enrollmentStatus}&program=${program}`;
          if(fromDate) {
            qs += `&fromDate=${moment(fromDate).toISOString()}`;
          }
          if(toDate) {
            qs += `&toDate=${moment(toDate).toISOString()}`;
          }
  
          // Actually perform search
          const { results = [], numberOfResults = 0 } = await AxiosPrivate.get(`/api/v2/rtop-admin/form?${qs}`);
          handleSetTableData(results, numberOfResults, page);
        } catch (e) {
          openToast({ status: 'error', message: e.message || 'Failed to lookup' });
        } finally {
          setFetching(false);
        }
      })();
    }, [history.location.search]);
  
    const handleSetTableData = (results, numberOfResults, currentPage) => {
      
      setTableData(prevState => ({
        ...prevState,
        totalRows: numberOfResults,
        currentPage: parseInt(currentPage),
        rows: results.map((item) => ({
          date: dateToString(item.arrivalDate, true, 'DD MMM YYYY'),
          firstName: item.firstName,
          lastName:item.lastName,
          status:  item.status ? (
            <StatusChips status={item.status} />
          ): 'None',
          enrollmentStatus:item.enrollmentStatus?.toUpperCase(),
          lastActivityNote: (item.lastActivityStatus && (
            item.lastActivityNote?
            (<a 
              style={{
                color: 'blue', 
                cursor: 'pointer', 
                textDecoration: 'underline'
              }} 
              onClick={() => openModal({
                open: true, 
                positiveActionOnClick: () => closeModal(), 
                positiveActionText: 'Close', 
                title: 'Most recent note', 
                description: item.lastActivityNote
                })
              }
            >
              {item.lastActivityStatus}
            </a>): item.lastActivityStatus) || '-'
          ),
          lastScreened : item.lastScreened ? utcToLocalString(item.lastScreened, 'DD MMM YYYY'): '-',
          viewMore: (
            <Button
              text="View"
              variant="outlined"
              size="small"
              onClick={() => item.program && item.program === 'arrival_testing' 
                ?
                  history.push(Route.RtopAdminLookupArrivalTestingConfirmationNumber.dynamicRoute(item.confirmationNumber)) 
                : 
                  history.push(Route.RtopAdminLookupConfirmationNumber.dynamicRoute(item.confirmationNumber))}
            />
          ),
        })),
      }));
    };
  
    return {
      onSubmit: ({query, fromDate, toDate, agent, enrollmentStatus, program}) => {
        if(fromDate) {
          fromDate = validDateOrEmpty(fromDate);
        }
        
        if(toDate) {
          toDate = validDateOrEmpty(toDate);
        }
  
        return setOptionalURLParams(history, {query, fromDate, toDate, agent, enrollmentStatus, page: 0, program})
      },
      onSearch: (query) => setOptionalURLParams(history, { query, page: 0 }),
      onFilter: (cardStatus) => setOptionalURLParams(history, { cardStatus, page: 0 }),
      onAgent: (agent) => setOptionalURLParams(history, { agent, page: 0 }),
      onSort: (orderBy, order) => setOptionalURLParams(history, { orderBy, order, page: 0 }),
      onChangePage: (page) => setOptionalURLParams(history, { page }),
      clearFilter: (cardStatus) => removeOptionalURLParam(history, cardStatus),
      tableData,
      isFetching,
      initialFilters,
      selectedSearchFilters,
      zeroStates,
    }
  };


  export const useMonitoringPortalActivity = () => {
    const history = useHistory();
    const [isFetching, setFetching] = useState(false);
    const [success, setSuccess] = useState(false);
    const { openModal, closeModal } = useModal();
    const { openToast } = useToast();
  
    const handleSubmit = async (formId, values) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/activity`, values);
        openToast({ status: 'success', message: 'Submission updated' });
        setSuccess(true)
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to update this submission' });
        setSuccess(false)
      } finally {
        setFetching(false);
      }
    };
  
    return {
      isFetching,
      success,
      submit: (formId, values) => {
        openModal({
          title: 'Ready to Submit?',
          description: 'Are you ready to submit this record?',
          negativeActionText: 'Make Changes',
          positiveActionText: 'Yes, submit now.',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleSubmit(formId, values),
        });
      }
    }
  };
  
  export const useMonitoringPortalShowSubmission = () => {
    const { openModal, closeModal } = useModal();

    return {
      showSubmission: description => {
        openModal({
          title: 'Submission',
          description,
          size: 'sm',
          textAlign: 'left',
          negativeActionText: 'Close',
          negativeActionOnClick: () => closeModal(),
        });
      }
    }
  }
  
  export const useDailyMonitoringLookupConfirmationNumber = () => {
    const { openModal, closeModal } = useModal();
    const { openToast } = useToast();
    const [isFetching, setFetching] = useState(true);
    const [error, setError] = useState(null);
    const [formValues, setFormValues] = useState(null);
    const [initialValues, setInitialValues] = useState({ status: '', note: '' });
    const [tableData, setTableData] = useState({
      columns: HouseholdMembersColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
    const [statusTableData, setStatusData] = useState({
      columns: TravellerMonitoringColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
    const [enrollmentActions, setEnrollmentActions] = useState([]);
    const [travellerActions, setTravellerActions] = useState([]);
    const [statusReason, setStatusReason] = useState(null);
  
    const handleEnrollModal = async (enrollData) => {
      openModal({
        title: 'Are you Sure?',
        description: `Are you sure you want to enroll this ${enrollData.household ? 'household' : 'traveller'} to the program?`,
        negativeActionText: 'No',
        positiveActionText: 'Yes',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => enrollData.household ? handleEnrollHousehold(enrollData.confirmationNumber) : handleEnrollTraveller(enrollData.confirmationNumber),
      });
    }
  
    const handleEnrollHousehold = async (confirmationNumber) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/enroll`);
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Household enrolled!' });
      }
      catch (e) {
        setError(e.message || 'Failed to enroll the household');
      } finally {
        setFetching(false);
      }
    }

    const handleEnrollTraveller = async (confirmationNumber) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/enroll`);
        await handleLookup(confirmationNumber.slice(0, -2));
        openToast({status: 'success', message: 'Traveller enrolled!' });
      }
      catch (e) {
        setError(e.message || 'Failed to enroll the traveller');
      } finally {
        setFetching(false);
      }
    }

    const handleGenerateLink = async (id, confirmationNumber) => {
      try{
        setFetching(true);
        const link = await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${id}/send-reminder`, {});
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Generated daily questionnaire link!' });
      }
      catch (e) {
        setError(e.message || 'Failed to enroll the traveller');
      } finally {
        setFetching(false);
      }
    }
  
    const handleWithdrawModal = async (withdrawData) => {
      openModal({
        title: 'Withdrawal Confirmation',
        textAlign: 'left',
        description: (
          <WithdrawnForm
            isHousehold={withdrawData.household}
            onSubmit={(values) => withdrawData.household ? handleWithdrawHousehold(withdrawData.confirmationNumber, values) : handleWithdrawTraveller(withdrawData.confirmationNumber, values)}
          ></WithdrawnForm>
        )
      });
    }
  
    const handleWithdrawHousehold = async (confirmationNumber, { withdrawnReason }) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/withdraw`, { withdrawnReason });
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Household withdrawn!' });
      }
      catch (e) {
        setError(e.message || 'Failed to withdraw the household');
      } finally {
        setFetching(false);
      }
    }

    const handleWithdrawTraveller = async (confirmationNumber , { withdrawnReason }) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/withdraw`, { withdrawnReason });
        await handleLookup(confirmationNumber.slice(0, -2));
        openToast({status: 'success', message: 'Traveller withdrawn!' });
      }
      catch (e) {
        setError(e.message || 'Failed to withdraw the traveller');
      } finally {
        setFetching(false);
      }
    }
  
    const handleAssign = async (formId, confirmationNumber) => {
      try {
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/assign`);
        await handleLookup(confirmationNumber);
        openToast({
          status: 'warning',
          message: `Record ${confirmationNumber} has been assigned to you.`,
          timeout: null,
          actionText: 'Undo Assignment',
          onActionClick: () => handleUnassign(formId, confirmationNumber),
        });
      } catch (e) {
        setError(e.message || 'Failed to assign this submission');
      } finally {
        setFetching(false);
      }
    };
  
    const handleUnassign = async (formId, confirmationNumber) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/assign/undo`);
        await handleLookup(confirmationNumber);
        openToast({ status: 'success', message: 'Submission unassigned' });
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to unassign this submission' });
      } finally {
        setFetching(false);
      }
    };
  
    const handleLookup = async (confirmationNumber) => {
      try {
        setFetching(true);
        const results = await AxiosPrivate.get(`/api/v2/rtop-admin/form/${confirmationNumber}`);
        const { status = '' } = {};
        setInitialValues({ status, note: '' });
        results.household = normalizeEnrollmentFormValues(results.household);
        handleSetTableData(normalizeEnrollmentFormValues(results));

        const statusRes = await AxiosPrivate.get(`/api/v2/rtop-admin/form/${confirmationNumber}/reason`);
        setStatusReason(statusRes[0]?.CARD_STATUS_REASON);
      } catch (e) {
        setError(`Failed to find submission with Confirmation Number: "${confirmationNumber}"`);
      } finally {
        setFetching(false);
      }
    };

  
    const setActions = (enrollmentStatus, confirmationNumber, householdCardStatus) => {
      const EnrollmentActionOptions = {
        "ENROLL" : { label: "Enroll", onClick: () => handleEnrollModal({confirmationNumber, household: true})},
        "WITHDRAW" : { label: "Withdraw", onClick: () => handleWithdrawModal({confirmationNumber, household: true})},
      };
  
      if( enrollmentStatus === EnrollmentStatus.APPLIED )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["ENROLL"],
          EnrollmentActionOptions["WITHDRAW"]
        ]);
      }
      else if ( enrollmentStatus === EnrollmentStatus.ENROLLED )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["WITHDRAW"]
        ]);
      }
      else if ( enrollmentStatus === EnrollmentStatus.WITHDRAWN )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["ENROLL"]
        ]);
      }

      if( householdCardStatus && householdCardStatus != HouseholdStatus.GREEN)
      {
        setEnrollmentActions((prevState) => [ ...prevState , { label: "Clear Flag", onClick: () => handleChangeStatusModal(confirmationNumber, HouseholdStatus.GREEN)}]);
      }
    }

    const getTravellerActions = (travellerData) => {
      const EnrollmentActionOptions = {
        "ENROLL" : { label: "Enroll", onClick: () => handleEnrollModal({confirmationNumber: travellerData.confirmationNumber, household: false})},
        "WITHDRAW" : { label: "Withdraw", onClick: () => handleWithdrawModal({confirmationNumber: travellerData.confirmationNumber, household: false})},
      };

      switch (travellerData?.enrollmentStatus) {
        
        case EnrollmentStatus.APPLIED :
         return [
            EnrollmentActionOptions.ENROLL,
            EnrollmentActionOptions.WITHDRAW
          ]
        
        case EnrollmentStatus.ENROLLED :
          return [
            EnrollmentActionOptions.WITHDRAW
          ]
        
        case EnrollmentStatus.WITHDRAWN :
          return [
            EnrollmentActionOptions.ENROLL,
          ]
        
        default :
          return [];
      }
    }

    const constructSubmissionAnswers = (submission) => {
      let submissionAnswers = [];
      if (submission) {
        submissionAnswers = submission.map(({question, answer}) => {
          let questionFormatted = question.toUpperCase();
          
          const questionLabel = [].concat(DailySymptoms).concat(DailyCheckOthers).concat(DailySurveyFlightInformation).find(({value, label}) => value === question )
          if(questionLabel) {
            questionFormatted = questionLabel.label.toUpperCase();
          }

          return {question: questionFormatted, answer: answer.toUpperCase()};
        })
      }

      return submissionAnswers;
    }

    const setSubmissionHistoryModal = (status) => {
      setStatusData(prevState => ({
        ...prevState,
        rows: status.map((ele) => ({
          status: ele.status,
          date: ele.date,
          submission: constructSubmissionAnswers(ele.submission)
        })),
      }));
      
    }

    const handleIsolationStatusChangeModal = (event, travellerConfirmationNumber, householdConfirmationNumber) => {
      const isolationStatus = event.target.checked;
      openModal({
        title: 'Change Isolation Status of traveller',
        description: `You are about to change isolation status for ${travellerConfirmationNumber}. Are you sure you want to do this?`,
        negativeActionText: 'Cancel',
        positiveActionText: 'Confirm',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleIsolationStatusChange(travellerConfirmationNumber, isolationStatus, householdConfirmationNumber),
      });
    }

    const handleIsolationStatusChange = async (confirmationNumber, isolationStatus, householdConfirmationNumber) => {
      try {
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/isolation-status`, { isolationStatus });
        await handleLookup(householdConfirmationNumber);
        openToast({
          status: 'warning',
          message: `Changed isolation status for ${confirmationNumber}`,
        });
      } catch (e) {
        setError(e.message || 'Failed to change status');
      } finally {
        setFetching(false);
        closeModal();
      }
    }
  
    const handleSetTableData = (results) => {  
      setFormValues(results);
      setActions(results.enrollmentStatus, results.confirmationNumber, results.householdCardStatus);

      setTableData(prevState => ({
        ...prevState,
        rows: results.travellerStatus.map((item, index) => ({
          name: `${item.firstName} ${item.lastName}`,
          age: item.age,
          confirmationNumber: item.confirmationNumber,
          status: item.cardStatus ? (
            <StatusChips status={item.cardStatus} />
          ): 'None',
          lastScreened: item.lastScreened && utcToLocalString(moment(item.lastScreened), 'DD MMM YYYY'),
          test2date: item.test2Date && utcToLocalString(item.test2Date, 'DD MMM YYYY'),
          isolationStatus: (
            <Switch checked={false || item.isolationStatus} color="primary" onChange={(e) => handleIsolationStatusChangeModal(e, item.confirmationNumber, results.confirmationNumber)}/>          
          ),
          statusHistory: (item.status && 
            <Button
              text="View History"
              variant="outlined"
              size="small"
              onClick={() => setSubmissionHistoryModal(item.status)} 
            />
              
          ),
          exemptionStatus: (
            item && index === 0
              ? results.household.exemptionType
              : results?.household.additionalTravellers[index -1]?.exemptionType
          ),
          enrollmentStatus: item.enrollmentStatus,
          enrollmentActions: (
            item 
              && 
            <Menu
              label="..."
              size="small"
              color="primary"
              variant="contained"
              options={getTravellerActions(item)}
            />
          )
        })),
      }));
    };

    const handleChangeStatus = async (confirmationNumber, cardStatus) => {
      try {
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/override-status/${cardStatus}`);
        await handleLookup(confirmationNumber);;
        openToast({
          status: 'warning',
          message: `Changed status for ${confirmationNumber}`,
        });
      } catch (e) {
        setError(e.message || 'Failed to change status');
      } finally {
        setFetching(false);
        closeModal();
      }
    };

    const handleChangeStatusModal = async (confirmationNumber, cardStatus) => {
      openModal({
        title: 'Change status to green',
        description: `You are about to change status for ${confirmationNumber} to green. Are you sure you want to do this?`,
        negativeActionText: 'Cancel',
        positiveActionText: 'Confirm',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleChangeStatus(confirmationNumber, cardStatus),
      });
    };

    const handleSendReminderLink = async (formId, confirmationNumber) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${formId}/send-reminder`, {});
        await handleLookup(confirmationNumber);
        openToast({ status: 'success', message: 'Daily link was successfully sent' });
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to re-send link' });
      } finally {
        setFetching(false);
      }
    };
  
    return {
      isFetching,
      error,
      initialValues,
      formValues,
      lookup: handleLookup,
      assign: handleAssign,
      changeStatus: handleChangeStatusModal,
      generateLink: handleGenerateLink,
      statusReason: statusReason,
      unassign: (formId, confirmationNumber, agent) => {
        openModal({
          title: 'Are you Sure?',
          description: `This record is already assigned to ${agent}. Are you sure you want to unassign this record?`,
          negativeActionText: 'No',
          positiveActionText: 'Yes',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleUnassign(formId, confirmationNumber),
        });
      },
      tableData,
      statusTableData,
      enrollmentActions,
      setStatusData,
      showStatusHistory: description => {
          openModal({
            title: 'Status history for traveller',
            description,
            size: 'md',
            textAlign: 'left',
            negativeActionText: 'Close',
            negativeActionOnClick: () => closeModal() && setStatusData({rows : []}),
          });
      },
      sendReminderLink: (formId, confirmationNumber) => {
        openModal({
          title: 'Re-send reminder?',
          description: 'This will re-send the latest daily questionairre to the household.',
          negativeActionText: 'Cancel',
          positiveActionText: 'Yes, send now.',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleSendReminderLink(formId, confirmationNumber),
        });
      }
    }
  };

  export const useMonitoringReportExport = () => {
    const { openToast } = useToast();
  
    const [isFetching, setFetching] = useState(false);
    const [hasExecuted, setHasExecuted] = useState(true);  
  
    const executeReport = async({ date, type }) => {
        setFetching(true);
        try {
            AxiosPrivate.get(`/api/v2/rtop-admin/export?date=${moment(date).format("YYYY-MM-DD")}&type=${type}`).then((response) => {
              const url = window.URL.createObjectURL(new Blob([response]));
              const link = document.createElement('a');
              link.href = url;
              link.setAttribute('download', `${type}.${moment(date).format("YYYY-MM-DD")}.csv`);
              document.body.appendChild(link);
              link.click();
            });
        } catch (e) {
            openToast({ status: 'error', message: e.message || 'Failed to fetch report' });
        } finally {
            setFetching(false);
            setHasExecuted(true);
        }
    }
  
    return {
        isFetching,
        hasExecuted,
        executeReport
    }
  };

  export const useMonitoringReportLookup = () => {
    const { openToast } = useToast();
  
    const [isFetching, setFetching] = useState(false);
    const [hasExecuted, setHasExecuted] = useState(false);
    const [tableData, setTableData] = useState({
        columns: [],
        rows: [],
    });
    const [reportOptions, setReportOptions] = useState([]);
  
    const executeReport = async({ date, type }) => {
        setFetching(true);
        try {
            const result = await AxiosPrivate.get(`/api/v2/rtop-admin/report/${type}`, {
              params: {
                date: moment(date).format("YYYY-MM-DD"),
              }
            });
            handleSetTableData(result || []);
        } catch (e) {
            openToast({ status: 'error', message: e.message || 'Failed to execute report' });
            handleSetTableData([]);
        } finally {
            setFetching(false);
            setHasExecuted(true);
        }
    }
  
    const handleSetTableData = (result=[]) => {
      const columns = result.length? Object.keys(result[0]).map(r => ({key: r, label: r})): [{
        label: 'No Results',
        value: 'noResults'
      }];
      
      result = result.length? result: [{noResult: 'No Result'}];
  
        setTableData(prevState => ({
            ...prevState,
            columns,
            rows: result,
        }));
    };
  
    return {
        isFetching,
        tableData,
        hasExecuted,
        reportOptions,
        executeReport
    }
  };