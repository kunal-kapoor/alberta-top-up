import { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { Route } from '../constants';
import { AxiosPublic, serializeEnrollmentFormValues } from '../utils';
import { useToast } from '.';
import { useTranslation } from "react-i18next";

export const useEnrollmentForm = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const { openToast } = useToast();
  const { t } = useTranslation();
  const {verificationToken} = useParams();

  return {
    isFetching,
    submit: async (values) => {
      try {
        setFetching(true);
        const { confirmationNumber, viableIsolationPlan, travellers } = await AxiosPublic.post(`/api/v2/rtop/traveller/enroll/${verificationToken}`, serializeEnrollmentFormValues(values));
        history.replace(Route.EnrollmentForm);
        history.push(Route.EnrollmentConfirmation, { confirmationNumber, viableIsolationPlan, travellers });
      } catch (e) {
        openToast({ status: 'error', message: e.message || t("Failed to submit form") });
      } finally {
        setFetching(false);
      }
    }
  }
};

export const useArrivalTestingEnrollmentForm = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const { openToast } = useToast();
  const { t } = useTranslation();
  const {verificationToken} = useParams();

  return {
    isFetching,
    submit: async (values) => {
      try {
        setFetching(true);
        const { confirmationNumber, viableIsolationPlan, travellers } = await AxiosPublic.post(`/api/v2/rtop/traveller/enroll/${verificationToken}/arrival_testing`, serializeEnrollmentFormValues(values));
        history.replace(Route.ArrivalTestingEnrollmentForm);
        history.push(Route.ArrivalTestingEnrollmentConfirmation, { confirmationNumber, viableIsolationPlan, travellers });
      } catch (e) {
        openToast({ status: 'error', message: e.message || t("Failed to submit form") });
      } finally {
        setFetching(false);
      }
    }
  }
};

export const useVerification = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const [hasSent, setHasSent] = useState(false);
  const [response, setResponse] = useState();
  const { openToast } = useToast();

  return {
    isFetching,
    hasSent,
    response,
    reset: () => setHasSent(false),
    submit: async (values) => {
      try {
        setHasSent(false);
        setFetching(true);
        if (values.contactMethod === 'callIn') {
          const {token} = await AxiosPublic.post('/api/v2/rtop/traveller/verification', values);
          values.program && values.program === 'arrival_testing'
            ?
              history.replace(`/arrival-testing/enroll/${token}`)
            :
              history.replace(`/enroll/${token}`)
        } else {
          setResponse(await AxiosPublic.post('/api/v2/rtop/traveller/verification', values));
        }
        setHasSent(true);
      } catch (e) {
        openToast({ status: 'error', message: e.message || "Failed to send verification" });
      } finally {
        setFetching(false);
      }
    }
  };
}