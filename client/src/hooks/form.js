import { useState } from 'react'
import { useHistory } from 'react-router-dom'

import { Route } from '../constants'
import { AxiosPublic, AxiosPrivate, serializeFormValues } from '../utils'
import { useToast } from '.'
import { useTranslation } from 'react-i18next'

export const useForm = () => {
  const history = useHistory()
  const [isFetching, setFetching] = useState(false)
  const { openToast } = useToast()
  const { t } = useTranslation()

  return {
    isFetching,
    submit: async (values) => {
      console.log('submit handler')
      console.dir(values)
      try {
        setFetching(true);
        const { confirmationNumber } = await AxiosPublic.post('/api/v1/topupform', serializeFormValues(values))
        history.push(Route.Confirmation, { confirmationNumber })
      } catch (e) {
        openToast({ status: 'error', message: e.message || t("Failed to submit form") })
        setFetching(false)
      }
    }
  }
};

export const useUpdateForm = () => {
  const [isFetching, setFetching] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const { openToast } = useToast();

  return {
    isFetching,
    isEditing,
    setIsEditing,
    updateForm: async (confirmationNumber, values) => {
      try {
        setFetching(true);
        const serializedFormValues = serializeFormValues(values);

        await AxiosPrivate.patch(`/api/v1/admin/back-office/form/${confirmationNumber}`, {
          hasPlaceToStayForQuarantine: serializedFormValues.hasPlaceToStayForQuarantine,
          quarantineLocation: serializedFormValues.quarantineLocation
        });

        openToast({status:"success", message: "Successfully updated form"});
        setIsEditing(false);
        setFetching(false);
      } catch (e) {
        openToast({ status: 'error', message: "Failed to update form"});
        setFetching(false);
      }
    }
  }
};
