import { useState } from 'react'
import { useHistory } from 'react-router-dom'

import { Route } from 'constants/enums'
import { AxiosPublic, AxiosPrivate, serializeTopupFormValues } from 'utils'
import { useToast } from '.'
import { useTranslation } from 'react-i18next'

export const useTopupForm = () => {
  const history = useHistory()
  const [isFetching, setFetching] = useState(false)
  const { openToast } = useToast()
  const { t } = useTranslation()

  return {
    isFetching,
    submit: async (values) => {
      try {
        setFetching(true);
        const { id } = await AxiosPublic.post('/api/v1/topup', serializeTopupFormValues(values));
        history.push(Route.Confirmation, { id });
      } catch (e) {
        openToast({ status: 'error', message: e.message || t("Failed to submit form") });
        setFetching(false);
      }
    }
  }
};

export const useUpdateTopupForm = () => {
  const [isFetching, setFetching] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const { openToast } = useToast();

  return {
    isFetching,
    isEditing,
    setIsEditing,
    updateForm: async (confirmationNumber, values) => {
      try {
        setFetching(true);
        const serializedFormValues = serializeTopupFormValues(values);

        await AxiosPrivate.patch(`/api/v1/admin/back-office/form/${confirmationNumber}`, {
          hasPlaceToStayForQuarantine: serializedFormValues.hasPlaceToStayForQuarantine,
          quarantineLocation: serializedFormValues.quarantineLocation
        });

        openToast({status:"success", message: "Successfully updated form"});
        setIsEditing(false);
        setFetching(false);
      } catch (e) {
        openToast({ status: 'error', message: "Failed to update form"});
        setFetching(false);
      }
    }
  }
};
