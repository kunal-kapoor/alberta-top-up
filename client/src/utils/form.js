import { Place, Vehicle, Places, Vehicles, Question, Province, Provinces } from '../constants';
import { dateToString } from './date';

export const serializeFormValues = (values) => {
  console.log(`serializeFormValues ${values}`)
  const valuesCopy = { ...values };

  if (valuesCopy.hasAdditionalTravellers === Question.No) {
    valuesCopy.additionalTravellers = [];
  }

  if(valuesCopy.hasTravellersOutsideHousehold === Question.No) {
    valuesCopy.travellersOutsideHousehold = '';
  }

  if (valuesCopy.hasPlaceToStayForQuarantine === Question.No) {
    valuesCopy.quarantineLocation = null;
  } else {
    if (valuesCopy.quarantineLocation.typeOfPlace === Place.Other) {
      valuesCopy.quarantineLocation.typeOfPlace = valuesCopy.quarantineLocation.typeOfPlaceDetails;
    }
    if (valuesCopy.quarantineLocation.howToGetToPlace === Vehicle.Other) {
      valuesCopy.quarantineLocation.howToGetToPlace = valuesCopy.quarantineLocation.howToGetToPlaceDetails;
    }
    if (valuesCopy.quarantineLocation.provinceTerritory === Province.Other) {
      valuesCopy.quarantineLocation.provinceTerritory = valuesCopy.quarantineLocation.provinceTerritoryDetails;
    }
    delete valuesCopy.quarantineLocation.typeOfPlaceDetails;
    delete valuesCopy.quarantineLocation.howToGetToPlaceDetails;
    delete valuesCopy.quarantineLocation.provinceTerritoryDetails;
  }

  delete valuesCopy.numberOfAdditionalTravellers;
  delete valuesCopy.numberOfAdditionalCitiesAndCountries;

  return valuesCopy;
};

export const normalizeFormValues = (values) => {
  const valuesCopy = { ...values };

  if (!valuesCopy.quarantineLocation) {
    valuesCopy.quarantineLocation = {
      address: '',
      cityOrTown: '',
      provinceTerritory: '',
      postalCode: '',
      phoneNumber: '',
      typeOfPlace: '',
      typeOfPlaceDetails: '',
      howToGetToPlace: '',
      howToGetToPlaceDetails: '',
      doesVulnerablePersonLiveThere: '',
    };
  } else {

    const fillOtherDetails = (optionsList, dropDownValue, detailsValue, criteria) => {
      if (!optionsList.find(eachValue => eachValue.value === valuesCopy.quarantineLocation[dropDownValue])) {
        valuesCopy.quarantineLocation[detailsValue] = valuesCopy.quarantineLocation[dropDownValue];
        valuesCopy.quarantineLocation[dropDownValue] = criteria;
      } else {
        valuesCopy.quarantineLocation[detailsValue] = '';
      }  
    }

    fillOtherDetails(Places, 'typeOfPlace', 'typeOfPlaceDetails', Place.Other);
    fillOtherDetails(Vehicles, 'howToGetToPlace', 'howToGetToPlaceDetails', Vehicle.Other);
    fillOtherDetails(Provinces, 'provinceTerritory', 'provinceTerritoryDetails', Province.Other);
    
  }

  if (valuesCopy.additionalTravellers?.length > 0) {
    valuesCopy.additionalTravellers.forEach(traveller => traveller.dateOfBirth = dateToString(traveller.dateOfBirth, false));
  }

  valuesCopy.dateOfBirth = dateToString(valuesCopy.dateOfBirth, false);
  
  valuesCopy.numberOfAdditionalTravellers = valuesCopy.additionalTravellers?.length || 0;
  valuesCopy.numberOfAdditionalCitiesAndCountries = valuesCopy.additionalCitiesAndCountries?.length || 0;

  return valuesCopy;
}
