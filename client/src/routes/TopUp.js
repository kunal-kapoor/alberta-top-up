import React, { Suspense, lazy } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import LinearProgress from '@material-ui/core/LinearProgress'

import { Route as Routes, UserType, Roles, Scope } from 'constants/enums'
import { useAuth, useToast } from 'hooks'

const TopUpForm = lazy(() => import('pages/public/TopUp'))
const TopupConfirmation = lazy(() => import('pages/public/TopupConfirmation'))

const TopUpRoutes = () => {
  return (
    <Switch>
      <Route exact path={Routes.Root} component={TopUpForm} />
      <Route exact path={Routes.Confirmation} component={TopupConfirmation} />
    </Switch>
  )
}

const AppRoutes = () => {
  return (
    <Suspense fallback={<LinearProgress />}>
      <TopUpRoutes />
    </Suspense>
  )
}

export { AppRoutes as TopUpRoutes }