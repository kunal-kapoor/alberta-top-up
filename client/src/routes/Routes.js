import React, { Suspense, lazy, useEffect, useState } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Route as Routes, UserType, Roles, Scope } from '../constants';
import { AxiosPrivate } from '../utils';
import { useAuth, useToast } from '../hooks';
import { isBorderPilotApp } from '../appversion';
import { useOpenSignInWindow } from '../hooks/authentication-window';

const Form = lazy(() => import('../pages/public/Form'));
const DailySurvey = lazy(() => import('../pages/public/DailySurvey'));
const Confirmation = lazy(() => import('../pages/public/Confirmation'));
const Login = lazy(() => import('../pages/public/Login'));
const BackOfficeLookup = lazy(() => import('../pages/private/back-office/Lookup'));
const BackOfficeLookupLastName = lazy(() => import('../pages/private/back-office/LookupLastName'));
const BackOfficeLookupConfirmationNumber = lazy(() => import('../pages/private/back-office/LookupConfirmationNumber'));
const ServiceAlbertaLookup = lazy(() => import('../pages/private/service-alberta/Lookup'));
const ServiceAlbertaLookupConfirmationNumber = lazy(() => import('../pages/private/service-alberta/LookupConfirmationNumber'));
const ReportDashboard = lazy(() => import('../pages/private/service-alberta/ReportDashboard'));
const UserRegistration = lazy(() => import('../pages/private/admin/AccessControlDashboard'));
const EnrollmentForm = lazy(() => import('../pages/public/EnrollmentForm'));
const EligibilityForm = lazy(() => import('../pages/public/EligibilityForm'));
const EnrollmentConfirmation = lazy(() => import('../pages/public/EnrollmentConfirmation'));
const ArrivalTestingContactVerification = lazy(() => import('../pages/public/arrival-testing/ArrivalTestingContactVerification'));
const ArrivalTestingEnrollmentForm = lazy(() => import('../pages/public/arrival-testing/ArrivalTestingEnrollmentForm'));
const RtopAdminLookupArrivalTestingStatus = lazy(() => import('../pages/private/monitoring/LookupArrivalTestingStatus'));
const RtopAdminLookupDailyReports = lazy(() => import('../pages/private/monitoring/LookupDailyReports'));
const RtopAdminLookupDailyTravellerStatus = lazy(() => import('../pages/private/monitoring/LookupDailyTravellerStatus'));
const RtopAdminUploadNightlyReport = lazy(() => import('../pages/private/monitoring/UploadNightlyReport'));
const RtopReportHomeDashboard = lazy(() => import('../pages/private/monitoring/ReportDashboard'));
const AuthCallback = lazy(() => import('../pages/private/auth/AuthCallback'));

const redirectTarget = isBorderPilotApp()? Routes.RtopAdminLookupDailyReports.dynamicRoute(): Routes.ServiceAlbertaLookup.dynamicRoute()


const loggedInState = (res, userType) => {
  const { accessToken, idToken, accessTokenPayload, ...response } = res;
  
  // Provide fallback if accessToken/ID Token is not set
  // This is the case when auth does not happen at the ingress level
  // if(!accessToken || !idToken) { return null; }
  // The backend requires a token consisting of accessToken + idToken for auth purposes
  const combinedToken = accessToken && idToken && `${accessToken} ${idToken}`;
  const authissuer = 'GOA';
  // In case of 'OTHER' check the scope of the accessToken payload and get the user type -> ADMIN/SCREENER
  userType = (authissuer === Roles.OTHER && accessTokenPayload?.scope.includes(Scope.ADMIN)) ? UserType.FreshworksAdmin : userType || UserType.ServiceAlberta ;
  return { userType, user: { accessToken: combinedToken, authissuer,  idTokenPayload: {...response } }};
}

const PrivateGuard = ({ component: Component, loginRedirect, ...rest }) => {
  const { openToast } = useToast();
  const { clearAuthState, state: { userType, isAuthenticated } } = useAuth();
  const [isFetching, setFetching] = useState(false);
  const [redirect, setRedirect] = useState('');
  const { updateAuthState, isUsingAccessToken } = useAuth();

  useEffect(() => {
    (async () => {
      try {
        setFetching(true);

        // Valid JWT. Therefore they should see the private route.
        const user = await AxiosPrivate.get('/api/v1/auth/validate');
        const authObj = loggedInState(user.data || user, userType);
        if(authObj && !isUsingAccessToken) {

          // TODO: Uncomment to re-enable ingress auth support. Removed in 2.9
          // updateAuthState(authObj);
        }
      } catch (e) {

        // Invalid JWT. Therefore they should not see the private route.
        setRedirect(loginRedirect || {
          [UserType.BackOffice]: Routes.BackOfficeLogin,
          [UserType.ServiceAlberta]: Routes.ServiceAlbertaLogin,
        }[userType] || Routes.Root);

        // If they were authenticated, we want to display an error and clear the authentication state.
        // This only gets hit when JWT fails AND they refresh they page, NOT when they click the logout
        // button.
        if (isAuthenticated) {
          openToast({ status: 'error', message: 'Authentication error. Please login again' });
          clearAuthState();
        }
      } finally {
        setFetching(false);
      }
    })();
  }, [isAuthenticated]);

  /**
   * 1. Is it validating the JWT?
   *    - Show loading spinner.
   *
   * 2. Was a redirect set?
   *    - Go there.
   *
   * 3. Are they a Back Office user trying to access a Service Alberta route or vice versa?
   *    - Redirect them. Otherwise, show the component.
   */
  return isFetching ? <LinearProgress /> : (
    <Route {...rest} render={(props) => redirect ? <Redirect to={redirect} /> : {
      [UserType.BackOffice]: rest.location.pathname.includes('back-office') || rest.location.pathname.includes("reports")
        ? <Component {...props} />
        : <Redirect to={Routes.BackOfficeLookup} />,
      [UserType.ServiceAlberta]: isBorderPilotApp()
          || rest.location.pathname.includes('monitoring')
          || rest.location.pathname.includes('household')
          || rest.location.pathname.includes('service-alberta')
          || rest.location.pathname.includes("reports")
          || rest.location.pathname.includes("rtop-admin")
        ? <Component {...props} />
        : <Redirect to={redirectTarget} />,
      [UserType.FreshworksAdmin]: rest.location.pathname.includes('admin-lookup')
        ? <Component {...props} />
        : <Redirect to={Routes.AdminLookup} />,
      }[userType]}
    />
  );
};

const PublicGuard = ({ component: Component, ...rest }) => {
  console.log('pub guard')
  const { clearAuthState, state: { userType, isAuthenticated } } = useAuth();
  const [isFetching, setFetching] = useState(false);
  const [redirect, setRedirect] = useState('');
  const { updateAuthState, isUsingAccessToken } = useAuth();


  useEffect(() => {
    (async () => {

      // Is the user authenticated?
      if (isAuthenticated) {
        try {
          setFetching(true);
          const user = await AxiosPrivate.get('/api/v1/auth/validate');

          const authObj = loggedInState(user.data || user, userType);
          if(authObj && !isUsingAccessToken) {
            // TODO: Uncomment to re-enable ingress auth support. Removed in 2.9
            // updateAuthState(authObj);
          }
  
          // Valid JWT. Therefore they should not see the public route.
          setRedirect({
            [UserType.BackOffice]: Routes.BackOfficeLookup,
            [UserType.ServiceAlberta]: redirectTarget,
            [UserType.FreshworksAdmin]: Routes.AdminLookup,
          }[userType]);
        } catch (e) {

          // Invalid JWT. Therefore they should see the public route.
          clearAuthState();
        } finally {
          setFetching(false);
        }
      }
    })();
  }, [isAuthenticated]);

  /**
   * 1. Is it validating the JWT?
   *    - Show loading spinner.
   *
   * 2. Was a redirect set?
   *    - Go there.
   *
   * 3. No redirect set...
   *    - Show the component.
   */
  return isFetching ? <LinearProgress /> : (
    <Route {...rest} render={(props) => redirect
      ? <Redirect to={redirect} />
      : <Component {...props} />}
    />
  );
};

/**
 * Routes to include in the travelling facing app.
 */
const TravellerRoutes = () => (
  <Switch>
    <Route exact path={Routes.Root + 'fr'} component={Form} />
    <Route exact path={Routes.Root + 'en'} component={Form} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <Route render={() => <Redirect to={Routes.Root + 'en'} />} />
  </Switch>
);

/**
 * Routes to include in the admin facing app.
 */
const AdminRoutes = () => (
  <Switch>
    <Route exact path={Routes.AdminForm} component={Form} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <PublicGuard exact path={Routes.Root} component={Login} />
    <PublicGuard exact path={Routes.BackOfficeLogin} component={Login} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />
    <PrivateGuard exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard exact path={Routes.BackOfficeLookup} component={BackOfficeLookup} />
    <PrivateGuard exact path={Routes.BackOfficeLookupLastName.staticRoute} component={BackOfficeLookupLastName} />
    <PrivateGuard exact path={Routes.BackOfficeLookupConfirmationNumber.staticRoute} component={BackOfficeLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookup.staticRoute} component={ServiceAlbertaLookup} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookupConfirmationNumber.staticRoute} component={ServiceAlbertaLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.AdminLookup} component={UserRegistration} />
    <Route render={() => <Redirect to={Routes.Root} />} />
  </Switch>
);

/**
 * Routes to include in the travelling facing app.
 */
const RtopTravellerRoutes = () => (
  <Switch>
    <Route exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <Route exact path={Routes.EnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.EnrollmentForm} component={EnrollmentForm} />
    <Route exact path={Routes.EligibilityForm} component={EligibilityForm} />
    <Route exact path={Routes.ArrivalTestingEnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.ArrivalTestingContactVerification} component={ArrivalTestingContactVerification} />
    <Route exact path={Routes.ArrivalTestingEnrollmentForm} component={ArrivalTestingEnrollmentForm} />
    <Route render={() => <Redirect to={Routes.EligibilityForm} />} />
  </Switch>
);

/**
 * Routes to include in the monitoring facing app.
 */
/**
 * Routes to include in the monitoring facing app.
 */
const RtopAdminRoutes = () => (
  <Switch>
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.AdminLookup} component={UserRegistration} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminLookupConfirmationNumber.staticRoute} component={RtopAdminLookupDailyTravellerStatus} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminUploadNightlyReport} component={RtopAdminUploadNightlyReport} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminReportDashboard} component={RtopReportHomeDashboard} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminLookupDailyReports.staticRoute} component={RtopAdminLookupDailyReports} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminLookupArrivalTestingConfirmationNumber.staticRoute} component={RtopAdminLookupArrivalTestingStatus} />
    <Route render={() => <Redirect to={Routes.ServiceAlbertaLogin} />} />
  </Switch>
);

/**
 * All available routes.
 */
const AllRoutes = () => (
  <Switch>
    <Route exact path={`${Routes.RtopForm}/fr`} component={Form} />
    <Route exact path={`${Routes.RtopForm}/en`} component={Form} />
    <Route exact path={Routes.RtopForm} component={Form} />
    <Route exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <Route exact path={Routes.EnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.EnrollmentForm} component={EnrollmentForm} />
    <Route exact path={Routes.EligibilityForm} component={EligibilityForm} />
    <Route exact path={Routes.ArrivalTestingEnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.ArrivalTestingContactVerification} component={ArrivalTestingContactVerification} />
    <Route exact path={Routes.ArrivalTestingEnrollmentForm} component={ArrivalTestingEnrollmentForm} />
    <PublicGuard exact path={Routes.BackOfficeLogin} component={Login} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />
    <PrivateGuard exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard exact path={Routes.BackOfficeLookup} component={BackOfficeLookup} />
    <PrivateGuard exact path={Routes.BackOfficeLookupLastName.staticRoute} component={BackOfficeLookupLastName} />
    <PrivateGuard exact path={Routes.BackOfficeLookupConfirmationNumber.staticRoute} component={BackOfficeLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookup.staticRoute} component={ServiceAlbertaLookup} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookupConfirmationNumber.staticRoute} component={ServiceAlbertaLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.AdminLookup} component={UserRegistration} />
    <PrivateGuard exact path={Routes.RtopAdminLookupDailyReports.staticRoute} component={RtopAdminLookupDailyReports} />
    <PrivateGuard exact path={Routes.RtopAdminLookupConfirmationNumber.staticRoute} component={RtopAdminLookupDailyTravellerStatus} />
    <PrivateGuard exact path={Routes.RtopAdminLookupArrivalTestingConfirmationNumber.staticRoute} component={RtopAdminLookupArrivalTestingStatus} />
    <PrivateGuard exact path={Routes.RtopAdminUploadNightlyReport} component={RtopAdminUploadNightlyReport} />
    <PrivateGuard exact path={Routes.RtopAdminReportDashboard} component={RtopReportHomeDashboard} />
    <Route render={() => <Redirect to={Routes.EligibilityForm} />} />
  </Switch>
);

/**
 * Routes to include when the app loads after re-authenticating with AppID.
 * This happens when a session has expired and you click "Re-authenticate"
 * in the modal that pops up when your session expires.
 */
const ReauthenticatingRoute = () => (
  <Switch>
    <Route exact path={Routes.AuthCallback} component={AuthCallback} />
    <Route render={() => <Redirect to={Routes.AuthCallback} />} />
  </Switch>
);

/**
 * Only show the Authentication successful page if the window
 * that opened this app is reauthenticating (when a user clicks "Re-authenticate"
 * in the modal that pops up when a session has expired
 */ 
const isReauthenticating = () => !!window?.opener?.isReauthenticating;

const AppRoutes = () => {

  let appOutput = process.env.REACT_APP_OUTPUT || 'ALL';

  // Include rtop traveller routes if
  // running on border pilot domain
  if(appOutput === 'TRAVELLER' && isBorderPilotApp()) {
    appOutput = 'RTOP_TRAVELLER';
  }

  // Include rtop admin routes if
  // running on border pilot domain
  if(appOutput === 'ADMIN' && isBorderPilotApp()) {
    appOutput = 'RTOP_ADMIN';
  }

  /**
   * Choose which routes to include based on
   * the REACT_APP_OUTPUT environment variable.
   */
  const EnvRoutes = isReauthenticating()? ReauthenticatingRoute: {
    'TRAVELLER': TravellerRoutes,
    'ADMIN': AdminRoutes,
    'RTOP_TRAVELLER': RtopTravellerRoutes,
    'RTOP_ADMIN': RtopAdminRoutes,
    'ALL': AllRoutes,
  }[appOutput];

  /**
   * Prompt for reauthentication if a 401 is encountered
   * or an undefinable network error happens
   * - For example if POST request gets 302'd to the APPID login page because of session expiry. Axios does not
   * provide a way for handle this more gracefully.
   */
  const {openAuthRequiredPrompt} = useOpenSignInWindow();

  useEffect(() => {
    AxiosPrivate.interceptors.response.use((response) => response.data, error => {
      if(error.message === "Network Error" && !error?.response || error?.response?.status === 401) {
        // TODO: Re-enable prompt for re-authentication (excluded from 2.9 release)
        // openAuthRequiredPrompt();
      }
      
      return Promise.reject(error);
    });  
  }, []);

  return (
    <Suspense fallback={<LinearProgress />}>
      <EnvRoutes />
    </Suspense>
  );
}

export { AppRoutes as Routes };
