import * as yup from 'yup';
import moment from 'moment';

import { stringToDate, isAirport } from '../utils';
import {
  Questions,
  DifficultyOptions,
  ContactOptions,
  BackOfficeDeterminations,
  ServiceAlbertaStatuses,
  AirportOrBorderCrossings,
  Provinces,
  Countries,
  Places,
  Vehicles,
  Question,
  Place,
  Gender,
  CitizenshipStatus,
  Vehicle,
  Province,
  ExemptionType,
  ExemptOccupation,
  StayDuration,
  ContactAdminOptions,
  EmploymentStatus,
  EmploymentIndustry,
  AdultsInHousehold, 
  ChildrenInHousehold,
  TeamHelpful,
  PotentialLostIncomeDays,
  CostPaidPerPerson,
  VaccinationTimeframe,
  VaccinationDoseCount,
  AirlinesAndAbbreviationValues,
} from '.';
import { EligibilityQuestions } from './eligibilityQuestions';

const mappedQuestions = Questions.map(item => item.value);
const mappedDifficultyOptions = DifficultyOptions.map(item => item.value);
const mappedContactAdminOptions = ContactAdminOptions.map(item => item.value);
const mappedBackOfficeDeterminations = BackOfficeDeterminations.map(item => item.value);
const mappedServiceAlbertaStatuses = ServiceAlbertaStatuses.map(item => item.value);
const mappedRequiredNotesStatuses = ServiceAlbertaStatuses.filter(item=>item.notes).map(item=>item.value);
const mappedAirportOrBorderCrossings = AirportOrBorderCrossings.map(item => item.value);
const mappedGender = Gender.map(item => item.value);
const mappedProvinces = Provinces.map(item => item.value);
const mappedCountries = Countries.map(item => item.value);
const mappedPlaces = Places.map(item => item.value);
const mappedVehicles = Vehicles.map(item => item.value);
const mappedCitizenshipStatus = CitizenshipStatus.map(item => item.value);
const mappedExemptionType = ExemptionType.map(item => item.value);
const mappedExemptOccupation = ExemptOccupation.map(item => item.value);
const mappedVaccinationTimeframe = VaccinationTimeframe.map(item => item.value);
const mappedVaccinationDoseCount = VaccinationDoseCount.map(item => item.value);
const mappedDurationOfStay = StayDuration.map(item => item.value);
const mappedTeamHelpful = TeamHelpful.map(item => item.value)
const mappedEmploymentStatus = EmploymentStatus.map(item => item.value);
const mappedEmploymentIndustry = EmploymentIndustry.map(item => item.value);
const mappedAdultsOption = AdultsInHousehold.map(item => item.value);
const mappedChildrenOption = ChildrenInHousehold.map(item => item.value);
const mappedPotentialLostIncomeDays = PotentialLostIncomeDays.map(item => item.value);
const mappedCostPaidPerPerson = CostPaidPerPerson.map(item => item.value);

const validateDateString = (s) => /^\d{4}\/\d{2}\/\d{2}$/.test(s);
const validateDateTime = (s) => stringToDate(s).isSameOrBefore(moment());
const isDateTodayOrEarlier = (s) => stringToDate(s).isSameOrBefore(moment());
const isDateWithinFiveDays = (s) => stringToDate(s).isSameOrAfter(moment().subtract(5,'d'), "day" );
const isDateAfterOneDayAgo = (s) => stringToDate(s).isSameOrAfter(moment().subtract(1, 'd'), "day");
const isDateBeforeFiveDaysFromNow = (s) => stringToDate(s).isSameOrBefore(moment().add(5, 'd'), "day");
const validateDateStringIfProvided = (s) => s? validateDateString(s) : true;
const validatePastDateTime = (s) => stringToDate(s).isBefore(moment(), "day");
const validateDynamicChecked = (s) => Object.values(s).find(({checked}) => checked);
// checks if any option is selected and if so then the days are selected and if the days are selected, the total number of days has a max of 14
const validateTotalDaysOfUse = (s) => {
  const filteredObjects = Object.values(s).filter(({ checked }) => checked);
  const totalCount = filteredObjects.length > 0 && filteredObjects.reduce((prev, cur) => prev + parseFloat(cur.count), 0);
  return totalCount && !isNaN(totalCount) && (totalCount > 0 && totalCount <= 14);
};

const validateIsAirport = (s) => {
  
}

export const notesRequiredForStatus = status => mappedRequiredNotesStatuses.includes(status);

export const BackOfficeUnwillingTravellerSchema = yup.object().shape({
  counter:yup.number().required('The number of travellers must be at least 1').min(1).max(10),
  notes: yup.string().max(5000,"Notes cannot have more than 5000 characters")
})

export const BackOfficeDeterminationSchema = yup.object().shape({
  determination: yup.string().required('Determination is required').oneOf(mappedBackOfficeDeterminations, 'Invalid determination'),
  notes: yup.string().required('Notes are required').max(2048, 'Notes must be 2048 characters or less'),
});

export const ServiceAlbertaDeterminationSchema = yup.object().shape({
  status: yup.string().required('Status is required').oneOf(mappedServiceAlbertaStatuses, 'Invalid status'),
  note: yup.string().when(["status"],(status,schema)=>{
      return notesRequiredForStatus(status)?
      schema.required('Notes are required for this field.').max(2048, 'Notes must be 2048 characters or less')
      : schema.max(2048, 'Notes must be 2048 characters or less')
  }),
});

const QuarantineLocationShape = yup.object().when('hasPlaceToStayForQuarantine', {
  is: Question.Yes,
  then: yup.object().shape({
    address: yup.string().required("Address is required").max(1024, "Address must be less than 1024 characters"),
    cityOrTown: yup.string().required("City or town is required").max(255, "City/town must be less than 255 characters"),
    provinceTerritory: yup.string().required("Province/Territory is required").oneOf(mappedProvinces, "Invalid option"),
    provinceTerritoryDetails: yup.string().when('provinceTerritory', {
      is: Province.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
    postalCode: yup.string().max(32, "Postal code must be less than 32 characters").matches(/^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] {0,1}-{0,1}[0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$/, 'Please provide a valid postal code'),
    phoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
    typeOfPlace: yup.string().required("Type of place is required").oneOf(mappedPlaces, "Invalid option"),
    typeOfPlaceDetails: yup.string().when('typeOfPlace', {
      is: Place.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
    howToGetToPlace: yup.string().required("Type of transportation is required").oneOf(mappedVehicles, "Invalid option"),
    howToGetToPlaceDetails: yup.string().when('howToGetToPlace', {
      is: Vehicle.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
    otherPeopleResiding: yup.string().required("Must specify if people are residing at that location who are not travelling with you today").oneOf(mappedQuestions, "Invalid option"),
    doesVulnerablePersonLiveThere: yup.string().required("Must specify if vulnerable people live with you").oneOf(mappedQuestions, "Invalid option"),
  })});

export const EditFormSchema = yup.object().shape({
  hasPlaceToStayForQuarantine: yup.string().required("Must specify if you have a place to stay").oneOf(mappedQuestions, "Invalid option"),
  quarantineLocation: QuarantineLocationShape,
});

export const FormSchema = yup.object().shape({

  // Primary Contact.
  firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
  lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
  dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
  phoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
  email: yup.string().matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address"),

  // Travel Information.
  hasAdditionalTravellers: yup.string().required("Must specify if there are additional travellers").oneOf(mappedQuestions, "Invalid option"),
  numberOfAdditionalTravellers: yup.number().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.number().min(1, "At least 1 traveller is required").max(10, "Travellers must be 10 or less"),
  }),
  additionalTravellers: yup.array().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.array().of(yup.object().shape({
      firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
      lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
      dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
    })),
  }),

  // Arrival Information.
  nameOfAirportOrBorderCrossing: yup.string().required("Name of airport or border crossing is required").oneOf(mappedAirportOrBorderCrossings, "Invalid option"),
  arrivalDate: yup.string().required("Arrival date is required").test('is-date', "Not a valid date", validateDateString).test('is-not-past-5-days',"Arrival Date must not be past 5 days",isDateWithinFiveDays),
  arrivalCityOrTown: yup.string().required("Arrival city/town is required").max(255, "Arrival city/town must be less than 255 characters"),
  arrivalCountry: yup.string().required("Arrival country is required").oneOf(mappedCountries, "Invalid option"),
  numberOfAdditionalCitiesAndCountries: yup.number().min(0, "Places must be 0 or more").max(10, "Places must be 10 or less"),
  additionalCitiesAndCountries: yup.array().of(yup.object().shape({
    cityOrTown: yup.string().required("City or town is required").max(255, "City/town must be less than 255 characters"),
    country: yup.string().required("Country is required").oneOf(mappedCountries, "Invalid option"),
  })),

  // Isolation Questionnaire.
  hasPlaceToStayForQuarantine: yup.string().required("Must specify if you have a place to stay").oneOf(mappedQuestions, "Invalid option"),
  quarantineLocation: QuarantineLocationShape,
  isAbleToMakeNecessaryArrangements: yup.string().required("Must specify if you are able to make the necessary arrangements").oneOf(mappedQuestions, "Invalid option"),
});

export const AccessControlValidationSchema = yup.object().shape({
  reportName: yup.string().required("Must select atleast one report option"),
  agentEmail: yup.string().required("Agent email is required").matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address")
});

/**
 * Construct validation schema from eligibility questions.
 * They all are yes/no answers that are required.
 */
export const EligibilityCheckValidationSchema = yup.object().shape(
  EligibilityQuestions.reduce((validationObj, {name, required}) => {
    validationObj[name] = yup.string().required(required).oneOf(mappedQuestions, 'Invaild Option');

    return validationObj;
  }, {})
);

export const ContactValidationSchema = yup.object().shape({
  contactMethod: yup.string().required("Must specify a contact preference"),
  contactEmail: yup.string().when('contactMethod', {
    is: ContactOptions[0].value,
    then: yup.string().required("Must specify email address").matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address")
  }),
  contactPhoneNumber: yup.string().when('contactMethod', {
    is: ContactOptions[1].value,
    then: yup.string().required("Must specify phone number").max(35, "Phone number must be less than 35 characters"),
  }),
});

export const EnrollmentFormSchema = yup.object().shape({

  // Primary Contact.
  firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
  lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
  dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
  phoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
  email: yup.string().required("Email is required").matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address"),
  // confirmEmail: yup.string().test('matches-email', "Email fields must match")
  confirmEmail: yup.string()
    .when('contactMethod', {
    is: ContactAdminOptions[1].value,
    then: yup.string().oneOf([yup.ref('email')], 'Email fields must match').required("Must confirm email")
    })
    .when('contactMethod', {
      is: ContactAdminOptions[2].value,
      then: yup.string().oneOf([yup.ref('email')], 'Email fields must match').required("Must confirm email")
      }),
  gender: yup.string().required("Must specify gender").oneOf(mappedGender, "Invalid option"),
  citizenshipStatus: yup.string().required("Must specify citizenship status").oneOf(mappedCitizenshipStatus, "Invalid option"),
  exemptionType: yup.string().required("Must specify exempt status").oneOf(mappedExemptionType, "Invalid option"),
  exemptOccupation: yup.string().when('exemptionType', {
    is: ExemptionType[0].value,
    then: yup.string().required("Must specify occupation type/category").oneOf(mappedExemptOccupation)
  }),
  exemptOccupationDetails: yup.string().when('exemptOccupation', {
    is: (value) => value && value !== 'Other',
    then: yup.string().required("Must specify occupation details")
  }),
  noPreDepartureTest:  yup.bool(),
  preDepartureTestDate: yup.string().when(['exemptionType','noPreDepartureTest', 'nameOfAirportOrBorderCrossing'], {
    is: (exemptionType, noPreDepartureTest, nameOfAirportOrBorderCrossing) => (
      exemptionType === mappedExemptionType[0]
        &&
      noPreDepartureTest !== true
        &&
      isAirport(nameOfAirportOrBorderCrossing)
    ),
    then: yup.string().required("Must specify your pre-departure test date").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Test Date cannot be greater than yesterday", validatePastDateTime).test('is-not-past-5-days',"Test Date must not be past 5 days",isDateWithinFiveDays),
  }),
  preDepartureTestCountry: yup.string().when(['exemptionType','noPreDepartureTest', 'nameOfAirportOrBorderCrossing'], {
    is: (exemptionType, noPreDepartureTest, nameOfAirportOrBorderCrossing) => (
      exemptionType === mappedExemptionType[0]
        &&
      noPreDepartureTest !== true
        &&
      isAirport(nameOfAirportOrBorderCrossing)
    ),
    then: yup.string().required("Must specify pre-departure test country"),
  }),
  preDepartureVaccination: yup.string().required("Must specify if you have received a vaccine").oneOf(mappedQuestions, "Invalid option"),
  timeSinceVaccination: yup.string().when('preDepartureVaccination', {
    is: Questions[0].value,
    then: yup.string().required("Must specify vaccination timeframe").oneOf(mappedVaccinationTimeframe)
  }),
  dosesOfVaccine: yup.string().when('preDepartureVaccination', {
    is: Questions[0].value,
    then: yup.string().required("Must specify number of doses").oneOf(mappedVaccinationDoseCount)
  }),

  // Travel Information.
  hasAdditionalTravellers: yup.string().required("Must specify if there are additional travellers").oneOf(mappedQuestions, "Invalid option"),
  numberOfAdditionalTravellers: yup.number().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.number().min(1, "At least 1 traveller is required").max(10, "Travellers must be 10 or less"),
  }),
  additionalTravellers: yup.array().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.array().of(yup.object().shape({
      firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
      lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
      dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
      gender: yup.string().required("Must specify gender").oneOf(mappedGender, "Invalid option"),
      citizenshipStatus: yup.string().required("Must specify citizenship status").oneOf(mappedCitizenshipStatus, "Invalid option"),
      exemptionType: yup.string().required("Must specify exempt status").oneOf(mappedExemptionType, "Invalid option"),
      exemptOccupation: yup.string().when('exemptionType', {
        is: ExemptionType[0].value,
        then: yup.string().required("Must specify occaption type/category").oneOf(mappedExemptOccupation)
      }),
      
      exemptOccupationDetails: yup.string().when('exemptOccupation', {
        is: (value) => value && value !== 'Other',
        then: yup.string().required("Must specify occupation details")
      }),

      reasonForTravel: yup.string(),
      durationOfStay: yup.string(),
      seatNumber: yup.string().max(35, "Seat number must be less than 35 characters"),
      noPreDepartureTest:  yup.bool(),
      /** 
       * explicit function definition required to have access to 'this' context. 
       * using arrow functions therefore do not allow access to the context, or by extension,
       * the parent schema
       */
      preDepartureTestDate: yup.string().test('is-not-land-border', 'Enter a valid date within the past 5 days', function(val) {
        // this.from[0] is the current (child) schema, this.from[1] is the parent schema
        return(
          this.from[0].value.exemptionType === mappedExemptionType[0]
            &&
          this.from[0].value.noPreDepartureTest !== true
            &&
          isAirport(this.from[1].value.nameOfAirportOrBorderCrossing) 
            ? 
              yup.string()
              .required()
              .isValidSync(val) 
            : 
              true
        )
        // val == null check for non-required field, returns early.
      }).test('is-date', "Not a valid date", val => {
        return val == null || validateDateString(val);
      })
      .test('is-not-future', "Test Date cannot be greater than yesterday", val => {
        return val == null || validatePastDateTime(val);
      })
      .test('is-not-past-5-days',"Test Date must not be past 5 days", val => {
        return val == null || isDateWithinFiveDays(val);
      }),
      /** 
       * explicit function definition required to have access to 'this' context. 
       * using arrow functions therefore do not allow access to the context, or by extension,
       * the parent schema.
       * 
       * Additionally, no 'return early' tests are required as there is no .test() chain that needs to be followed.
       */
      preDepartureTestCountry: yup.string().test('is-not-land-border', 'Must specify pre-departure test country', function(val) {
        // this.from[0] is the current (child) schema, this.from[1] is the parent schema
        return(
          this.from[0].value.exemptionType === mappedExemptionType[0]
            &&
          this.from[0].value.noPreDepartureTest !== true
            &&
          isAirport(this.from[1].value.nameOfAirportOrBorderCrossing) 
            ? 
              yup.string()
              .required("Must specify pre-departure test country")
              .isValidSync(val)
            : 
              true
        )
      }),
      preDepartureVaccination: yup.string().required("Must specify if you have received a vaccine").oneOf(mappedQuestions, "Invalid option"),
      timeSinceVaccination: yup.string().when('preDepartureVaccination', {
        is: Questions[0].value,
        then: yup.string().required("Must specify vaccination timeframe").oneOf(mappedVaccinationTimeframe)
      }),
      dosesOfVaccine: yup.string().when('preDepartureVaccination', {
        is: Questions[0].value,
        then: yup.string().required("Must specify number of doses").oneOf(mappedVaccinationDoseCount)
      }),
    })),
  }),
  numberOfAdditionalCitiesAndCountries: yup.number().min(0, "Places must be 0 or more").max(10, "Places must be 10 or less"),
  additionalCitiesAndCountries: yup.array().of(yup.object().shape({
    cityOrTown: yup.string().required("City or town is required").max(255, "City/town must be less than 255 characters"),
    country: yup.string().required("Country is required").oneOf(mappedCountries, "Invalid option"),
  })),

  // Arrival Information.
  nameOfAirportOrBorderCrossing: yup.string().required("Name of airport or border crossing is required").oneOf(mappedAirportOrBorderCrossings, "Invalid option"),
  arrivalDate: yup.string().required("Arrival date is required").test('is-date', "Not a valid date", validateDateString).test('is-not-past-1-days',"Arrival Date must not be further than 1 day ago",isDateAfterOneDayAgo).test('is-not-after-5-days',"Arrival Date must not be after 5 days from today",isDateBeforeFiveDaysFromNow),
  arrivalCityOrTown: yup.string().required("Arrival city/town is required").max(255, "Arrival city/town must be less than 255 characters"),
  arrivalCountry: yup.string().required("Arrival country is required").oneOf(mappedCountries, "Invalid option"),

  dateLeftCanada: yup.string().test('is-date', "Not a valid date", validateDateStringIfProvided),
  airline: yup.string(),
  airlineDetails: yup.string().when('airline', {
    is: AirlinesAndAbbreviationValues.Other,
    then: yup.string().required("Airline Details are required").max(35, "Details must be less than 35 characters"),
  }),
  flightNumber: yup.string().max(35, "Flight number must be less than 35 characters"),
  seatNumber: yup.string().max(35, "Seat number must be less than 35 characters"),
  // dateLeftCanada: yup.string().test('is-date', "Not a valid date", validateDateStringIfProvided).test('is-today-or-earlier', 'Date Left Canada cannot be in the future', isDateTodayOrEarlier),
  countryOfResidence: yup.string().required("Country of residence is required").oneOf(mappedCountries, "Invalid option"),
  reasonForTravel: yup.string(),
  durationOfStay: yup.string().required("Must specify duration of stay").oneOf(mappedDurationOfStay, "Invalid option"),
  dateLeavingCanada: yup.string().when('exemptionType', {
    is: ExemptionType[0].value,
    then: yup.string().when('durationOfStay', {
      is: StayDuration[0].value,
      then: yup.string().required('Must specify date leaving Canada')
    })
  }),

    // Isolation Questionnaire.
    address: yup.string().required("Address is required").max(1024, "Address must be less than 1024 characters"),
    cityOrTown: yup.string().required("City or town is required").max(255, "City/town must be less than 255 characters"),
    provinceTerritory: yup.string().required("Province/Territory is required").oneOf(mappedProvinces, "Invalid option"),
    provinceTerritoryDetails: yup.string().when('provinceTerritory', {
      is: Province.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
    postalCode: yup.string().max(32, "Postal code must be less than 32 characters").matches(/^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] {0,1}-{0,1}[0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$/, 'Please provide a valid postal code'),
    quarantineLocationPhoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
    typeOfPlace: yup.string().required("Type of place is required").oneOf(mappedPlaces, "Invalid option"),
    typeOfPlaceDetails: yup.string().when('typeOfPlace', {
      is: Place.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
    howToGetToPlace: yup.string().required("Type of transportation is required").oneOf(mappedVehicles, "Invalid option"),
    howToGetToPlaceDetails: yup.string().when('howToGetToPlace', {
      is: Vehicle.Other,
      then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
    }),
});

export const ArrivalTestingEnrollmentFormSchema = yup.object().shape({

  // Primary Contact.
  firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
  lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
  dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
  phoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
  email: yup.string().required("Email is required").matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address"),
  // confirmEmail: yup.string().test('matches-email', "Email fields must match")
  confirmEmail: yup.string()
    .when('contactMethod', {
      is: ContactAdminOptions[1].value,
      then: yup.string().oneOf([yup.ref('email')], 'Email fields must match').required("Must confirm email")
    })
    .when('contactMethod', {
      is: ContactAdminOptions[2].value,
      then: yup.string().oneOf([yup.ref('email')], 'Email fields must match').required("Must confirm email")
    }),

  // Travel Information.
  hasAdditionalTravellers: yup.string().required("Must specify if there are additional travellers").oneOf(mappedQuestions, "Invalid option"),
  numberOfAdditionalTravellers: yup.number().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.number().min(1, "At least 1 traveller is required").max(10, "Travellers must be 10 or less"),
  }),
  additionalTravellers: yup.array().when('hasAdditionalTravellers', {
    is: Question.Yes,
    then: yup.array().of(yup.object().shape({
      firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
      lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
      dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
    })),
  }),

  // Arrival Information.
  nameOfAirportOrBorderCrossing: yup.string().required("Name of airport or border crossing is required").oneOf(mappedAirportOrBorderCrossings, "Invalid option"),
  arrivalDate: yup.string().required("Arrival date is required").test('is-date', "Not a valid date", validateDateString).test('is-not-past-1-days',"Arrival Date must not be further than 1 day ago",isDateAfterOneDayAgo).test('is-not-after-5-days',"Arrival Date must not be after 5 days from today",isDateBeforeFiveDaysFromNow),
  arrivalCityOrTown: yup.string().required("Arrival city/town is required").max(255, "Arrival city/town must be less than 255 characters"),
  arrivalCountry: yup.string().required("Arrival country is required").oneOf(mappedCountries, "Invalid option"),

  // Isolation Questionnaire.
  address: yup.string().required("Address is required").max(1024, "Address must be less than 1024 characters"),
  cityOrTown: yup.string().required("City or town is required").max(255, "City/town must be less than 255 characters"),
  provinceTerritory: yup.string().required("Province/Territory is required").oneOf(mappedProvinces, "Invalid option"),
  provinceTerritoryDetails: yup.string().when('provinceTerritory', {
    is: Province.Other,
    then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
  }),
  postalCode: yup.string().max(32, "Postal code must be less than 32 characters").matches(/^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] {0,1}-{0,1}[0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$/, 'Please provide a valid postal code'),
  quarantineLocationPhoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
  typeOfPlace: yup.string().required("Type of place is required").oneOf(mappedPlaces, "Invalid option"),
  typeOfPlaceDetails: yup.string().when('typeOfPlace', {
    is: Place.Other,
    then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
  }),
  howToGetToPlace: yup.string().required("Type of transportation is required").oneOf(mappedVehicles, "Invalid option"),
  howToGetToPlaceDetails: yup.string().when('howToGetToPlace', {
    is: Vehicle.Other,
    then: yup.string().required("Details are required").max(512, "Details must be less than 512 characters"),
  }),
});

export const DailySurveySymptomsSchema = {
  coughSymptom: yup.string().required("Must specify if you have new or worsening cough").oneOf(mappedQuestions, "Invalid option"),
  shortnessOfBreathSymptom: yup.string().required("Must specify if you have shortness of breath/difficulty breathing").oneOf(mappedQuestions, "Invalid option"),
  temperatureSymptom: yup.string().required("Must specify if your temperature is equal to or over 38°C").oneOf(mappedQuestions, "Invalid option"),
  feverishSymptom: yup.string().required("Must specify if you are feeling feverish, chills, fatigue or weakness").oneOf(mappedQuestions, "Invalid option"),
  muscleAchesSymptom: yup.string().required("Must specify if you have muscle or body aches").oneOf(mappedQuestions, "Invalid option"),
  lossOfSmellSymptom: yup.string().required("Must specify if you have new loss of smell or taste").oneOf(mappedQuestions, "Invalid option"),
  headacheSymptom: yup.string().required("Must specify if you have a headache").oneOf(mappedQuestions, "Invalid option"),
  gastrointestinalSymptom: yup.string().required("Must specify if you have gastrointestinal symptoms like abdominal pain, diarrhea, vomiting, or feeling very unwell").oneOf(mappedQuestions, "Invalid option"),
};

export const DailySurveySymptoms = yup.object().shape(DailySurveySymptomsSchema);

export const DailySurveyOthersSchema = {
  usingABTraceTogether: yup.string().required("Must specify if you have the ABTraceTogether App").oneOf(mappedQuestions, "Invalid option")
};

export const DailySurveyOthers = yup.object().shape({
  ...DailySurveySymptomsSchema,
  ...DailySurveyOthersSchema
});

export const DailySurveyFlightInformationSchema = yup.object().shape({
  ...DailySurveySymptomsSchema,
  ...DailySurveyOthersSchema,
  noPreDepartureTest: yup.boolean(),
  preDepartureTestDate: yup.string().when('noPreDepartureTest', {
    is: (noPreDepartureTest) =>  noPreDepartureTest !== true,
    then: yup.string().required("Must specify your pre-departure test date").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Test Date cannot be greater than yesterday", validatePastDateTime).test('is-not-past-5-days',"Test Date must not be past 5 days",isDateWithinFiveDays),
  }),
  preDepartureTestCountry: yup.string().when('noPreDepartureTest', {
    is: (noPreDepartureTest) =>  noPreDepartureTest !== true,
    then: yup.string().required("Must specify pre-departure test country"),
  }),
  airline: yup.string().required("Must specify your Airlines"),
  airlineDetails: yup.string().when('airline', {
    is: AirlinesAndAbbreviationValues.Other,
    then: yup.string().required("Airline Details are required").max(35, "Details must be less than 35 characters"),
  }),
  flightNumber: yup.string().required("Must specify your flight number").max(35, "Flight number must be less than 35 characters"),
  seatNumber: yup.string().required("Must specify your seat number").max(35, "Seat number must be less than 35 characters"),
});

export const HouseholdMember =  yup.object().shape({
  birthday: yup.string().required()
})

export const PrimaryContact = yup.object().shape({
  email: yup.string().required("Must specify email address").matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address"),
  phoneNumber: yup.string().required("You must provide a contact phone number"),
  arrivalDate: yup.string().required("Arrival date is required").test('is-date', "Not a valid date", validateDateString),
  contactMethod: yup.string().required("Must specify a contact preference").oneOf(mappedContactAdminOptions, "Invalid option"),
})

export const SmsVerification = yup.object().shape({
  verificationCode: yup.string().matches(/^[0-9]{4,10}$/, 'Verification code must be between 4 and 10 numbers long').required("Verification code must be between 4 and 10 numbers long")
});

export const RTOPExportReportValidationSchema = yup.object().shape({
  type: yup.string().required("Enrollment type is required"),
  date: yup.string().required("Date is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date cannot be greater than yesterday", validatePastDateTime)
});

export const RTOPReportStatsValidationSchema = yup.object().shape({
  type: yup.string().required("Enrollment type is required"),
  date: yup.string().required("Date is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date cannot be greater than yesterday", validatePastDateTime)
});

export const DayFourSurveyValidationSchema = yup.object().shape({
  difficultyCompletingRegistration: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  difficultyGettingConfirmationId: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  difficultyFindingStaff: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  easeOfUnderstandingProgram: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  difficultyLocatingBooth: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  receivedTestResults: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  acceptableResponseTime: yup.string().when('receivedTestResults', {
    is: Questions[0].value,
    then: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  }),
  acceptableWaitTime: yup.string().when('receivedTestResults', {
    is: Questions[1].value,
    then: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  }),
  difficultyAccessingResult: yup.string().when('receivedTestResults', {
    is: Questions[0].value,
    then: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  }),  
  difficultyCompletingCheckin: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  teamHelpful: yup.string().required("This question is required").oneOf(mappedTeamHelpful, "Invalid option"),
  adultsInHousehold: yup.string().required("This question is required").oneOf(mappedAdultsOption, "Invalid option"),
  childrenInHousehold: yup.string().required("This question is required").oneOf(mappedChildrenOption, "Invalid option"),
  employmentStatus: yup.string().required("This question is required").oneOf(mappedEmploymentStatus, "Invalid option"),
  employmentIndustry: yup.string().when('employmentStatus', {
    is: EmploymentStatus[0].value,
    then: yup.string().required("This question is required").oneOf(mappedEmploymentIndustry, "Invalid option"),
  })
})

export const DayThirteenSurveyValidationSchema = yup.object().shape({
  difficultyCompletingCheckin: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  difficultyDlingABTrace: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  difficultyBookingSecondTest: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  difficultyGettingSecondTest: yup.string().required("This question is required").oneOf(mappedDifficultyOptions, "Invalid option"),
  acceptableSecondTimeLength: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  potentialTravel: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  potentialLostIncome: yup.string().when('potentialTravel', {
    is: Questions[0].value,
    then: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  }),
  potentialLostIncomeDays: yup.object().when('potentialLostIncome', {
    is: Questions[0].value,
    then: yup.object().test('is-checked','Select atleast one option',validateDynamicChecked).test('is-days-selected','The total number of days must be selected and less than or equal to 14',validateTotalDaysOfUse),
  }),
  teamHelpful: yup.string().required("This question is required").oneOf(mappedTeamHelpful, "Invalid option"),
  wouldUseAgain: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  wouldCoverCost: yup.string().required("This question is required").oneOf(mappedQuestions, "Invalid option"),
  costPerPerson: yup.string().when('wouldCoverCost', {
    is: Questions[0].value,
    then: yup.string().required("This question is required").oneOf(mappedCostPaidPerPerson, "Invalid option"),
  }),
})

// TOP-UP Schemas start here

export const TopUpFormSchema = yup.object().shape({
  firstName: yup.string().required("First name is required").max(255, "First name must be less than 255 characters"),
  lastName: yup.string().required("Last name is required").max(255, "Last name must be less than 255 characters"),
  dateOfBirth: yup.string().required("Date of birth is required").test('is-date', "Not a valid date", validateDateString).test('is-not-future', "Date of birth cannot be greater than today", validateDateTime),
  phoneNumber: yup.string().required("Phone number is required").max(35, "Phone number must be less than 35 characters"),
  email: yup.string().matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, "Invalid email address"),
  sin: yup.string().matches(/^([0-9_\-\.]+)$/, "Invalid SIN"),
})