import { isBorderPilotApp } from "../appversion";

const monitoringPrefix = isBorderPilotApp() && process.env.REACT_APP_OUTPUT === 'RTOP_ADMIN' ? '' : 'monitoring/';
const rtopTravellerPrefix = isBorderPilotApp() ? '' : 'enroll/';

export const Route = Object.freeze({

    // Shared routes.
    Root: '/',
    Daily: '/daily',
    Confirmation: '/confirmation',

    // Admin routes.
    AdminForm: '/form',
    BackOfficeLogin: '/back-office-login',
    ServiceAlbertaLogin: '/service-alberta-login',
    BackOfficeLookup: '/back-office-lookup',
    BackOfficeLookupLastName: {
        staticRoute: '/back-office-lookup-last-name/:query?',
        dynamicRoute: (params = '') => `/back-office-lookup-last-name${params}`,
    },
    BackOfficeLookupConfirmationNumber: {
        staticRoute: '/back-office-lookup-confirmation-number/:confirmationNumber',
        dynamicRoute: (confirmationNumber) => `/back-office-lookup-confirmation-number/${confirmationNumber}`,
    },
    ServiceAlbertaLookup: {
        staticRoute: '/service-alberta-lookup/:query?/:filter?/:orderBy?/:order?/:page?',
        dynamicRoute: (params = '') => `/service-alberta-lookup${params}`,
    },
    ServiceAlbertaLookupConfirmationNumber: {
        staticRoute: '/service-alberta-lookup-confirmation-number/:confirmationNumber',
        dynamicRoute: (confirmationNumber) => `/service-alberta-lookup-confirmation-number/${confirmationNumber}`,
    },
    RtopAdminLookupDailyReports: {
        staticRoute: `/${monitoringPrefix}:query?/:filter?/:orderBy?/:order?/:page?`,
        dynamicRoute: (params = '') => `/${monitoringPrefix}${params}`,
    },
    RtopAdminLookupConfirmationNumber: {
        staticRoute: `/household/:confirmationNumber`,
        dynamicRoute: (confirmationNumber) => `/household/${confirmationNumber}`,
    },
    RtopAdminUploadNightlyReport: '/rtop-admin-upload-report',
    RtopAdminReportDashboard: '/rtop-admin-report-dashboard',
    AdminLookup: '/admin-lookup',
    ReportDashboard: '/reports',

    // Pilot program traveller routes
    EligibilityForm: `/${rtopTravellerPrefix}`,
    EnrollmentForm: '/enroll/:verificationToken',
    EnrollmentConfirmation: '/enroll/confirmation',
    RtopAdminReportDashboard: '/rtop-admin-report-dashboard',
    RtopForm: '/rtop',

    // Auth callback
    AuthCallback: '/auth_callback',

    // Provided by Kube Ingress (App ID)
    AppIDLogout: '/appid_logout',

    // Arrival testing routes
    ArrivalTestingContactVerification: '/arrival-testing',
    ArrivalTestingEnrollmentForm: '/arrival-testing/enroll/:verificationToken',
    ArrivalTestingEnrollmentConfirmation: '/arrival-testing/enroll/confirmation',
    RtopAdminLookupArrivalTestingConfirmationNumber: {
      staticRoute: `/arrival-testing/household/:confirmationNumber`,
      dynamicRoute: (confirmationNumber) => `/arrival-testing/household/${confirmationNumber}`,
  },
});

export const UserType = Object.freeze({
    BackOffice: 'Back Office',
    ServiceAlberta: 'Service Alberta',
    FreshworksAdmin: 'Admin'
});

export const Question = Object.freeze({
    Yes: 'Yes',
    No: 'No',
});

export const Place = Object.freeze({
    PrivateResidence: 'Private residence',
    Commercial: 'Commercial (e.g. hotel)',
    Other: 'Other (please specify)',
});

export const Vehicle = Object.freeze({
    PrivateVehicle: 'Private vehicle',
    TaxiOrRideshare: 'Taxi or rideshare',
    Other: 'Other (please specify)',
});

export const Province = Object.freeze({
    Alberta: 'Alberta',
    BritishColumbia: 'British Columbia',
    Manitoba: 'Manitoba',
    NewBrunswick: 'New Brunswick',
    NewfoundlandAndLabrador: 'Newfoundland and Labrador',
    NorthwestTerritories: 'Northwest Territories',
    NovaScotia: 'Nova Scotia',
    Nunavut: 'Nunavut',
    Ontario: 'Ontario',
    PrinceEdwardIsland: 'Prince Edward Island',
    Québec: 'Québec',
    Saskatchewan: 'Saskatchewan',
    Yukon: 'Yukon',
    Other: 'Other (please specify)'
});

export const Roles = Object.freeze({
    AHS: 'AHS',
    GOA: 'GOA',
    OTHER: 'OTHER',
})

export const Scope = Object.freeze({
    SCREENER: 'screener_portal',
    ADMIN: 'admin_report_access'
})

export const EnrollmentStatus = Object.freeze({
    APPLIED: 'applied',
    WITHDRAWN: 'withdrawn',
    ENROLLED: 'enrolled'
});

export const HouseholdStatus = Object.freeze({
    RED: 'red',
    GREEN: 'green',
    YELLOW: 'yellow'
});

export const ContactMethod = Object.freeze({
    SMS: 'sms',
    EMAIL: 'email',
    CALLIN: 'callIn',
});