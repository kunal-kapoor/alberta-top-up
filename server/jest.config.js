module.exports = {
  testEnvironment: 'node',
  modulePathIgnorePatterns: ['<rootDir>/build/'],
  coveragePathIgnorePatterns: ['/node_modules/'],
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/src/tests/arrival-form/', '<rootDir>/src/tests/service-alberta/'],
  globals: {
    'ts-jest': {
      'tsconfig': './tsconfig.json'
    }
  },
  moduleFileExtensions: [
    'ts',
    'js',
    'json',
  ],
  transform: {
    '^.+\\.(ts)$': 'ts-jest'
  }
};
