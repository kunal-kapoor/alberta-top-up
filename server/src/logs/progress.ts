import cliProgress from 'cli-progress';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LogsProgressService {
    bar: any;

    constructor() {
        this.bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
    }

    public start(total: number, defaultStart: number) {
        this.bar.start(total, defaultStart);
    }

    public update(updateValue) {
        this.bar.update(updateValue);
    }

    public stop() {
        this.bar.stop();
    }
}