import { ProgramType } from "../rtop/entities/program";

// DB2 condition for whether a household is part of the border pilot program or not
export const isBp = (prop='h') => `(${prop}.program='${ProgramType.BORDER_PILOT}' or ${prop}.program is null)`;
