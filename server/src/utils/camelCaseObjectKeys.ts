
/**
 * Converts the keys of the given object from snake case to camel case
 * 
 * {
 *    TEST: '1234',
 *    MULTI_WORD: '567',
 *    TEST_TEST_TEST: '89'
 * }
 * ->
 * {
 *    test: '1234',
 *    multiWord: '567',
 *    testTestTest: '89'
 * }
 * 
 */
export const camelCaseObjectKeys = obj => {
    return Object.entries(obj).reduce((ret, [key, val]) => {
        ret[camelCaseString(key)] = val;
        return ret;
    }, {});
}

export const camelCaseString = key => {
    return key.toLowerCase().split('_').map((k, idx) => {
        if(idx === 0) {
            return k;
        } else {
            return `${k.charAt(0).toUpperCase()}${k.slice(1)}`;
        }
    }).join('');
}