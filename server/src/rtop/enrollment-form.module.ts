import { Module } from '@nestjs/common';
import { EnrollmentFormService } from './enrollment-form.service';
import { EnrollmentFormValidationNumberGenerator } from './validation-number.service';
import { EnrollmentConfirmationService } from './enrollment-confirmation.service';
import { ServiceAlbertaRepository} from '../service-alberta/service-alberta.repository';
import { EnrollmentFormController } from './enrollment-form.controller';
import { EnrollmentFormRepository } from './repositories/enrollment-form.repository';
import { ServiceAlbertaService } from '../service-alberta/service-alberta.service';
import { EmailService } from '../email/email.service';
import { TextService } from '../text.service';
import { ContactMethodVerificationService } from './contact-method-verification.service';
import { ContactMethodVerificationRepository } from './repositories/contact-method-verification.repository';
import { CSVConverterService } from './csv-converter.service';
import { MaskTravellersService } from './mask-travellers.service';
import { DailyReminderRepository } from '../rtop-admin/repositories/daily-reminder.repository.';
import { FeatureFlagService } from './feature-flag.service';
import { HouseholdSeeder } from '../tests/util/household-seeder';
import { BulkNotificationService } from './bulk-notification.service';
import { SlackNotifierService } from '../slack-notifier.service';

const routes = {
  'controllers': [EnrollmentFormController],
  'imports': [],
};

@Module({
  providers: [
    ContactMethodVerificationService,
    EnrollmentConfirmationService,
    EnrollmentFormService,
    EnrollmentFormRepository,
    EnrollmentFormValidationNumberGenerator,
    HouseholdSeeder,
    ContactMethodVerificationRepository,
    ServiceAlbertaRepository,
    ServiceAlbertaService,
    SlackNotifierService,
    EmailService,
    TextService,
    BulkNotificationService,
    CSVConverterService,
    MaskTravellersService,
    DailyReminderRepository,
    FeatureFlagService,
  ],
  imports: routes.imports || [],
  controllers: routes.controllers || [],
})
export class EnrollmentFormModule {}
