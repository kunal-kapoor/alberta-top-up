import { IsArray, ValidateNested, IsIn, IsOptional } from "class-validator";
import { Exclude, Expose } from "class-transformer";
import { MaskTravellersService } from "../mask-travellers.service";
import { SURVEY_QUESTIONNARIE_TYPE } from "../constants";
import moment from 'moment';
import { USER_TIMEZONE, DATE_FORMAT } from "../enrollment-form.constants";

@Exclude()
export class EnrollmentFormSearchRO {
    @Expose()
    id: number;

    @Expose()
    confirmationNumber: string;

    @Expose()
    status: string;

    @Expose()
    submissionStatus: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    arrivalCityOrTown: string;

    @Expose()
    arrivalCountry: string;

    @Expose()
    arrivalDate: Date;

    @Expose()
    dobVerificationAttempt: number;

    @Expose()
    dobAttemptTime: string;

    @Expose()
    cardStatusReason: string;

    @Exclude()
    householdId: number;
    enrollmentDate: string;
    airportOrBorderCrossing: string;
    submission: any;

    constructor(data: any) {
        Object.assign(this, data);
        this.enrollmentDate = data.enrollmentDate && moment(data.enrollmentDate).tz(USER_TIMEZONE).format(DATE_FORMAT);
    }
}

export class EnrollmentFormSearchResultRO {
    @IsArray()
    @ValidateNested({ each: true })
    travellers: EnrollmentFormSearchRO[];

    @Exclude()
    date: string;

    @Expose()
    abttRequired: boolean;

    @IsOptional()
    travellerSurvey: string;

    @Expose()
    flightInformationRequired: boolean;

    @Expose()
    filledFlightInformation: any;

    constructor(date: string, trls: any[], abttRequired = false, surveyQuestionnaire: any[], maskNames = false, numberOfDailyCheckInsReceived = 0) {
        this.travellers = trls.map(t => new EnrollmentFormSearchRO(t));
        this.date = date;
        this.abttRequired = abttRequired;

        if (maskNames) {
            MaskTravellersService.mask(this.travellers);
        }

        this.travellerSurvey = this.surveyForToday(trls, surveyQuestionnaire);
        this.flightInformationRequired = this.isFlightInformationRequired(trls, numberOfDailyCheckInsReceived);
        this.filledFlightInformation = this.preFilledFlightInformation(trls);
    }

    @Exclude()
    private surveyForToday(trls: any[], surveyQuestionnaire: any[]): string {
        const enrollmentDate = trls[0]?.enrollmentDate;
        const isDay4 = moment().tz(USER_TIMEZONE).format(DATE_FORMAT) === moment(enrollmentDate).tz(USER_TIMEZONE).add('3', 'days').format(DATE_FORMAT);
        const isDay13 = moment().tz(USER_TIMEZONE).format(DATE_FORMAT) === moment(enrollmentDate).tz(USER_TIMEZONE).add('12', 'days').format(DATE_FORMAT);
        if (isDay4 || isDay13) {
            const surveyType = isDay13 ?
                SURVEY_QUESTIONNARIE_TYPE.DAY_13 : SURVEY_QUESTIONNARIE_TYPE.DAY_4;
            const surveySubmission = surveyQuestionnaire.find((e) => e.SURVEY_TYPE === surveyType);

            if (trls.length === 1 && moment(new Date(), "YYYY/MM/DD").diff(moment(trls[0].dob, "YYYY/MM/DD"), 'years') <= 18) {
                return null;
            }
            else if (!surveySubmission || surveySubmission['SUBMISSION_STATUS'] === 'not_submitted') {
                return surveyType;
            }
        }
    }
    
    @Exclude()
    private isFlightInformationRequired(trls: any[], numberOfDailyCheckInsReceived: number): boolean {
        const airportOrBorderCrossing = trls[0]?.airportOrBorderCrossing;
        return airportOrBorderCrossing.includes("Airport") && numberOfDailyCheckInsReceived === 1;
    }

    @Exclude()
    private preFilledFlightInformation(trls: any[]): any {
        let flightInformation;
        trls.forEach((each) => {
            if (each.submissionStatus === 'submitted' && each.submission) {
                const submission = each.submission && JSON.parse(each.submission)['answers'];
                const airline = submission.filter((a) => a.includes('airline')).length > 0 ? JSON.parse(submission.filter((a) => a.includes('airline'))[0]).answer : '';
                const flightNumber = submission.filter((a) => a.includes('flightNumber')).length > 0 ? JSON.parse(submission.filter((a) => a.includes('flightNumber'))[0]).answer : '';
                flightInformation = { airline, flightNumber };
            }
        });
        return flightInformation;
    }
}

