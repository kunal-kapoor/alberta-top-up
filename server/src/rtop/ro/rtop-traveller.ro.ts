import { Exclude, Expose } from "class-transformer";

@Exclude()
export class RtopTravellerSearchRO {
    @Expose()
    id: number;

    constructor(data) {
        this.id = (data.result||[])[0]?.ID;
    }
}
