import { CONTACT_METHOD } from "../../rtop-admin/repositories/daily-reminder.repository.";

export class ContactMethodRO {
    contactMethod: CONTACT_METHOD;
    contactEmail: string;
    contactPhoneNumber: string;

    constructor(contactMethod: CONTACT_METHOD, contactEmail: string, contactPhoneNumber: string) {
        this.contactMethod = contactMethod;
        this.contactEmail = contactEmail;
        this.contactPhoneNumber = contactPhoneNumber;
    }
}