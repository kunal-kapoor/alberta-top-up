export class FeatureFlagService {
    private static featureFlag: any;

    private static loadEnvVariables() {
        this.featureFlag = this.featureFlag ?? Object.freeze(JSON.parse(process.env.FEATURE_FLAG));
    }

    static async isFeatureEnabled(feature: string): Promise<boolean> {
        this.loadEnvVariables();

        if (this.featureFlag && feature) {
            return this.featureFlag[feature] ?? false;
        }
        return false;
    }

}