import { COMPLETED_STATUSES } from "../enrollment-form.constants";
import { RTOPDbService } from '../../db/rtop-dbservice';
import moment from 'moment';
import { enrollmentFormQuery } from '../../db/rtop-db2connectionUtils';
import { EnrollmentStatus } from "../entities/household.entity";
import { DAILY_STATUS, WITHDRAWN_REASON } from "../constants";
import { Logger } from "@nestjs/common";

/**
 * Utility class for searching form recoords
 */
export class EnrollmentSearchQueryBuilder {

    args = [];
    queryStr = '';

    filterStr = '';
    orderByStr = '';
    agentStr = '';
    fromDateStr = '';
    toDateStr = '';
    programStr = '';

    pageStr = '';
    splitNulls = false;
    splitColumnName = "";
    enrollmentStatusStr = '';

    constructor(private readonly user) {}

    /**
     * Adds a WHERE clause which filter based on the property passed in
     * @param filter filter type to search for (unassigned/completed/mine)
     */
    filter(filter): EnrollmentSearchQueryBuilder {
        this.filterStr = '';

        if(filter === "unassigned"){
            this.filterStr += `(f.assigned_to IS NULL) AND (f.status NOT IN (${
                //Take the list of completed statuses, turn it to json
                (JSON.stringify(COMPLETED_STATUSES)
                  // replace all double quotes with single quotes. 
                  .replace(/"/g,'\'')
                  // remove the brackets
                  .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
              }) OR f.status IS NULL) AND `;
        }else if (filter === 'completed'){
            this.filterStr += `(f.status IN (${
                //Take the list of completed statuses, turn it to json
                (JSON.stringify(COMPLETED_STATUSES)
                // replace all double quotes with single quotes. 
                .replace(/"/g,'\'')
                // remove the brackets
                .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
            })) AND `
        }else if(filter ==="mine"){
            this.filterStr += `(f.assigned_to = ? ) AND (f.status NOT IN (${
              //Take the list of completed statuses, turn it to json
              (JSON.stringify(COMPLETED_STATUSES)
                // replace all double quotes with single quotes. 
                .replace(/"/g,'\'')
                // remove the brackets
                .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
            }) OR f.status IS NULL) AND `
            this.args.push(this.user.id)
        }

        return this;
    }

    /**
     * Adds a WHERE clause which filter based on the property passed in
     * @param filter filter type to search for (unassigned/completed/mine)
     */
    filterCardStatus(filter): EnrollmentSearchQueryBuilder {
        this.filterStr = '';

        if(Object.values(DAILY_STATUS).filter((ele) => ele === filter).length > 0)
        {
            this.filterStr += `(f.card_status = ?) AND `
            this.args.push(filter);
        }

        return this;
    }

    /**
     * Allow filtering of records by agents.
     * Only exact matches are allowed.
     *
     * @param name Name of agent to filter by
     */
    fromDate(fromDate: Date) {
        this.fromDateStr = '';

        if(fromDate) {
            const dtStr = moment.utc(fromDate).format('YYYY-MM-DD HH:mm:ss');

            this.fromDateStr = ` f.arrival_date >= ? AND `;
            this.args.push(dtStr);
        }

        return this;
    }

    /**
     * Allow filtering of records by agents.
     * Only exact matches are allowed.
     *
     * @param name Name of agent to filter by
     */
    toDate(toDate: Date) {
        this.toDateStr = '';

        if(toDate) {
            const dtStr = moment.utc(toDate).format('YYYY-MM-DD HH:mm:ss');

            this.toDateStr = ` f.arrival_date < ? AND `;
            this.args.push(dtStr);
        }

        return this;
    }

    /**
     * Allow filtering of records by fromDate.
     *
     * @param fromDate date to filter by
     */
    agent(name: string) {
        this.agentStr = '';

        if(name && name.trim() && name.trim() !== 'all') {
            name = name.trim();

            this.agentStr = ' a.name=? AND ';
            this.args.push(name);
        }

        return this;
    }

    /**
     * Adds a order by clause to the query.
     * @param orderby Property to order by date | traveller | owner | status
     * @param order asc|desc (default)
     */
    orderBy(orderby, order): EnrollmentSearchQueryBuilder {
        this.orderByStr = '';

        if(orderby && order){
            let orderval = "";
            switch (orderby){
                // Use date as the default case
                case "date":
                default:
                    orderval = "f.arrival_date";
                    break;
                case "firstName":
                    orderval = "f.first_name";
                    break;
                case "lastName":
                    orderval = "f.last_name";
                    break;
                case "status":
                    orderval = "f.card_status";
                    break;
                case "enrollmentStatus":
                    orderval = "f.enrollment_status";
                    break;
                case "confirmationNumber":
                    orderval = "f.confirmation_number";
                    break;
                case "lastUpdated":
                    orderval = "f.card_status_time";
                    this.splitNulls = true;
                    this.splitColumnName = orderval;
                    break;
                case "lastScreened":
                    orderval = "f.last_screened";
                    this.splitNulls = true;
                    this.splitColumnName = orderval;
                    break;
                case "lastActivityNote":
                    orderval = "f.enrollment_status";
                    this.splitNulls = true;
                    this.splitColumnName = orderval;
                    break;
            
            }
            this.orderByStr += ` ORDER BY ${orderval} ${order==="asc"?"ASC":"DESC"} `
        }
        return this;
    }

    /**
     * Filters query by confirmationNumber or lastName
     * @param query Query to filter by
     */
    query(query): EnrollmentSearchQueryBuilder {
        this.queryStr = '';
        if(query){
            const travellerByName = `
                select h.ID from rtop_traveller rtop 
                inner join household h 
                on h.ID = rtop.HOUSEHOLD_ID 
                where 
                    (LOWER(rtop.last_name) LIKE ? || '%') OR 
                    (LOWER(rtop.first_name) LIKE ? || '%')`;
            
            this.queryStr = `( (LOWER(f.confirmation_number) LIKE ? || '%') 
            OR (f.CONTACT_PHONE_NUMBER LIKE ? || '%') 
            OR (f.PHONE_NUMBER LIKE ? || '%') 
            OR (LOWER(f.EMAIL) LIKE ? || '%') 
            OR (LOWER(f.CONTACT_EMAIL) LIKE ? || '%') 
            OR f.ID IN (${travellerByName}) ) AND `;

            // if confirmation number of travellers is used, comparing the base ones alone
            this.args.push(query.split('-')[0].toLowerCase());
            this.args.push(query);
            this.args.push(query);
            this.args.push(query.toLowerCase());
            this.args.push(query.toLowerCase());
            this.args.push(query.toLowerCase());
            this.args.push(query.toLowerCase());
        }

        return this;
    }

    enrollmentStatus(status): EnrollmentSearchQueryBuilder {
        this.enrollmentStatusStr = '';

        if(status && status.trim() !== 'all' && Object.values(EnrollmentStatus).find((ele) => status.trim() === ele )){
            this.enrollmentStatusStr = ` f.enrollment_status = ? AND `;
            this.args.push(status.trim());
        }
        else if( status && status.trim() !== 'all' && Object.values(WITHDRAWN_REASON).find((ele) => status.trim() === ele) ){
            this.enrollmentStatusStr = ` f.enrollment_status = '${EnrollmentStatus.WITHDRAWN}' AND f.WITHDRAWN_REASON = ? AND `;
            this.args.push(status.trim());
        }

        return this;
    }

    /**
     * Adds program type to the query
     * @param program The program type the traveller is enrolled in 
     */
    program(program): EnrollmentSearchQueryBuilder {
        this.programStr = '';

        if(program && program.trim()){
            this.programStr = ` f.program = ? AND `;
            this.args.push(program.trim());
        }

        return this;
    }

    /**
     * Adds pagination to the query.
     * @param page Current page (0 default)
     * @param pageSize Size of page
     */
    paginate(page, pageSize): EnrollmentSearchQueryBuilder {
        page = page? parseInt(page): 0;

        this.pageStr =`
            OFFSET ${pageSize * page} ROWS
            FETCH FIRST ${pageSize} ROWS ONLY
        `;

        return this;
    }

    /**
     * 
     * Splits the query into two queries
     *  - One with columnName is null
     *  - Other with columnName without null and ordered
     * After splitting, the values are unified and pagination is applied
     * 
     * @param query - Query to be split and unified
     * @param orderByStr - The order by attribute for the query
     * @param pageStr - Pagination to be applied for the query
     * @param columnName - Column which is split for null and not null
     */
    private splitNullsWithUnion(query:string, orderByStr:string, pageStr:string, columnName:string): string {
        const unionQueryStr = `${query} AND ${columnName} is null UNION ALL select * from (${query} AND ${columnName} is not null ${orderByStr})  ${pageStr}`;
        return unionQueryStr;
    }

    /**
     * Formats and executes a search query
     */
    async execute() {
        const qs = select => `
            select ${select}
            FROM household f
            LEFT JOIN agent a
                ON a.id = f.assigned_to
            LEFT JOIN rtop_activity act
                ON f.last_activity_id=act.id
            WHERE `;

        // Statement to execute (eventually)
        let stmt = '';

        const formSelect = RTOPDbService.FormSelect;
        const countSelect = RTOPDbService.CountSelect;

        let selectQuery = qs(formSelect);
        let argsList = this.args;

        if(this.agentStr) {
            stmt += this.agentStr;
        }

        if(this.fromDateStr) {
            // Filter query by fromDate
            stmt += this.fromDateStr;
        }

        if(this.filterStr) {
            // Add filter
            stmt += this.filterStr;
        }

        if(this.enrollmentStatusStr) {
            stmt += this.enrollmentStatusStr;
        }

        if(this.toDateStr) {
            // Filter query by toDate
            stmt += this.toDateStr;
        }

        if(this.programStr) {
            // Filter query by program
            stmt += this.programStr;
        }

        /**
         * NOTE: despite having an AND in the query string builder, this addition needs to happen last.
         * Any stmt additions after this query will be neglected from the executed statement
         */
        if(this.queryStr) {
            // Add query filter
            stmt += this.queryStr;
        }
        

        // All filter statements add an AND at the end. Make sure we terminate that correctly by adding a TRUE statement
        stmt += ' TRUE ';
        
        // Find total number of results without pagination
        const numberOfResults = await (await enrollmentFormQuery(qs(countSelect) + stmt, argsList)).count();

        if( this.splitNulls ) {
            // Use a union query if the query requires null values to be at the bottom of the search results
            selectQuery = this.splitNullsWithUnion(qs(formSelect) + stmt , this.orderByStr, this.pageStr, this.splitColumnName);
            
            // Since the query is repeated twice for with and without null values, the args value list is repeated
            argsList = [...this.args,...this.args]
            
        } else{
            // Add order by + pagination
            if(this.orderByStr) {
                stmt += this.orderByStr;
            }

            if(this.pageStr) {
                stmt += this.pageStr;
            }

            selectQuery += stmt
        }
        // Find actual results (paginated)
        const results = await (await enrollmentFormQuery(selectQuery, argsList)).multiple()
        return [selectQuery, results, numberOfResults];
    }
}
