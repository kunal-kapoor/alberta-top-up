import { query } from "../../db/db2connection";
import { enrollmentFormQuery } from "../../db/rtop-db2connectionUtils";
import moment from "moment";

export class HouseholdQuery {
    private static readonly UpdateLastScreened = `
        UPDATE household SET
            last_screened = ?
        WHERE id=?
    `;

    private static readonly UpdateLastActivity = `
        UPDATE household SET
            last_activity_id = ?
        WHERE id=?
    `;

    private static readonly AddActivity: string = `
        SELECT ID FROM FINAL TABLE(
            INSERT INTO rtop_activity(date, agent_id, household_id, type, note, status) VALUES (?,?,?,?,?,?)
        )
    `;

    ////////////////////////////////////////

    /**
     * Updates last screened timestamp for the given household
     * @param householdId ID of household to update
     */
    public static async updateLastScreened(householdId: number, lastScreened: string) {
        await query(HouseholdQuery.UpdateLastScreened, [lastScreened, householdId]);
    }

    /**
     * Updates the given households latest activity record
     * @param householdId ID of household to update
     * @param activityId ID of activity to set as last activity
     */
    public static async updateLastActivity(householdId: number, activityId: number): Promise<any> {
        return await enrollmentFormQuery(HouseholdQuery.UpdateLastActivity, [activityId, householdId]);
    }

    /**
     * Creates a new activity
     *
     * @param activity - activity data
     * @param userId - agent id
     * @param formId - id of a form
     */
    public static async addActivity(activity: any, userId: string, formId: number): Promise<number> {
        const args = [
            moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            userId,
            formId,
            "Status Changed",
            activity.note,
            activity.status];

        return (await enrollmentFormQuery(HouseholdQuery.AddActivity, args))
            .normalized()
            .single()
            .id;
    }


}