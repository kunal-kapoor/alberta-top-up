import { HouseholdMonitoringRO } from "../ro/household-monitoring.ro";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { RTOPActivityRO } from "../ro/activity.ro";
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { AgentDTO } from "../../service-alberta/service-alberta.repository";
import { HouseholdTravellersQueryStatusRO } from "../ro/household-submission-status.ro";
import { DailyQuestionnaireService } from "../../rtop-admin/daily-reminder.service";
import { DailyReminderRO } from "../../rtop-admin/repositories/daily-reminder.repository.";
import { PerformedTestsRecordsDTO } from "../../rtop-admin/dto/performed-tests-record.dto";
import moment from "moment";
import { PerformedTestImportError, PerformedTestRecordRO } from "../../rtop-admin/ro/performed-tests-record.ro";
import { ContactMethodVerificationDTO } from "../dto/contact-method-verification.dto";
import { EnrollmentStatus } from "../entities/household.entity";
import { CSVConverterService } from "../csv-converter.service";
import { ENROLMENT_REPORT_EXPORT, CARD_STATUS_REASON, DAILY_STATUS, WITHDRAWAL_REPORT_EXPORT } from "../constants";
import { DATE_FORMAT, USER_TIMEZONE } from "../enrollment-form.constants";
import { LoggerService } from "../../logs/logger";
import { TravellerStatusRO } from "../ro/traveller-status.ro";
import { RTOPMonitoringQuery } from "./rtop-monitoring.query";

@Injectable()
export class RTopMonitoringRepository {
    constructor(
        private readonly dailyReminderservice: DailyQuestionnaireService,
        private readonly csvConverterService : CSVConverterService,
    ) {}

    /**
     * Edit contact info for household
     * @param householdId household to update contact info for
     * @param contactInfo contact info to update
     */
    async editContactInfo(householdId: number, contactInfo: ContactMethodVerificationDTO) {
      try {
        const updateRequest = JSON.stringify(
          {
            "$set":
            {
              arrivalDate: contactInfo.arrivalDate,
            }
          }
        );
        await RTOPDbService.updateContactInfo(householdId, contactInfo, updateRequest);
      } catch (err) {
        LoggerService.error(`Failed to edit contact info for household ${householdId}`, err);
        throw err;
      }
    }
  
    /**
     * Retrieve an agent record for the given users
     * Creates one if it doesn't already exist
     * @param user 
     */
    async getOrCreateAgent(user: any): Promise<AgentDTO> {
        const res = await RTOPDbService.getAgent(user);

        const [agent] = res || [null];

        if (agent) {
            return {
              id: agent.ID,
              name: agent.NAME,
            };
        } else {
            const res = await RTOPDbService.createAgent(user);

            const [{ ID = null }] = res || [];

            return {
                id: ID,
                name: user.name,
            };
        }

    }

    /**
     * Lists agents that have submitted at least one form activity
     */
    async listActiveAgents(): Promise<AgentDTO[]> {
        return await RTOPDbService.listActiveAgents();
    }
    
    /**
     * Retrieve a travellers status by confirmation number
     * @param confirmationNumber 
     */
    async getTravellerStatusByConfirmationNumber(confirmationNumber: string): Promise<HouseholdTravellersQueryStatusRO> {
      const res = await RTOPDbService.getTravellerStatusForHousehold(confirmationNumber);
      return res;
    }

    /**
     * Retrieves the latest daily reminder for a household
     * @param householdId id of the household
     */
    async getLatestDailyReminderForHousehold(householdId: number): Promise<DailyReminderRO> {
        const reminder = await RTOPDbService.getLatestDailyReminders(householdId);
        return reminder && reminder.length > 0 && new DailyReminderRO(reminder[0]);
    }

    /**
     * Retrieves a link to the latest reminder for the given household
     * @param householdId id of the household
     */
    async getLatestDailyReminderLinkForHousehold(householdId: number): Promise<any> {
        const reminder = await this.getLatestDailyReminderForHousehold(householdId);
        return reminder && this.dailyReminderservice.constructAdminQuestionnarieLink(reminder.token);
    }

    /**
     * Retrieves a link to the latest reminder for the given household
     * @param householdId id of the household
     */
    async getLatestDailyReminderTravellerLink(householdId: number): Promise<any> {
      const reminder = await this.getLatestDailyReminderForHousehold(householdId);
      return reminder && this.dailyReminderservice.constructQuestionnarieLink(reminder.token);
  }


    /**
     * Find a household record by confirmation number
     *
     * @param confirmationNumber
     *
     * @returns - Arrival form with the matching confirmation number
     */
    async findByConfirmationNumber(confirmationNumber: string, req): Promise<HouseholdMonitoringRO> {
        const res = await(await RTOPDbService.getMonitoringFormByNumber(confirmationNumber))
            .singleMonitoring();

        if(!res) {return null}

        res.activities = await this.getFormActivity(res.id);
        res.assignedToMe = res.agentId === req?.user?.sub;
        const travellerStatus: HouseholdTravellersQueryStatusRO = await this.getTravellerStatusByConfirmationNumber(confirmationNumber);
        
        if(travellerStatus) {
            res.travellerStatus = travellerStatus.results;
            res.dailyReminderLink = await this.getLatestDailyReminderLinkForHousehold(res.id);    
            res.dailyReminderTravellerLink = await this.getLatestDailyReminderTravellerLink(res.id);    
        } else {
          res.travellerStatus = [];
        }
    
        return res;
    }

    /**
     * Find an activity by id
     *
     * @param formId - id of a form associated with this activity
     *
     * @returns - activity details
     */
    async getFormActivity(formId: number): Promise<any> {
        const res = await RTOPDbService.getActivity(formId);

        if(!res) {
            return [];
        }

        return res.map(r => new RTOPActivityRO({
            date: r.DATE,
            agent: r.AGENTNAME,
            type: r.TYPE,
            note: r.NOTE,
            status: r.STATUS
        }));
    }


    /**
     * Assign a household record to the given user for processing
     * @param householdId household to assign
     * @param user user to assign to
     * @param forceAssign override current assignment?
     */
    async assignTo(householdId: number, user: any, forceAssign?: boolean) {
        // Check if form has already been assigned
        const assignedAgent = await this.assignedTo(householdId);
    
        if (!forceAssign) {
          if (assignedAgent && assignedAgent.id !== user.sub) {
            // Throw en error if record is assigned to someone else
            throw new HttpException(
              `Record is already assigned to ${assignedAgent.name}`,
              HttpStatus.BAD_REQUEST
            );
          } else if (assignedAgent && assignedAgent.id === user.sub) {
            // Otherwise no need to re-assigbn it
            return;
          }
        }
    
        const agent = await this.getOrCreateAgent(user);
    
        await RTOPDbService.assignTo(agent.id, assignedAgent?.id || null, householdId);
    }
  
    /**
     * Unassign the current user if they are assigned to it
     *
     * @param householdId - form id
     * @param user - Service Alberta agent id
     */
    async unassignFrom(householdId: number, user: any) {
      const assignedAgent = await this.assignedTo(householdId);
      if (!assignedAgent) {
        // Throw an error if record is assigned to someone else
        throw new HttpException(
          `There is no agent assigned to that record`,
          HttpStatus.BAD_REQUEST
        );
      }
  
      await RTOPDbService.assignTo(null, assignedAgent.id, householdId);
    }
  
    /**
     * Retrieve the agent the given household is assigned to
     * @param householdId id of the household to retrieve
     */
    private async assignedTo(
      householdId: number
    ): Promise<AgentDTO> {
      const res = await RTOPDbService.assignedTo(householdId);
  
      const [agent] = res || [null];
  
      return agent && agent !== []
        ? {
            id: agent.ID,
            name: agent.NAME,
          }
        : null;
    }

    /**
     * Update traveller tested dates for the given test records
     * 
     * This is used by the monitoring team which daily uploads 
     * a csv with which traveller confirmation numbers have been tested that day
     * 
     * @param testRecords test records to update
     */
    async updatePerformedTestRecords(testRecords: PerformedTestsRecordsDTO): Promise<PerformedTestRecordRO[]> {

      const errors = [];

      for(const r of testRecords.results) {
        let testDate;

        try {
          testDate = moment(r.testedAt);
          // Check that date is valid
          if(!testDate.isValid()) {
            errors.push(new PerformedTestRecordRO(r.confirmationNumber, PerformedTestImportError.INVALID_DATE));
            continue
          }
        } catch(e) {
          errors.push(new PerformedTestRecordRO(r.confirmationNumber, PerformedTestImportError.INVALID_DATE));
          continue;
        }

        const travellerId = await (await RTOPDbService.findTravellerByConfirmationNumber(r.confirmationNumber.toLowerCase())).idOnly();

        // Check that confirmation number matches a record
        if(!travellerId) {
          errors.push(new PerformedTestRecordRO(r.confirmationNumber, PerformedTestImportError.INVALID_CONFIRMATION_NUMBER));
          continue;
        }

        const formattedDate = testDate.format('YYYY-MM-DD HH:mm:ss');
        await RTOPDbService.updateTravellerTestedDate(travellerId, formattedDate);
      }

      return errors;
    }

  async updateTravellerIsolationStatus(confirmationNumber: string, isolationStatus: boolean): Promise<boolean> {
    try {
      await RTOPDbService.updateTravellerIsolationStatus(confirmationNumber, isolationStatus);
      return true;
    } catch (error) {
      return false;
    }
  }

  async exportTravellersReport(fromDate: string, toDate: string, type: string): Promise<any>
  {
    let travellerData;
    if( type === EnrollmentStatus.ENROLLED )
    {
      travellerData = await RTOPMonitoringQuery.getEnrolledTravellersReport(fromDate, toDate);
    }
    else{
      travellerData = await RTOPMonitoringQuery.getWithdrawnTravellersReport(fromDate, toDate);
    }

    travellerData.map((data)=> {
      if("ARRIVAL_DATE" in data)
      {
        data["ARRIVAL_DATE"] = moment(data["ARRIVAL_DATE"]).format(DATE_FORMAT);
      }
      if("ENROLLMENT_DATE" in data)
      {
        data["ENROLLMENT_DATE"] = moment(data["ENROLLMENT_DATE"]).tz(USER_TIMEZONE).format(DATE_FORMAT);
      }
      if("WITHDRAWN_DATE" in data)
      {
        data["WITHDRAWN_DATE"] = moment(data["WITHDRAWN_DATE"]).tz(USER_TIMEZONE).format(DATE_FORMAT);
      }
    });
    
    return this.csvConverterService.convertJSONToCSV(type === EnrollmentStatus.ENROLLED? ENROLMENT_REPORT_EXPORT: WITHDRAWAL_REPORT_EXPORT, travellerData);
  }

  async getTravellersStatusForDate(fromDate: string, toDate: string, type: string): Promise<TravellerStatusRO[]> {
    if (type === EnrollmentStatus.ENROLLED) {
      return await (await RTOPMonitoringQuery.getEnrolledTravellerStatusForDate(fromDate, toDate)).multiple();
    }
    else if (type === EnrollmentStatus.WITHDRAWN) {
      return await (await RTOPMonitoringQuery.getWithdrawnTravellerStatusForDate(fromDate, toDate)).multiple();
    }
  }

  async getTravellersStatus(type: string): Promise<TravellerStatusRO[]> {
    if (type === EnrollmentStatus.ENROLLED) {
      return await (await RTOPMonitoringQuery.getEnrolledTravellerStatus()).multiple();
    }
    else if (type === EnrollmentStatus.WITHDRAWN) {
      return await (await RTOPMonitoringQuery.getWithdrawnTravellerStatus()).multiple();
    }
  }

  async getAllDailyCheckin(): Promise<any> {
    return await RTOPDbService.getAllDailyCheckin();
  }
  
  async getAllDailyCheckinForDate(fromDate: string, toDate: string): Promise<any> {
    return await RTOPDbService.getAllDailyCheckinForDate(fromDate, toDate);
  }

  /**
   * Fetches the count of all the travellers enrolled during the specified time border-wise.
   * 
   * @param fromDate 
   * @param toDate 
   * @param type 
   */
  async generateTravellersReport(fromDate: string, toDate: string, type: string): Promise<any> {
    const travellerData = await RTOPMonitoringQuery.getEnrolledTravellersCount(fromDate, toDate);
    return travellerData;
  }
}