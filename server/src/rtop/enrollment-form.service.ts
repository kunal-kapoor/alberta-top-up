import moment from 'moment';
import { Injectable } from "@nestjs/common";
import { EnrollmentFormRepository } from './repositories/enrollment-form.repository';
import { Household, EnrollmentStatus } from './entities/household.entity';
import { RTOPTraveller } from './entities/rtop-traveller.entity';
import { EnrollmentFormValidationNumberGenerator } from './validation-number.service';
import { EnrollmentFormDTO, ProgramEnrollmentFormDTO } from './dto/enrollment-form.dto';
import { EnrollmentFormSubmissionRO } from './ro/form-submission.ro';
import { AdditionalTravellers, DATE_FORMAT, CitiesAndCountries } from './enrollment-form.constants';
import { EnrollmentFormRO } from './ro/enrollment-form.ro';
import { RTOPDbService } from '../db/rtop-dbservice';
import { ContactMethodVerification } from './entities/contact-method-verification';
import { DailySubmission } from './entities/daily-submission.entity';
import { ReminderIDRO } from './ro/reminder-id.ro';
import { CARD_STATUS_REASON, DAILY_STATUS, WITHDRAWN_REASON } from './constants';
import { DailyReminderRepository } from '../rtop-admin/repositories/daily-reminder.repository.';
import { DailyQuestionAnswerService, SubmissionCardStatus } from './daily-question-answer.service';
import { HouseholdTraveller } from '../rtop-admin/dto/household-traveller.dto';
import { HouseholdQuery } from './repositories/enrollment-form.query';
import { ProgramType } from './entities/program';

export enum DeterminationDecision {
    ACCEPTED = 'Accepted'
}

const isViableIsolationPlan = (dto: ProgramEnrollmentFormDTO): boolean => {
    return dto.address? true: false;
};

@Injectable()
export class EnrollmentFormService {
    constructor(
        private readonly enrollmentFormRepository: EnrollmentFormRepository,
        private readonly confirmationNumberGenerator:EnrollmentFormValidationNumberGenerator,
        private readonly dailyReminderRepository:DailyReminderRepository
      ) {}

    /**
     * Creates a new ArrivalForm
     * - Generate a unique confirmation number
     * - Create entity
     * - Update daily report based on isolation plan status
     *
     * @param body data of the arrival form to create.
     */
    async saveForm(body: ProgramEnrollmentFormDTO, verification: ContactMethodVerification, programType: ProgramType): Promise<EnrollmentFormSubmissionRO> {
        const confirmationNumber = await this.confirmationNumberGenerator.generateValidationNumber(programType);
        const form: Household = this.mapArrivalFormDtoToEntity(body, confirmationNumber, verification, programType);
        await this.enrollmentFormRepository.save(form);
        // Create separate traveller records with unique confirmation numbers
        // for use in the daily questionnaire flow
        // Primary traveller gets confirmation number ${confirmationNumber}-0
        // additional travellers gets ${confirmationNumber}-1, ${confirmationNumber}-2 etc.
        const travellers = [
            this.mapToAdditionalTraveller(`${confirmationNumber}-0`, form),
            ...(form.additionalTravellers || []).map(t => this.mapToAdditionalTraveller(t.confirmationNumber, t))
        ];
        await this.enrollmentFormRepository.createTravellerRecords(form, travellers);

        return new EnrollmentFormSubmissionRO(
            {
                id: form.id,
                message: 'Form submission successful',
                confirmationNumber,
                viableIsolationPlan: form.viableIsolationPlan,
                travellers
            }
        );
    }

    /**
     * Mapping from EnrollmentFormDTO + Confirmation number -> ArrivalForm
     * @param dto
     * @param confirmationNumber
     */
    mapArrivalFormDtoToEntity(dto: ProgramEnrollmentFormDTO, confirmationNumber: string, verification: ContactMethodVerification, programType: ProgramType): Household {
        return <Household> {
            ...dto,
            id: null,
            contactMethod: verification.contactMethod,
            email: dto.email,
            phoneNumber: dto.phoneNumber,
            contactEmail: verification.email,
            contactPhoneNumber: verification.phoneNumber,
            confirmationNumber,
            enrollmentStatus: EnrollmentStatus.APPLIED, 
            viableIsolationPlan: isViableIsolationPlan(dto),
            determination: undefined,
            created: new Date(),
            dateOfBirth: moment(dto.dateOfBirth,'YYYY/MM/DD').toDate(),
            arrivalDate: moment(dto.arrivalDate,'YYYY/MM/DD').toDate(),
            additionalTravellers: dto.additionalTravellers && dto.additionalTravellers.map((t, idx) => this.mapToAdditionalTraveller(`${confirmationNumber}-${idx+1}`, t)),
            additionalCitiesAndCountries: dto.additionalCitiesAndCountries && dto.additionalCitiesAndCountries.map(this.mapToAdditionalCountry),
            hasTravellersOutsideHousehold: false,
            program: programType
          }
    }

    mapToAdditionalCountry(additionalLocation: CitiesAndCountries): CitiesAndCountries {
        return {
            cityOrTown: additionalLocation.cityOrTown,
            country: additionalLocation.country,
        }
    }

    mapToAdditionalTraveller(confirmationNumber, additionalTraveller: AdditionalTravellers): RTOPTraveller {
        return {
            firstName: additionalTraveller.firstName,
            confirmationNumber,
            lastName: additionalTraveller.lastName,
            dateOfBirth: moment(additionalTraveller.dateOfBirth,'YYYY/MM/DD').toDate(),
            gender: additionalTraveller.gender,
            citizenshipStatus: additionalTraveller.citizenshipStatus,
            exemptionType: additionalTraveller.exemptionType,
            exemptOccupation: additionalTraveller.exemptOccupation,
            exemptOccupationDetails: additionalTraveller.exemptOccupationDetails,
            dateLeavingCanada: additionalTraveller.dateLeavingCanada,
            reasonForTravel: additionalTraveller.reasonForTravel,
            durationOfStay: additionalTraveller.durationOfStay,
            preDepartureVaccination: additionalTraveller.preDepartureVaccination,
            timeSinceVaccination: additionalTraveller.timeSinceVaccination,
            dosesOfVaccine: additionalTraveller.dosesOfVaccine,
            seatNumber: additionalTraveller.seatNumber,
            preDepartureTest: additionalTraveller.preDepartureTest,
            preDepartureTestDate: additionalTraveller.preDepartureTestDate, 
            preDepartureTestCountry: additionalTraveller.preDepartureTestCountry
        }
    }

    /**
     * 
     * Updates enrollment status of household, and 
     * backfills the enrollment status of each traveller in the household
     * 
     * 
     * @param householdId - Household that needs to be updated
     * @param enrollmentStatus - Status to be updated
     */
    async updateHouseholdEnrollmentStatus(householdId: number, enrollmentStatus: EnrollmentStatus, withdrawalReason?: WITHDRAWN_REASON) {
        await this.enrollmentFormRepository.updateHouseholdEnrollmentStatus(enrollmentStatus, householdId, withdrawalReason);
        await this.enrollmentFormRepository.backfillTravellerEnrollmentStatus(enrollmentStatus, householdId, withdrawalReason);
    }

    /**
     * 
     * Updates the enrollment status of the traveller, and 
     * backfills the household's enrollment status accordingly
     * 
     * @param travellerConfirmationNumber - Confirmation number of the traveller
     * @param householdId - Household corresponding to the traveller
     * @param enrollmentStatus - Status to be updated
     */
    async updateTravellerEnrollmentStatus(travellerConfirmationNumber: string, householdId: number, enrollmentStatus: EnrollmentStatus, withdrawalReason?: WITHDRAWN_REASON) {
        await this.enrollmentFormRepository.updateTravellerEnrollmentStatus(enrollmentStatus, travellerConfirmationNumber, withdrawalReason);
        await this.enrollmentFormRepository.backfillHouseholdEnrollmentStatus(enrollmentStatus, householdId, withdrawalReason);

    }

    async enrollHouseholdForTraveller(confirmationNumber: string, householdId: number, currentEnrollmentStatus: string) {
        await RTOPDbService.enrolHouseholdForTraveller(confirmationNumber);
        if (currentEnrollmentStatus === EnrollmentStatus.APPLIED) {
            await this.enrollmentFormRepository.makeABTTMandatoryFlagHousehold(householdId);
            await RTOPDbService.addEnrollmentStatusHistory(householdId, EnrollmentStatus.ENROLLED);
        }
    }

    /**
     * Look up a traveller record based on a travellers confirmation number.
     * 
     * The record returned contains the entire household form submission
     * with the primary travellers contact info replaced with the requested travellers contact info,
     * and additional travellers removed.
     * 
     * This transformation is required as AHS needs to be able to look up data for individual travellers,
     * not by household as this app has the data structured.
     * 
     * @param confirmationNumber 
     */
    async findByTravellerConfirmationNumber(confirmationNumber: string) {
        // If not matching test data, actually perform the lookup
        let household: EnrollmentFormRO = await this.enrollmentFormRepository.findByTravellerConfirmationNumber(confirmationNumber);

        // Find traveller record
        const travellerRecord = (household.additionalTravellers || []).find(t => t.confirmationNumber.toLowerCase() === confirmationNumber.toLowerCase());

        if(travellerRecord) {
            // If traveller record is found, override the households primary traveller info
            // with the info for traveller requested

            // TODO: This is currently naively done. Once new form fields have been added,
            // this might need to change to make sure the record doesn't contain any primary traveller info.
            household = Object.assign({}, household, travellerRecord);
        }

        // Make sure we return the travellers confirmation number
        // intead of household confirmation number for primary traveller
        household.confirmationNumber = confirmationNumber;

        // Remove additional travellers from the returned JSON
        // This is not needed by AHS
        delete household.additionalTravellers;
        delete household.preDepartureTest;
        delete household.preDepartureTestDate;
        delete household.preDepartureTestCountry;
        delete household.preDepartureVaccination;
        delete household.timeSinceVaccination;
        delete household.dosesOfVaccine;

        return household;
    }

    async createDailySubmission(confirmationNumber: string, submission: DailySubmission, travellerId: number, reminder: ReminderIDRO): Promise<number> {
        const {id: submissionId, created} = await(await this.enrollmentFormRepository.createDailySubmission(submission, reminder))
            .normalized()
            .single();
        
        const cardStatus: SubmissionCardStatus = DailyQuestionAnswerService.submissionCardStatus(submission);

        await this.enrollmentFormRepository.updateHouseholdCardStatus([confirmationNumber], cardStatus.STATUS, cardStatus.FLAG, submissionId);
        await this.enrollmentFormRepository.updateTravellerCardStatus([reminder.travellerId], cardStatus.STATUS);
        await this.enrollmentFormRepository.updateLastScreened(reminder.householdId, reminder.travellerId, created);

        return submissionId;
    }

    async withdrawTraveller(confirmationNumber: string, householdId: number, currentEnrollmentStatus: string) {
        await RTOPDbService.enrolHouseholdForTraveller(confirmationNumber);
        if (currentEnrollmentStatus === EnrollmentStatus.APPLIED) {
            await RTOPDbService.addEnrollmentStatusHistory(householdId, EnrollmentStatus.ENROLLED);
        }
    }

    async updateTravellerInformation(actualHouseholdData: EnrollmentFormRO, confirmationNumber: string, updatedHouseholdTraveller: HouseholdTraveller) {
        // Finds traveller record
        const travellerRecord = (actualHouseholdData.additionalTravellers || []).find(t => t.confirmationNumber.toLowerCase() === confirmationNumber.toLowerCase());
        if (updatedHouseholdTraveller.dateOfBirth) {
            updatedHouseholdTraveller.dateOfBirth = moment(updatedHouseholdTraveller.dateOfBirth).toDate();
        }
        // checks if primary contact or additional traveller
        if (travellerRecord) {
            await this.updateHouseholdAdditionalTravellers(actualHouseholdData, confirmationNumber, updatedHouseholdTraveller);
        }
        else {
            await this.updateHouseholdPrimaryTraveller(updatedHouseholdTraveller, actualHouseholdData.id);
        }

        // Updates rtop_traveller table with information if available
        const formattedTravellerRecord = this.filterObjectValues(this.mapAdditionalTravellerToEntity(updatedHouseholdTraveller));
        if (Object.values(formattedTravellerRecord).length > 0) {
            await this.enrollmentFormRepository.updateTravellerRecords(formattedTravellerRecord, confirmationNumber);
        }
    }

    /**
     * 
     * Fetches the houshold information using the token and 
     * inserts the survey questionnarie submissions
     */
    async submitSurveyQuestionnaire(token: string, submission: DailySubmission, surveyType: string): Promise<any> {
        const reminderInfo = await this.dailyReminderRepository.getReminderInformationFromToken(token);
        const householdId: number = reminderInfo[0]?.HOUSEHOLD_ID;

        await this.enrollmentFormRepository.submitSurveyQuestionnaire(submission, householdId, surveyType);
    }

    /**
     * 
     * Updates additionalTravellers record in the household table
     * 
     */
    async updateHouseholdAdditionalTravellers(actualHouseholdData: EnrollmentFormRO, confirmationNumber: string, updatedHouseholdTraveller: HouseholdTraveller) {
        actualHouseholdData.additionalTravellers = actualHouseholdData.additionalTravellers.map(t => {
            if (t.confirmationNumber.toLowerCase() === confirmationNumber.toLowerCase()) {
                return Object.assign(t, updatedHouseholdTraveller);
            }
            return t;
        });

        await this.enrollmentFormRepository.updateAdditionalTravellersInformation(actualHouseholdData.additionalTravellers, actualHouseholdData.id);
    }

    /**
     * 
     * Since primary traveller, contact_method_verification table, and household is updated
     * 
     */
    async updateHouseholdPrimaryTraveller(updatedHouseholdTraveller: HouseholdTraveller, householdId: number) {
        const primaryTravellerHousehold = this.filterObjectValues(this.mapPrimaryTravellerDtoToJSONEntity(updatedHouseholdTraveller));
        const formattedHouseholdEntity = this.filterObjectValues(this.mapPrimaryTravellerDtoToHousehold(updatedHouseholdTraveller));

        await this.enrollmentFormRepository.updatePrimaryTravellerHousehold(primaryTravellerHousehold, formattedHouseholdEntity, householdId);

        const contactMethod = this.filterObjectValues(this.mapDtoToContactMethodEntity(updatedHouseholdTraveller));
        if (Object.values(contactMethod).length > 0) {
            await this.enrollmentFormRepository.updateHouseholdContactMethod(contactMethod, householdId);
        }
    }

    filterObjectValues(filteredObject: any) {
        return this.mapToObj(new Map(Object.entries(filteredObject).filter(([key, value]) => value)));
    }

    mapToObj(inputMap) {
        const obj = {};

        inputMap.forEach(function (value, key) {
            obj[key] = value
        });

        return obj;
    }

    /**
     * 
     * Maps DTO to the form_record available in houshold table
     * 
     */
    mapPrimaryTravellerDtoToJSONEntity(dto: HouseholdTraveller) {
        return {
            firstName: dto.firstName,
            lastName: dto.lastName,
            dateOfBirth: dto.dateOfBirth,
            email: dto.email,
            phoneNumber: dto.phoneNumber,
            contactEmail: dto.email,
            contactPhoneNumber: dto.phoneNumber,
            address: dto.address,
            cityOrTown: dto.cityOrTown,
            postalCode: dto.postalCode,
            gender: dto.gender
        }
    }

    mapDtoToContactMethodEntity(dto: HouseholdTraveller) {
        return {
            email: dto.email,
            phoneNumber: dto.phoneNumber,
        }
    }

    mapAdditionalTravellerToEntity(dto: HouseholdTraveller) {
        return {
            firstName: dto.firstName,
            lastName: dto.lastName,
            dateOfBirth: dto.dateOfBirth && moment(dto.dateOfBirth).format(DATE_FORMAT),
        }
    }

    /**
     * 
     * Maps DTO to the columns in household table
     * 
     */
    mapPrimaryTravellerDtoToHousehold(dto: HouseholdTraveller) {
        return {
            firstName: dto.firstName,
            lastName: dto.lastName,
            email: dto.email,
            phoneNumber: dto.phoneNumber,
            contactEmail: dto.email,
            contactPhoneNumber: dto.phoneNumber,
        }
    }
}
