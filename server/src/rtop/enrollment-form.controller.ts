import { Controller, Body, Post, Patch, UseInterceptors, ClassSerializerInterceptor, Req, Get, Param, HttpException, HttpStatus, Logger, } from '@nestjs/common';
import { EnrollmentFormDTO, ArrivalTestingEnrollmentFormDTO, ProgramEnrollmentFormDTO } from './dto/enrollment-form.dto';
import { EnrollmentFormService } from './enrollment-form.service';
import { PublicRoute, Roles } from '../decorators';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { EnrollmentFormSubmissionRO } from './ro/form-submission.ro';
import { IsString, MinLength, MaxLength, Matches } from 'class-validator';
import { EnrollmentFormRepository } from './repositories/enrollment-form.repository';
import { EnrollmentFormSearchResultRO } from '../rtop/ro/enrollment-form-search.ro';
import moment from 'moment';
import { DailySubmissionDTO } from './dto/daily-submission.dto';
import { DailySubmission } from './entities/daily-submission.entity';
import { ReminderIDRO } from './ro/reminder-id.ro';
import { ContactMethodVerificationService } from './contact-method-verification.service';
import { ContactMethodVerificationDTO, CodeVerificationDTO, CreateContactMethodVerificationDTO } from './dto/contact-method-verification.dto';
import { EnrollmentFormRO } from './ro/enrollment-form.ro';
import { DAILY_STATUS, CARD_STATUS_REASON } from './constants';
import { USER_TIMEZONE, DATE_FORMAT, DailySubmissionAnswer } from './enrollment-form.constants';
import { CONTACT_METHOD } from '../rtop-admin/repositories/daily-reminder.repository.';
import { CodeVerificationRO } from './ro/code-verification.ro';
import { ContactMethodRO } from './ro/contact-method.ro';
import { EnrollmentConfirmationService } from './enrollment-confirmation.service';
import { SurveyQuestionnaireDTO } from './dto/survey-questionnaire.dto';
import { ProgramType } from './entities/program';


const throw404 = (message = 'No entries found') => {
    throw new HttpException({
      status: HttpStatus.NOT_FOUND,
      error: message,
    }, HttpStatus.NOT_FOUND);
  }

const throw400 = (message, code) => {
  throw new HttpException({
    status: HttpStatus.BAD_REQUEST,
    error: {
      message,
      code
    }
  }, HttpStatus.BAD_REQUEST);
}


class SearchParams {
    @IsString()
    @Matches(/^\d{4}-\d{2}-\d{2}$/)
    birthdate: string;

    @IsString()
    @MinLength(2)
    @MaxLength(128)
    token: string;
  }

@Controller('api/v2/rtop/traveller')
@UseInterceptors(ClassSerializerInterceptor)
export class EnrollmentFormController {
    constructor(
        private readonly enrollmentFormService: EnrollmentFormService,
        private readonly enrollmentFormRepository: EnrollmentFormRepository,
        private readonly contactMethodVerificationService: ContactMethodVerificationService,
        private readonly enrollmentConfirmationService: EnrollmentConfirmationService
    ){}

    /**
     * Submit an enrollment form.
     * Verifies that the given token has not expired, and has not been used
     * Sends a confirmation email to the primary contact email on success.
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post('enroll/:verificationToken/arrival_testing')
    async submitArrivalTestingEnrollmentForm(
      @Req() req,
      @Param('verificationToken') verificationToken: string,
      @Body() body: ArrivalTestingEnrollmentFormDTO
    ):Promise<EnrollmentFormSubmissionRO>{
      return await this.validateAndSubmitForm(verificationToken, req, body, ProgramType.ARRIVAL_TEST);
    }

    /**
     * Submit an enrollment form.
     * Verifies that the given token has not expired, and has not been used
     * Sends a confirmation email to the primary contact email on success.
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post('enroll/:verificationToken')
    async submitEnrollmentForm(
      @Req() req,
      @Param('verificationToken') verificationToken: string,
      @Body() body:EnrollmentFormDTO
    ):Promise<EnrollmentFormSubmissionRO>{
        return await this.validateAndSubmitForm(verificationToken, req, body, ProgramType.BORDER_PILOT);
    }

  private async validateAndSubmitForm(verificationToken: string, req: any, body: ProgramEnrollmentFormDTO, programType: ProgramType) {
    const verification = await this.contactMethodVerificationService.validateContactMethod(verificationToken);
   
    if (!verification) {
      LoggerService.logData(req, ActionName.SEARCH, verificationToken, null);
      throw400('Invalid token', 'invalid_token');
    }
  
    if (verification.expired()) {
      Logger.log('Tried to verify expired token', verificationToken);
      throw400('Token has expired', 'expired_token');
    }
  
    if (!verification.available()) {
      LoggerService.logData(req, ActionName.SEARCH, verificationToken, null);
      throw400('Invalid token', 'invalid_token');
    }

    const household: EnrollmentFormSubmissionRO = await this.enrollmentFormService.saveForm(body, verification, programType);
    await this.contactMethodVerificationService.markAsVerified(verification, household);
    
    LoggerService.logData(req, ActionName.CREATE, null, [household.id]);
    
    this.enrollmentConfirmationService.sendConfirmationEmail(body.email, household.travellers, programType);
    LoggerService.logData(req, ActionName.SEND_ENROLLMENT_CONFIRMATION, null, [household.id]);
    return household;
  }

    /**
     * Verify that the given verification token is valid, has not expired, and is not used.
     * 
     * Returns the associated phone number, email, and contact method.
     * @param verificationToken Verification token to verify
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('verification/:verificationToken')
    async verifyVerificationToken(@Req() req, @Param('verificationToken') verificationToken: string): Promise<ContactMethodRO> {
      const verification = await this.contactMethodVerificationService.validateContactMethod(verificationToken);

      if (!verification) {
        LoggerService.logData(req, ActionName.SEARCH, verificationToken, null);
        throw400('Invalid token', 'invalid_token');
      }

      if(verification.expired()) {
        Logger.log('Tried to verify expired token', verificationToken);
        throw400('Token has expired', 'expired_token');
      }

      if (!verification.available()) {
        LoggerService.logData(req, ActionName.SEARCH, verificationToken, null);
        throw400('Invalid token', 'invalid_token');
      }

      return new ContactMethodRO(verification.contactMethod, verification.email, verification.phoneNumber);
    }

    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post('verification')
    async sendContactMethodVerification(@Req() req, @Body() body: CreateContactMethodVerificationDTO):Promise<CodeVerificationRO>{
        const verification = await this.contactMethodVerificationService.sendVerification(body);

        LoggerService.logData(req, ActionName.CREATE, null, []);

        return verification;
    }

    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post('verification/verify/code')
    async verifyVerificationCode(@Req() req, @Body() codeVerification: CodeVerificationDTO):Promise<CodeVerificationRO>{
        const verification = await this.contactMethodVerificationService.verifyCode(codeVerification.verificationId, codeVerification.code);

        if(!verification) {
          throw400('No verification found', 'verification_not_found');
        }

        LoggerService.logData(req, ActionName.CREATE, null, []);

        return verification;
    }

    /**
     * Verify date of individual traveller
     * Returns travellers id if the record is found
     * Throws 404 otherwise
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':confirmationNumber/verify/:token/:birthdate')
    async verifyByBirthDate(@Req() req, @Param('confirmationNumber') confirmationNumber: string, @Param() {birthdate, token}: SearchParams): Promise<any> {
        birthdate = moment(birthdate).format(DATE_FORMAT);

        const household: EnrollmentFormSearchResultRO = await this.enrollmentFormRepository.findByToken(token);

        if (!household.travellers || !household.travellers.length) {
          LoggerService.logData(req, ActionName.SEARCH, token, null);
          throw404();
        }
        const traveller = household.travellers.find((ele) => ele.confirmationNumber === confirmationNumber);

        if( traveller.dobVerificationAttempt > 4 &&  moment().isBefore(moment(traveller.dobAttemptTime).add('5','minutes')) )
        {
          await this.enrollmentFormRepository.updateDOBVerificationAttempts(traveller.confirmationNumber, false);
          LoggerService.logData(req, ActionName.SEARCH, token, null);
          throw400('You have tired a lot of times', 'max_verification_attempts');
        }

        const res = await this.enrollmentFormRepository.verifyByBirthDate(confirmationNumber, token, birthdate);
        const isVerified = !!(res && res.id);

        await this.enrollmentFormRepository.updateDOBVerificationAttempts(traveller.confirmationNumber, isVerified);

        LoggerService.logData(req, ActionName.SEARCH, birthdate, [res && res.id]);
        return { "verified": isVerified };
    }

    /**
     * Returns the list of travellers of a specific household
     * Hides first and last names of each traveller 
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('reminder/:token')
    async getRtopTravellers(@Req() req, @Param() {token}): Promise<EnrollmentFormSearchResultRO> {
        if(!token) {
            LoggerService.logData(req, ActionName.SEARCH, token, null);
            throw404();
        }

        const res: EnrollmentFormSearchResultRO = await this.enrollmentFormRepository.findByToken(token);
  
        if (!res.travellers || !res.travellers.length) {
          LoggerService.logData(req, ActionName.SEARCH, token, null);
          throw404();
        }

        // Verify that token is associated with a reminder for today
        if(res.date !== moment().format(DATE_FORMAT) && res.date !== moment().tz(USER_TIMEZONE).format(DATE_FORMAT)) {
          throw400('This link is no longer valid. If you need assistance please call 1-888-245-940', 'expired_token');
        }
    
        LoggerService.logData(req, ActionName.SEARCH, token, res.travellers.map(t => t.id));
        return res;
    }

    /**
     * Submits the daily questionnaire for a traveller by monitoring portal agent
     * Verify that the given token is valid for today
     */
    @Roles('GOA')
    @UseInterceptors(ClassSerializerInterceptor)
    @Post(':confirmationNumber/reminder/:token')
    async submitDailyFormByAdmin(@Req() req, @Body() body:DailySubmissionDTO, @Param() {confirmationNumber}, @Param() {token}) :Promise<EnrollmentFormSearchResultRO>{
      if(!token) {
        LoggerService.logData(req, ActionName.SEARCH, token, null);
        throw404();
      }
      if(!confirmationNumber) {
        LoggerService.logData(req, ActionName.SEARCH, confirmationNumber, null);
        throw404();
      }

      // Verifiy that reminder with token exists
      const reminderId: ReminderIDRO = await this.enrollmentFormRepository.getReminderId(confirmationNumber, token);
      if(!reminderId) {
        LoggerService.logData(req, ActionName.SEARCH, confirmationNumber, null);
        throw404();
      }

      // Verify that token is associated with a reminder for today
      if(reminderId.date !== moment().tz(USER_TIMEZONE).format(DATE_FORMAT)) {
        throw400('Submission link has expired', 'expired_token');
      }

      const form = {
        answers: body.answers,
      };

      const submissionId: number = await this.enrollmentFormService.createDailySubmission(confirmationNumber, form, reminderId.travellerId, reminderId);

      LoggerService.logData(req, ActionName.CREATE, null, [confirmationNumber, submissionId]);
      const res: EnrollmentFormSearchResultRO = await this.enrollmentFormRepository.findByToken(token);

      return res;
    }

    /**
     * Submit the daily questionnaire by traveller
     * Verify that the given token is valid for today
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post(':confirmationNumber/reminder/:token/:birthdate')
    async submitDailyForm(@Req() req, @Body() body:DailySubmissionDTO, @Param() {confirmationNumber}, @Param() {birthdate, token}: SearchParams) :Promise<EnrollmentFormSearchResultRO>{
      if(!token) {
        LoggerService.logData(req, ActionName.SEARCH, token, null);
        throw404();
    }
      if(!confirmationNumber) {
        LoggerService.logData(req, ActionName.SEARCH, confirmationNumber, null);
        throw404();
      }

      /**
       * Verify that birth date matches travellers birth date
       * 
       */
      birthdate = moment(birthdate).format(DATE_FORMAT);
      const verifiedBirthDate = await this.enrollmentFormRepository.verifyByBirthDate(confirmationNumber, token, birthdate);
      if(!verifiedBirthDate || !verifiedBirthDate.id) {
        throw404();
      }
      
      return this.submitDailyFormByAdmin(req, body, {confirmationNumber}, { token });
    }

    /**
     * 
     * Submits survey questionnaire on day 3 and day 13
     */
    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post(':token/surveyQuestionnaire')
    async submitSurveyQuestionnaire(@Req() req, @Body() body: SurveyQuestionnaireDTO, @Param() { token }): Promise<EnrollmentFormSearchResultRO> {
      if (!token) {
        LoggerService.logData(req, ActionName.SEARCH, token, null);
        throw404();
      }

      const reminder: EnrollmentFormSearchResultRO = await this.enrollmentFormRepository.findByToken(token);

      if (!reminder.travellers || !reminder.travellers.length) {
        LoggerService.logData(req, ActionName.SEARCH, token, null);
        throw404();
      }

      // Verify that token is associated with a reminder for today
      if (reminder.date !== moment().format(DATE_FORMAT) && reminder.date !== moment().tz(USER_TIMEZONE).format(DATE_FORMAT)) {
        throw400('This link is no longer valid. If you need assistance please call 1-888-245-940', 'expired_token');
      }

      const submission = {
        answers: body.answers,
      };

      await this.enrollmentFormService.submitSurveyQuestionnaire(token, submission, body.surveyType);
      const res: EnrollmentFormSearchResultRO = await this.enrollmentFormRepository.findByToken(token);

      return res;

    }
}
