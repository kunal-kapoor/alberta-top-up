import { EmailTemplates } from "../../email/email.service";
import { TextTemplates } from "../../text.service";

export enum ProgramType {
    BORDER_PILOT='border_pilot',
    ARRIVAL_TEST='arrival_testing'
}

export interface EmailConfig {
    template: string;
    subject: string;
}

export interface TextConfig {
    text: string;
}

export interface Program {
    // Frontend URL for 
    verificationUrl(token: string): string;
    confirmationNumberPrefix: string;
    emailTemplates?: {[key: string]: EmailConfig}
    textTemplates?: {[key: string]: TextConfig}
}

export class BorderPilotProgram implements Program {
    confirmationNumberPrefix = 'AB';

    emailTemplates = {
        [EmailTemplates.EMAIL_VERIFICATION]: {
            template: 'email-verification',
            subject: 'Alberta COVID-19 Border Testing Pilot Program Application',
        },
        [EmailTemplates.ENROLLMENT_CONFIRMATION]: {
            template: 'enrollment-confirmation',
            subject: 'Alberta Border Pilot: Application Confirmation',
        }
    }

    textTemplates = {
        [TextTemplates.VERIFICATION]: {
            text: 'Please use the following code to complete the Alberta COVID-19 Border Testing Pilot Program application: %VERIFICATION_CODE%'
        },
        [TextTemplates.ENROLLMENT_CONFIRMATION]: {
            text: ''
        },
    }

    verificationUrl(token: string): string {
        return `${process.env.APP_URL}/${process.env.ENROL_SUBMISSION_CONFIRMATION_ROUTE || 'enroll'}/${token}`;
    }
}

export class ArrivalTestingProgram implements Program {
    confirmationNumberPrefix = 'CA'

    emailTemplates = {
        [EmailTemplates.EMAIL_VERIFICATION]: {
            template: 'arrival-testing-verification',
            subject: 'Alberta Mandatory COVID-19 Testing Contact Method Verification',
        },
        [EmailTemplates.ENROLLMENT_CONFIRMATION]: {
            template: 'arrival-testing-confirmation',
            subject: 'Alberta Mandatory COVID-19 Testing: Confirmation',
        }
    }

    textTemplates = {
        [TextTemplates.VERIFICATION]: {
            text: 'Please use the following code to complete the Alberta COVID-19 Mandatory Testing registration: %VERIFICATION_CODE%'
        },
        [TextTemplates.ENROLLMENT_CONFIRMATION]: {
            text: ''
        },
    }

    verificationUrl(token: string): string {
        return `${process.env.APP_URL}/arrival-testing/enroll/${token}`;
    }
}

export const programFor = (type: ProgramType): Program => {
    return {
        [ProgramType.ARRIVAL_TEST]: new ArrivalTestingProgram(),
        [ProgramType.BORDER_PILOT]: new BorderPilotProgram()
    }[type];
}