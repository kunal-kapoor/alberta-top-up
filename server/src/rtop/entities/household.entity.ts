import { RTOPTraveller } from './rtop-traveller.entity';
import { SubmissionDetermination } from './submission-determination.entity';
import { CONTACT_METHOD } from '../../rtop-admin/repositories/daily-reminder.repository.';
import { DAILY_STATUS } from '../constants';
import { Exclude } from 'class-transformer';
import { CitiesAndCountries } from '../../arrival-form/arrival-form.constants';
import { ProgramType } from './program';

export enum EnrollmentStatus {
  APPLIED = 'applied',
  WITHDRAWN = 'withdrawn',
  ENROLLED = 'enrolled',
  COMPLETED = 'completed'
}


export class Household extends RTOPTraveller {
  id: number;
  program: ProgramType;
  created: Date;
  lastUpdated: Date;
  viableIsolationPlan: boolean;
  confirmationNumber: string;
  enrollmentStatus: EnrollmentStatus;
  determination: SubmissionDetermination;
  cardStatus?: DAILY_STATUS;

  // Primary Contact
  contactMethod: CONTACT_METHOD;
  contactPhoneNumber: string;
  contactEmail: string;
  phoneNumber: string;
  email: string;
  gender: string;
  citizenshipStatus: string;
  exemptionType: string;
  exemptOccupation: string;
  exemptOccupationDetails: string;
  dateLeftCanada: string;
  dateLeavingCanada: string;
  countryOfResidence: string;
  airline: string;
  flightNumber: string;
  seatNumber: string;
  reasonForTravel: string;
  durationOfStay: string;

  // Travel Information
  hasAdditionalTravellers: boolean;
  additionalTravellers: RTOPTraveller[];

  additionalCitiesAndCountries: CitiesAndCountries[];

  hasTravellersOutsideHousehold: boolean;

  // Arrival Information
  nameOfAirportOrBorderCrossing: string;
  arrivalDate: Date;
  arrivalCityOrTown: string;
  arrivalCountry: string;

  // Determination Information
  determinationDate: Date;

  // Isolation Questionnaire
  address: string;
  cityOrTown: string;
  provinceTerritory: string;
  postalCode: string;
  quarantineLocationPhoneNumber: string;
  typeOfPlace: string;
  howToGetToPlace: string;

  last_updated: Date;
  lastScreened: Date;

  @Exclude()
  lastActivityStatus: string;

  @Exclude()
  lastActivityNote: string;
}
