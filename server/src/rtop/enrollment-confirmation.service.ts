import { Injectable } from "@nestjs/common";
import { EmailService, EmailSendable, EmailTemplates } from "../email/email.service";
import { LoggerService } from "../logs/logger";
import { EnrollmentFormTravellerRO } from "./ro/form-submission.ro";
import { ProgramType } from "./entities/program";

class EnrollmentConfirmationEmail extends EmailSendable {
    from: string;
    to: any;
    subject: string;
    template: string;
    context: any;

    constructor(to: string, data: { travellers: string }, programType: ProgramType = ProgramType.BORDER_PILOT) {
      super(EmailTemplates.ENROLLMENT_CONFIRMATION, programType);
      this.to = to;
      this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
      this.context = data;
      this.subject = 'Alberta Border Pilot: Application Confirmation';
    }
}


@Injectable()
export class EnrollmentConfirmationService {

    constructor(
        private readonly emailService: EmailService,
      ) {
    }

    /**
     * Send confirmation emails to primary contact email
     * on successful enrollment
     */
    async sendConfirmationEmail(contactEmail: string, travellers: Array<EnrollmentFormTravellerRO>, program: ProgramType): Promise<void> {
      try {
        const travellersString = [];
        travellers.map(element => travellersString.push(`${element.firstName} ${element.lastName}: ${element.confirmationNumber}`))

        const msg = new EnrollmentConfirmationEmail(
          contactEmail,
          {
            travellers: travellersString.join(', ')
          },
          program
        );
        await this.emailService.sendEmail(msg);
      }
      catch (e) {
        LoggerService.error("Unable to send enrollment confirmation email.", e)
      }
    }
}


