import { IsNotEmpty, IsString, Length, Matches, Validate, IsOptional, IsArray, ValidateNested, IsBoolean } from 'class-validator';
import { IsNotFutureDate, MaxArrayLength, TransformBooleanString, IsEmptyIfNotToggled } from '../decorators';
import { Type, Exclude, Expose } from 'class-transformer';

export class CitiesAndCountries {
  @IsString()
  @Length(1, 255)
  cityOrTown: string;

  @IsString()
  @Length(1, 255)
  country: string;
}

export class DailySubmissionAnswer {
  @IsString()
  question: string;

  @IsString()
  @Length(0, 60)
  answer: string;
}

export class AdditionalTravellers {
  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  lastName: string;

  @IsNotEmpty()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  @Validate(IsNotFutureDate)
  dateOfBirth: Date;

  @IsOptional()
  @IsString()
  @Length(0, 250)
  gender: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  citizenshipStatus: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  exemptionType: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  exemptOccupation: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  exemptOccupationDetails: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  dateLeavingCanada: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  reasonForTravel: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  durationOfStay: string;

  @IsOptional()
  @IsBoolean()
  @TransformBooleanString()
  preDepartureTest: boolean;

  @IsOptional()
  @IsString()
  preDepartureTestDate: string;

  @IsOptional()
  @IsString()
  preDepartureTestCountry: string;

  @IsOptional()
  @IsBoolean()
  @TransformBooleanString()
  preDepartureVaccination: boolean;

  @IsOptional()
  @IsString()
  timeSinceVaccination: string;

  @IsOptional()
  @IsString()
  dosesOfVaccine: string;

  @IsOptional()
  @IsString()
  seatNumber: string;
}

export function stringToBoolean(yesOrNo: string): boolean {
  if (yesOrNo === 'Yes' || yesOrNo === 'No') {
    return yesOrNo === 'Yes'
  } else {
    return false
  }
}

export function booleanToString(bool:boolean):string{
  return bool? 'Yes':'No'
}

export const COMPLETED_STATUSES = [
  'Not called within 7 days',
  'Filled out form online',
  'No answer, 3 call attempts made',
  'Contact made, no issues',
  'Contact made, would not provide info',
  'Contact made, essential/exempt worker',
  'Contact made, essential/exempt worker',
  'Contact made, other',
  'Contact made, indicated unwilling to self-isolate',
  'Incorrect contact info provided'
]
export const ALL_STATUSES = [].concat(
  COMPLETED_STATUSES, 
  [  
  'None',
  '1st call made, no contact',
  '2nd call made, no contact',
  ]
);

export const USER_TIMEZONE = "America/Edmonton";

export const DATE_FORMAT = 'YYYY-MM-DD';

export const DATE_TIME_FORMAT = 'YYYY-MM-DD-HH.mm.ss';
