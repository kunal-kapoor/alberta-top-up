import { CONTACT_METHOD } from "../rtop-admin/repositories/daily-reminder.repository.";
import { EmailSendable, EmailService, EmailTemplates } from "../email/email.service";
import { TextSendable, TextService, TextTemplates } from "../text.service";
import { Injectable, Logger } from "@nestjs/common";
import { RTOPDbService } from "../db/rtop-dbservice";
import { ContactMethodVerificationRepository } from "./repositories/contact-method-verification.repository";
import { EnrollmentFormSubmissionRO } from "./ro/form-submission.ro";
import { CreateContactMethodVerificationDTO } from "./dto/contact-method-verification.dto";
import { ContactMethodVerification } from "./entities/contact-method-verification";
import { VerificationTokenService } from "./verification-token.service";
import { CodeVerificationRO } from "./ro/code-verification.ro";
import { LoggerService } from "../logs/logger";
import { programFor, ProgramType } from "./entities/program";

const fallbackText = `

`;


class EmailVerificationEmail extends EmailSendable {
    from: string;
    to: any;
    subject: string;
    context: any;

    constructor(to: string, data: any, program?: ProgramType) {
        super(EmailTemplates.EMAIL_VERIFICATION, program);
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
    }
}

class SMSVerificationText extends TextSendable {
    to: string;
    message: string;

    constructor(phoneNumber: string, data: any, program?: ProgramType) {
        super(TextTemplates.VERIFICATION, program, data);
        this.to = phoneNumber;
    }
}

@Injectable()
export class ContactMethodVerificationService {
    constructor(
        private readonly emailService: EmailService,
        private readonly txtService: TextService,
        private readonly contactMethodVerificationRepository: ContactMethodVerificationRepository,
    ) {}
    
    /**
     * Find household by contact method token
     * @param contactMethodToken 
     */
    async validateContactMethod(contactMethodToken: string): Promise<ContactMethodVerification> {
        return await this.contactMethodVerificationRepository.findVerification(contactMethodToken);
    }

    /**
     * Update status of verification to verified + add household record reference
     * @param verification verification to update
     * @param household household to associate to 
     */
    async markAsVerified(verification: ContactMethodVerification, household: EnrollmentFormSubmissionRO) {
        return await this.contactMethodVerificationRepository.markAsVerified(verification, household);
    }
  
  
    /**
     * Sends verifications to verify the chosen contact method.
     * 
     * Phone In:
     * - No verification is required, but we're still creating a record for the event
     * - Return verificationToken to client so it immediately can be used to submit the enrollment form
     * 
     * Email
     * - Send Verification Token to email address as a magic link the user can click on to submit the enrollment form
     *
     * SMS
     * - Send a numeric verification code to the users phone
     * - Return verificationId to client
     * - Verification code (that user receives) and verification ID can be used to retrieve the verification token required to submit the
     *   enrolment form by calling `api/v2/rtop/traveller/verification/verify/code` 
     */
    async sendVerification(verification: CreateContactMethodVerificationDTO): Promise<CodeVerificationRO> {
        // Token that is needed to submit the enrolment form
        const verificationToken = VerificationTokenService.createToken(48); // Random token of 48 characters

        // Numeric verification code sent to user when SMS is selected
        const verificationCode = VerificationTokenService.createNumericCode(6); // Random numeric code of 6 characters

        // Verification id used together with the numeric verification code to
        // verify a phone number
        const verificationId = VerificationTokenService.createToken(48); // Random token of 48 characters

        await RTOPDbService.createContactMethodVerification(verificationToken, verification.contactMethod, verification.email, verification.phoneNumber, `${verificationCode}`, verificationId);
        const questionnaireLink = programFor(verification.program || ProgramType.BORDER_PILOT).verificationUrl(verificationToken);

        switch (verification.contactMethod) {
            case CONTACT_METHOD.EMAIL: {
                await this.sendEmailVerification(verification.email, questionnaireLink, verification.program);
                break;
            }

            case CONTACT_METHOD.SMS: {
                await this.sendSMSVerification(verification.phoneNumber, verificationCode, verification.program);
                return new CodeVerificationRO({TOKEN: verificationId});
            }

            case CONTACT_METHOD.CALLIN:
                return new CodeVerificationRO({TOKEN: verificationToken});
        }

        return null;
    }

    /**
     * 
     * @param verification 
     */
    public async verifyCode(verificationId: string, code: string): Promise<CodeVerificationRO> {
        return await this.contactMethodVerificationRepository.verifyCode(verificationId, code);
    }

    /**
     * Sends verification text to verify a households phone number
     */
    private async sendSMSVerification(phoneNumber: string, verificationCode: number, program: ProgramType) {
        try {
            const msg = new SMSVerificationText(phoneNumber, {
                verificationCode
            }, program);
            await this.txtService.sendText(msg);
        }
        catch (e) {
            LoggerService.logger.error('Failed to send sms verification', e);
            throw e;
        }
    }

    /**
     * Sends verification email to verify a households email address
     */
    private async sendEmailVerification(email: string, applicationLink: string, program: ProgramType) {
        try {
            const msg = new EmailVerificationEmail(email, {
                applicationLink
            }, program);
            await this.emailService.sendEmail(msg);
        }
        catch (e) {
            LoggerService.logger.error('Failed to send email verification', e);
            throw e;
        }
    }

  
}