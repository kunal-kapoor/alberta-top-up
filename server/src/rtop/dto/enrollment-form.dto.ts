import {
  IsOptional,
  IsString,
  IsArray,
  IsBoolean,
  ValidateNested,
  Length,
  Matches,
  Validate,
  ValidateIf,
} from 'class-validator';

import { Exclude, Type } from 'class-transformer';
import { TransformBooleanString, MaxArrayLength, IsNotFutureDate, IsEmptyIfNotToggled } from '../../decorators';
import { AdditionalTravellers, CitiesAndCountries } from '../enrollment-form.constants';

export class ProgramEnrollmentFormDTO {
  @Exclude()
  id?: number;

  @Exclude({ toClassOnly: true })
  viableIsolationPlan: boolean;

  @IsString()
  @Length(1, 255)
  firstName: string;

  @IsString()
  @Length(1, 255)
  lastName: string;

  @IsString()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  @Validate(IsNotFutureDate)
  dateOfBirth: string;

  @IsOptional()
  @IsString()
  @Length(0, 35)
  phoneNumber: string;

  @IsOptional()
  @IsString()
  @Matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$|$^/)// eslint-disable-line no-useless-escape
  email: string;

  @IsString()
  @Length(1, 255)
  nameOfAirportOrBorderCrossing: string;

  @IsString()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  arrivalDate: string;

  @IsString()
  @Length(1, 255)
  arrivalCityOrTown: string;

  @IsString()
  @Length(1, 255)
  arrivalCountry: string;

  @IsArray()
  @IsOptional()
  @Validate(MaxArrayLength, [10], { message: 'Cannot submit more than 10 additional cities and countries' })
  @ValidateNested({ each: true})
  @Type(() => CitiesAndCountries)
  additionalCitiesAndCountries: Array<CitiesAndCountries>

  @IsString()
  @Length(1, 1024)
  address: string;

  @IsString()
  @Length(1, 255)
  cityOrTown: string;

  @IsString()
  @Length(1, 255)
  provinceTerritory: string;

  @IsOptional()
  @IsString()
  @Length(0, 32)
  postalCode: string;

  @IsString()
  @Length(1, 35)
  quarantineLocationPhoneNumber: string;

  @IsString()
  @Length(1, 512)
  typeOfPlace: string;

  @IsString()
  @Length(1, 512)
  howToGetToPlace: string;

  @IsBoolean()
  @TransformBooleanString()
  hasAdditionalTravellers: boolean;

  @IsOptional()
  @IsArray()
  @Validate(MaxArrayLength, [10], { message: 'Cannot submit more than 10 additional travellers' })
  @ValidateNested({ each: true })
  @Type(() => AdditionalTravellers)
  @Validate(IsEmptyIfNotToggled, ['hasAdditionalTravellers'])
  additionalTravellers: Array<AdditionalTravellers>

  constructor(data?: ProgramEnrollmentFormDTO) {
    if(data) {
      Object.assign(this, data);
    }
  }
}

export class ArrivalTestingEnrollmentFormDTO extends ProgramEnrollmentFormDTO {
  constructor(data: ArrivalTestingEnrollmentFormDTO) {
    super(data);
  }
}

export class EnrollmentFormDTO extends ProgramEnrollmentFormDTO{

  @IsString()
  @Length(1, 250)
  gender: string;

  @IsString()
  @Length(1, 255)
  citizenshipStatus: string;

  @IsString()
  @Length(1, 255)
  exemptionType: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  exemptOccupation: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  exemptOccupationDetails: string;
  
  @IsOptional()
  @IsString()
  @Length(0, 255)
  dateLeftCanada: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  dateLeavingCanada: string;

  @IsString()
  @Length(1, 255)
  countryOfResidence: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  airline: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  flightNumber: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  seatNumber: string;

  @IsOptional()
  @IsString()
  @Length(0, 255)
  reasonForTravel: string;

  @IsString()
  @Length(1, 255)
  durationOfStay: string;

  @IsOptional()
  @IsBoolean()
  @TransformBooleanString()
  preDepartureTest: boolean;

  @IsOptional()
  @IsString()
  @ValidateIf(obj => obj.preDepartureTest && obj.preDepartureTestDate !== '')
  @Validate(IsEmptyIfNotToggled, ['preDepartureTest'])
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  preDepartureTestDate: string;

  @IsOptional()
  @IsString()
  @Validate(IsEmptyIfNotToggled, ['preDepartureTest'])
  preDepartureTestCountry: string;

  @IsBoolean()
  @TransformBooleanString()
  preDepartureVaccination: boolean;

  @IsOptional()
  @IsString()
  @Validate(IsEmptyIfNotToggled, ['preDepartureVaccination'])
  timeSinceVaccination: string;

  @IsOptional()
  @IsString()
  @Validate(IsEmptyIfNotToggled, ['preDepartureVaccination'])
  dosesOfVaccine: string;

  constructor(data?: EnrollmentFormDTO) {
    super(data);

    if(data) {
      Object.assign(this, data);
    }
  }
}
