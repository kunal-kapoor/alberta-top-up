import { Injectable } from "@nestjs/common";
import { RTOPDbService } from "../db/rtop-dbservice";
import { CONTACT_METHOD, NOTIFICATION_STATUS } from "../rtop-admin/repositories/daily-reminder.repository.";
import { EmailService, EmailSendable } from "../email/email.service";
import { TextService, TextSendable } from "../text.service";
import { LoggerService } from "../logs/logger";
import { SlackNotifierService } from "../slack-notifier.service";

class BulkEmailSendable implements EmailSendable {
    from: string;
    to: any;
    subject: string;
    template: string;
    context: any;

    constructor(to: string, template: string, subject: string) {
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = {};
        this.subject = subject;
        this.template = template;
    }
}

class BulkTextSendable implements TextSendable {
    message: string;
    to: string;

    constructor(to: string, message: string) {
        this.to = to;
        this.message = message;
    }
}

@Injectable()
export class BulkNotificationService {

    constructor(
        private readonly emailService: EmailService,
        private readonly textService: TextService,
        private readonly slackNotificationSerivce: SlackNotifierService
    ) {}

    async sendBulk(notificationName: string, emailTemplate: string, emailSubject: string, smsMessage: string) {
        LoggerService.info('Initiating sending of bulk notification ' + notificationName);

        const unsent = (await RTOPDbService
            .listUnsentBulkNotifications(notificationName))
            .value()
            .map(res => ({
                id: res.ID,
                contactMethod: res.CONTACT_METHOD,
                contactEmail: res.CONTACT_EMAIL,
                contactPhoneNumber: res.CONTACT_PHONE_NUMBER
            }));

        const logMessage = `Sending bulk notification ${notificationName} to ${unsent?.length} households`;
        LoggerService.info(logMessage);
        await this.slackNotificationSerivce.postMessage(logMessage);

        let successful = 0;
        let failed = 0;
        let i = 0;

        for(const household of unsent) {
            if(i % 50 === 0) {
                LoggerService.info(`Sent ${i + 1} of ${unsent?.length} notifications. ${successful} succeeded, ${failed} failed.`)
            }
            try {
                switch(household.contactMethod) {
                    case CONTACT_METHOD.SMS:
                        try {
                            await this.textService.sendText(new BulkTextSendable(household.contactPhoneNumber, smsMessage));
                            await this.sendingSuccessful(household, notificationName);
                            successful++;
                        } catch(e) {
                            await this.sendingFailed(household, notificationName);
                            failed++;
                        }
                        break;
                    case CONTACT_METHOD.EMAIL:
                    case CONTACT_METHOD.CALLIN:
                        try {
                            await this.emailService.sendEmail(new BulkEmailSendable(household.contactEmail, emailTemplate, emailSubject));
                            await this.sendingSuccessful(household, notificationName);
                            successful++;
                        } catch(e) {
                            await this.sendingFailed(household, notificationName);
                            failed++;
                        }
                        break;
                }

            } catch(e) {
                LoggerService.error(`Failed sending notification to household ${household.id}`)
            }
            i++;
        }

        LoggerService.info(`Sent bulk notification ${notificationName} to ${unsent?.length} households. ${successful} succeeded, ${failed} failed.`);

        return [successful, failed];
    }

    private contactMethods(household) {
        const email = (household.contactMethod === CONTACT_METHOD.EMAIL || household.contactMethod === CONTACT_METHOD.CALLIN)? household.contactEmail: null;
        const phoneNumber = household.contactMethod === CONTACT_METHOD.SMS? household.contactPhoneNumber: null;

        return [email, phoneNumber];
    } 

    private async sendingFailed(household, notificationName) {
        try {
            const [email, phoneNumber] = this.contactMethods(household);

            return RTOPDbService.addNotificationLogEntry(household.id, notificationName, household.contactMethod, email, phoneNumber, NOTIFICATION_STATUS.FAILED);
        } catch(e) {
            LoggerService.error(`failed to log error when sending to ${household.id}, ${household.contactMethod}`);
        }
    }

    private async sendingSuccessful(household, notificationName) {
        try {
            const [email, phoneNumber] = this.contactMethods(household);

            return RTOPDbService.addNotificationLogEntry(household.id, notificationName, household.contactMethod, email, phoneNumber, NOTIFICATION_STATUS.SENT);
        } catch(e) {
            LoggerService.error(`failed to log success when sending to ${household.id}, ${household.contactMethod}`);
        }
    }


}
