import { LoggerService } from '../logs/logger';
import { DB2_ERRORS } from "./db2errors";
import { DB2Pool } from './db2pool';

// Errors that should trigger failover at query time
const CONNECTION_ISSUE_SQL_CODES = [DB2_ERRORS.COMMUNICATIONS_ERROR];

// Parse individual connection settings
const [MAIN_HOST, SECONDARY_HOST] = process.env.hostname.split(',');
const MAIN_CERTIFICATE = process.env.primaryCertificate || '/mnt/db2-cert-primary/db2-primary.crt';
const SECONDARY_CERTIFICATE = process.env.secondaryCertificate || '/mnt/db2-cert-secondary/db2-secondary.crt';

// Failover connections
const primaryConnection = new DB2Pool(MAIN_HOST, MAIN_CERTIFICATE, 'Primary Database');
const secondaryConnection = SECONDARY_HOST? new DB2Pool(SECONDARY_HOST, SECONDARY_CERTIFICATE, 'Secondary Database'): null;

// The current active connection
let currentConnection = primaryConnection;


/**
 * Switches the current connection between primary - secondary as need
 * @param e Error message that caused the switch
 */
const triggerFailover = e => {
    if(!currentConnection) {
        currentConnection = primaryConnection;
    } 
    
    let errorMsg = `Connection to ${currentConnection.name} failed.`;
    
    if(secondaryConnection) {
        const newConnection = currentConnection === primaryConnection? secondaryConnection: primaryConnection;
        
        errorMsg += ` Switching to ${newConnection.name}`;

        currentConnection = newConnection;
    } else {
        errorMsg += ` No backup options available`;
    }

    LoggerService.error(errorMsg + '. SQLCODE: ' + e.sqlcode, e);
}

/**
 * Opens a new db2 connection from the coonnection pool of the current connection.
 * If an error happens at time of connection creation, this triggers a failover between the primary and secondary database (if available),
 * and the connection setup is retried.
 * 
 * Note: Whenever db.close() is called, that connection is freed to be
 * reused for subsequent calls to the coonnection pool.
 * 
 * Wraps callback in a promise for easier usage.
 */
const newConnection = (retry=true) => new Promise((resolve, reject) => {
    const doOpenConnection = doRetry => {
        currentConnection.open((err, db) => {
            if(err) {
                LoggerService.error('Failed to open connection pool', err);
                
                triggerFailover(err);
    
                if(doRetry) {
                    return doOpenConnection(false);
                }
            
                reject(err);
            } else {
                resolve(db);
            }
        });    
    }

    doOpenConnection(retry);
});

/**
 * Returns a new db2 connection taken from the connection pool
 */
export async function db2Connection(): Promise<any> {
    try {
        return await newConnection();
    } catch(e) {
        LoggerService.error('Failed to open new connection', e);
        throw e;
    }
}


/**
 * Utility function that performs the given query with queryArgs.
 * Handles opening and closing of the connection.
 * 
 * Triggers a failover and retries the query If a connection error is detected
 * @param query The query to perform (sql)
 * @param queryArgs The arguments to pass in to the query
 */
export async function query(query, queryArgs?): Promise<any> {
    console.log(`doQuery`)
    return doQuery(query, queryArgs, true)
}

const doQuery = async (query, queryArgs?, retry=true) => {
    const conn = await db2Connection();
    try {
        return await conn.query(query, queryArgs);
    } catch(e) {
        console.error(e)
        if(CONNECTION_ISSUE_SQL_CODES.includes(e.sqlcode)) {
            triggerFailover(e);

            if(retry) {
                return doQuery(query, queryArgs, false);
            }
        }

        throw e;
    } finally {
        await conn.close();
    }
}
