import { TopupForm } from "../topup/entities/topup-form.entity";
import { query } from "./db2connection";
import { TopupFormSubmissionRO } from "../topup/ro/topup-submission.ro";
import { ServiceAlbertaFormRO } from "../service-alberta/ro/form.ro";
import { HttpException, HttpStatus } from "@nestjs/common";

/**
 * Provides utilities to transform results of ArrivalForm queries
 * to a form that's more easier to work with
 */
class ResultTransformer {
    constructor(private result) {}

    count = ():number => {
        const [{COUNT=0}] = this.result || [{COUNT: 0}];
        return COUNT;
    }

    /**
     * Returns a ArrivalFormDTO as the FORM_RECORD column of the first
     * row in the query results.
     */
    single = async ():Promise<TopupFormSubmissionRO> => {
        console.log('single')
        console.dir(this.result)
        const [form] = this.result || [null];
        if(form) {
            console.log('got form')
            const record = JSON.parse(form['FORM_RECORD']);
            record.id = form['ID'];
            console.dir(record)
            return record
            // return new TopupFormSubmissionRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleRaw = async (): Promise<TopupForm> => {
        console.log('singleRaw')
        const [form] = this.result || [null];
        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.id = form['ID'];
            return record;
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleService = async ():Promise<ServiceAlbertaFormRO> => {
        const [form] = this.result || [null];

        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.id = form['ID'];
            return new ServiceAlbertaFormRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns a list of TopupForms as the FORM_RECORD columns
     * from all the search results
     */
    multiple = async (): Promise<TopupForm[]> => {
        return this.result.map(({ID, FORM_RECORD,STATUS,OWNER,OWNERID, LAST_UPDATED, DETERMINATION_DATE}) => {
            const rec = JSON.parse(FORM_RECORD);
            rec.id = ID;
            rec.status = STATUS;
            rec.owner = OWNER;
            rec.lastUpdated = LAST_UPDATED;
            rec.assignedTO = OWNERID;
            rec.determinationDate = DETERMINATION_DATE;
            return rec;
        });
    }

    /**
     * Returns the id of the first row of the results.
     */
    idOnly = async (): Promise<number> => {
        console.log(`idOnly`)
        if (!this.result || !this.result.length) { return null }
        const [{ ID = null }] = this.result || [];

        return ID;
    }

    value = ():any => this.result;
}

export async function topupFormQuery(queryStr, queryArgs?) {
    const res = await query(queryStr, queryArgs);
    return new ResultTransformer(res);
}