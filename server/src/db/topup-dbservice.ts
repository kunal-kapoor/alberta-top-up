import { Injectable } from "@nestjs/common";
import { query } from "./db2connection";
import { arrivalFormQuery } from "./db2connectionUtils";
import { topupFormQuery } from "./topup-db2connectionUtils";
import { IsolationPlanReportProperty } from "../arrival-form/repositories/isolation-plan.repository";
import { IsolationPlanReport } from "../arrival-form/entities/isolation-plan-report.entity";
import moment from "moment";
import { UnwillingTravellerRO } from "../arrival-form/ro/unwilling-traveller.ro";
import { AgentDTO } from "../service-alberta/service-alberta.repository";
import { CheckPointResultRO } from "../service-alberta/ro/checkpoint-report.ro";
import { REPORTS } from "../service-alberta/constants";

import { TopupForm } from '../topup/entities/topup-form.entity'

@Injectable()
export class DbService {
    // Arrival form queries
    private static readonly AddForm: string = `
        select id from final table(
            insert into form(
                form_record,
                last_name,
                first_name
            )
            values (SYSTOOLS.JSON2BSON(?), ?, ?)
        )`;
    private static readonly FindByLastName: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from form WHERE LOWER(last_name) LIKE ? || '%'`;
    private static readonly FindByLastNameExact: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from form WHERE LOWER(last_name) = ?`;
    private static readonly FindByNumber: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from form WHERE LOWER(confirmation_number)=?`;
    private static readonly FindById: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from form where id=?`;

    // Submit determination
    private static readonly SubmitDetermination: string = `
        update form SET
            form_record=SYSTOOLS.JSON_UPDATE(form_record,?),
            exempt=?,
            determination_date=MIN(CURRENT_TIMESTAMP, arrival_date)
        where id=?`;

    private static readonly EditForm: string = `
        update form SET
            form_record=SYSTOOLS.JSON_UPDATE(form_record,?)
        where id=?`;

    private static readonly CountNumber: string = `select count(*) as count from form where confirmation_number=?;`;
    private static readonly AddActivity: string = `INSERT INTO activity(date, agent_id, form_id, type, note, status) VALUES (?,?,?,?,?,?)`;
    private static readonly UpdateStatus: string = `UPDATE form SET status = ? , last_updated = ? WHERE id = ?`;
    public static readonly CountSelect: string = `count(*) as count`;

    // Note on last_updated: If act.date (latest activity for record) is not defined
    // we fall back to the created date of the record. We convert it to a TIMESTAMP
    // so we can compare the two for sorting purposes. 
    public static readonly FormSelect: string = `
        f.id AS ID,
        f.status AS STATUS,
        a.name AS OWNER,
        f.assigned_to AS OWNERID,
        f.last_updated,
        f.determination_date,
        SYSTOOLS.BSON2JSON(f.form_record) AS FORM_RECORD
    `;

    // Isolation plan queries
    private static readonly AddReport: string = `select id from final table(INSERT INTO report(date, passed, failed, passedThenFailed, failedThenPassed) values (?,?,?,?,?))`;
    private static readonly GetReport: string = 'select * from report where id=?';
    private static readonly GetReportByDate: string = 'SELECT * FROM report WHERE date=?;';

    // Unwilling Traveller queries
    private static readonly AddUnwillingTraveller: string = `select id from final table(insert into unwilling_traveller(agent_id, num_travellers, note) values (?,?,?));`;
    private static readonly GetUnwillingTraveller: string = `select * from unwilling_traveller where id=?`;

    private static readonly CountUnwillingTravellers: string = `
    SELECT COALESCE(sum(num_travellers), 0) as count
    FROM unwilling_traveller
    WHERE ? <= created
    AND ? >= created
    `

    // Service Alberta queries
    private static readonly ListActiveAgents = `
        select distinct a.name from activity act
        join agent a
            on a.id=act.agent_id
    `;
    private static readonly GetAgent = `select * from agent where id=?`;
    private static readonly CreateAgent = `select id from final table(insert into agent(id, name) values(?, ?))`;
    private static readonly AssignAgent = `update form set assigned_to=?, prev_user=? where id=?`;
    private static readonly GetActivity = `
        select act.id, a.name as agentname, act.date, act.type, act.note, act.status from activity act
        join agent a
          on a.id=act.agent_id
        where act.form_id=?
        ORDER BY 
        act.date DESC
    `;
    private static readonly GetFormByNumber = `
        select f.id as id, f.determination_date, a.id AS agentId, a.name AS agentName, SYSTOOLS.BSON2JSON(form_record) as form_record
            FROM form f
            LEFT JOIN agent a
            ON f.assigned_to=a.id
        WHERE LOWER(f.confirmation_number)=?
    `;
    private static readonly AssgnedTo = `select agent.id, agent.name from form inner join agent on form.assigned_to=agent.id where form.id=?`;

    // Report Queries

    private static readonly ServiceAlbertaReport = `
    SELECT status, COUNT(*) as count
    FROM form
    WHERE ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY status;
    `;

    private static readonly CheckpointAllTravellers = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from form
    WHERE ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly CheckpointExempt= `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from form
    WHERE exempt = TRUE
    AND ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;
    private static readonly CheckpointRequiringAccommodation = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from form
    WHERE JSON_VAL(form_record,'determination.determination', 's:32672' ) = 'Support'
    AND JSON_VAL(form_record,'hasPlaceToStayForQuarantine','s:32672') = FALSE
    AND ? <= arrival_date
    AND ? >= arrival_date
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly CheckpointRevision = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, COUNT(*) as COUNT from form
    WHERE JSON_VAL(form_record,'determination.determination', 's:32672' ) = 'Support'
    AND ? <= arrival_date
    AND ? >= arrival_date
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly callCount = `
       SELECT 
            agent.id, agent.name, count(*) call_count
        FROM
            activity
            INNER JOIN agent
                ON agent.id = activity.agent_id
            WHERE ? <= activity.date
            AND ? >= activity.date
        GROUP BY 
            agent.id, agent.name
        ORDER BY 
            agent.name
    `;

    private static readonly totalCallCount = `
    SELECT 
        'Total' as name, count(*) call_count
     FROM
         activity
         WHERE ? <= activity.date
         AND ? >= activity.date
    `;
    
    private static readonly selectReportAccess = `
    SELECT ar.REPORT_NAME,ara.AGENT_EMAIL FROM AGENT_REPORT_ACCESSCONTROL ara 
        INNER JOIN AVAILABLE_REPORT ar ON ARA.REPORT_TYPE = ar.ID 
    `;

    private static readonly agentActiveReportAccess = `
    ${DbService.selectReportAccess} WHERE AGENT_EMAIL = ? AND ENABLED = ?;`

    private static readonly agentFullReportAccess = `
    ${DbService.selectReportAccess} WHERE AGENT_EMAIL = ? AND ar.REPORT_NAME in (?);`

    private static readonly createReportAccess = `
    INSERT INTO AGENT_REPORT_ACCESSCONTROL 
        (AGENT_EMAIL, REPORT_TYPE, ENABLED,LAST_UPDATED) 
        SELECT ?,ID,TRUE,CURRENT_TIMESTAMP FROM AVAILABLE_REPORT WHERE REPORT_NAME = ?;
    `;

    private static readonly fetchAccessControlList = `
    ${DbService.selectReportAccess} WHERE ENABLED = TRUE ORDER BY AGENT_EMAIL;
    `;

    private static readonly updateReportAccess = `
    UPDATE AGENT_REPORT_ACCESSCONTROL 
        SET ENABLED = ?, LAST_UPDATED = CURRENT_TIMESTAMP 
        WHERE AGENT_EMAIL = ?
    `;

    private static readonly enableReportAccess = `
    ${DbService.updateReportAccess} AND REPORT_TYPE = 
        (SELECT ID FROM AVAILABLE_REPORT WHERE REPORT_NAME = ?) AND ENABLED = FALSE;
    `;

    private static readonly revokeReportAccess = `
    ${DbService.updateReportAccess} AND REPORT_TYPE NOT IN 
        (SELECT ID FROM AVAILABLE_REPORT WHERE REPORT_NAME IN 
            (?${',?'.repeat(Object.values(REPORTS).length-1)}) ) AND ENABLED = TRUE;
    `;

    private static readonly revokeFullReportAccess = `
    ${DbService.updateReportAccess} AND ENABLED = TRUE;
    `;

    /**
     * Returns the number of rows in a table
     *
     * @param name - table name
     */
    public static async getCount(name: string): Promise<any> {
        return (await arrivalFormQuery(`select count(*) as count from ${name};`))
            .count();

    }

     /**
     * Inserts a new topup form into a database
     *
     * @param form - from details
     */
    public static async saveTopupForm(form: TopupForm) {
        const formAsJsonString = JSON.stringify(form);
        return await topupFormQuery(
            DbService.AddForm,
            [
                formAsJsonString,
                form.lastName,
                form.firstName,
            ]);
    }
    /**
     * Selects a specific topup form by id
     *
     * @param id - from id
     */
    public static async getTopupForm(id: number) {
        return await topupFormQuery(DbService.FindById, [id]);
    }
    /**
     * Selects a specific form by confirmation number
     *
     * @param num - confirmation number
     */
    public static async getFormByNumber(num: string) {
        return await arrivalFormQuery(DbService.FindByNumber, [num.toLowerCase()]);
    }
    /**
     * Selects a specific report by last name partial matching
     *
     * @param lname - last name of a traveller who submitted a form
     */
    public static async getFormByLastName(lname: string) {
        return await arrivalFormQuery(DbService.FindByLastName, [lname.toLowerCase()]);
    }

    /**
     * Selects a specific report by last name exact matching
     *
     * @param lname - last name of a traveller who submitted a form
     */
    public static async getFormByLastNameExact(lname: string) {
        return await arrivalFormQuery(DbService.FindByLastNameExact, [lname.toLowerCase()]);
    }
    
    /**
     * Submits determination for a form
     *
     * @param arrivalFormId - form id
     * @param updateField - JSON string of determination
     */
    public static async submitDetermination(updateField: any, exemptStatus:boolean ,arrivalFormId: number) {
        return await arrivalFormQuery(DbService.SubmitDetermination, [updateField,exemptStatus,arrivalFormId]);
    }

    /**
     * Updates the forms fields of the given form to the given values 
     *
     * @param agentID - ID of the agent making the change
     * @param arrivalFormId - ID of the form to update
     * @param updateField - JSON string of fields to update
     */
    public static async editForm(agentID: string, arrivalFormId: number, updatedFields: string) {
        const res = await arrivalFormQuery(DbService.EditForm, [updatedFields, arrivalFormId]);
    }

    /**
     * Returns the number of submitted forms by confirmation number
     *
     * @param num - confirmation number
     */
    public static async countByNum(num: string) {
        return (await arrivalFormQuery(DbService.CountNumber, [num])).count();
    }

    /**
     * Creates a new activity
     *
     * @param activity - activity data
     * @param userId - agent id
     * @param formId - id of a form
     */
    public static async addActivity(activity:any, userId:string, formId:number): Promise<any> {
        const args = [
            moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            userId,
            formId,
            "Status Changed",
            activity.note,
            activity.status];
        return (await arrivalFormQuery(DbService.AddActivity, args));
    }
    /**
     * Updates activity status
     *
     * @param status - new status of a form
     * @param id - form id
     */
    public static async updateStatus(status:string, id:number): Promise<any> {
        return (await query(DbService.UpdateStatus, [status, moment().utc().format('YYYY-MM-DD HH:mm:ss') ,id]));
    }

    // isolation plan
    /**
     * Updates report
     *
     * @param id - report id
     * @param property - property to be updated
     */
    public static async updateReport(id: number, property: IsolationPlanReportProperty) {
        return await query(`
              UPDATE report SET 
              ${property}=${property}+1
              WHERE id=?;
          `, [id]);
    }

    /**
     * Inserts a new report into a database
     *
     * @param plan - report properties
     */
    public static async saveReport(plan: IsolationPlanReport): Promise<any> {
        return await query(DbService.AddReport,
            [plan.date, plan.passed, plan.failed, plan.passedThenFailed, plan.failedThenPassed]);
    }
    /**
     * Selects a specific report by id
     *
     * @param id - report id
     */
    public static async getReportById(id: number): Promise<any> {
        return await query(DbService.GetReport, [id]);
    }
    /**
     * Selects a specific report by data
     *
     * @param date - report date
     */
    public static async getReportByDate(date: string): Promise<any> {
        return await query(DbService.GetReportByDate, [date]);
    }

    // Service Alberta
    /**
     * Returns the information about an agent
     *
     * @param user
     */
    public static async getAgent(user: any): Promise<any> {
        return await query(DbService.GetAgent, [user.sub]);
    }
    /**
     * Creates a new agent
     *
     * @param user
     */
    public static async createAgent(user: any): Promise<any> {
        return await query(DbService.CreateAgent, [user.sub, user.name]);
    }

    /**
     * Lists agents that have submitted at least one form activity
     */
    public static async listActiveAgents(): Promise<AgentDTO[]> {
        return (await query(DbService.ListActiveAgents, []))
            .map(r => ({id: r.ID, name: r.NAME}));
    }

    /**
     * Returns limited form data for Service Alberta
     *
     * @param num - confirmation number
     */
    public static async getServiceFormByNumber(num: string): Promise<any> {
        return await arrivalFormQuery(DbService.GetFormByNumber, [num.toLowerCase()]);
    }
    /**
     * Returns notes for specific traveller
     *
     * @param formId - id of a submitted form
     */
    public static async getActivity(formId: number): Promise<any> {
        return await query(DbService.GetActivity, [formId]);
    }
    /**
     * Assigns a new agent to a form
     *
     * @param agent - agent id (null - for unassign)
     * @param prevAgent - id of a person who was assigned to this form before (null - if none)
     * @param form - form id
     */
    public static async assignTo(agent: any, prevAgent: any, form: number): Promise<any> {
        return await query(DbService.AssignAgent, [agent, prevAgent, form]);
    }
    /**
     * Returs information about the agent who is assigned to the form
     *
     * @param form - id of a submitted form
     */
    public static async assignedTo(form: number): Promise<any> {
        return await query(DbService.AssgnedTo, [form]);
    }

    /**
     * Creates a new unwilling traveller record
     */
    public static async recordUnwillingTraveller(agentId: string, numTravellers: number, note: string): Promise<UnwillingTravellerRO> {
        const res = await query(DbService.AddUnwillingTraveller, [agentId, numTravellers, note || '']);
        const [{ID}] = res || {ID: null};
        return new UnwillingTravellerRO(ID);
    }

    /**
     * Retrieves an unwilling traveller record
     */
    public static async getUnwillingTraveller(id: number): Promise<UnwillingTravellerRO> {
        const res = await query(DbService.GetUnwillingTraveller, [`${id}`]);

        const [{ID, CREATED, AGENT_ID, NOTE, NUM_TRAVELLERS}] = res || {};

        const record = new UnwillingTravellerRO(ID);

        record.created = CREATED;
        record.agentId = AGENT_ID;
        record.note = NOTE;
        record.numTravellers = NUM_TRAVELLERS;

        return record;
    }

    public static async generateCheckpointReport(startDate:string,endDate:string) : Promise<CheckPointResultRO[]> {

        return [
            new CheckPointResultRO('# of travellers',(await query(this.CheckpointAllTravellers,[startDate,endDate]))),
            new CheckPointResultRO('# of exempt travellers',(await query(this.CheckpointExempt,[startDate,endDate]))),
            new CheckPointResultRO('# of isolation plans requiring revisions',(await query(this.CheckpointRevision,[startDate,endDate]))),
            new CheckPointResultRO('# of travellers requiring accommodation/ transportation',(await query(this.CheckpointRequiringAccommodation,[startDate,endDate])))
        ]
    }
    public static async generateServiceAlbertaReport(startDate:string,endDate:string){
        return await query(this.ServiceAlbertaReport,[startDate,endDate]);  
    }
    public static async generateCallCountReport(startDate:string,endDate:string){
        const agentReport = await query(this.callCount,[startDate,endDate]);
        const overallReport = await query(this.totalCallCount,[startDate,endDate]);

        return [].concat(overallReport).concat(agentReport).map(({NAME, CALL_COUNT}) =>({
            'Agent': NAME,
            'Calls Made': CALL_COUNT
        }));
    }

    public static async getActiveReportAccessForAgent(agentEmail: string): Promise<any> {
        return await query(DbService.agentActiveReportAccess, [agentEmail.toLowerCase(), true ]);
    }

    public static async getReportAccessStatusForAgent(agentEmail: string,reportName: string): Promise<any> {
        return await query(DbService.agentFullReportAccess, [agentEmail.toLowerCase(), reportName ]);
    }

    public static async createReportAccessForAgent(agentEmail: string, reportName: string): Promise<any>{
        return await query(DbService.createReportAccess, [agentEmail.toLowerCase(), reportName]);
    }

    public static async enableReportAccessForAgent(agentEmail: string, reportName: string): Promise<any>{
        return await query(DbService.enableReportAccess, [true, agentEmail.toLowerCase(), reportName]);
    }

    public static async revokeReportAccessForAgent(agentEmail: string, reportName: string[]): Promise<any>{
        while(reportName.length < Object.values(REPORTS).length )
        {
            reportName.push(reportName[0]);
        }
        return await query(DbService.revokeReportAccess, [false, agentEmail.toLowerCase(), ...reportName]);
    }

    public static async revokeFullReportAccessForAgent(agentEmail: string): Promise<any>{
        return await query(DbService.revokeFullReportAccess, [false, agentEmail.toLowerCase()]);
    }

    public static async getReportAccessControlList(): Promise<any>{
        return await query(DbService.fetchAccessControlList);
    }
}