import { db2Connection } from "./db2connection";
import { LoggerService } from "../logs/logger";
import { EnrollmentStatus } from "../rtop/entities/household.entity";
import { ProgramType } from "../rtop/entities/program";

const createDailyReminerTable = `
CREATE TABLE daily_reminder(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    date VARCHAR(12) NOT NULL,
    household_id INT NOT NULL,
    status VARCHAR(255) NOT NULL,
    sms_status VARCHAR(255) NOT NULL,
    email_status VARCHAR(255) NOT NULL,
    token VARCHAR(512) NOT NULL UNIQUE,

    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const createDailySubmissionTable = `
CREATE TABLE daily_submission(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    status VARCHAR(255) NOT NULL,
    submission BLOB,
    traveller_id INT NOT NULL,
    reminder_id INT NOT NULL,
    
    FOREIGN KEY(reminder_id)
        REFERENCES daily_reminder(id),
    FOREIGN KEY(traveller_id)
        REFERENCES rtop_traveller(id),
    CONSTRAINT submission_traveller_unique
        UNIQUE(traveller_id, reminder_id)
);
`;

const createRtopTravellerTable = `
CREATE TABLE rtop_traveller(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    first_name VARCHAR(512) NOT NULL,
    last_name VARCHAR(512) NOT NULL,
    birth_date VARCHAR(12) NOT NULL,
    household_id INT NOT NULL,

    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addRtopTravellerConfirmationNumber = `
ALTER TABLE rtop_traveller
    ADD COLUMN confirmation_number VARCHAR(64) NOT NULL default '';
`;

const createHouseholdTable = `
CREATE TABLE HOUSEHOLD (
    ID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) , 
    ENROLLMENT_STATUS VARCHAR(120) DEFAULT '${EnrollmentStatus.APPLIED}',
    FORM_RECORD BLOB , 
    ASSIGNED_TO VARCHAR(512) ,
    STATUS VARCHAR(128) , 
    PREV_USER VARCHAR(512) ,
    ARRIVAL_DATE TIMESTAMP , 
    LAST_NAME VARCHAR(512) , 
    EXEMPT BOOLEAN WITH DEFAULT FALSE , 
    LAST_UPDATED TIMESTAMP WITH DEFAULT NULL , 
    DETERMINATION_DATE TIMESTAMP WITH DEFAULT NULL , 
    FIRST_NAME VARCHAR(512) , 
    CONFIRMATION_NUMBER VARCHAR(20),
    EMAIL VARCHAR(512),
    PHONE_NUMBER VARCHAR(20), 

    FOREIGN KEY(ASSIGNED_TO)
      REFERENCES agent(id),
    FOREIGN KEY(PREV_USER)
      REFERENCES agent(id)
    );
`;

const addCardStatusToHouseHold = `
ALTER TABLE household ADD COLUMN card_status VARCHAR(10);
`;

const addCardStatusToRtopTraveller = `
ALTER TABLE rtop_traveller ADD COLUMN card_status VARCHAR(10);
`;

const addCardStatusTimeToHouseHold = `
ALTER TABLE household ADD COLUMN card_status_time TIMESTAMP WITH DEFAULT NULL;
`;

const addCardStatusTimeToRtopTraveller = `
ALTER TABLE rtop_traveller ADD COLUMN card_status_time TIMESTAMP WITH DEFAULT NULL;
`;

const addContactMethod = `
ALTER TABLE HOUSEHOLD
    ADD COLUMN CONTACT_METHOD VARCHAR(32) NOT NULL default '';
`;

const addCardStatusReasonToRtopTraveller = `
ALTER TABLE HOUSEHOLD ADD COLUMN CARD_STATUS_REASON VARCHAR(512);
`;

const createContactMethodVerificationTable = `
CREATE TABLE contact_method_verification(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    status VARCHAR(30) NOT NULL WITH DEFAULT 'created',
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    verified_at TIMESTAMP,
    token VARCHAR(256) NOT NULL,
    contact_method VARCHAR(32) NOT NULL,
    email VARCHAR(32),
    phone_number VARCHAR(32),
    household_id INT,
    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addHouseholdTest2Column = `
ALTER TABLE rtop_traveller
ADD COLUMN test2_date TIMESTAMP NULL;
`;

const addHouseholdTest2RecordedDateColumn = `
ALTER TABLE rtop_traveller
ADD COLUMN test2_date_created TIMESTAMP NULL;
`;

const addContactMethodFields = `
ALTER TABLE HOUSEHOLD
ADD COLUMN contact_email VARCHAR(512) NOT NULL default ''
ADD COLUMN contact_phone_number VARCHAR(20) NOT NULL default '';
`;

const increaseContactInfoEmailLength = `
ALTER TABLE contact_method_verification
    ALTER COLUMN email
    SET DATA TYPE VARCHAR(512);
`;

const createActivityTable = `
CREATE TABLE rtop_activity(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    date TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    agent_id VARCHAR(512) NOT NULL,
    household_id INT NOT NULL,
    type VARCHAR(255) NOT NULL,
    note VARCHAR(2048),
    status VARCHAR(255) NOT NULL,

    FOREIGN KEY(agent_id)
        REFERENCES agent(id),
    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addContactMethodVerificationCode = `
    ALTER TABLE contact_method_verification
        ADD COLUMN verification_code VARCHAR(32)
        ADD COLUMN verification_id VARCHAR(256);
`;

const createHouseholdStatusHistoryTable = `
CREATE TABLE household_status_history(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP, 
    household_id INT NOT NULL, 
    status VARCHAR(255) NOT NULL, 
    status_reason VARCHAR(512), 
    daily_submission_id INT DEFAULT NULL , 
    
    FOREIGN KEY(daily_submission_id) 
        REFERENCES DAILY_SUBMISSION(id), 
    FOREIGN KEY(household_id) 
        REFERENCES household(id));
`;

const addDOBVerificationAttempts = `
ALTER TABLE RTOP_TRAVELLER 
    ADD COLUMN dob_attempt INT DEFAULT 0
    ADD COLUMN dob_attempt_time TIMESTAMP DEFAULT NULL;
`;

const addIsolationStatus = `
ALTER TABLE RTOP_TRAVELLER 
    ADD COLUMN isolation_status BOOLEAN WITH DEFAULT FALSE;
`;

const addWithdrawnTime = `
ALTER TABLE HOUSEHOLD 
    ADD COLUMN WITHDRAWN_DATE TIMESTAMP WITH DEFAULT NULL;
`;

const migrateWithdrawnDate = `
    UPDATE HOUSEHOLD SET WITHDRAWN_DATE = LAST_UPDATED 
        WHERE ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}';
`;

const createEnrollmentStatusHistoryTable = `
CREATE TABLE enrollment_status_history(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP, 
    household_id INT NOT NULL, 
    enrollment_status VARCHAR(120) NOT NULL, 
    
    FOREIGN KEY(household_id) 
        REFERENCES household(id));
`;

const createSurveyQuestionnaireTable = `
CREATE TABLE survey_questionnaire(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    submission BLOB,
    household_id INT NOT NULL,
    survey_type VARCHAR(255) NOT NULL,
    
    FOREIGN KEY(household_id) 
        REFERENCES household(id))
`;
const addExemptWorkerColumn = `
ALTER TABLE RTOP_TRAVELLER 
    ADD COLUMN exempt BOOLEAN;
`;

// Update travellers with whether or not they are exempt.
// 1. Look up primary travellers + additionalTravellers
// 2. Merge exempt into rtop_traveller table joined on confirmation number
const migrateExemptWorkerColumn = `
MERGE INTO RTOP_TRAVELLER rt
USING (SELECT jrt.CONFIRMATION_NUMBER, jrt.exempt FROM
    -- Create one view of primary traveller + all additional travellers to update
    (SELECT h.CONFIRMATION_NUMBER || '-0' AS CONFIRMATION_NUMBER, json_val(h.form_record, 'exemptionType', 's:200')='Exempt' AS EXEMPT
        FROM HOUSEHOLD h 
    UNION ALL (
        -- Look up additional travellers to update (unnest additionalTravellers using json_table
        SELECT
            json_val(SYSTOOLS.JSON2BSON(dt.value), 'confirmationNumber', 's:200') AS CONFIRMATION_NUMBER ,
            json_val(SYSTOOLS.JSON2BSON(dt.value), 'exemptionType', 's:200')='Exempt' AS EXEMPT
        FROM HOUSEHOLD h,
        TABLE(
            SYSTOOLS.JSON_TABLE(h.FORM_RECORD, 'additionalTravellers', 's:5000')
        ) AS dt
      )
) jrt) upd
ON(rt.confirmation_number=upd.confirmation_number)
WHEN MATCHED THEN
 UPDATE SET
    exempt = upd.exempt
`;

const addEnrollmentStatusToRtopTraveller = `
ALTER TABLE rtop_traveller 
    ADD COLUMN ENROLLMENT_STATUS VARCHAR(120) DEFAULT '${EnrollmentStatus.APPLIED}'
    ADD COLUMN DETERMINATION_DATE TIMESTAMP WITH DEFAULT NULL
    ADD COLUMN WITHDRAWN_DATE TIMESTAMP WITH DEFAULT NULL
`;

const createTravellerEnrollmentStatusHistoryTable = `
CREATE TABLE traveller_enrollment_status_history(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP, 
    enrollment_status VARCHAR(120) NOT NULL,
    traveller_id INT NOT NULL,
    
    FOREIGN KEY(traveller_id)
        REFERENCES rtop_traveller(id));
`;

const migrateEnrollmentStatus = `
MERGE INTO RTOP_TRAVELLER rtop
  USING (SELECT ID, ENROLLMENT_STATUS , DETERMINATION_DATE , WITHDRAWN_DATE FROM HOUSEHOLD) h
  ON (rtop.HOUSEHOLD_ID = h.ID)
  WHEN MATCHED THEN
     UPDATE SET
        rtop.ENROLLMENT_STATUS = h.ENROLLMENT_STATUS,
        rtop.DETERMINATION_DATE = h.DETERMINATION_DATE,
        rtop.WITHDRAWN_DATE = h.WITHDRAWN_DATE;
`;

const createFlagTable = `
    CREATE TABLE flag(
        id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
        created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP, 
        household_id INT NOT NULL, 
        prop_name VARCHAR(120) NOT NULL,
        enabled BOOLEAN WITH DEFAULT TRUE NOT NULL,     
        
        FOREIGN KEY(household_id) 
            REFERENCES household(id));
`;

const addHouseholdLastScreenedColumn = `
    ALTER TABLE household
        ADD COLUMN last_screened TIMESTAMP;
`;

const addTravellerLastScreenedColumn = `
    ALTER TABLE rtop_traveller
        ADD COLUMN last_screened TIMESTAMP;
`;

const backfillTravellerLastScreenedDate = `
MERGE INTO rtop_traveller rt
  USING (
  	SELECT rt.ID AS TRAVELLER_ID,max(ds.CREATED) AS CREATED
	FROM DAILY_SUBMISSION ds
	JOIN RTOP_TRAVELLER rt ON ds.TRAVELLER_ID=rt.id
	GROUP BY rt.ID
  ) upd
  ON (rt.id=upd.TRAVELLER_ID)
  WHEN MATCHED THEN
     UPDATE SET
        rt.LAST_SCREENED=upd.CREATED
`;

const backfillHouseholdLastScreenedDate = `
MERGE INTO household h
  USING (
  	SELECT rt.HOUSEHOLD_ID,max(ds.CREATED) AS CREATED
	FROM DAILY_SUBMISSION ds
	JOIN RTOP_TRAVELLER rt ON ds.TRAVELLER_ID=rt.id
	GROUP BY HOUSEHOLD_ID
  ) upd
  ON (h.id=upd.household_id)
  WHEN MATCHED THEN
     UPDATE SET
        h.LAST_SCREENED=upd.CREATED
`;

const addWithdrawnReasonToRtopTraveller = `
ALTER TABLE rtop_traveller 
    ADD COLUMN WITHDRAWN_REASON VARCHAR(200) WITH DEFAULT NULL
`;

const addWithdrawnReasonToHousehold = `
ALTER TABLE household 
    ADD COLUMN WITHDRAWN_REASON VARCHAR(200) WITH DEFAULT NULL
`;

const addWithdrawnReasonToHouseholdHistory = `
ALTER TABLE ENROLLMENT_STATUS_HISTORY 
    ADD COLUMN WITHDRAWN_REASON VARCHAR(200) WITH DEFAULT NULL
`;

const addWithdrawnReasonToTravellerHistory = `
ALTER TABLE traveller_enrollment_status_history 
    ADD COLUMN WITHDRAWN_REASON VARCHAR(200) WITH DEFAULT NULL
`;

const addHouseholdLastActivityColumn = `
ALTER TABLE household 
    ADD COLUMN last_activity_id INT
    FOREIGN KEY(last_activity_id)
        REFERENCES rtop_activity(id)
`;

const backfillHouseholdLastActivity = `
MERGE INTO household h
  USING (
	SELECT act.household_id, act.id AS activity_id
	FROM RTOP_ACTIVITY act
	join(
		SELECT ra.HOUSEHOLD_ID, max(ra.date) AS date FROM RTOP_ACTIVITY ra
		GROUP BY ra.HOUSEHOLD_ID
	) act2 ON act.date=act2.date AND act.HOUSEHOLD_ID=act2.household_id
  ) upd
  ON (h.id=upd.household_id)
  WHEN MATCHED THEN
     UPDATE SET
        h.LAST_ACTIVITY_ID=upd.activity_id
`;


const createNotificationLogTable = `
    CREATE TABLE notification_log(
        id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
        created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
        household_id INT NOT NULL,
        notification_type VARCHAR(255) NOT NULL,
        contact_method VARCHAR(32) NOT NULL,
        email VARCHAR(32),
        phone_number VARCHAR(32),
        result VARCHAR(255) NOT NULL,
        FOREIGN KEY(household_id) 
            REFERENCES household(id)
    )
`

const addHouseholdProgramColumn = `
    ALTER TABLE household
    ADD COLUMN program VARCHAR(50) NOT NULL WITH DEFAULT '${ProgramType.BORDER_PILOT}';
`;

export const ClearRTOPDB = async ():Promise<void> => {
    if(process.env.NODE_ENV === 'test') {
        const conn = await db2Connection();
        conn.beginTransactionSync();

        const res = [];

        res.push(conn.querySync(`DROP TABLE CONTACT_METHOD_VERIFICATION`));
        res.push(conn.querySync(`DROP TABLE ENROLLMENT_STATUS_HISTORY`));
        res.push(conn.querySync(`DROP TABLE DAILY_REMINDER`));
        res.push(conn.querySync(`DROP TABLE DAILY_SUBMISSION`));
        res.push(conn.querySync(`DROP TABLE survey_questionnaire`));
        res.push(conn.querySync(`DROP TABLE traveller_enrollment_status_history`));
        res.push(conn.querySync(`DROP TABLE HOUSEHOLD_STATUS_HISTORY`));
        res.push(conn.querySync(`DROP TABLE FLAG`));
        res.push(conn.querySync(`DROP TABLE RTOP_ACTIVITY`));
        res.push(conn.querySync(`DROP TABLE RTOP_TRAVELLER`));
        res.push(conn.querySync(`DROP TABLE HOUSEHOLD`));
        LoggerService.info(`Dropped tables with the following results: ${JSON.stringify(res)}`);
        
        await CreateRTOPTables(true, conn);
        await conn.commitTransactionSync();
        await conn.close();
    } else {
        throw 'Can only clear db in dev environment'
    }
}

export const CreateRTOPTables = async (withLogging?: boolean, connection?):Promise<void> => {
    const conn = connection || await db2Connection();

    const res = [];
    
    res.push(await conn.querySync(createHouseholdTable));
    res.push(await conn.querySync(createDailyReminerTable));
    res.push(await conn.querySync(createRtopTravellerTable));
    res.push(await conn.querySync(createDailySubmissionTable));
    res.push(await conn.querySync(addRtopTravellerConfirmationNumber));
    res.push(await conn.querySync(addCardStatusToHouseHold));
    res.push(await conn.querySync(addCardStatusToRtopTraveller));
    res.push(await conn.querySync(addCardStatusTimeToHouseHold));
    res.push(await conn.querySync(addCardStatusTimeToRtopTraveller));
    res.push(await conn.querySync(addCardStatusReasonToRtopTraveller));
    res.push(await conn.querySync(addContactMethod));
    res.push(await conn.querySync(createContactMethodVerificationTable));
    res.push(await conn.querySync(addHouseholdTest2Column));
    res.push(await conn.querySync(addContactMethodFields));
    res.push(await conn.querySync(increaseContactInfoEmailLength));
    res.push(await conn.querySync(createActivityTable));
    res.push(await conn.querySync(addContactMethodVerificationCode));
    res.push(await conn.querySync(createHouseholdStatusHistoryTable));
    res.push(await conn.querySync(addDOBVerificationAttempts));
    res.push(await conn.querySync(addIsolationStatus));
    res.push(await conn.querySync(addWithdrawnTime));
    res.push(await conn.querySync(migrateWithdrawnDate));
    res.push(await conn.querySync(createEnrollmentStatusHistoryTable));
    res.push(await conn.querySync(addExemptWorkerColumn));
    res.push(await conn.querySync(addHouseholdTest2RecordedDateColumn));
    res.push(await conn.querySync(createFlagTable));
    res.push(await conn.querySync(createSurveyQuestionnaireTable));
    res.push(await conn.querySync(addExemptWorkerColumn));
    res.push(await conn.querySync(addHouseholdTest2RecordedDateColumn));
    res.push(await conn.querySync(addHouseholdProgramColumn));
    res.push(await conn.querySync(addEnrollmentStatusToRtopTraveller));
    res.push(await conn.querySync(createTravellerEnrollmentStatusHistoryTable));
    res.push(await conn.querySync(createFlagTable));
    res.push(await conn.querySync(addHouseholdLastScreenedColumn));
    res.push(await conn.querySync(addTravellerLastScreenedColumn));
    res.push(await conn.querySync(addWithdrawnReasonToHousehold));
    res.push(await conn.querySync(addWithdrawnReasonToRtopTraveller));
    res.push(await conn.querySync(addWithdrawnReasonToHouseholdHistory));
    res.push(await conn.querySync(addWithdrawnReasonToTravellerHistory));
    res.push(await conn.querySync(addHouseholdLastActivityColumn));
    res.push(await conn.querySync(addHouseholdProgramColumn));
    res.push(await conn.querySync(createNotificationLogTable));

    if (!connection) {
        await conn.close();
    }

    if(withLogging) {
        LoggerService.info(`Created tables with the following results: ${JSON.stringify(res)}`);
    }
}

export const MigrateLatest = async ():Promise<void> => {
    const conn = await db2Connection();

    const res = [];
    res.push(await conn.querySync(backfillHouseholdLastScreenedDate));
    res.push(await conn.querySync(backfillTravellerLastScreenedDate));
    res.push(await conn.querySync(backfillHouseholdLastActivity));
    res.push(await conn.querySync(migrateEnrollmentStatus));

    await conn.close();
    LoggerService.info(`Migrated db tables with the following results: ${JSON.stringify(res)}`);
};