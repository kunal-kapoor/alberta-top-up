import { db2Connection } from "./db2connection";
import { LoggerService } from "../logs/logger";
import { REPORTS } from "../service-alberta/constants";

const createReportsTable = `
CREATE TABLE report(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    date VARCHAR(12) NOT NULL UNIQUE,
    passed INT,
    failed INT,
    passedThenFailed INT,
    failedThenPassed INT
)
`;

const createAgentTable = `
CREATE TABLE agent(
    id VARCHAR(255) NOT NULL UNIQUE,
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    name VARCHAR(512) NOT NULL
);
`;

const createActivityTable = `
CREATE TABLE activity(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    date TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    agent_id VARCHAR(512) NOT NULL,
    form_id INT NOT NULL,
    type VARCHAR(255) NOT NULL,
    note VARCHAR(2048),
    status VARCHAR(255) NOT NULL,

    FOREIGN KEY(agent_id)
        REFERENCES agent(id),
    FOREIGN KEY(form_id)
        REFERENCES form(id)
);
`;

const createFormsTable = `
CREATE TABLE form(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    form_record BLOB,
    assigned_to VARCHAR(512),
    status VARCHAR(128),
    FOREIGN KEY(assigned_to)
        REFERENCES agent(id)
);
`;

const addPreviousUserColumn = `
    ALTER TABLE form ADD COLUMN
        prev_user VARCHAR(512)
    FOREIGN KEY(prev_user)
        REFERENCES agent(id);
`;

const addSortColumns = `
    ALTER TABLE form 
        ADD COLUMN
            arrival_date TIMESTAMP
        ADD COLUMN
            last_name VARCHAR(512);
`;

const backfillFormColumns = `
    UPDATE form set
    last_name=JSON_VAL(form_record, 'lastName' , 's:32672' ),
    arrival_date=JSON_VAL(form_record, 'arrivalDate' , 'd' )
    WHERE id < 100000000;
`;

const createSchema = `
CREATE SCHEMA ${process.env.schema || 'selfiso'}
`;

const addExemptColumn = `
    ALTER TABLE form ADD COLUMN exempt BOOLEAN DEFAULT FALSE;
`;

const createUnwillingTravellerTable = `
CREATE TABLE unwilling_traveller(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    agent_id VARCHAR(512) NOT NULL,
    note VARCHAR(5000),
    num_travellers integer not null,
    FOREIGN KEY(agent_id)
        REFERENCES agent(id)
);
`;

const createAvailableReportsTable = `
CREATE TABLE available_report(id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
            report_name varchar(512) NOT NULL UNIQUE);`;

const createAgentReportAccessControlTable = `
CREATE TABLE agent_report_accesscontrol(id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
			agent_email varchar(512) NOT NULL ,
            report_type INTEGER NOT NULL,
            enabled boolean DEFAULT FALSE,
            last_updated TIMESTAMP WITH DEFAULT NULL ,
            created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		    FOREIGN KEY(report_type) REFERENCES available_report(id));
`;

const addLastUpdatedColumn = `ALTER TABLE form ADD COLUMN last_updated TIMESTAMP WITH DEFAULT NULL;`

const addDeterminationDate = `
    ALTER TABLE form ADD COLUMN determination_date TIMESTAMP WITH DEFAULT NULL;
`;

const addDeterminationDateIndex = `
    CREATE INDEX determination_date_idx ON form(determination_date);
`;

const migrateDeterminationDate = `
    UPDATE form
        SET determination_date=JSON_VAL(form_record, 'arrivalDate' , 'd' )
        WHERE determination_date is null and
            JSON_VAL(form_record, 'arrivalDate' , 'd' ) is not null
`;

const addFirstNameConfirmationNumberColumns = `
    ALTER TABLE form 
    ADD COLUMN
        first_name VARCHAR(512)
    ADD COLUMN
        confirmation_number VARCHAR(20);
`;

const addFirstNameIdx = `
    CREATE INDEX first_name_lower_idx ON form(LOWER(first_name));
`;

const addLastNameIdx = `
    CREATE INDEX last_name_lower_idx ON form(LOWER(last_name));
`;

const addConfirmationIdx = `
    CREATE INDEX confirmation_number_lower_idx ON form(LOWER(confirmation_number));
`;

const migrateFirstNameConfirmationNumber = `
    UPDATE form
    SET
        first_name = JSON_VAL(form_record, 'firstName' , 's:32672' ),
        confirmation_number = JSON_VAL(form_record, 'confirmationNumber' , 's:32672' )
    WHERE true
`;

const migrateDefaultReports = (value) => `
    INSERT INTO AVAILABLE_REPORT(REPORT_NAME) values('${value}');
`;

export const ClearDB = async ():Promise<void> => {
    if(process.env.NODE_ENV === 'test') {
        const conn = await db2Connection();
        await conn.querySync(`DROP TABLE form`);
        await conn.querySync(`DROP TABLE report`);
        await conn.querySync(`DROP TABLE agent`);
        await conn.querySync(`DROP TABLE activity`);
        await conn.querySync(`DROP TABLE unwilling_traveller`);
        await conn.querySync(`DROP TABLE agent_report_accesscontrol`);
        await conn.querySync(`DROP TABLE available_report`);
        await conn.close();
        await CreateTables(true);
    } else {
        throw 'Can only clear db in dev environment'
    }
}

export const MigrateLatest = async ():Promise<void> => {
    const conn = await db2Connection();

    const res = [];
    res.push(await conn.querySync(addDeterminationDate));
    res.push(await conn.querySync(migrateDeterminationDate));
    res.push(await conn.querySync(addDeterminationDateIndex));
    res.push(await conn.querySync(addFirstNameConfirmationNumberColumns));
    res.push(await conn.querySync(migrateFirstNameConfirmationNumber));
    res.push(await conn.querySync(addFirstNameIdx));
    res.push(await conn.querySync(addLastNameIdx));
    res.push(await conn.querySync(addConfirmationIdx));
    res.push(await conn.querySync(createAvailableReportsTable));
    res.push(await conn.querySync(createAgentReportAccessControlTable));
    await Object.values(REPORTS).forEach( async (item) => {
        res.push(await conn.querySync(migrateDefaultReports(item)));
    });
    await conn.close();
    LoggerService.info(`Migrated db tables with the following results: ${JSON.stringify(res)}`);

};

export const CreateTables = async (withLogging?: boolean):Promise<void> => {
    const conn = await db2Connection();

    const res = [];
    
    res.push(await conn.querySync(createSchema));
    res.push(await conn.querySync(createAgentTable));
    res.push(await conn.querySync(createFormsTable));
    res.push(await conn.querySync(createReportsTable));
    res.push(await conn.querySync(createActivityTable));
    res.push(await conn.querySync(createAvailableReportsTable));
    res.push(await conn.querySync(createAgentReportAccessControlTable));

    res.push(await conn.querySync(addPreviousUserColumn));
    res.push(await conn.querySync(addSortColumns));
    res.push(await conn.querySync(backfillFormColumns));
    
    res.push(await conn.querySync(addExemptColumn));
    res.push(await conn.querySync(createUnwillingTravellerTable));

    res.push(await conn.querySync(addLastUpdatedColumn));
    res.push(await conn.querySync(addDeterminationDate));
    res.push(await conn.querySync(migrateDeterminationDate));
    res.push(await conn.querySync(addDeterminationDateIndex));

    res.push(await conn.querySync(addFirstNameConfirmationNumberColumns));
    res.push(await conn.querySync(migrateFirstNameConfirmationNumber));
    res.push(await conn.querySync(addFirstNameIdx));
    res.push(await conn.querySync(addLastNameIdx));
    res.push(await conn.querySync(addConfirmationIdx));

    await Object.values(REPORTS).forEach( async (item) => {
        res.push(await conn.querySync(migrateDefaultReports(item)));
    });

    await conn.close();
    if(withLogging) {
        LoggerService.info(`Created tables with the following results: ${JSON.stringify(res)}`);
    }
}
