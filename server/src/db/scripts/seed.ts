import { NestFactory } from "@nestjs/core";
import { AppModule } from "../../app.module";
import { ArrivalFormSeedService } from "../../arrival-form/arrival-form-seed.service";
import { LoggerService } from "../../logs/logger";
import { HouseholdSeeder } from "../../tests/util/household-seeder";
import { ClearRTOPDB } from "../db2rtopmigrations";
import moment from "moment";
import { USER_TIMEZONE, DATE_FORMAT } from "../../rtop/enrollment-form.constants";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";

/**
 * Seeds the database with entries for testing purposes.
 */

const node_env = process.env.NODE_ENV;
(async () => {
  if(node_env !== 'development' && node_env !== 'test') {
    LoggerService.info('Aborting seeding. NODE_ENV not development or test.')
    // Sanity check to make sure we only seed dev and test databases
    return;
  }

  const appContext = await NestFactory.createApplicationContext(AppModule)
  
  try {
    const seeder = await appContext.get(ArrivalFormSeedService);

    if(process.env.npm_config_cleardb === 'true'){
      LoggerService.info('Clearing DB');
      await ClearRTOPDB();
    }

    const numOfRecords = process.env.npm_config_records || 25;

    // Seed RTOP1.0 forms
    await (await seeder.seedBuilder(+numOfRecords)).create();
    
    LoggerService.info('Seeding of arrival forms complete');

    // Seed households
    const householdSeeder = await appContext.get(HouseholdSeeder);

    /**
     * Add 10 new households for today
     */
    for(const f of [...Array(10)]) {
      await householdSeeder.createHousehold();
    }

    /**
     * Creates an enrolled household with an enrollment date of `date`
     * and creates a reminder for today with the given token
     * @param date Enrollment date for household
     * @param token Token for reminder
     */
    const createReminder = async (date, token) => {
      const h = await householdSeeder.createHousehold({
        dateOfBirth: '2000-02-02'
      }, EnrollmentStatus.ENROLLED, date);
      await householdSeeder.createReminder(h, null, token);
    };

    // Create a reminder for today. Accessible at localhost:3000/daily/YYYY-MM-DD (e.g. 2020-12-14)
    const today = moment().tz(USER_TIMEZONE).format(DATE_FORMAT);
    await createReminder(null, today);

    // Create a reminder that has day 4 survey. Accessible at localhost:3000/daily/YYYY-MM-DD-day4 (e.g. 2020-12-14-day4)
    const day4 = moment().subtract(3, 'days').tz(USER_TIMEZONE).format('YYYY-MM-DD HH:mm:ss');
    await createReminder(day4, `${today}-day4`);

    // Create a reminder that has day 4 survey. Accessible at localhost:3000/daily/YYYY-MM-DD-day13 (e.g. 2020-12-14-day13)
    const day13 = moment().subtract(14, 'days').tz(USER_TIMEZONE).format('YYYY-MM-DD HH:mm:ss');
    await createReminder(day13, `${today}-day13`);

  } catch (e){
    LoggerService.info('Failed to seed arrival forms');
    throw e;
  } finally {
    appContext.close();
  }
})();

module.exports = {};
