import { Logger, Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import node_handlebars from 'nodemailer-express-handlebars';
import Mail from 'nodemailer/lib/mailer';
import { join } from 'path';
import { LoggerService } from '../logs/logger';
import { programFor, ProgramType, EmailConfig } from '../rtop/entities/program';


export enum EmailTemplates {
    EMAIL_VERIFICATION='email-verification',
    DAILY_CHECKIN='daily-checkin',
    DAILY_REMINDER='daily-reminder',
    ENROLLMENT_CONFIRMATION='enrollment-confirmation'
}

export abstract class EmailSendable {
    // Recipient address
    to: string | string[];
    cc?: string | string[];
    // Sender email address. Must match a configured email service sender
    from: string;
    subject: string;

    // Name of handlebars template to send.
    // Must correspond to a .hbs template in the templates/ folder
    readonly template: string;

    // Injectable values for handlebars template
    readonly context?: any;

    /**
     * Optionally pass in template to use and program type to look for a template override.
     * 
     * @param template Template to use
     * @param programType If specified, looks up a template override configured for the given template.
     */
    constructor(template?: EmailTemplates, programType?: ProgramType) {
        if(programType && template) {
            const conf: EmailConfig = (programFor(programType)?.emailTemplates || {})[template];

            if(conf) {
                this.subject = conf.subject;
                this.template = conf.template;
            } else {
                this.template = template;
            }
        } else if(template) {
            this.template = template;
        }
    }
}

@Injectable()
export class EmailService {

    transporter: Mail;

    constructor() {
        this.transporter = createTransport({
            host: process.env.MAIL_HOST as string,
            port: parseInt(process.env.MAIL_PORT, 10),
            secure: process.env.SECURE_MAIL === 'true',
            auth: process.env.MAIL_USER ? {
                user: process.env.MAIL_USER as string,
                pass: process.env.MAIL_PASSWORD as string,
            } : null,
        });

        this.transporter.use(
            'compile',
            node_handlebars({
                viewEngine: {
                    extname: '.hbs',
                    layoutsDir: join(__dirname, 'templates/emails/'),
                    partialsDir: join(__dirname, 'templates/partials/'),
                },
                viewPath: join(__dirname, 'templates/emails/'),
                extName: '.hbs',
            }),
        );

    }

    /**
     * Sends an email to the given email address
     */
    async sendEmail(email: EmailSendable): Promise<void> {
        if (process.env.USE_MAIL_SANDBOX === 'true' || process.env.NODE_ENV === 'test') {
            return;
        }
        else {
            try {
                const mailRequest: Mail.Options = {
                    ...email,
                    attachments: [{
                        filename: 'logo-small.png',
                        path: __dirname + '/assets/logo-small.png',
                        cid: 'ahs-logo'
                    }]
                };
                return new Promise((resolve, reject) => {
                    this.transporter.sendMail(mailRequest, (error: Error) => {
                        error ? reject(error) : resolve();
                    });
                })
            } catch (e) {
                LoggerService.logger.error('Failed to send email', e);

                throw e;
            }
        }
    }
}