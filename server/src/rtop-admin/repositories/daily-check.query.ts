import { Injectable } from "@nestjs/common";
import { query } from "../../db/db2connection";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";
import { isBp } from "../../utils/query-utils";


@Injectable()
export class DailyCheckQuery {

    private static readonly GetUnfilledTravellers = `
    SELECT rt.ID AS TRAVELLER_ID, 
        rt.CONFIRMATION_NUMBER, 
        h2.contact_email as email, 
        h2.contact_phone_number as phone_number, 
        h2.contact_method , 
        dr.* 
    FROM DAILY_REMINDER dr INNER JOIN HOUSEHOLD h2 
        ON h2.ID = dr.HOUSEHOLD_ID
    INNER JOIN RTOP_TRAVELLER rt 
        ON rt.HOUSEHOLD_ID = h2.ID
    WHERE h2.ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}'
        AND ${isBp('h2')}
        AND dr."DATE" = ?
        AND rt.ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}'
        AND rt.ID NOT IN (
            SELECT rt2.ID FROM DAILY_REMINDER dr 
            INNER JOIN DAILY_SUBMISSION ds ON ds.REMINDER_ID = dr.ID
            INNER JOIN RTOP_TRAVELLER rt2 ON rt2.ID = ds.TRAVELLER_ID 
            WHERE dr."DATE" = ?
        );
    `;

    // Update status of travellers to completed with an enrolment date > 14 days ago
    private static readonly UpdateHouseholdCompletedStatus = `
        UPDATE HOUSEHOLD h
        SET h.ENROLLMENT_STATUS = '${EnrollmentStatus.COMPLETED}'
        WHERE h.ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}'
            AND date(CURRENT TIMESTAMP) > date(timezone(h.DETERMINATION_DATE,'Etc/UTC', 'America/Edmonton')) + 13 day
            AND ${isBp()}
    `;

    // Update status of travellers to completed with an enrolment date > 14 days ago
    private static getUpdateTravellerCompletedStatus = (householdIds) => `
        UPDATE RTOP_TRAVELLER 
            SET ENROLLMENT_STATUS =  '${EnrollmentStatus.COMPLETED}' 
            WHERE ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}' AND HOUSEHOLD_ID IN (${householdIds})
    `;

    private static readonly GetUpdateStatusIdPrefix = `
    select ID from FINAL table
    `;

    /**
     * Returns the count of number of rows updated by the query.
     * 
     * @param query update query string to executed
     */
    public static getUpdatedIdQuery(query: string): string {
        return `${DailyCheckQuery.GetUpdateStatusIdPrefix} (${query})`;
    }

    /**
     * Fetches all the travellers for household whose enrollment status is enrolled.
     * Travellers are checked for submission only for the previous day
     * 
     * Scheduler runs 11.59 PM MT daily - which is the next day in UTC
     * 
     * only 'enrolled' household status are checked for submissions
     */
    public static async getExpiredTravellerStatus(date: string): Promise<any> {
        return query(DailyCheckQuery.GetUnfilledTravellers, [date, date]);
    }

    public static async updateCompletedHouseholdStatus(): Promise<any> {
        return query(DailyCheckQuery.getUpdatedIdQuery(DailyCheckQuery.UpdateHouseholdCompletedStatus));
    }

    public static async updateCompletedTravellerStatus(householdIds: any): Promise<any> {
        const parametersLength = '?' + (householdIds.length > 1 ? ',?'.repeat(householdIds.length - 1) : '');
        return query(DailyCheckQuery.getUpdateTravellerCompletedStatus(parametersLength), [...householdIds]);
    }
}