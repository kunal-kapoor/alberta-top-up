import { IsString, IsIn } from 'class-validator';
import { WITHDRAWN_REASON } from '../../rtop/constants';

export class WithdrawnFormDTO {
    @IsString()
    @IsIn(Object.values(WITHDRAWN_REASON))
    withdrawnReason: WITHDRAWN_REASON;
}
