import { IsString, IsIn, IsOptional, Length, IsDate } from 'class-validator';
import { Type } from 'class-transformer';
import { EnrollmentStatus } from '../../rtop/entities/household.entity';
import { WITHDRAWN_REASON } from '../../rtop/constants';

const validEnrollmentStatusFilters: string[] = Object.values(EnrollmentStatus)
    .map(t => <string>t)
    .concat('all')
    .concat(Object.values(WITHDRAWN_REASON));

export class DailyMonitoringFormQuery {
    @IsString()
    @IsOptional()
    @Length(3,64)
    query: string;

    @IsString()
    @IsOptional()
    @Length(0, 256)
    agent: string;

    @IsDate()
    @Type(() => Date)
    @IsOptional()
    fromDate: Date;
  
    @IsDate()
    @Type(() => Date)
    @IsOptional()
    toDate: Date;

    @IsString()
    @IsIn(["mine", "unassigned", "completed", "all"])
    @IsOptional()
    filter: string;

    @IsString()
    @IsIn(["all", "green", "red", "yellow"])
    @IsOptional()
    cardStatus: string;

    @IsOptional()
    @IsString()
    @IsIn(["asc", "desc"])
    order: string;

    @IsOptional()
    @IsString()
    @IsIn(["status", "date", "firstName", "lastName","lastUpdated", "lastScreened", "enrollmentStatus", "lastActivityNote"])
    orderBy: string;

    @IsString()
    @IsOptional()
    page: string

    @IsOptional()
    @IsString()
    @IsIn(validEnrollmentStatusFilters)
    enrollmentStatus: string;

    @IsString()
    @IsIn(["border_pilot", "arrival_testing"])
    program: string;
}
