import { Injectable } from "@nestjs/common";
import { DailyReminderRepository, NOTIFICATION_STATUS, DailyReminderRO, CONTACT_METHOD } from "./repositories/daily-reminder.repository.";
import { EmailService, EmailSendable, EmailTemplates } from "../email/email.service";
import { TextService, TextSendable } from "../text.service";
import moment from "moment";
import { USER_TIMEZONE, DATE_FORMAT } from "../rtop/enrollment-form.constants";
import { DailyMonitoringFromQueryRO } from "./ro/daily-monitoring-query.ro";
import { LoggerService } from '../logs/logger';
import { LogsProgressService } from "../logs/progress";

const fallbackText = `
Your mandatory daily symptom check for %DATE% is ready to be completed by clicking the following link. %QUESTIONNAIRE_LINK%

If you are unable to submit online, please call 1-888-245-9403.
`;

export enum JobTypes {
    SEND_DAILY_NOTIFICATION = 'daily-notification',
}

class DailyCheckinEmail implements EmailSendable {
    from: string;
    to: any;
    subject: string;
    template = EmailTemplates.DAILY_CHECKIN;
    context: any;

    constructor(to: string, data: any) {
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
        this.subject = process.env.DAILY_REMINDER_EMAIL_SUBJECT || 'Alberta Border Pilot: Submit your Daily Check-in';
    }
}

class DailyCheckinText implements TextSendable {
    to: string;
    message: string;

    constructor(phoneNumber: string, data: any) {
        this.to = phoneNumber;
        this.message = (process.env.DAILY_TEXT_CONTENT || fallbackText)
            .replace('%QUESTIONNAIRE_LINK%', data.questionnaireLink)
            .replace('%DATE%', data.date);
    }
}

class DailyReminderEmail implements EmailSendable {
    from: string;
    to: any;
    subject: string;
    template = EmailTemplates.DAILY_REMINDER;
    context: any;

    constructor(to: string, data: any) {
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
        this.subject = process.env.DAILY_REMINDER_EMAIL_SUBJECT || 'Alberta Border Pilot: Submit your Daily Check-in';
    }
}

class DailyReminderText implements TextSendable {
    to: string;
    message: string;

    constructor(phoneNumber: string, data: any) {
        this.to = phoneNumber;
        this.message = "Reminder: " + (process.env.DAILY_TEXT_CONTENT || fallbackText)
            .replace('%QUESTIONNAIRE_LINK%', data.questionnaireLink)
            .replace('%DATE%', data.date);
    }
}

@Injectable()
export class DailyQuestionnaireService {

    constructor(
        private readonly resp: DailyReminderRepository,
        private readonly emailService: EmailService,
        private readonly txtService: TextService
      ) {
    }

    constructQuestionnarieLink(token: string): string {
        return `${process.env.APP_URL}/${process.env.DAILY_SUBMISSION_ROUTE || 'daily'}/${token}`;
    }

    constructAdminQuestionnarieLink(token: string): string {
        return `${process.env.DAILY_SUBMISSION_ROUTE || 'daily'}/${token}`;
    }

    /**
     * Send daily reminders to fill out questionnaire
     * to households that haven't received one yet today.
     */
    async sendDailyReminders(): Promise<any> {
        const progressBar = new LogsProgressService();
        // Create reminder records for today
        await this.resp.createReminders();

        // TODO: Should probably paginate this
        // Only consider sending reminders that failed to send
        const reminders = await this.resp.listUnsentReminders(moment().tz(USER_TIMEZONE).format(DATE_FORMAT));
        let errorCount = 0;
        let successCount = 0;
        const totalCount = reminders.length;
        LoggerService.info(`Total number of reminders to be sent ${totalCount}`);
        progressBar.start(totalCount, 0);
        for (const reminder of reminders) {
            try {
                await this.sendDailyReminder(reminder, false, true);
                successCount++;
            } catch (e) {
                errorCount++;
            }
            progressBar.update(successCount + errorCount);
        }
        progressBar.stop();
        return [totalCount, successCount, errorCount];
    }

    /**
     * Send daily reminders to fill out questionnaire
     * to households that have chosen call in option
     */
    async sendCallInDailyReminders(): Promise<any> {
        // Create reminder records for today
        await this.resp.createReminders();

        // TODO: Should probably paginate this
        // Only consider sending reminders that failed to send
        const reminders = await this.resp.listCallInDailyReminders(moment().tz(USER_TIMEZONE).format('YYYY-MM-DD'));
        
        for (const reminder of reminders) {
            try {
                await this.sendDailyReminder(reminder, false, false);
            } catch (e) {
                LoggerService.error(`Error occured sending daily call-in reminders ${e}`);
            }
        }        
    }

    /**
     * Sends the given reminder
     * @param reminder reminder to send
     * @param forceSend send reminder even if it has previously been sent
     */
    private async sendDailyReminder(reminder: DailyReminderRO, forceSend=false, sendCallInEmail=false) : Promise<string> {
        const questionnaireLink = this.constructQuestionnarieLink(reminder.token);
        const date = moment(reminder.date).format('MMMM DD, YYYY');

        switch (reminder.contactMethod) {
            case CONTACT_METHOD.EMAIL:
                if (reminder.emailStatus !== NOTIFICATION_STATUS.SENT || forceSend) {
                    // Send daily email if not already sent
                    const msg = new DailyCheckinEmail(reminder.email, {
                        date,
                        questionnaireLink
                    });
                    await this.sendDailyEmail(reminder, date, questionnaireLink, msg);
                }
                break;

            case CONTACT_METHOD.SMS:
                if (reminder.smsStatus !== NOTIFICATION_STATUS.SENT || forceSend) {
                    // Send daily SMS if not already sent
                    const msg = new DailyCheckinText(reminder.phoneNumber, {
                        date,
                        questionnaireLink
                    });
                    await this.sendDailySms(reminder, date, questionnaireLink, msg);
                }
                break;

            case CONTACT_METHOD.CALLIN:
                if(reminder.emailStatus !== NOTIFICATION_STATUS.SENT && reminder.email && sendCallInEmail) {
                    // Send email to check-in email to
                    // travellers with call-in option set
                    const msg = new DailyCheckinEmail(reminder.email, {
                        date,
                        questionnaireLink
                    });
                    await this.sendDailyEmail(reminder, date, questionnaireLink, msg);
                }

                return questionnaireLink;
        }

        return null;
    }

    /**
     * Sends the reminder for today for the given household
     * @param householdId id of household to send reminder for
     */
    async sendReminder(householdId: number): Promise<string> {
        // Create reminder records for today
        await this.resp.createReminders(householdId);
        const reminder = await this.resp.getTodaysReminder(householdId, moment().tz(USER_TIMEZONE).format(DATE_FORMAT));

        if(reminder) {
            try {
                return this.sendDailyReminder(reminder, true);
            } catch (e) {
                return null;
            }
        }

        return null;
    }
  
    /**
     * Sends the given daily SMS reminder.
     * 
     * Updates the reminder's SMS_STATUS depending on the result
     */
    private async sendDailySms(reminder: DailyReminderRO, date: string, questionnaireLink: string, text: TextSendable) {
        try {
            await this.txtService.sendText(text);
            await this.resp.updateSmsStatus(reminder.id, NOTIFICATION_STATUS.SENT);
        }
        catch (e) {
            await this.resp.updateSmsStatus(reminder.id, NOTIFICATION_STATUS.FAILED);
            throw e;
        }
    }

    /**
     * Sends the given daily email reminder.
     * 
     * Updates the reminder's EMAIL_STATUS depending on the result
     */
    private async sendDailyEmail(reminder: DailyReminderRO, date: string, questionnaireLink: string, email: EmailSendable) {
        try {
            await this.emailService.sendEmail(email);
            await this.resp.updateEmailStatus(reminder.id, NOTIFICATION_STATUS.SENT);
        }
        catch (e) {
            await this.resp.updateEmailStatus(reminder.id, NOTIFICATION_STATUS.FAILED);
            throw e;
        }
    }


     mapToQueryRO(result:any,userId): DailyMonitoringFromQueryRO{
        return{
            id:result.id,
            lastUpdated: result.lastUpdated,
            confirmationNumber:result.confirmationNumber,
            agent:result.owner,
            firstName:result.firstName,
            lastName:result.lastName,
            status:result.status,
            enrollmentStatus:result.enrollmentStatus,
            assignedToMe:result.assignedTo===userId,
            program: result.program,
            // Use determination date as arrival date
            arrivalDate:result.determinationDate,
            lastScreened: result.lastScreened,
            lastActivityNote: result.lastActivityNote,
            lastActivityStatus: result.lastActivityStatus
        }
    }

    /**
     * 
     * Gets the households who has not submitted their daily-submissions
     * and sends a reminder to them
     * 
     */
    async sendCheckInReminders(): Promise<any> {
        const todaysDate = moment().tz(USER_TIMEZONE).format(DATE_FORMAT);
        const reminders = await this.resp.getUnfilledHousholdReminder(todaysDate);
        const progressBar = new LogsProgressService();
        let errorCount = 0;
        let successCount = 0;
        const totalCount = reminders.length;
        LoggerService.info(`Total number of reminders for daily-checkin to be sent ${totalCount}`);
        progressBar.start(totalCount, 0);
        for (const reminder of reminders) {
            try {
                await this.sendCheckinReminder(reminder);
                successCount++;
            } catch (e) {
                errorCount++;
            }
            progressBar.update(successCount + errorCount);
        }
        progressBar.stop();
        return [totalCount, successCount, errorCount];
    }

    /**
     * Sends the given reminder to checkin
     * @param reminder reminder to send
     * @param forceSend send reminder even if it has previously been sent
     */
    private async sendCheckinReminder(reminder: DailyReminderRO): Promise<string> {
        const questionnaireLink = this.constructQuestionnarieLink(reminder.token);
        const date = moment(reminder.date).format('MMMM DD, YYYY');
        let msg;
        switch (reminder.contactMethod) {
            case CONTACT_METHOD.EMAIL:
                msg = new DailyReminderEmail(reminder.email, {
                    date,
                    questionnaireLink
                });
                await this.sendDailyEmail(reminder, date, questionnaireLink, msg);
                break;
            case CONTACT_METHOD.SMS:
                msg = new DailyReminderText(reminder.phoneNumber, {
                    date,
                    questionnaireLink
                });
                await this.sendDailySms(reminder, date, questionnaireLink, msg);
                break;
        }

        return null;
    }

}


