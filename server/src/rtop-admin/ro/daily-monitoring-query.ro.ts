export class DailyMonitoringFromQueryRO{
    id: number;
    confirmationNumber: string;
    firstName:string;
    lastName:string;
    status: string;
    enrollmentStatus: string;
    arrivalDate:string;
    agent: string;
    assignedToMe: boolean;
    lastUpdated: Date;
    lastScreened: Date;
    lastActivityNote: string;
    lastActivityStatus: string;
    program: string;
    
    constructor(data:  DailyMonitoringFromQueryRO) {
        (data);
    }
}

export class DailyMonitoringFromQueryResultRO {
    numberOfResults: number;
    results: DailyMonitoringFromQueryRO[];

    constructor(numberOfResults: number, results: DailyMonitoringFromQueryRO[]) {
        this.numberOfResults = numberOfResults;
        this.results = results;
    }
}