/**
 * Returns the AppId configuration properties for the given type.
 * Falls back to dev config when the associated env variable is not available
 * @param type AppId type (AHS/OTHER/GOA)
 */
export const AppIDConfig = (type: string) => {
    const devConfig = {
        oauthServerUrl: process.env.OAUTH_SERVER_URL,
        tenantId: process.env.TENANT_ID,
        discoveryEndpoint: process.env.REACT_APP_IBM_DISCOVERY_ENDPOINT,
        clientId: process.env.REACT_APP_IBM_CLIENT_ID
    };
    
    const envConfOrDev = envVariable => {
        envVariable = envVariable? JSON.parse(envVariable): {};

        return {
            oauthServerUrl: devConfig.oauthServerUrl || envVariable.oauthServerUrl,
            tenantId: devConfig.tenantId || envVariable.tenantId,
            discoveryEndpoint: devConfig.discoveryEndpoint || envVariable.discoveryEndpoint,
            clientId: devConfig.clientId || envVariable.clientId
        };
    };
    
    const config = {
        AHS: envConfOrDev(process.env.APPID_AHS),
        OTHER: envConfOrDev(process.env.APPID_CLOUD),
        GOA: envConfOrDev(process.env.APPID_SA),
    };

    const conf = config[type];

    return {
        OAUTH_SERVER_URL: (conf.oauthServerUrl || '').trim(),
        TENANT_ID: (conf.tenantId || '').trim(),
        CLIENT_ID: (conf.clientId || '').trim(),
        DISCOVERY_ENDPOINT: (conf.discoveryEndpoint || '').trim()
    };
}

// AppID env variable configuration syntax:
// {
//     "apikey": ""
//     "appidServiceEndpoint": ""
//     "clientId": ""
//     "discoveryEndpoint": ""
//     "iam_apikey_description": ""
//     "iam_apikey_name": ""
//     "iam_role_crn": ""
//     "iam_serviceid_crn": ""
//     "managementUrl": ""
//     "oauthServerUrl": ""
//     "profilesUrl": ""
//     "secret": ""
//     "tenantId": ""
//     "version": ""
// }

