import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
/**
 * PassportJS Jwt Strategy for the analytics API
 * @export
 * @class AnalyticsAPIJWTStrategy
 * @extends {PassportStrategy(Strategy)}
 */
@Injectable()
export class AnalyticsAPIJWTStrategy extends PassportStrategy(Strategy, 'analytics') {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.ANALYTICS_API_JWT_SECRET.replace(/\\n/gm, '\n'),
      algorithms: ['RS256'],
    });
  }

  async validate(payload, done: VerifiedCallback) {
    return done(null, payload);
  }
}
