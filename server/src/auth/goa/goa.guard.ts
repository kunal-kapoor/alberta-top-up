import {
    ExecutionContext,
    Injectable,
    UnauthorizedException,
  } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LoggerService } from "../../logs/logger";
import { Observable } from 'rxjs';
import { nameOrEmail } from '../parsename';

@Injectable()
export class GOAGuard extends AuthGuard("goa") {

  constructor() {
      super()
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    return super.canActivate(context)
  }

  handleRequest(err: any, user: any, info: any): any {
    if (err || !user) {
      // Login failed
      LoggerService.info(info);
      throw err || new UnauthorizedException();
    }

    user.name = nameOrEmail(user);

    return user;
  }
}
