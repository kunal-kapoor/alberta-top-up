import { Module } from '@nestjs/common';
import { ArrivalFormController } from './arrival-form.controller';
import { ArrivalFormSeedService } from './arrival-form-seed.service';
import { ArrivalFormAdminController } from './arrival-form-admin.controller';
import { ArrivalFormService } from './arrival-form.service';
import { ArrivalFormValidationNumberGenerator } from './validation-number.service';
import { ArrivalFormRepository } from './repositories/arrival-form.repository';
import { IsolationPlanReportRepository } from './repositories/isolation-plan.repository';
import { AuthController } from '../auth/auth.controller';
import { ServiceAlbertaModule } from '../service-alberta/service-alberta.module';
import { ServiceAlbertaRepository} from '../service-alberta/service-alberta.repository';
import { UnwillingTravellerService } from './unwilling-traveller.service';
import { UnwillingTravellerRepository } from './repositories/unwilling-traveller.repository';
import { DailyQuestionnaireService } from '../rtop-admin/daily-reminder.service';
import { DailyReminderRepository } from '../rtop-admin/repositories/daily-reminder.repository.';
import { EnrollmentFormModule } from '../rtop/enrollment-form.module';
import { TextService } from '../text.service';
import { EmailService } from '../email/email.service';
import { RTOPAdminModule } from '../rtop-admin/rtop-admin.module';
import { SlackNotifierService } from '../slack-notifier.service';
import { BulkNotificationService } from '../rtop/bulk-notification.service';

const APP_OUTPUT = process.env.APP_OUTPUT;
/**
 * Api routes to include based on the environment we're running in.
 * TRAVELLER: Endpoints that should be included in the user (traveller) facing app
 * ADMIN: Admin specific endpoints
 * ALL: Should contain all endpoints. Used in dev environments.
 */
const routes = {
  'TRAVELLER': {
    'controllers': [ArrivalFormController],
    'imports': [EnrollmentFormModule],
  },
  'ADMIN': {
    'controllers': [ArrivalFormAdminController,ArrivalFormController, AuthController],
    'imports': [ServiceAlbertaModule, RTOPAdminModule, EnrollmentFormModule]
  },
  'SA': {
    'controllers': [AuthController],
    'imports': [ServiceAlbertaModule]
  },
  get ALL() { return {
      'controllers': [].concat(this['TRAVELLER'].controllers).concat(this['ADMIN'].controllers).concat(this['SA'].controllers),
      'imports': [].concat(this['TRAVELLER'].imports).concat(this['ADMIN'].imports).concat(this['SA'].imports),
    }
  }
}[APP_OUTPUT];

if(!routes) {
  throw new Error('No APP_OUTPUT environment variable specified. Must be one of [TRAVELLER, ADMIN, SA, ALL]');
}

@Module({
  providers: [
    ArrivalFormSeedService,
    ArrivalFormService,
    ArrivalFormRepository,
    ArrivalFormValidationNumberGenerator,
    UnwillingTravellerService,
    UnwillingTravellerRepository,
    ServiceAlbertaRepository,
    IsolationPlanReportRepository,
    EmailService,
    BulkNotificationService,
    TextService,
    DailyQuestionnaireService,
    DailyReminderRepository,
    SlackNotifierService,

  ],
  imports: routes.imports || [],
  controllers: routes.controllers || [],
})
export class ArrivalFormModule {}
