import { AdditionalTraveller } from "../entities/additional-traveller.entity";
import { CitiesAndCountries, booleanToString } from "../arrival-form.constants";
import { ArrivalForm } from "../entities/arrival-form.entity";
import { QuarantineLocationRO } from "./quarantine-location.entity";

export class ArrivalFormRO {
      constructor(data: ArrivalForm) {
            Object.assign(this, data);
            this.hasAdditionalTravellers = booleanToString(data.hasAdditionalTravellers)
            this.hasPlaceToStayForQuarantine = booleanToString(data.hasPlaceToStayForQuarantine);
            if (this.quarantineLocation) {
                  this.quarantineLocation.doesVulnerablePersonLiveThere = booleanToString(data.quarantineLocation.doesVulnerablePersonLiveThere);
                  this.quarantineLocation.otherPeopleResiding = booleanToString(data.quarantineLocation.otherPeopleResiding);
            }
            this.isAbleToMakeNecessaryArrangements = booleanToString(data.isAbleToMakeNecessaryArrangements);
      }

      id: number;
      viableIsolationPlan: boolean;
      firstName: string;
      lastName: string;
      dateOfBirth: string;
      phoneNumber: string;
      email: string;
      hasAdditionalTravellers: string;
      additionalTravellers: Array<AdditionalTraveller>
      nameOfAirportOrBorderCrossing: string;
      arrivalDate: string;
      arrivalCityOrTown: string;
      arrivalCountry: string;
      additionalCitiesAndCountries: Array<CitiesAndCountries>
      hasPlaceToStayForQuarantine: string;
      quarantineLocation: QuarantineLocationRO;
      isAbleToMakeNecessaryArrangements: string;
}
