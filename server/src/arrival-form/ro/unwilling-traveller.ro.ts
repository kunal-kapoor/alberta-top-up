export class UnwillingTravellerRO {
    id: number;
    agentId: string;
    note: string;
    numTravellers: number;
    created: Date;

    constructor(id: number) {
        this.id = id;
    }
}