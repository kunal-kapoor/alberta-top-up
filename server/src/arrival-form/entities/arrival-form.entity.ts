import { AdditionalTraveller } from './additional-traveller.entity';
import { AdditionalCityAndCountry } from './additional-city-and-country.entity';
import { QuarantineLocation } from './quarantine-location.entity';
import { SubmissionDetermination } from './submission-determination.entity';

export class ArrivalForm {
  id: number;
  created: Date;
  lastUpdated: Date;
  viableIsolationPlan: boolean;
  confirmationNumber: string;
  determination: SubmissionDetermination;

  // Primary Contact
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  phoneNumber: string;
  email: string;

  // Travel Information
  hasAdditionalTravellers: boolean;
  additionalTravellers: AdditionalTraveller[];

  // Arrival Information
  nameOfAirportOrBorderCrossing: string;
  arrivalDate: Date;
  arrivalCityOrTown: string;
  arrivalCountry: string;
  additionalCitiesAndCountries: AdditionalCityAndCountry[];

  // Determination Information
  determinationDate: Date;

  // Isolation Questionnaire
  hasPlaceToStayForQuarantine: boolean;
  quarantineLocation: QuarantineLocation;
  isAbleToMakeNecessaryArrangements: boolean;

  last_updated:Date;
}
