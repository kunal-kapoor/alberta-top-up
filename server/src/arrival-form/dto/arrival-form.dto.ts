import {
  IsOptional,
  IsString,
  IsArray,
  IsBoolean,
  ValidateNested,
  Length,
  Matches,
  Validate,
} from 'class-validator';

import { Exclude, Type } from 'class-transformer';
import { TransformBooleanString, MaxArrayLength, IsNotFutureDate, IsEmptyIfNotToggled } from '../../decorators';
import { AdditionalTravellers, CitiesAndCountries } from '../arrival-form.constants';
import { QuarantineLocationDTO } from './quarantine-location.dto';

export class ArrivalFormDTO {

  constructor(data: ArrivalFormDTO) {
    Object.assign(this, data);
  }

  @Exclude()
  id?: number;

  @Exclude({ toClassOnly: true })
  viableIsolationPlan: boolean;

  @IsString()
  @Length(1, 255)
  firstName: string;

  @IsString()
  @Length(1, 255)
  lastName: string;

  @IsString()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  @Validate(IsNotFutureDate)
  dateOfBirth: string;

  @IsString()
  @Length(1, 35)
  phoneNumber: string;

  @IsOptional()
  @IsString()
  @Matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$|$^/)// eslint-disable-line no-useless-escape
  email: string;

  @IsBoolean()
  @TransformBooleanString()
  hasAdditionalTravellers: boolean;

  @IsOptional()
  @IsArray()
  @Validate(MaxArrayLength, [10], { message: 'Cannot submit more than 10 additional travellers' })
  @ValidateNested({ each: true })
  @Type(() => AdditionalTravellers)
  @Validate(IsEmptyIfNotToggled, ['hasAdditionalTravellers'])
  additionalTravellers: Array<AdditionalTravellers>

  @IsString()
  @Length(1, 255)
  nameOfAirportOrBorderCrossing: string;

  @IsString()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  arrivalDate: string;

  @IsString()
  @Length(1, 255)
  arrivalCityOrTown: string;

  @IsString()
  @Length(1, 255)
  arrivalCountry: string;

  @IsArray()
  @IsOptional()
  @Validate(MaxArrayLength, [10], { message: 'Cannot submit more than 10 additional cities and countries' })
  @ValidateNested({ each: true})
  @Type(() => CitiesAndCountries)
  additionalCitiesAndCountries: Array<CitiesAndCountries>

  @IsBoolean()
  @TransformBooleanString()
  hasPlaceToStayForQuarantine: boolean;

  @IsOptional()
  @ValidateNested()
  @Type(() => QuarantineLocationDTO)
  quarantineLocation: QuarantineLocationDTO;

  @IsBoolean()
  @TransformBooleanString()
  isAbleToMakeNecessaryArrangements: boolean;
}
