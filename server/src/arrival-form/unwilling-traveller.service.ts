import { Injectable } from "@nestjs/common";
import { UnwillingTravellerDTO } from "./dto/unwilling-traveller.dto";
import { ServiceAlbertaRepository, AgentDTO } from "../service-alberta/service-alberta.repository";
import { UnwillingTravellerRepository } from "./repositories/unwilling-traveller.repository";
import { UnwillingTravellerRO } from "./ro/unwilling-traveller.ro";

@Injectable()
export class UnwillingTravellerService {
    constructor(
      private readonly serviceAlbertaRepository: ServiceAlbertaRepository,
      private readonly unwillingTravellerRepository: UnwillingTravellerRepository
    ) {}
  
    async recordUnwillingTraveller(req: any, unwillingTraveller: UnwillingTravellerDTO): Promise<UnwillingTravellerRO> {
      const agent: AgentDTO = await this.serviceAlbertaRepository.getOrCreateAgent(req.user);
      return this.unwillingTravellerRepository.recordUnwillingTraveller(agent.id, unwillingTraveller);
    }
}
