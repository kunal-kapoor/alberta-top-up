import { ClearDB } from "../../db/db2migrations";
import { LoggerService } from "../../logs/logger";
import { ClearRTOPDB } from "../../db/db2rtopmigrations";

(async () => {
    try {
        await ClearDB();
        await ClearRTOPDB();
        LoggerService.info('Successfully migrated db');
    } catch(e) {
        LoggerService.error('Failed to migrate db', e);
    }    
})();
