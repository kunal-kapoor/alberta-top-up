import faker from 'faker';
import { CONTACT_METHOD } from '../../rtop-admin/repositories/daily-reminder.repository.';
import { ExemptionType } from '../../rtop/entities/rtop-traveller.entity';
import { RTOP_AIRPORT_OR_BORDER_CROSSINGS } from '../../service-alberta/constants';
import moment from 'moment';
import { USER_TIMEZONE, DATE_FORMAT } from '../../rtop/enrollment-form.constants';
import { EnrollmentFormDTO } from '../../rtop/dto/enrollment-form.dto';

export function createContactVerificationData() {
    faker.locale = 'en_CA';
    const verification = {
        contactMethod: faker.random.arrayElement(Object.values(CONTACT_METHOD)),
        email: faker.internet.email(),
        phoneNumber: faker.phone.phoneNumber()
    }

    return verification;
}

export function createStaticVerification() {
    const verification = {
        contactMethod: CONTACT_METHOD.CALLIN,
        email: 'karthikk@freshworks.io',
        phoneNumber: '1-132-078-1566'
    }

    return verification;
}

export function createStaticEnrollmentFormWithoutAdditionalTravellers() {
    const form = {
        firstName: 'Jenifer',
        lastName: 'Lynch',
        dateOfBirth: '1954/01/31',
        phoneNumber: '1-132-078-1566',
        email: 'karthikk@freshworks.io',
        gender: 'Male',
        citizenshipStatus: 'Citizen',
        exemptionType: 'Exempt',
        hasAdditionalTravellers: 'No',
        hasTravellersOutsideHousehold: false,
        nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
        arrivalDate: '2020/11/27',
        arrivalCityOrTown: 'Cotton',
        arrivalCountry: 'Cambridgeshire',
        dateLeftCanada: '2020-09-08T18:29:42.863Z',
        dateLeavingCanada: '2020-12-10T22:20:02.376Z',
        countryOfResidence: 'Cambridgeshire',
        airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
        flightNumber: 'wocg5',
        seatNumber: 'rgt',
        address: '77191 Walsh Brooks',
        cityOrTown: 'Stewartview',
        provinceTerritory: 'Northwest Territories',
        postalCode: 'D0D 6E5',
        quarantineLocationPhoneNumber: '(737)303-2560 x1972',
        typeOfPlace: 'generating',
        howToGetToPlace: 'olive',
        durationOfStay: '346',
        contactMethod: 'callIn',
        contactEmail: 'karthikk@freshworks.io',
        contactPhoneNumber: '1-132-078-1566',
        confirmationNumber: 'abe21fe-0',
        viableIsolationPlan: true,
        created: '2020-11-26T00:32:45.032Z',
        preDepartureVaccination: 'No',
        preDepartureTest: 'No'
    }

    return form;
}

export function createStaticEnrollmentFormWithAdditionalTravellers() {
    const form = {
        firstName: 'Jenifer',
        lastName: 'Lynch',
        dateOfBirth: '1954/01/31',
        phoneNumber: '1-132-078-1566',
        email: 'karthikk@freshworks.io',
        gender: 'Male',
        citizenshipStatus: 'Citizen',
        exemptionType: 'Exempt',
        hasAdditionalTravellers: 'Yes',
        additionalTravellers: [
            {
                firstName: 'Leonardo',
                lastName: 'Caprio',
                dateOfBirth: '1967/10/12',
                preDepartureVaccination: 'No',
                preDepartureTest: 'No'
            }
        ],
        hasTravellersOutsideHousehold: false,
        nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
        arrivalDate: '2020/11/27',
        arrivalCityOrTown: 'Cotton',
        arrivalCountry: 'Cambridgeshire',
        dateLeftCanada: '2020-09-08T18:29:42.863Z',
        dateLeavingCanada: '2020-12-10T22:20:02.376Z',
        countryOfResidence: 'Cambridgeshire',
        airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
        flightNumber: 'wocg5',
        seatNumber: 'rgt',
        address: '77191 Walsh Brooks',
        cityOrTown: 'Stewartview',
        provinceTerritory: 'Northwest Territories',
        postalCode: 'D0D 6E5',
        quarantineLocationPhoneNumber: '(737)303-2560 x1972',
        typeOfPlace: 'generating',
        howToGetToPlace: 'olive',
        durationOfStay: '346',
        contactMethod: 'callIn',
        contactEmail: 'karthikk@freshworks.io',
        contactPhoneNumber: '1-132-078-1566',
        confirmationNumber: 'abe21fe-0',
        viableIsolationPlan: true,
        created: '2020-11-26T00:32:45.032Z',
        preDepartureVaccination: 'No',
        preDepartureTest: 'No'
    }

    return form;
}

export function generateHousehold(verification: any): EnrollmentFormDTO {
    faker.locale = 'en_CA';
    const form = {
        viableIsolationPlan: true,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        dateOfBirth: moment(faker.date.past(75)).format('YYYY/MM/DD'),
        phoneNumber: verification.phoneNumber,
        email: verification.email,
        gender: faker.name.title(),
        citizenshipStatus: faker.random.word(),
        exemptionType: faker.random.arrayElement(Object.values(ExemptionType)),
        hasAdditionalTravellers: false,
        hasTravellersOutsideHousehold: false,
        nameOfAirportOrBorderCrossing: faker.random.arrayElement(RTOP_AIRPORT_OR_BORDER_CROSSINGS),
        arrivalDate: moment(faker.date.between(moment.tz(USER_TIMEZONE).toDate(), moment.tz(USER_TIMEZONE).add('4', 'day').toDate())).format('YYYY/MM/DD'),
        arrivalCityOrTown: faker.random.word(),
        arrivalCountry: faker.address.county(),
        dateLeftCanada: faker.date.past().toISOString(),
        dateLeavingCanada: faker.date.future(2).toISOString(),
        countryOfResidence: faker.address.county(),
        airline: faker.random.words(10),
        flightNumber: faker.random.alphaNumeric(5),
        seatNumber: faker.random.alphaNumeric(3),
        address: faker.address.streetAddress(),
        cityOrTown: faker.address.city(),
        provinceTerritory: faker.address.state(),
        postalCode: faker.address.zipCode(),
        quarantineLocationPhoneNumber: faker.phone.phoneNumber(),
        typeOfPlace: faker.random.word(),
        howToGetToPlace: faker.random.word(),
        durationOfStay: `${faker.random.number(1000)}`,
        exemptOccupation: '',
        exemptOccupationDetails: '',
        additionalTravellers: [],
        reasonForTravel: '',
        preDepartureVaccination: false,
        additionalCitiesAndCountries: [],
        preDepartureTest: false,
        preDepartureTestDate: '',
        preDepartureTestCountry: '',
        timeSinceVaccination: '',
        dosesOfVaccine: ''
    }

    return form;
}