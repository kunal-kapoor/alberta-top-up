import { Injectable } from "@nestjs/common";
import { EnrollmentFormService } from "../../rtop/enrollment-form.service";
import { ContactMethodVerificationService } from "../../rtop/contact-method-verification.service";
import { CONTACT_METHOD, NOTIFICATION_STATUS } from "../../rtop-admin/repositories/daily-reminder.repository.";
import { ContactMethodVerification } from "../../rtop/entities/contact-method-verification";
import { generateHousehold } from "./rtop-data-generator";
import { EnrollmentFormDTO } from "../../rtop/dto/enrollment-form.dto";
import { EnrollmentFormSubmissionRO } from "../../rtop/ro/form-submission.ro";
import { RTOPDbService } from "../../db/rtop-dbservice";
import moment from "moment";
import { VerificationTokenService } from "../../rtop/verification-token.service";
import { USER_TIMEZONE, DATE_FORMAT } from "../../rtop/enrollment-form.constants";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";
import { TravellerQuery } from "../../rtop/repositories/traveller.query";
import { ProgramType } from "../../rtop/entities/program";

@Injectable()
export class HouseholdSeeder {

    constructor(
        private readonly enrollmentFormService: EnrollmentFormService,
        private readonly contactMethnodVerification: ContactMethodVerificationService,
    ) {}

    async createHousehold(data?: Partial<EnrollmentFormDTO>, enrollmentStatus: EnrollmentStatus=EnrollmentStatus.ENROLLED, date?: string): Promise<EnrollmentFormSubmissionRO> {
        const res: ContactMethodVerification = await this.createVerification();
        const householdRequest: EnrollmentFormDTO = generateHousehold(res);
        
        if(data) {
            Object.assign(householdRequest, data);
        }

        const household = await this.enrollmentFormService.saveForm(householdRequest, res, ProgramType.BORDER_PILOT);

        await this.enrollmentFormService.updateHouseholdEnrollmentStatus(household.id, enrollmentStatus);


        if(enrollmentStatus === EnrollmentStatus.ENROLLED && date) {
            await TravellerQuery.updateHouseholdEnrolledStatus(household.id, date);
        }

        return household;
    }

    async createVerification(): Promise<ContactMethodVerification> {
        const ver = await this.contactMethnodVerification.sendVerification({
            contactMethod: CONTACT_METHOD.CALLIN,
            email: '',
            phoneNumber: '',
            arrivalDate: '',
            program: ProgramType.BORDER_PILOT
        });

        return await this.contactMethnodVerification.validateContactMethod(ver.token);
    }

    async createReminder(household, date?: string, token?: string) {
        token = token || VerificationTokenService.createToken(48);
        date = date || moment().tz(USER_TIMEZONE).format(DATE_FORMAT);

        // Construct query to insert missing reminders
        const toCreate = [[date, household.id, 'created', token,  NOTIFICATION_STATUS.NOT_SENT, NOTIFICATION_STATUS.NOT_SENT]];

        // Create new reminders
        return await RTOPDbService.createDailyReminders(toCreate)[0];
    }
}