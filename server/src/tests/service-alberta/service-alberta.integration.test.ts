import { createModule } from "../util/db-test-utils";
import { ServiceAlbertaFormRO } from "../../service-alberta/ro/form.ro";
import { ServiceAlbertaRepository } from "../../service-alberta/service-alberta.repository";
import { envCond } from "../util/test-utils";
import { ServiceAlbertaAdminController } from "../../service-alberta/service-alberta.controller";
import { ServiceAlbertaFormQuery } from "../../service-alberta/dto/form-query.dto";
import { ArrivalFormService } from "../../arrival-form/arrival-form.service";
import { SubmissionDeterminationDTO } from "../../arrival-form/dto/submission-determination.dto";
import { ServiceAlbertaFromQueryResultRO } from "../../service-alberta/ro/service-alberta-query.ro";
import { DbService } from "../../db/dbservice";
import moment from 'moment';
import { ConfigModule } from "@nestjs/config";

describe("Service Alberta tests", () => {
  let repository: ServiceAlbertaRepository;
  let ctrl: ServiceAlbertaAdminController;
  let service: ArrivalFormService;
  let module;
  let agent;

  afterEach(async () => {
    await module.close();
  });

  beforeEach(async () => {
    module = await createModule({ entities: [ServiceAlbertaFormRO, ServiceAlbertaRepository], providers: [ServiceAlbertaRepository], imports: [ConfigModule.forRoot()] });
    repository = module.module.get(ServiceAlbertaRepository);
    ctrl = module.module.get(ServiceAlbertaAdminController);
    service = module.module.get(ArrivalFormService);
    agent = {user: {sub: 'abc123', name: 'Test User', email: 'test@user.com'}};
  });

  describe('create new agent', () => {
    let res;

    beforeEach(async () => {
      res = await repository.getOrCreateAgent({sub: 1, name: 'user'});
    });

    envCond('SA')('creates a new agent', async () => {
      expect(res.id).toBe("1");
    });
  });

  describe('get an existing agent', () => {
    let res;

    beforeEach(async () => {
      res = await repository.getOrCreateAgent({sub: 1, name: 'user'});
    });

    envCond('SA')('returns agent data', async () => {
      expect(res.id).toBe("1");
    });
  });

  describe('find form by id', () => {
    let res;

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB516K3918';
          el2.confirmationNumber = '134GWER';
          el1.confirmationNumber = 'WERTJKN34';
          return [el0, el1, el2];
        })
        .create();

      res = await repository.findByConfirmationNumber('AB516K3918');
    });

    envCond('SA')('returns result', async () => {
      expect(res.id).toBeDefined();
    });
  });

  describe('find activities', () => {
    let res;

    envCond('SA')('returns nothing for wrong form id', async () => {
      res = await repository.getFormActivity(-1);
      expect(res.length).toBe(0);
    });
  });

  const performSearch = async (query?): Promise<ServiceAlbertaFromQueryResultRO> => {
    query = query || <ServiceAlbertaFormQuery> {
      query: '',
      filter: 'all',
      order: 'asc',
      orderBy: 'date',
      page: '0'
    }; 

    return await ctrl.search(agent, query);
  }

  describe('search', () => {
    it('should return 0 matches when no records exist', async () => {
      const res = await performSearch();

      expect(res.results.length).toBe(0);
      expect(res.numberOfResults.toString()).toBe("0");
    });
    
    it('should return 0 matches when no records have a determination', async () => {
      await (await module.seedBuilder(1))
        .create();

      const res = await performSearch();

      expect(res.results.length).toBe(0);
      expect(res.numberOfResults.toString()).toBe("0");
    });

    it('should return 1 match when 1 record have a determination', async () => {
      const record = await (await module.seedBuilder(1))
        .create();

      await service.submitDetermination(record[0], Object.assign(new SubmissionDeterminationDTO(), {
        determination: 'Support',
        notes: 'Needs support',
        exempt: false
      }));

      const res = await performSearch();

      expect(res.results.length).toBe(1);
      expect(res.numberOfResults.toString()).toBe("1");
    });

    it('should return multiple matches when multiple records have a determination', async () => {
      const records = await (await module.seedBuilder(3))
        .create();

      for(const r of records) {
        await service.submitDetermination(r, Object.assign(new SubmissionDeterminationDTO(), {
          determination: 'Support',
          notes: 'Needs support',
          exempt: false
        }));  
      }

      const res = await performSearch();

      expect(res.results.length).toBe(3);
      expect(res.numberOfResults.toString()).toBe("3");
    });

    it('should exclude exempt workers', async () => {
      const record = await (await module.seedBuilder(1))
        .create();

      await service.submitDetermination(record[0], Object.assign(new SubmissionDeterminationDTO(), {
        determination: 'Support',
        notes: 'Needs support',
        exempt: true
      }));

      const res = await performSearch();

      expect(res.results.length).toBe(0);
      expect(res.numberOfResults.toString()).toBe("0");
    });

    it('should return null date as last updated when no activity is recorded', async () => {
      const [record] = await (await module.seedBuilder(1))
        .create();

      await service.submitDetermination(record, Object.assign(new SubmissionDeterminationDTO(), {
        determination: 'Support',
        notes: 'Needs support',
        exempt: false
      }));

      const fullRecord = await (
        await DbService.getServiceFormByNumber(record.confirmationNumber)
      ).singleRaw();
      const {results: [lookupRecord]} = await performSearch();

      expect(lookupRecord.lastUpdated).toBe(null);
    });

    it('should return date of activity as last updated date when activity exists', async () => {
      const [record] = await (await module.seedBuilder(1))
        .create();

      await service.submitDetermination(record, Object.assign(new SubmissionDeterminationDTO(), {
        determination: 'Support',
        notes: 'Needs support',
        exempt: false
      }));

      await ctrl.createActivity(agent, record.id, {
        status: 'Contact made, no issues',
        note: 'All good'
      });

      const fullRecord = await (
        await DbService.getServiceFormByNumber(record.confirmationNumber)
      ).singleRaw();
      const {results: [lookupRecord]} = await performSearch();

      expect(lookupRecord.lastUpdated).toBeTruthy();
      expect(lookupRecord.lastUpdated).not.toEqual(fullRecord.created);
    });

    it('should return date of latest activity as last updated date when multiple activity exists', async () => {
      const [record] = await (await module.seedBuilder(1))
        .create();

      await service.submitDetermination(record, Object.assign(new SubmissionDeterminationDTO(), {
        determination: 'Support',
        notes: 'Needs support',
        exempt: false
      }));

      await new Promise((r) => setTimeout(r, 200));

      await ctrl.createActivity(agent, record.id, {
        status: 'Contact made, no issues',
        note: 'All good'
      });

      await new Promise((r) => setTimeout(r, 1000));

      await ctrl.createActivity(agent, record.id, {
        status: 'No answer, 3 call attempts made',
        note: 'Needs follow up'
      });

      const fullRecord = await repository.findByConfirmationNumber(record.confirmationNumber);
      const {results} = await performSearch();

      expect(fullRecord.activities.length).toEqual(2);

      const latestRecord = fullRecord.activities.find(r => r.status === 'No answer, 3 call attempts made');

      expect(results.length).toEqual(1);
      expect(results[0].lastUpdated).toBeTruthy();
      expect(moment(results[0].lastUpdated).toDate()).toEqual(moment(latestRecord.date).toDate());
      expect(fullRecord.activities[0].date).not.toEqual(fullRecord.activities[1].date);
    });


  });


  describe('assign agent', () => {
    let formId;
    let res;

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB516K3918';
          el2.confirmationNumber = '134GWER';
          el1.confirmationNumber = 'WERTJKN34';
          return [el0, el1, el2];
        })
        .create();

      const form = await repository.findByConfirmationNumber('AB516K3918');
      formId = form.id;
    });

    envCond('SA')('assigns user', async () => {
      await repository.assignTo(formId, {sub: 1, name: 'user'});
    });

    envCond('SA')('fails to unassign', async () => {
      try{
        res = await repository.unassignFrom(formId, {sub: 1, name: 'user'});
      }catch(err){
        expect(err.status).toEqual(400);
      }

    });
  });
});
