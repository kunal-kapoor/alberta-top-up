import { createTestingApp } from "../util/db-test-utils";
import request from "supertest";
import { ArrivalFormRepository } from "../../arrival-form/repositories/arrival-form.repository";
import moment from "moment";
import { envCond } from "../util/test-utils";
import { ServiceAlbertaRepository, AgentDTO } from "../../service-alberta/service-alberta.repository";
import { TestingModuleBuilder } from "@nestjs/testing";
import { ConfigModule } from "@nestjs/config";

class ServiceAlbertaTestRepository extends ServiceAlbertaRepository {
  async getOrCreateAgent(user: any): Promise<AgentDTO> {
    return {
      id: 'test123',
      name: 'TestAgent',
    };
  }
}

const searchByNameEndpoint = '/api/v1/admin/back-office/lastname';
const formEndpoint = '/api/v1/admin/back-office/form';
const retrieveEndpoint = '/api/v1/admin/back-office/form';

let app;
let appContext;

afterEach(async () => {
  await app.close();
});

beforeEach(async () => {
  app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null}, {disableAuth: true}, module =>
    module.overrideProvider(ServiceAlbertaRepository).useClass(ServiceAlbertaTestRepository)
  );
  appContext = await app.start();
});

/**
 * Adds 3 forms to the db and performs a search with the given query
 * @param query query to search for
 */
const simpleQuery = async (query, seed=true, expect?) => {
  if(seed) {
    await (await app.seedBuilder(3))
    .transform(([el0, el1, el2]) => {
      el0.lastName = 'Ray';
      el2.lastName = 'Rayland';
      el1.lastName = 'Johnson';

      return [el0, el1, el2];
    })
    .create();
  }

  const res = request.agent(app.server())
    .get(`${searchByNameEndpoint}/${query}`);

  return expect? await res.expect(expect): await res;
}

describe('retrieve endpoint', () => {
  let res;

  describe('returns transformed boolean values', () => {
    envCond('ADMIN')('returns yes for boolean fields', async () => {
      await (await app.seedBuilder(1))
        .transform(([el0]) => {
          el0.confirmationNumber = 'ABCD';
          el0.lastName = 'Johnson';
          el0.hasAdditionalTravellers = 'Yes';
          el0.additionalTravellers = [
            {
              dateOfBirth: moment().format('YYYY/MM/DD'),
              firstName: 'Test',
              lastName: 'Traveller'
            }
         ];

          return [el0];
        })
        .create();
      const res = await request.agent(app.server())
        .get(`${retrieveEndpoint}/ABCD`);
      expect(res.body.hasAdditionalTravellers).toBe('Yes');
    });

    envCond('ADMIN')('returns no for boolean fields', async () => {
      await (await app.seedBuilder(1))
        .transform(([el0]) => {
          el0.confirmationNumber = 'ABCDE';
          el0.lastName = 'Trev';
          el0.hasAdditionalTravellers = 'No';
          delete(el0.additionalTravellers);

          return [el0];
        })
        .create();

      const res = await request.agent(app.server())
        .get(`${retrieveEndpoint}/ABCDE`);

      expect(res.body.hasAdditionalTravellers).toBe('No');
    });
  });
});

describe('last-name search endpoint', () => {

  describe('name matches one result', () => {
    let res;

    beforeEach(async() => res = await simpleQuery('Johns'));

    envCond('ADMIN')('request succeeds', async () => {
      expect(res.statusCode).toEqual(200);
    });

    envCond('ADMIN')('only returns matched element', async () => {
        expect(res.body).toEqual(
          expect.objectContaining({
            travellers: expect.arrayContaining([expect.objectContaining({ lastName: 'Johnson' })]),
          }),
        );

        expect(res.body.travellers.length).toEqual(1);
    });
  });

  describe('name matches multiple results', () => {
    let res;

    beforeEach(async() => res = await simpleQuery('ray'));

    envCond('ADMIN')('request succeeds', () => {
      expect(res.statusCode).toEqual(200);
    });

    envCond('ADMIN')('2 results are returned', () => {
      expect(res.body).toEqual(
        expect.objectContaining({
          travellers: expect.arrayContaining([
            expect.objectContaining({ lastName: 'Ray' }),
            expect.objectContaining({ lastName: 'Rayland' })
          ]),
        }),
      );

      expect(res.body.travellers.length).toEqual(2);
    });
  })

  describe('name does not match start of result', () => {
    let res;

    beforeEach(async() => res = await simpleQuery('land'));

    envCond('ADMIN')('request succeeds', () => {
      expect(res.statusCode).toEqual(404);
    });

    envCond('ADMIN')('no results are returned', () => {
      expect(res.body.travellers).toBeUndefined();
    });
  });

  describe('name does not match any char in the result', () => {
    let res;

    beforeEach(async() => res = await simpleQuery('vzew'));

    envCond('ADMIN')('request returns a 404', () => {
      expect(res.statusCode).toEqual(404);
    });

    envCond('ADMIN')('no results are returned', () => {
      expect(res.body.travellers).toBeUndefined();
    });
  });

  describe('name is empty', () => {
    let res;

    beforeEach(async() => res = await simpleQuery('vzew'));

    envCond('ADMIN')('request returns a 404', () => {
      expect(res.statusCode).toEqual(404);
    });

    envCond('ADMIN')('no results are returned', () => {
      expect(res.body.travellers).toBeUndefined();
    });
  });

  describe('allow searching for two characters', () => {    

    it('should return 0 matches when 2 char search matches no attrs', async () => {
      const rec = await (await app.seedBuilder(2))
        .transform(([r1, r2]) => {
          r1.lastName = 'Yuu';
          r2.lastName = 'Yuuu';
          return [r1, r2];
        })
        .create();
        
      const res = await simpleQuery('Yu', false, 404);
    });

    it('should return 1 match when 2 char search matches exactly', async () => {
      const rec = await (await app.seedBuilder(3))
        .transform(([r1, r2, r3]) => {
          r1.lastName = 'Yuu';
          r2.lastName = 'Yuuu';
          r3.lastName = 'Yu';
          return [r1, r2, r3];
        })
        .create();

        const res = await simpleQuery('Yu', false);

        expect(res.body.travellers.length).toBe(1);
      });

    it('should return 2 matches when 2 char search matches exactly', async () => {
      const rec = await (await app.seedBuilder(3))
        .transform(([r1, r2, r3]) => {
          r1.lastName = 'Yuu';
          r2.lastName = 'Yu';
          r3.lastName = 'Yu';
          return [r1, r2, r3];
        })
        .create();

      const res = await simpleQuery('Yu', false);

      expect(res.body.travellers.length).toBe(2);
   });

    it('should return inclusive results when >= 3 characters matches', async () => {
      const rec = await (await app.seedBuilder(5))
        .transform(([r1, r2, r3, r4, r5]) => {
          r1.lastName = 'Yuu';
          r2.lastName = 'Yu';
          r3.lastName = 'Yu';
          r4.lastName = 'Yuuu';
          r5.lastName = 'Yuuuu';
          return [r1, r2, r3, r4, r5];
        })
        .create();

      const res = await simpleQuery('Yuu', false);

      expect(res.body.travellers.length).toBe(3);
    });

    it('should not allow 1 letter matches', async () => {
      const rec = await (await app.seedBuilder(3))
        .transform(([r1, r2, r3]) => {
          r1.lastName = 'Yuu';
          r2.lastName = 'Yuuu';
          r3.lastName = 'Yu';
          return [r1, r2, r3];
        })
        .create();

        const res = await simpleQuery('Y', false, 400);
    });
  });
});

describe('editForm', () => {
  let form;
  let formWithoutLocation;
  let resp;
  let quarantineLocation;

  beforeEach(async () => {
    quarantineLocation = {
      "address": "A Street",
      "cityOrTown": "Calgary",
      "provinceTerritory": "Alberta",
      "postalCode": "EFG099",
      "phoneNumber": "123456789",
      "doesVulnerablePersonLiveThere": "Yes",
      "otherPeopleResiding": "Yes",
      "typeOfPlace": "N/A",
      "howToGetToPlace": "Taxi or rideshare"
    };

    resp = await appContext.get(ArrivalFormRepository);
    [form, formWithoutLocation] = (await (await app.seedBuilder(2))
      .transform(([f, f2]) => {
        f.hasPlaceToStayForQuarantine = 'Yes';
        f.quarantineLocation = quarantineLocation;

        f2.hasPlaceToStayForQuarantine = 'No';
        f2.quarantineLocation = null;

        return [f, f2];

      })
      .create());
  });

  it('should update quarantine location fields', async () => {
    await request.agent(app.server())
      .patch(`${formEndpoint}/${form.id}`)
      .send(
        {
          "hasPlaceToStayForQuarantine": "Yes",
          "quarantineLocation": {
              "address": "Test Street",
              "cityOrTown": "Victoria",
              "provinceTerritory": "Nova Scotia",
              "postalCode": "ABC123",
              "phoneNumber": "3456346346",
              "doesVulnerablePersonLiveThere": "No",
              "otherPeopleResiding": "Yes",
              "typeOfPlace": "Random House",
              "howToGetToPlace": "Taxi or rideshare"
          }
        }
      );
    const updatedForm = await resp.findOne(form.id);

    expect(updatedForm.quarantineLocation.address).toBe('Test Street');
    expect(updatedForm.quarantineLocation.doesVulnerablePersonLiveThere).toBe('No');
  });

  it('should not overwrite existing location when you do not have place to stay', async () => {
    await request.agent(app.server())
      .patch(`${formEndpoint}/${form.id}`)
      .send(
        {
          "hasPlaceToStayForQuarantine": "No",
          "quarantineLocation": null
        }
      );

    const updatedForm = await resp.findOne(form.id);

    expect(updatedForm.quarantineLocation.address).toBe('A Street');
    expect(updatedForm.quarantineLocation.otherPeopleResiding).toBe('Yes');
    expect(updatedForm.hasPlaceToStayForQuarantine).toBe('No');
  });

  it('should add location if form doesnt already have one set', async () => {
    expect(formWithoutLocation.hasPlaceToStayForQuarantine).toBe(false);
    expect(formWithoutLocation.quarantineLocation).toBe(null);

    await request.agent(app.server())
      .patch(`${formEndpoint}/${formWithoutLocation.id}`)
      .send(
        {
          "hasPlaceToStayForQuarantine": "Yes",
          "quarantineLocation": quarantineLocation
        }
      );

    const updatedFormWithoutLocation = await resp.findOne(formWithoutLocation.id);

    expect(updatedFormWithoutLocation.quarantineLocation.address).toBe('A Street');
    expect(updatedFormWithoutLocation.quarantineLocation.otherPeopleResiding).toBe('Yes');
    expect(updatedFormWithoutLocation.hasPlaceToStayForQuarantine).toBe('Yes');
  });
});

describe('submitDetermination', () => {
  const validDetermination = {determination: 'Yes', notes: 'Nothing out of the ordinary'};

  beforeEach(async () => {
    await (await app.seedBuilder(1))
      .create();
  });

  describe('request is valid', () => {
    let res;

    beforeEach(async () => {
      res =  await request.agent(app.server())
        .patch(`${formEndpoint}/1/determination`)
        .send(validDetermination);
    });

    envCond('ADMIN')('submits successfully', async () => {
      expect(res.status).toBe(200);
    });

    envCond('ADMIN')('determination is persisted', async () => {
      const resp = appContext.get(ArrivalFormRepository);

      const res = await resp.findOne(1);

      expect(res.determination).toEqual(
        expect.objectContaining({
          ...validDetermination,
        }),
      )
    });
  });

  describe('request fails validation', () => {
    const invalidDeterminationType = {determination: 2, notes: 'Nothing out of the ordinary'};
    const missingProperty = {notes: 'Nothing out of the ordinary'};
    const nullProperty = {determination: 'Great!', notes: null};

    const performRequest = async body => await request.agent(app.server())
      .patch(`${formEndpoint}/${134}/determination`)
      .send(body);

    envCond('ADMIN')('400 is returned when property is wrong type', async () => {
      const res = await performRequest(invalidDeterminationType);
      expect(res.status).toEqual(400);
    });

    envCond('ADMIN')('400 is returned when missing property', async () => {
      const res = await performRequest(missingProperty);
      expect(res.status).toEqual(400);
    });
    envCond('ADMIN')('400 is returned when property is null', async () => {
      const res = await performRequest(missingProperty);
      expect(res.status).toEqual(400);

    });
  });

  describe('non-existing id is passed in', () => {
    envCond('ADMIN')('404 is returned', async () => {
      const res = await request.agent(app.server())
        .patch(`${formEndpoint}/${11111}/determination`)
        .send(validDetermination);
      expect(res.status).toEqual(404);
    });
  });
});
