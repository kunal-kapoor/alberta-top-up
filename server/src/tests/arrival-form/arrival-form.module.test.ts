import request from "supertest";
import { envCond } from "../util/test-utils";
import { ConfigModule } from "@nestjs/config";

const searchByNameEndpoint = '/api/v1/admin/back-office/lastname';
const createEndpoint = '/api/v1/form';
const retrieveByConfirmationNumber = '/api/v1/admin/back-office/form';


describe('module endpoint tests', () => {
  let app;
  const OLD_ENV = process.env;

  afterEach(async () => {
    process.env = OLD_ENV;
    await app.close();
  });

  const simpleQuery = async () => {
    await (await app.seedBuilder(3))
      .transform(res => {
        res[0].lastName = 'test';
        res[0].confirmationNumber = 'ABCDE';

        return res;
      })
      .create();

    return request.agent(app.server());
  }

  const initApp = async OUTPUT => {
    // Initializes the app and sets APP_OUTPUT env variable
    // to OUTPUT.
    process.env = { ...OLD_ENV };
    process.env.APP_OUTPUT = OUTPUT;
    
    // Manually require testing app so that the above env variable
    // will be picked up during initialization
    app = await require("../util/db-test-utils").createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null});
    
    await app.start();
  }

  const performSearch = async () => {
    return (await simpleQuery()).get(`${searchByNameEndpoint}/test`);
  }
  
  const performLookup = async () => {
    return (await simpleQuery()).get(`${retrieveByConfirmationNumber}/ABCDE`);
  }
  
  const createForm = async () => {
    const entity = (await app.seedEntities(1))[0]
    entity.quarantineLocation = null;

    return (await simpleQuery()).post(createEndpoint)
      .send(entity);
  }

  describe('admin app', () => {
    beforeEach(async () => {
      jest.resetModules();

      await initApp('ADMIN');
    });  

    envCond('TRAVELLER')('search endpoint should be included', async () => {
      const res = await performLookup();
      expect(res.status).toEqual(200);  
    });

    envCond('TRAVELLER')('lookup endpoint should be included', async () => {
      const res = await performSearch();
      expect(res.status).toEqual(200);  
    });

    envCond('TRAVELLER')('create endpoint should be included', async () => {
      const res = await createForm();
      expect(res.status).toEqual(201);
    });
  });
  
  describe('traveller app', () => {
    beforeEach(async () => {
      jest.resetModules();

      await initApp('TRAVELLER');
    });  

    envCond('TRAVELLER')('search endpoint should not be included', async () => {
      const res = await performLookup();
      expect(res.status).toEqual(404);  
    });

    envCond('TRAVELLER')('search endpoint should not be included', async () => {
      const res = await performSearch();
      expect(res.status).toEqual(404);  
    });

    envCond('TRAVELLER')('create endpoint should be included', async () => {
      const res = await createForm();
      expect(res.status).toEqual(201);  
    });
  });

  describe('dev app', () => {
    beforeEach(async () => {
      jest.resetModules();

      await initApp('ALL');
    });  

    envCond('TRAVELLER')('search endpoint should be included', async () => {
      const res = await performSearch();
      expect(res.status).toEqual(200);  
    });

    envCond('TRAVELLER')('create endpoint should be included', async () => {
      const res = await createForm();
      expect(res.status).toEqual(201);  
    });
  });
});
