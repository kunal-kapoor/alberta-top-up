import request from 'supertest';
import { createTestingApp } from '../util/db-test-utils';
import { envCond } from "../util/test-utils";
import { ConfigModule } from '@nestjs/config';
import { ClearRTOPDB } from '../../db/db2rtopmigrations';
import { createStaticVerification, createStaticEnrollmentFormWithoutAdditionalTravellers, createStaticEnrollmentFormWithAdditionalTravellers } from '../util/rtop-data-generator';

jest.setTimeout(10000);

const getConfirmationNumbers = async (app, withAdditionalTravellers = false) => {
    const verification = createStaticVerification();
    const res = await request(app.server())
        .post('/api/v2/rtop/traveller/verification')
        .send(verification)
        .expect(201);

    expect(res.body.token).toBeDefined();
    const verificationToken = res.body.token;
    const tempForm = withAdditionalTravellers ?
        createStaticEnrollmentFormWithAdditionalTravellers() :
        createStaticEnrollmentFormWithoutAdditionalTravellers();

    const enrollmentRes = await request(app.server())
        .post(`/api/v2/rtop/traveller/enroll/${verificationToken}`)
        .send(tempForm)
        .expect(201);

    expect(enrollmentRes.body.travellers).toBeDefined();

    return enrollmentRes.body.travellers.map(e => e.confirmationNumber);
}

const staticAPIResponseWithoutAdditionalTravellers = () => {
    const res = {
        firstName: 'Jenifer',
        lastName: 'Lynch',
        dateOfBirth: '1954-01-31T00:00:00.000Z',
        phoneNumber: '1-132-078-1566',
        email: 'karthikk@freshworks.io',
        gender: 'Male',
        citizenshipStatus: 'Citizen',
        exemptionType: 'Exempt',
        hasAdditionalTravellers: 'No',
        nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
        arrivalDate: '2020-11-27T00:00:00.000Z',
        arrivalCityOrTown: 'Cotton',
        arrivalCountry: 'Cambridgeshire',
        dateLeftCanada: '2020-09-08T18:29:42.863Z',
        dateLeavingCanada: '2020-12-10T22:20:02.376Z',
        countryOfResidence: 'Cambridgeshire',
        airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
        flightNumber: 'wocg5',
        seatNumber: 'rgt',
        address: '77191 Walsh Brooks',
        cityOrTown: 'Stewartview',
        program: 'border_pilot',
        provinceTerritory: 'Northwest Territories',
        postalCode: 'D0D 6E5',
        quarantineLocationPhoneNumber: '(737)303-2560 x1972',
        typeOfPlace: 'generating',
        howToGetToPlace: 'olive',
        durationOfStay: '346',
        id: 1,
        contactMethod: 'callIn',
        contactEmail: 'karthikk@freshworks.io',
        contactPhoneNumber: '1-132-078-1566',
        confirmationNumber: 'abe21fe-0',
        enrollmentStatus: 'enrolled',
        viableIsolationPlan: true,
        created: '2020-11-26T00:32:45.032Z',
        hasTravellersOutsideHousehold: 'No',
        quarantineLocation: {
            address: '77191 Walsh Brooks',
            phoneNumber: '(737)303-2560 x1972',
            cityOrTown: 'Stewartview',
            provinceTerritory: 'Northwest Territories',
            postalCode: 'D0D 6E5',
            typeOfPlace: 'generating',
            howToGetToPlace: 'olive',
            doesVulnerablePersonLiveThere: '',
            otherPeopleResiding: ''
        }
    };

    return res;
}

const staticAPIResponseWithAdditionalTravellers = () => {
    const res = staticAPIResponseWithoutAdditionalTravellers();
    res.firstName = 'Leonardo';
    res.lastName = 'Caprio';
    res.dateOfBirth = '1967-10-12T00:00:00.000Z';
    return res;
}

const verifyAPIStructure = async (apiResponse, confirmationNumber: string, hasAdditionalTravellers = false,
    isAdditionalTraveller = false, enrollmentStatus = 'enrolled', updatedFields = {}) => {
    const expectedAPIResponse = isAdditionalTraveller ?
        staticAPIResponseWithAdditionalTravellers() :
        staticAPIResponseWithoutAdditionalTravellers();
    expectedAPIResponse.id = apiResponse.id;
    expectedAPIResponse.confirmationNumber = confirmationNumber;
    expectedAPIResponse.created = apiResponse.created;
    if (hasAdditionalTravellers) {
        expectedAPIResponse.hasAdditionalTravellers = 'Yes';
    }

    if (updatedFields && Object.keys(updatedFields).length > 0) {
        Object.entries(updatedFields).forEach(([key, value]) => {
            if (key === 'quarantineLocation') {
                Object.entries(value).forEach(([quarantineKey, quarantineValue]) => expectedAPIResponse.quarantineLocation[quarantineKey] = quarantineValue);
            }
            else {
                expectedAPIResponse[key] = value;
            }
        });
    }

    expectedAPIResponse.enrollmentStatus = enrollmentStatus;

    expect(apiResponse).toEqual(expectedAPIResponse);
}

describe('AHS v2 retrieval endpoint', () => {
    let app;
    const BASE_URL = '/api/v2/rtop/ahs/traveller';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('returns expected response without additional travellers - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase());

    });

    envCond('ADMIN')('returns expected response without additional travellers - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase());

    });

    envCond('ADMIN')('returns expected response with additional travellers - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), true);
        }
        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true);
        }
    });

    envCond('ADMIN')('returns expected response with additional travellers - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), true);
        }
        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toUpperCase(), true, true);
        }
    });
});

describe('AHS v3 endpoints', () => {
    let app;
    const BASE_URL = '/api/v3/rtop/ahs/traveller';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('returns expected response without additional travellers - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), false, false, 'applied');

    });

    envCond('ADMIN')('returns expected response with additional travellers - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), true, false, 'applied');
        }
        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true, 'applied');
        }
    });

    envCond('ADMIN')('enrolls the traveller - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}/enroll`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), false, false);

    });

    envCond('ADMIN')('withdraws the traveller - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}/withdraw`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), false, false, 'withdrawn');

    });

    envCond('ADMIN')('returns expected response without additional travellers - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), false, false, 'applied');

    });

    envCond('ADMIN')('returns expected response with additional travellers - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), true, false, 'applied');
        }
        {
            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toUpperCase(), true, true, 'applied');
        }
    });

    envCond('ADMIN')('enrolls the traveller - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}/enroll`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), false, false);

    });

    envCond('ADMIN')('withdraws the traveller - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app);

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}/withdraw`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), false, false, 'withdrawn');

    });

    envCond('ADMIN')('enrolls travellers - with additional travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        // enrolls primary traveller
        {
            await request(app.server())
                .patch(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}/enroll`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), true, false);
        }

        // enrolls additional traveller
        {
            // verifies whether additional traveller status is affected by primary traveller
            {
                const res = await request(app.server())
                    .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);

                expect(res.body.id).toBeDefined();

                const apiResponse = res.body;
                await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true, 'applied');

            }
            await request(app.server())
                .patch(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}/enroll`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true);
        }

    });

    envCond('ADMIN')('withdraws travellers - with additional travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);
        
        // withdraws primary traveller
        {
            await request(app.server())
                .patch(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}/withdraw`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), true, false, 'withdrawn');
        }

        // withdraws additional traveller
        {
            // verifies whether additional traveller status is affected by primary traveller
            {
                const res = await request(app.server())
                    .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);

                expect(res.body.id).toBeDefined();

                const apiResponse = res.body;
                await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true, 'applied');

            }

            await request(app.server())
                .patch(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}/withdraw`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const res = await request(app.server())
                .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            expect(res.body.id).toBeDefined();

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true, 'withdrawn');
        }

    });

    envCond('ADMIN')('updating the primary traveller contact information - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            email: 'dev-null@freshworks.io',
            phoneNumber: '9028180963'
        }
        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        updatedFields['contactEmail'] = 'dev-null@freshworks.io';
        updatedFields['contactPhoneNumber'] = '9028180963';

        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), true, false, 'applied', updatedFields);
    });

    envCond('ADMIN')('updating the primary traveller name, dob - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            firstName: 'Test',
            dateOfBirth: '1999-01-31T00:00:00.000Z'
        }

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), true, false, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the primary traveller name, dob - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            firstName: 'Test',
            dateOfBirth: '1999-01-31T00:00:00.000Z'
        }

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toLowerCase(), true, false, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the primary traveller - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            firstName: 'Test',
            lastName: 'lastName',
            dateOfBirth: '1999-01-31T00:00:00.000Z',
            email: 'dev-null@freshworks.io',
            phoneNumber: '9028180963',
            gender: 'Female',
            address: '1333 South Park St',
            cityOrTown: 'Victoria',
            postalCode: 'B3J 2K9'
        }

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[0].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        updatedFields['contactEmail'] = 'dev-null@freshworks.io';
        updatedFields['contactPhoneNumber'] = '9028180963';

        updatedFields['quarantineLocation'] = {
            address: updatedFields.address,
            cityOrTown: updatedFields.cityOrTown,
            postalCode: updatedFields.postalCode
        };

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[0].toUpperCase(), true, false, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the additional traveller contact information - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            email: 'dev-null@freshworks.io',
            phoneNumber: '9028180963'
        };

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        await verifyAPIStructure(apiResponse, confirmationNumbers[1].toUpperCase(), true, true, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the additional traveller contact information - with lowercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            email: 'dev-null@freshworks.io',
            phoneNumber: '9028180963'
        };

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[1].toLowerCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        await verifyAPIStructure(apiResponse, confirmationNumbers[1].toLowerCase(), true, true, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the additional traveller name, dob - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            firstName: 'Test',
            dateOfBirth: '1999-01-31T00:00:00.000Z'
        }

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;

        await verifyAPIStructure(apiResponse, confirmationNumbers[1].toUpperCase(), true, true, 'applied', updatedFields);

    });

    envCond('ADMIN')('updating the additional traveller - with uppercase confirmation number', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const updatedFields = {
            firstName: 'Test',
            lastName: 'lastName',
            dateOfBirth: '1999-01-31T00:00:00.000Z',
            email: 'dev-null@freshworks.io',
            phoneNumber: '9028180963',
            gender: 'Female',
            address: '1333 South Park St',
            cityOrTown: 'Victoria',
            postalCode: 'B3J 2K9'
        }

        await request(app.server())
            .patch(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .send(updatedFields)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const res = await request(app.server())
            .get(`${BASE_URL}/${confirmationNumbers[1].toUpperCase()}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        expect(res.body.id).toBeDefined();

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, confirmationNumbers[1].toUpperCase(), true, true, 'applied', updatedFields);

    });

});