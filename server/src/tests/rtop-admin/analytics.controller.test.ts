import request from 'supertest';
import { createTestingApp } from '../util/db-test-utils';
import { envCond } from "../util/test-utils";
import { ConfigModule } from '@nestjs/config';
import { ClearRTOPDB } from '../../db/db2rtopmigrations';
import { createStaticVerification, createStaticEnrollmentFormWithoutAdditionalTravellers, createStaticEnrollmentFormWithAdditionalTravellers } from '../util/rtop-data-generator';
import { DATE_FORMAT, USER_TIMEZONE } from '../../rtop/enrollment-form.constants';
import moment from 'moment';
import { WITHDRAWN_REASON } from '../../rtop/constants';

import { ProgramType, Program } from '../../rtop/entities/program';

jest.setTimeout(10000);

const createTravellerRecords = async (app, withAdditionalTravellers = false, program=ProgramType.BORDER_PILOT) => {
    const verification = createStaticVerification();
    const res = await request(app.server())
        .post('/api/v2/rtop/traveller/verification')
        .send(verification)
        .expect(201);

    expect(res.body.token).toBeDefined();
    const verificationToken = res.body.token;
    const tempForm = withAdditionalTravellers ?
        createStaticEnrollmentFormWithAdditionalTravellers() :
        createStaticEnrollmentFormWithoutAdditionalTravellers();

    const enrollmentUrl = {
        [ProgramType.BORDER_PILOT]: `/api/v2/rtop/traveller/enroll/${verificationToken}`,
        [ProgramType.ARRIVAL_TEST]: `/api/v2/rtop/traveller/enroll/${verificationToken}/arrival_testing`
    }[program || ProgramType.BORDER_PILOT]

    const enrollmentRes = await request(app.server())
        .post(enrollmentUrl)
        .send(tempForm)
        .expect(201);


    expect(enrollmentRes.body.travellers).toBeDefined();

    return enrollmentRes.body.travellers.map(e => e.confirmationNumber);
}

const enrollTraveller = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/enroll`)
        .expect(200);
}

const withdrawTraveller = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/withdraw`)
        .send({withdrawnReason: WITHDRAWN_REASON.OTHER})
        .expect(200);
}

const staticAPIResponse = (confirmationNumber, created, enrollmentDate, enrollmentStatus, withdrawnDate) => {
    const res = {
        travellers: [
            {
                dateOfBirth: '1954-01-31',
                gender: 'Male',
                citizenshipStatus: 'Citizen',
                exemptionType: 'Exempt',
                hasAdditionalTravellers: 'Yes',
                hasTravellersOutsideHousehold: 'No',
                nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
                arrivalDate: '2020-11-26',
                arrivalCityOrTown: 'Cotton',
                arrivalCountry: 'Cambridgeshire',
                dateLeftCanada: '2020-09-08',
                countryOfResidence: 'Cambridgeshire',
                airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
                flightNumber: 'wocg5',
                seatNumber: 'rgt',
                durationOfStay: '346',
                dateLeavingCanada: '2020-12-10',
                address: '77191 Walsh Brooks',
                provinceTerritory: 'Northwest Territories',
                postalCode: 'D0D 6E5',
                typeOfPlace: 'generating',
                howToGetToPlace: 'olive',
                enrollmentStatus,
                viableIsolationPlan: true,
                created,
                id: `${confirmationNumber}-0`,
                enrollmentDate,
                travellerStatus: null,
                lastUpdated: null,
                withdrawnDate,
                firstName: 'Jenifer',
                lastName: 'Lynch',
                preDepartureVaccination: 'No',
                preDepartureTest: 'No'
            },
            {
                dateOfBirth: '1967-10-12',
                gender: 'Male',
                citizenshipStatus: 'Citizen',
                exemptionType: 'Exempt',
                hasAdditionalTravellers: 'Yes',
                hasTravellersOutsideHousehold: 'No',
                nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
                arrivalDate: '2020-11-26',
                arrivalCityOrTown: 'Cotton',
                arrivalCountry: 'Cambridgeshire',
                dateLeftCanada: '2020-09-08',
                countryOfResidence: 'Cambridgeshire',
                airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
                seatNumber: 'rgt',
                durationOfStay: '346',
                flightNumber: 'wocg5',
                dateLeavingCanada: '2020-12-10',
                address: '77191 Walsh Brooks',
                provinceTerritory: 'Northwest Territories',
                postalCode: 'D0D 6E5',
                typeOfPlace: 'generating',
                howToGetToPlace: 'olive',
                enrollmentStatus,
                viableIsolationPlan: true,
                created,
                id: `${confirmationNumber}-1`,
                enrollmentDate,
                travellerStatus: null,
                lastUpdated: null,
                withdrawnDate,
                firstName: 'Leonardo',
                lastName: 'Caprio',
                preDepartureVaccination: 'No',
                preDepartureTest: 'No'
            }
        ]
    };

    return res;
}

const verifyAPIStructure = async (apiResponse: any, enrollmentStatus = 'enrolled', hasAdditionalTravellers = false) => {
    const expectedAPIResponse = staticAPIResponse(apiResponse.travellers[0].id.split("-")[0],
        apiResponse.travellers[0].created, apiResponse.travellers[0].enrollmentDate, enrollmentStatus, apiResponse.travellers[0].withdrawnDate);
    if(!hasAdditionalTravellers)
    {
        expectedAPIResponse.travellers.pop();
        expectedAPIResponse.travellers[0].hasAdditionalTravellers = 'No';
    } else {
        apiResponse.travellers.map((t, idx) => expectedAPIResponse.travellers[idx].enrollmentDate = t.enrollmentDate);
        apiResponse.travellers.map((t, idx) => expectedAPIResponse.travellers[idx].withdrawnDate = t.withdrawnDate);
    }
    expect(apiResponse).toEqual(expectedAPIResponse);
}

const verifyEmptyAPIResponse = async (apiResponse: any) => {
    expect(apiResponse).toEqual({ travellers: [] });
}

describe('Analytics API', () => {
    let app;
    let URL;
    let API_HEADER;
    let confirmationNumbers;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    })

    describe('Analytics API retrieve enrolled travellers - only primary traveller', () => {
        beforeAll(async () => {
            URL = '/api/v2/rtop/analytics/enrolled'
        });
    
        beforeEach(async () => {
            await ClearRTOPDB();
            confirmationNumbers = await createTravellerRecords(app, false);
        });
        
        envCond('ADMIN')('is empty when no travellers are enrolled', async () => {
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    
        describe('when traveller is enrolled', () => {
            beforeEach(async () => {
                await enrollTraveller(app, confirmationNumbers[0]);
            });
            envCond('ADMIN')('returns todays traveller when no date filter is set', async () => {
                const res = await request(app.server())
                    .get(URL)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;
                await verifyAPIStructure(apiResponse);
            });
        
            envCond('ADMIN')('returns todays traveller when filtered by today', async () => {    
                const res = await request(app.server())
                    .get(URL)
                    .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;
                await verifyAPIStructure(apiResponse);
            });
        
            envCond('ADMIN')('does not return todays traveller when filtered by next day', async () => {    
                const res = await request(app.server())
                    .get(URL)
                    .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;
                await verifyEmptyAPIResponse(apiResponse);
            });
        
            envCond('ADMIN')('returns todays traveller when filtered by yesterday', async () => {    
                const res = await request(app.server())
                    .get(URL)
                    .query(`date=${moment().tz(USER_TIMEZONE).subtract('1','day').format(DATE_FORMAT)}`)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;
                await verifyEmptyAPIResponse(apiResponse);
            });    
        });
    });
    
    describe('Analytics API retrieve enrolled travellers - additional travellers', () => {    
        beforeAll(async () => {
            URL = '/api/v2/rtop/analytics/enrolled';
        });
    
        beforeEach(async () => {
            await ClearRTOPDB();
            confirmationNumbers = await createTravellerRecords(app, true);
        });
        
        envCond('ADMIN')('Does not return travellers when no travellers are enrolled', async () => {
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
        
        describe('no date filter is set', () => {
            envCond('ADMIN')('returns all travellers in party when all are enrolled', async () => {
                await enrollTraveller(app, confirmationNumbers[0]);
                await enrollTraveller(app, confirmationNumbers[1]);
                const res = await request(app.server())
                    .get(URL)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;
                await verifyAPIStructure(apiResponse, 'enrolled', true);
            });

            envCond('ADMIN')('returns primary traveller when enrolled', async () => {
                await enrollTraveller(app, confirmationNumbers[0]);
                const res = await request(app.server())
                    .get(URL)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;

                expect(apiResponse.travellers?.length).toEqual(1);
                expect(apiResponse.travellers[0].id).toEqual(confirmationNumbers[0]);
                expect(apiResponse.travellers[0].enrollmentStatus).toEqual('enrolled');
            });
            envCond('ADMIN')('returns additional traveller when enrolled', async () => {
                await enrollTraveller(app, confirmationNumbers[1]);
                const res = await request(app.server())
                    .get(URL)
                    .set('Authorization', `Bearer ${API_HEADER}`)
                    .expect(200);
        
                const apiResponse = res.body;

                expect(apiResponse.travellers?.length).toEqual(1);
                expect(apiResponse.travellers[0].id).toEqual(confirmationNumbers[1]);
                expect(apiResponse.travellers[0].enrollmentStatus).toEqual('enrolled');
            });
            
        })
        envCond('ADMIN')('returns todays travellers when filtered by today', async () => {
            await enrollTraveller(app, confirmationNumbers[0]);
            await enrollTraveller(app, confirmationNumbers[1]);
            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'enrolled', true);
        });

        envCond('ADMIN')('does not return todays travellers when filtered by tomorrow', async () => {
            await enrollTraveller(app, confirmationNumbers[0]);
            await enrollTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });

        envCond('ADMIN')('includes enrolled border pilot travellers', async () => {
            await ClearRTOPDB();

            const confirmationNumbers = await createTravellerRecords(app, true, ProgramType.BORDER_PILOT);
            await enrollTraveller(app, confirmationNumbers[0])
            await enrollTraveller(app, confirmationNumbers[1])
            

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            console.log(apiResponse.travellers)
            expect(apiResponse.travellers.length).toEqual(2);

            const confNumbers = apiResponse.travellers.map(t => t.id);

            expect(confNumbers).toContain(confirmationNumbers[0]);
            expect(confNumbers).toContain(confirmationNumbers[1]);
        });

        envCond('ADMIN')('does not include enrolled arrival testing travellers', async () => {
            await ClearRTOPDB();

            const confirmationNumbers = await createTravellerRecords(app, true, ProgramType.ARRIVAL_TEST);
            await enrollTraveller(app, confirmationNumbers[0])
            await enrollTraveller(app, confirmationNumbers[1])
            

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            expect(apiResponse.travellers.length).toEqual(0);
        });
    });
});


describe('Analytics API retrieve withdrawn travellers', () => {
    let app;
    let URL;
    let API_HEADER;
    let confirmationNumbers;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });    
    
    describe('Analytics API retrieve withdrawn travellers - only primary traveller', () => {
    
        beforeAll(async () => {
            URL = '/api/v2/rtop/analytics/withdrawn';
        });
    
        beforeEach(async () => {
            await ClearRTOPDB();
            confirmationNumbers = await createTravellerRecords(app, false);
        });
    
        envCond('ADMIN')('returns nothing when no travellers are withdrawn', async () => {
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });

        envCond('ADMIN')('does not return enrolled travellers', async () => {
            await enrollTraveller(app, confirmationNumbers[0]);
    
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });

        envCond('ADMIN')('returns withdrawn traveller after being enrolled', async () => {
            await enrollTraveller(app, confirmationNumbers[0]);
            await withdrawTraveller(app, confirmationNumbers[0]);
    
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'withdrawn');
        });

    
        envCond('ADMIN')('returns todays withdrawn traveller when no date is specified', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'withdrawn');
        });
    
        envCond('ADMIN')('returns todays withdrawn traveller when filtered by today', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'withdrawn');
        });
    
        envCond('ADMIN')('does not return todays withdrawn traveller when filtered by future date', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    });
    
    describe('Analytics API retrieve withdrawn travellers - additional traveller', () => {
        beforeAll(async () => {
            URL = '/api/v2/rtop/analytics/withdrawn';
        });

        beforeEach(async () => {
            await ClearRTOPDB();
            confirmationNumbers = await createTravellerRecords(app, true);
        });
    
        afterAll(async () => {
            await ClearRTOPDB();
            await app.close();
        });
    
        envCond('ADMIN')('Does not return travellers when none are enrolled', async () => {
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    
        envCond('ADMIN')('Does not return enrolled travellers', async () => {    
            await enrollTraveller(app, confirmationNumbers[0]);
            await enrollTraveller(app, confirmationNumbers[1]);
    
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    
        envCond('ADMIN')('does not return todays travellers when filtered by past date', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);
            await withdrawTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).subtract('1','day').format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    
        envCond('ADMIN')('returns todays travellers when not filtered by date', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);
            await withdrawTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'withdrawn', true);
        });
    
        envCond('ADMIN')('returns todays travellers when filtered by today', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);
            await withdrawTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyAPIStructure(apiResponse, 'withdrawn', true);
        });
    
        envCond('ADMIN')('does not return todays travellers when filtered by future date ', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);
            await withdrawTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        });
    
        envCond('ADMIN')('returns primary traveller when withdrawn ', async () => {
            await withdrawTraveller(app, confirmationNumbers[0]);

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            expect(apiResponse.travellers?.length).toEqual(1);
            expect(apiResponse.travellers[0].id).toEqual(confirmationNumbers[0]);
            expect(apiResponse.travellers[0].enrollmentStatus).toEqual('withdrawn');
        });
        envCond('ADMIN')('returns additional traveller when withdrawn ', async () => {
            await withdrawTraveller(app, confirmationNumbers[1]);

            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);
    
            const apiResponse = res.body;
            expect(apiResponse.travellers?.length).toEqual(1);
            expect(apiResponse.travellers[0].id).toEqual(confirmationNumbers[1]);
            expect(apiResponse.travellers[0].enrollmentStatus).toEqual('withdrawn');
        });
    });

    envCond('ADMIN')('includes withdrawn border pilot travellers', async () => {
        await ClearRTOPDB();

        const confirmationNumbers = await createTravellerRecords(app, true, ProgramType.BORDER_PILOT);
        await withdrawTraveller(app, confirmationNumbers[0])
        await withdrawTraveller(app, confirmationNumbers[1])
        

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        expect(apiResponse.travellers.length).toEqual(2);

        const confNumbers = apiResponse.travellers.map(t => t.id);

        expect(confNumbers).toContain(confirmationNumbers[0]);
        expect(confNumbers).toContain(confirmationNumbers[1]);
    });

    envCond('ADMIN')('does not include withdrawn arrival testing travellers', async () => {
        await ClearRTOPDB();

        const confirmationNumbers = await createTravellerRecords(app, true, ProgramType.ARRIVAL_TEST);
        await withdrawTraveller(app, confirmationNumbers[0])
        await withdrawTraveller(app, confirmationNumbers[1])
        

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

            expect(res.body.travellers.length).toEqual(0);
    });
});
