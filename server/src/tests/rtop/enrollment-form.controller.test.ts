import { createTestingApp } from "../util/db-test-utils";
import { ClearRTOPDB } from "../../db/db2rtopmigrations";
import { isOctal } from "class-validator";
import { HouseholdSeeder } from "../util/household-seeder";
import request from 'supertest';
import { ConfigModule } from "@nestjs/config";
import { enrollmentFormQuery } from "../../db/rtop-db2connectionUtils";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { Res } from "@nestjs/common";


describe('submitDaily Form', () => {
    let app;
    let context;
    let household;
    let seeder: HouseholdSeeder;
    let reminder;
    const token = 'abc1234';
    const dateOfBirth = '2000-12-12';
    let requestUrl;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null});
        context = await app.start();
        seeder = await context.get(HouseholdSeeder);
        jest.setTimeout(10000);
    });

    beforeEach(async () => {
        await ClearRTOPDB();
        household = await seeder.createHousehold({dateOfBirth: '2000-12-12'});
        reminder = await seeder.createReminder(household, null, token);

        requestUrl = `/api/v2/rtop/traveller/${household.confirmationNumber}-0/reminder/${token}/${dateOfBirth}`;
    });

    it('throws 400 when answers are missing', async () => {
        await request(app.server())
            .post(requestUrl)
            .send({})
            .expect(400);

    })

    it('successfully creates record when answers are present', async () => {
        await request(app.server())
            .post(requestUrl)
            .send({
                answers: [{
                    question: 'muscleAchesSymptom',
                    answer: 'yes'
                }]
            })
            .expect(201);
    });

    describe('for households', () => {
        it('last screened is null when no submissions have been made', async () => {
            const res = (await RTOPDbService.getMonitoringFormByNumber(household.confirmationNumber))
                .normalized()
                .single();
            expect(res.lastScreened).toBeNull();
        });

        it('last screened is set when a submission is made', async () => {
            await request(app.server())
                .post(requestUrl)
                .send({
                    answers: [{
                        question: 'muscleAchesSymptom',
                        answer: 'yes'
                    }]
                })
                .expect(201);

            const res = (await RTOPDbService.getMonitoringFormByNumber(household.confirmationNumber))
                .normalized()
                .single();

            expect(res.lastScreened).not.toBeNull();
        });
    });

    describe('for travellers', () => {
        it('last screened is null when no submissions have been made', async () => {
            const res = (await RTOPDbService.findTravellerByConfirmationNumber(`${household.confirmationNumber}-0`))
                .normalized()
                .single();
            expect(res.lastScreened).toBeNull();
        });

        it('last screened is set when a submission is made', async () => {
            await request(app.server())
                .post(requestUrl)
                .send({
                    answers: [{
                        question: 'gastrointestinalSymptom',
                        answer: 'yes'
                    }]
                })
                .expect(201);

            const res = (await RTOPDbService.findTravellerByConfirmationNumber(`${household.confirmationNumber}-0`))
                .normalized()
                .single();

            expect(res.lastScreened).not.toBeNull();
        });
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });
});
