import { Controller, Get, UseInterceptors, ClassSerializerInterceptor, Post, Req, Query, Param, Body, HttpException, HttpStatus } from '@nestjs/common';
import { ServiceAlbertaRepository, AgentDTO } from './service-alberta.repository';
import { ArrivalFormRO } from '../arrival-form/ro/arrival-form.ro';
import { ArrivalFormRepository } from '../arrival-form/repositories/arrival-form.repository';
import { ServiceAlbertaFormRO } from './ro/form.ro';
import { ServiceAlbertaFormQuery } from './dto/form-query.dto';
import { ServiceAlbertaService } from './service-alberta.service';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { ActivityDTO } from './dto/activity.dto';
import { ServiceAlbertaFromQueryResultRO } from './ro/service-alberta-query.ro';
import { Roles } from '../decorators';
import { AgentRO } from './ro/agent.ro';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('/api/v1/admin/service-alberta')
export class ServiceAlbertaAdminController {

  constructor(
    private readonly serviceAlbertaRepository: ServiceAlbertaRepository,
    private readonly arrivalFormRepository: ArrivalFormRepository,
    private readonly serviceAlbertaService: ServiceAlbertaService
  ){}

  @Roles('GOA')
  @Get('/form')
  async search(@Req() req, @Query() query:ServiceAlbertaFormQuery): Promise<ServiceAlbertaFromQueryResultRO> {
    const user = await this.serviceAlbertaRepository.getOrCreateAgent(req.user);
    const [queryString, results, numberOfResults] = await this.arrivalFormRepository.getFilteredFormList(user, query);
    const filtredResults = results.map((result)=>{
      return this.serviceAlbertaService.mapToQueryRO(result,user.id)
    });

    LoggerService.logData(req, ActionName.VIEW, queryString, [filtredResults.map(result=>result.id)]);
    
    return new ServiceAlbertaFromQueryResultRO(numberOfResults, filtredResults);
  }
  @Roles('GOA')
  @Get('/form/:confirmationNumber')
  async findOneByConfirmationNumber(@Req() req, @Param('confirmationNumber') confirmationNumber): Promise<any>{
    const arrivalForm: ServiceAlbertaFormRO = await this.serviceAlbertaRepository.findByConfirmationNumber(confirmationNumber);
    if(!arrivalForm){
      throw new HttpException({
        status: HttpStatus.NOT_FOUND,
        error: 'Form not found',
      }, HttpStatus.NOT_FOUND);
    }

    LoggerService.logData(req, ActionName.VIEW, confirmationNumber, [arrivalForm.id]);

    return this.serviceAlbertaService.mapToFormRO(arrivalForm, req.user.sub);
  }
  @Roles('GOA')
  @Post('/form/:id/assign')
  async assign(@Req() req, @Param('id') formId): Promise<any> {
    const arrivalForm: ArrivalFormRO = await this.arrivalFormRepository.findOne(formId);
    if(!arrivalForm){
      throw new HttpException({
        status: HttpStatus.NOT_FOUND,
        error: 'Form not found',
      }, HttpStatus.NOT_FOUND);
    }

    LoggerService.logData(req, ActionName.UPDATE, formId, [arrivalForm.id]);

    await this.serviceAlbertaRepository.assignTo(arrivalForm.id, req.user);
    return 'Ok';
  }
  @Roles('GOA')
  @Post('/form/:id/assign/undo')
  async undo(@Req() req, @Param('id') formId): Promise<any> {
    const arrivalForm: ArrivalFormRO = await this.arrivalFormRepository.findOne(formId);
    if(!arrivalForm){
      throw new HttpException({
        status: HttpStatus.NOT_FOUND,
        error: 'Form not found',
      }, HttpStatus.NOT_FOUND);
    }

    LoggerService.logData(req, ActionName.UPDATE, formId, [arrivalForm.id]);

    await this.serviceAlbertaRepository.unassignFrom(arrivalForm.id, req.user);
    return 'Ok';
  }

  @Roles('GOA')
  @Post('/form/:id/activity')
  async createActivity(@Req() req, @Param('id') id: number, @Body() activity:ActivityDTO): Promise<any> {
    LoggerService.logData(req,ActionName.UPDATE,JSON.stringify(activity),[id]);
      const user = await this.serviceAlbertaRepository.getOrCreateAgent(req.user);
      if(!await this.arrivalFormRepository.updateStatus(activity.status,id)){
        throw new HttpException({message:"invalid request"},HttpStatus.BAD_REQUEST);
      }
      
      await this.arrivalFormRepository.addActivity(activity,user.id,id);
      
    return "Successful";
  }

  @Roles('GOA')
  @Get('/agents')
  async listActiveAgents(@Req() req): Promise<AgentRO[]> {

    const agents: AgentDTO[] = await this.serviceAlbertaRepository.listActiveAgents();

    LoggerService.logData(req, ActionName.LIST_AGENTS, null, agents.map(a => a.id));

    return agents.map(r => new AgentRO(r));
  }
}