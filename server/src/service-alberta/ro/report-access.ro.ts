import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class AgentReportAccessRO {

    @Expose()
    agentEmail: string;

    @Expose()
    reportName: string[];

    constructor(data: any, agentEmail?: string, reportName?: string[]) {
        if (agentEmail && reportName) {
            this.agentEmail = agentEmail;
            this.reportName = reportName;
        }
        else {
            this.reportName = []
            data.forEach(element => {
                this.agentEmail = element['AGENT_EMAIL']
                this.reportName.push(element['REPORT_NAME']);
            });
        }
    }
}

@Exclude()
export class ReportAccessControlListRO {

    @Expose()
    accessControl: AgentReportAccessRO[]

    constructor(data: any) {
        let prevAgent: string;
        let reportName: string[] = [];
        this.accessControl = [];
        data.forEach(element => {
            if (prevAgent && !(prevAgent === element['AGENT_EMAIL'])) {
                this.accessControl.push(new AgentReportAccessRO([], prevAgent, reportName));
                prevAgent = element['AGENT_EMAIL'];
                reportName = [element['REPORT_NAME']];
            }
            else {
                prevAgent = element['AGENT_EMAIL'];
                reportName.push(element['REPORT_NAME']);
            }

        });
        if (prevAgent && reportName) {
            this.accessControl.push(new AgentReportAccessRO([], prevAgent, reportName));
        }
    }
}
