import {
  Controller,
  UseInterceptors,
  ClassSerializerInterceptor,
  Post,
  Body,
  Req,
} from '@nestjs/common';
import { PublicRoute } from '../decorators';
import { LoggerService } from '../logs/logger';
import { TopupService } from './topup.service';
import { ActionName } from '../logs/enums';
import { TopupFormSubmissionRO } from './ro/topup-submission.ro';
import { TopupFormSubmissionDTO } from './dto/topup-form.dto';

@Controller('api/v1/topup')
@UseInterceptors(ClassSerializerInterceptor)
export class TopupController {
  constructor(
    private readonly topupService: TopupService
  ) {}
  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Post()
  async submitArrivalForm(
    @Req() req,
    @Body() body: TopupFormSubmissionDTO,
  ): Promise<TopupFormSubmissionRO> {
    console.dir(req.body);
    console.dir(body);
    const form = await this.topupService.saveForm(req.body);

    LoggerService.logData(req, ActionName.CREATE, null, [form.id]);

    return form;
  }
}
