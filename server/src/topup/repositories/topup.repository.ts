import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logs/logger';
import { TopupForm } from '../entities/topup-form.entity';
import { TopupFormSubmissionDTO } from '../dto/topup-form.dto';
import { TopupFormSubmissionRO } from '../ro/topup-submission.ro';
import { DbService } from '../../db/topup-dbservice';

@Injectable()
export class TopupFormRepository {

    async count(): Promise<number> {
        return await DbService.getCount('form');
    }
    /**
     *
     * Find a topup form by id
     *
     * @param id - Id of the record
     *
     * @returns Topup form
     */
    async findOne(id: number): Promise<TopupFormSubmissionRO> {
        try {
            const res = await (await DbService.getTopupForm(id)).single();
            return res;
        } catch (e){
            LoggerService.error('Failed to find form', e);

            throw e;
        }
    }

    /**
     * Saves an arrival form
     *
     * @param form - form to save
     *
     * @returns - The arrival form
     */
    async save(form: TopupForm): Promise<TopupFormSubmissionRO> {
        const save = await DbService.saveTopupForm(form);
        const id:number = await save.idOnly();
        return Object.assign({}, form, { id });
    }

}
