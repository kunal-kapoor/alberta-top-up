import { Exclude } from 'class-transformer';

export class TopupFormSubmissionDTO {
  
  @Exclude()
  id?: number;

  firstName: string;

  lastName: string;

  dateOfBirth: string;
  
  phoneNumber: string;

  email: string;

  sin: string;
}
