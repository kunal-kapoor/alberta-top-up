import { Injectable } from '@nestjs/common';
import { TopupForm } from './entities/topup-form.entity'
import { TopupFormRepository } from './repositories/topup.repository';
import { TopupFormSubmissionDTO } from './dto/topup-form.dto';
import { TopupFormSubmissionRO } from './ro/topup-submission.ro';

@Injectable()
export class TopupService {
  constructor(
    private readonly topupFormRepository: TopupFormRepository
  ){}
  async saveForm(body: TopupFormSubmissionDTO): Promise<TopupFormSubmissionRO> {
    const form: TopupForm = body;
    const submission = await this.topupFormRepository.save(form);
    return submission;
    // return new TopupFormSubmissionRO(
    //   {
    //     id: form.id,
    //     message: 'Form submission successful',
    //     confirmationNumber,
    //     viableIsolationPlan: form.viableIsolationPlan,
    //   }
    // );
  }
}