export class TopupFormSubmissionRO {
  id: number;
  firstName: string;
  lastName: string;
  dateOfBirth: Date | string;
  phoneNumber: string;
  email: string;
  sin: string;
}
