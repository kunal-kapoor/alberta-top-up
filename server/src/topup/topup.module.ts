import { Module } from '@nestjs/common';
import { TopupService } from './topup.service';
import { TopupController } from './topup.controller';
import { TopupFormRepository } from './repositories/topup.repository';

@Module({
  controllers: [TopupController],
  providers: [
    TopupService,
    TopupFormRepository,
  ]
})
export class TopupModule {}
