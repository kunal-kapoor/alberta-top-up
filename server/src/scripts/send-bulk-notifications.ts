import { NestFactory } from "@nestjs/core";
import { AppModule } from "../app.module";
import { SlackNotifierService } from "../slack-notifier.service";
import { LoggerService } from "../logs/logger";
import { BulkNotificationService } from "../rtop/bulk-notification.service";


const Jan27PolicyFollowUp = `
A notification was distributed Monday to inform pilot participants of changes to the program.  This follow-up is to address common questions we have received regarding these changes.  For clarity, the date that you land and take your first test will be considered Day 1. Your second test will be taken on day 7 or 8 based on this metric.

Effective January 25, 2020, all participants of the Border Pilot MUST:

Remain in quarantine until you receive negative results from the day 7-8 test;
  o Travelers currently in the program who have not yet received the results of their day 7-8 test are required to return to quarantine immediately until they receive those negative results.
  o Participants are able to leave quarantine for the purpose of getting their day 7-8 test done at a participating Shoppers Drug Mart, and must then return to quarantine until they receive negative results.

Not return to work outside of the home until after day 14;
  o This is retroactive for all participants, regardless if they have already returned to work. Participants can no longer attend any workplace outside their home until after day 14. 

Not return to school or daycare until after day 14;
  o This is retroactive for all participants, regardless if they have already returned to school or day care. Participants can no longer attend these locations until after day 14.

For more information on the Alberta Border Pilot, including conditions and restrictions associated with participation, please see https://www.alberta.ca/international-border-pilot-project.aspx
`;

const BulkNotificationConfig = {
    'jan-27-policy-followup': {
        emailTemplate: 'jan-27-policy-followup',
        emailSubject: `Clarification to Pilot Program Changes`,
        smsMessage: Jan27PolicyFollowUp,
    }
};

(async () => {
    const appContext = await NestFactory.createApplicationContext(AppModule);
    const slackNotifierService = await appContext.get(SlackNotifierService);
    const notificationService = await appContext.get(BulkNotificationService);

    const notificationType = process.env.BULK_NOTIFICATION;
    const config = BulkNotificationConfig[notificationType];

    if(!config) {
        LoggerService.error(`Bulk notification ${notificationType} not found. Aborting.`);
        return;
    }

    await slackNotifierService.postMessage(`Initiating sending of ${notificationType} bulk notification.`);

    const [succeeded, failed] = await notificationService.sendBulk(
        notificationType,
        config.emailTemplate,
        config.emailSubject,
        config.smsMessage
    );
    
    await slackNotifierService.postMessage(`Finished sending of jan-20-policy-change bulk notification. ${succeeded} Succeeded. ${failed} Failed.`);

})();

module.exports = {};